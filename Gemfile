source 'https://rubygems.org'
#ruby '1.9.3'

gem 'rails', '3.2.11'

group :development do
  gem 'therubyracer', :platforms => :ruby
end

group :development, :test do
  gem 'sqlite3'
  gem 'cucumber'
  gem 'capybara'
  gem 'rails-erd'
  gem 'gherkin'
  gem 'rspec-rails'
  gem 'spork'
  gem 'factory_girl_rails'
end

group :test do
  gem 'database_cleaner'
  gem 'cucumber-rails','~> 1.0', require: false
  gem 'awesome_print'
  gem 'test-unit'
 end

group :staging, :production do
  gem 'pg'
  gem 'newrelic_rpm'
  # gem 'unicorn'
  gem 'thin'
end

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  # See https://github.com/sstephenson/execjs#readme for more supported runtimes

  # We cannot deploy to Heroku with therubyracer enabled.
  # gem 'libv8', '~> 3.11.8'
  # gem 'therubyracer', :platforms => :ruby
  gem 'execjs'
  gem 'uglifier', '>= 1.0.3'
  gem 'yui-compressor'
end

gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'modernizr-rails'
# To use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# To use debugger
# gem 'debugger'

# Authorization
gem 'cancan'
gem 'rolify'

# Authentication
gem 'omniauth'
gem 'omniauth-google-oauth2'
gem 'devise'
gem 'devise_invitable'
# gem 'users' # not compatible with Heroku 

# Mail
gem 'mandrill-api'

# Assets
gem 'paperclip'
gem 'delayed_paperclip'
gem 'aws-sdk'

# Seed
gem 'seed-fu'

# Templates
gem 'sprockets'
gem 'compass'
gem 'modular-scale', '~> 1.0.3'
gem 'haml'
gem 'haml-rails'
gem 'sass'
gem 'sass-rails'
gem 'coffee-rails', '~> 3.2.1'

# Database
gem 'composite_primary_keys'
gem 'edave-enumerated_attribute', :git => 'https://github.com/edave/enumerated_attribute.git'

#gem 'geonames_rails'

gem 'active_shipping'
gem 'populator', '~> 1.0.0'
gem 'faker'

# i18n
gem 'i18n'
gem 'i18n-js', '~> 2.1.2'
gem 'i18n_data'
gem 'uuid'

# For pagination
gem 'will_paginate'


#Tracking History
gem 'paper_trail', :git => 'https://github.com/airblade/paper_trail.git'

