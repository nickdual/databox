# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
require "enumerated_attribute"
require "modular-scale"
require 'composite_primary_keys'
Databox::Application.initialize!

