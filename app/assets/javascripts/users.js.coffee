$(document).ready ->
  $("#button_reset").live 'click', (e) ->
    el = $(this).closest("#new_user")
    el.validate(
      errorLabelContainer: el.find('#validate_js')
      errorClass: "invalid"
      errorElement: "p"
      rules:
        'user[email]':
          required:true
          email: true
      messages:
        'user[email]':
          required: I18n.t("user.email_required")
          email: I18n.t("user.email_wrong")
    )
    if el.valid()
      loading = el.find("#loading")
      loading.show()
      $.ajax
        type: "POST"
        url: "/users/check_email"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        data:
          "email": el.find(".email_reset").val()

        dataType: "json"
        success: (data) ->
          if(data)
            $('#error_reset').hide()
            loading.show()
            $.ajax
              type: "POST",
              url: "/users/secret"
              beforeSend:(xhr) ->
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
              data:
                "user[email]": $(".email_reset").val()
              dataType: "html",
              success:(data)->
                $("#ml-forgot").removeClass('enabled')
                loading.hide()
              error:(e)->
                $('#error_reset').show()

          else if(!data)
            $('#error_reset').show()
            loading.hide()
        error: (data) ->
          loading.hide()
    else
      el.find('#validate_rails').hide()
    e.preventDefault()

#  $("#btn-login").live 'click', (e) ->
#    $(this).closest("#new_user").submit();

  $("#btn-login").live 'click', (e) ->
    el = $(this).closest("#new_user")
    el.validate(
      errorLabelContainer: el.find('#validate_js')
      errorClass: "invalid"
      errorElement: "p"
      rules:
        'user[email]':
          required:true
          email: true
        'user[password]':
          required:true
      messages:
        'user[email]':
          required: I18n.t("user.email_required")
          email: I18n.t("user.email_wrong")
        'user[password]':
          required:I18n.t("user.password_required")
    )
    if el.valid()
      loading = el.find("#loading")
      loading.show()
      $(this).closest("#new_user").submit();
    else
      el.find('#validate_rails').hide()
    e.preventDefault()
