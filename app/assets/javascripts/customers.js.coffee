$(document).ready ->
	# admin-customer page initializing
	$(".admin-customers .previous").live 'click', (e) ->
		if $(this).hasClass('disabled') 
			return false
			
		firstindex = $(".admin-customers .firstindex").text()
		lastindex = $(".admin-customers .lastindex").text()
		searchvalue = $(".admin-customers .search_text").val()
		issearch = $(".admin-customers .issearch").val()
		$.ajax
			type: "POST"
			url: "/admin_customers_previous_page"
			beforeSend: (xhr) ->
				xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
			data: {
				"firstindex": firstindex
				"lastindex": lastindex
				"searchvalue" : searchvalue
				"issearch": issearch
			}
			success: (data) ->
				$(".admin-customers").html(data)
			error: (data) ->
				alert "Error"

	$(".admin-customers .next").live 'click', (e) ->
		if $(this).hasClass('disabled')
			return false
			
		firstindex = $(".admin-customers .firstindex").text()
		lastindex = $(".admin-customers .lastindex").text()
		searchvalue = $(".admin-customers .search_text").val()
		issearch = $(".admin-customers .onsearch").val()
		$.ajax
			type: "POST"
			url: "/admin_customers_next_page"
			beforeSend: (xhr) ->
				xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
			data: {
				"firstindex": firstindex
				"lastindex": lastindex
				"searchvalue" : searchvalue
				"issearch": issearch
			}
			success: (data) ->
				$(".admin-customers").html(data)
			error: (data) ->
				alert "Error"
	
	$(".admin-customers .go_search").live 'click', (e) ->
		search = $(".admin-customers .search_text").val()
		if search isnt ''
			location.href = '/search_customer?searchvalue=' + search
		else
			location.href = '/admin_customers' + search
		
		return true
	
	#admin-customer-detail page initializing
	$(".admin-customer-detail #tabs").tabs()		
