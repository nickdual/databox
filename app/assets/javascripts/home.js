/* Author:
 *
 */

$(function(){

	var DOM = {
		btnSignUp: $('#btn-sign-up'),
		btnEnterCode: $('#btn-enter-code'),
		intro: $('#intro'),
		invite: $('#invite'),
		inviteCode: $('#verify-code'),
		modal: $('#modal'),
		main: $('#modal-main'),
		hero: $('#hero'),
		heroBg: $('#hero-bg'),
		inner: $('#hero').find('.inner'),
		getStarted: $('#modal-get-started'),
		createAccount: $('#modal-create-account'),
			signup_agree: $('#modal-signup-agree'),
			signup_phone: $('#modal-signup-phone'),
		co_op: $('#modal-create-account-co-op'),
			body_html: $('body, html'),
			steps_inner: $('#steps-inner')
	};

	var CONST = {
		ms: 500
	};

	var transitions = {

	};


	/*
	 * Landing page top modal
	 */
	
  /*
   * fade intro buttons on hover
   */

  $('#intro a').hover(function(){
    $(this).animate({opacity: 1}, 500);
  }, function(){
    $(this).animate({opacity: .85}, 500);
  });

  /*
   * Move hero background on mouse move
   */

  var lameBrowser = function() {
    if ($.browser.msie) {
        if (parseInt($.browser.version, 0) < 9) {
            return true;
        }
    }
    return false;
  };

  var heroBgH = DOM.heroBg.height();
  var c = 0;
  var imgzh = 0;

  DOM.hero.mousemove(function(e){
    if(mouse_over_modal || bg_hero_fading) return;
    $s = $(this);
    $e = DOM.heroBg;
    var mouseCoordsX=(e.pageX);
    var mouseCoordsY=(e.pageY - $s.offset().top);
    var mousePercentX=mouseCoordsX/$s.width();
    var mousePercentY=mouseCoordsY/$s.height();                
    var destY=-((($e.height()-($s.height()))-$s.height())*(mousePercentY));                
    var thePosC=mouseCoordsY-destY;
    var thePosD=destY-mouseCoordsY;                
                
    var doc = $s.height();
    var mar = parseInt($e.height()) - doc;
                
    if((thePosC - c) >= 1 || (c - thePosC) >= 1){
      c = thePosC;
      if(lameBrowser() === true){
        var fparam = false;
        var sparam = true;
      } else {
        var fparam = true;
        var sparam = false;
      }

      if ((thePosC <= ($e.height()-imgzh)) && (thePosC > 0)){
        thePosC = thePosC.toFixed();
        $e.stop(fparam,sparam).animate({top: -thePosC+'px'}, {'duration': 1000, 'easing': 'easeOutQuint'});
      }
      else if(thePosC > ($e.height()-imgzh))
        $e.stop(fparam,sparam).animate({top: -($e.height()-imgzh)}, {'duration': 1000, 'easing': 'easeOutQuint'});
      else
        $e.stop(fparam,sparam).animate({top: 0}, {'duration': 1000, 'easing': 'easeOutQuint'});
    }
  });

  var mouse_over_modal = false; 
  DOM.modal.hover(function(){mouse_over_modal = true;}, function(){mouse_over_modal = false;});
  DOM.invite.hover(function(){mouse_over_modal = true;}, function(){mouse_over_modal = false;});
  DOM.intro.hover(function(){mouse_over_modal = true;}, function(){mouse_over_modal = false;});

	/*
	 * makes the modal bigger
	 */

	transitions.getBigger = function(ms, fn){
		DOM.modal.animate({
			width: '860px',
			height: '440px'
		}, ms, fn);
	};

	/*
	 * makes the modal smaller
	 */

	transitions.getSmaller = function(ms, fn){
		DOM.modal.animate({
			width: '760px',
			height: '345px'
		}, ms, fn);		
	};
	
	/*
	 * Accepting tos
	 */

	$('#sign-up-agree').change(function(){
		DOM.signup_agree.toggle();
		DOM.signup_phone.toggle();
	});
	
	/*
	 * Button Interaction
	 */
	
	DOM.btnEnterCode.hover(function(){
		DOM.btnSignUp.addClass('off');
	},function(){
		DOM.btnSignUp.removeClass('off');
	});
	
	/*
	 * Hero scrollable
	 */

  var bg_hero_fading = false;
	DOM.hero.hover(function(){
    bg_hero_fading = true;
		DOM.heroBg.stop().animate({
			opacity: 1
		}, function(){
      bg_hero_fading = false;
    });
	}, function(){
    bg_hero_fading = true;
		DOM.heroBg.stop().animate({
			opacity: .5
		}, function(){
      bg_hero_fading = false;
    });
  });
	
	/*
	 * Click on 'get started'
	 */

	$('#btn-sign-up').click(function(e){
		DOM.intro.fadeOut(CONST.ms,function(){
			DOM.modal.fadeIn(CONST.ms);
			DOM.getStarted.slideDown(CONST.ms);
		});
		e.preventDefault();
	});
	
	$('#lnk-sign-up').click(function(e){
		DOM.invite.fadeOut(CONST.ms,function(){
			DOM.modal.fadeIn(CONST.ms);
			DOM.getStarted.slideDown(CONST.ms);
		});
		e.preventDefault();
	});
	
	/*
	 * Click on 'enter code'
	*/
	$('#btn-enter-code').click(function(e){
		DOM.intro.fadeOut(CONST.ms,function(){
			DOM.invite.fadeIn(CONST.ms);
		});
		e.preventDefault();
	});
	
	/*
	 * Verify signup
	 */

	$('#btn-verify').click(function(e){
		if(!/^[0-9]+$/.test($('#verify-code').val())) {
			$(this).siblings('.icon-alert').removeClass('hidden');
			$(this).siblings('.error').slideDown(CONST.ms);
			$('#verify-code').addClass('error');
			return false;			
		}
		$(this).siblings('.icon-alert').addClass('hidden');
		$(this).siblings('div.error').slideUp(CONST.ms);
		DOM.invite.fadeOut(CONST.ms);
			DOM.hero.fadeOut(CONST.ms, function(){
				$('#hero-bg').css('background', 'url(assets/img/onboarding/bg_hero-3.jpg)' + ' top center');
				$(this).fadeIn(CONST.ms,function(){
					DOM.modal.fadeIn(CONST.ms);
					DOM.co_op.slideDown(CONST.ms);
				});
			});
		e.preventDefault();
	});
	
	/*
	 * Signup validation
	 */

	$('#btn-form-signup').click(function(e){
		var valid = true;
		$('#modal-create-account').find('.icon-alert').hide();
		if(!$('#sign-up-name').val()){
			$('#sign-up-name').addClass('error').siblings('.icon-alert').show();
			valid = false;
		} if(!/^.+@.+\..+$/.test($('#sign-up-email').val())){
			$('#sign-up-email').addClass('error').siblings('.icon-alert').show();
			valid = false;
		} if(!$('#sign-up-password').val()){
			$('#sign-up-password').addClass('error').siblings('.icon-alert').show();
			valid = false;
		} if(!$('#sign-confirm-password').val() || $('#sign-up-password').val() != $('#sign-confirm-password').val()){
			$('#sign-confirm-password').addClass('error').siblings('.icon-alert').show();
			valid = false;
		} if(!$('#sign-up-agree').is(':checked')) {
			valid = false;
		} if(!/^[0-9][0-9][0-9]$/.test($('#sign-phone-1').val()) || !/^[0-9][0-9][0-9]$/.test($('#sign-phone-2').val()) ||!/^[0-9][0-9][0-9][0-9]$/.test($('#sign-phone-3').val()) ) {	
			$('#modal-signup-phone').find('.icon-alert').show();
			$('#modal-signup-phone').find('input').addClass('error');
			valid = false;
		}

		if(!valid) return false;
	
		$('#modal-create-account').fadeOut(CONST.ms, function(){
			DOM.modal.addClass('sign-up-completed');
			$('#modal-account-creation').fadeIn(CONST.ms);	
		});		
		e.preventDefault();
	});

	/*
	 * "on-the-fly validation"
	 */
	

	$('#sign-up-name, #sign-up-password').keyup(function(e){
		if($(this).val().length) $(this).siblings('.icon-check').show();
		else $(this).siblings('.icon-check').hide();
	});

	$('#sign-up-email').keyup(function(e){
		if(/^.+@.+\..+$/.test($(this).val())) $(this).siblings('.icon-check').show();
		else $(this).siblings('.icon-check').hide();
	});

	$('#modal-signup-phone input').keyup(function(e){
		var valid = true;
		if($(this).val().length == 3) $(this).next().focus();
		$('#modal-signup-phone input').each(function(i){
			if(i < 2)
				if(!/^[0-9][0-9][0-9]$/.test($(this).val())) valid = false;
			if(i == 2)
				if(!/^[0-9][0-9][0-9][0-9]$/.test($(this).val())) valid = false;
		});
		if(valid) $(this).siblings('.icon-check').show();
		else $(this).siblings('.icon-check').hide();
	});

	$('#sign-confirm-password').keyup(function(e){
		if($(this).val().length && $(this).val() == $('#sign-up-password').val()) $(this).siblings('.icon-check').show();
		else $(this).siblings('.icon-check').hide();
	});

	$('#modal-create-account').find('input').keyup(function(){
		$(this).removeClass('error');
		$(this).siblings('.icon-alert').hide();
		if($('#modal-create-account').find('.icon-check:visible').length >= 4) $('#sign-up-agree').removeAttr('disabled');
		else $('#sign-up-agree').attr('disabled', 'disabled');
		if($('#modal-create-account').find('.icon-check:visible').length >= 5) $('#btn-form-signup').removeClass('disabled');
		else $('#btn-form-signup').addClass('disabled');
	});


	/*
	 * Verify code validation
	 */

	$('#verify-signup').click(function(e){
		$('#modal-create-account-co-op').find('.icon-alert,.icon-check').hide();
		$('#modal-create-account-co-op').find('input').removeClass('error');
			
		var valid = true;		

		if(!/^.+@.+\..+$/.test($('#verify-email').val())) {
			$('#verify-email').addClass('error').siblings('.icon-alert').show();
			valid = false;
		} if(!$('#verify-password').val()) {
			$('#verify-password').addClass('error').siblings('.icon-alert').show();
			valid = false;
		} if($('#verify-password').val() != $('#verify-confirm-password').val()) {
			$('#verify-confirm-password').addClass('error').siblings('.icon-alert').show();
			valid = false;
		} if(valid) {
			$('#modal-create-account-co-op').fadeOut(CONST.ms, function(){
				DOM.modal.addClass('sign-up-completed');
				$('#modal-account-creation').fadeIn(CONST.ms);	
			});		
		}	
		e.preventDefault();	
	});


	$('#verify-email').keyup(function(e){
		if(/^.+@.+\..+$/.test($(this).val())) $(this).siblings('.icon-check').show();
		else $(this).siblings('.icon-check').hide();
	});

	$('#verify-password').keyup(function(e){
		if($(this).val()) $(this).siblings('.icon-check').show();
		else $(this).siblings('.icon-check').hide();
	});
	
	$('#verify-confirm-password').keyup(function(e){
		if($(this).val() == $('#verify-password').val()) $(this).siblings('.icon-check').show();
		else $(this).siblings('.icon-check').hide();
	});

	$('#modal-create-account-co-op').find('input').keyup(function(){
		$(this).siblings('.icon-alert').hide();
	});


	/*
	 * Product type selected
	 */

	$('.modal-product-type').click(function(e){
		var self = this;
		DOM.createAccount.find('img').attr('src', $(this).attr('href'));
		DOM.hero.fadeOut(CONST.ms, function(){
			$('#hero-bg').css('background', 'url(' + $(self).attr('data-bg') + ')' + ' top center');
			$(this).fadeIn();
		});
		DOM.getStarted.fadeOut(CONST.ms, function(){
			DOM.createAccount.fadeIn();
		});
		e.preventDefault();
	});
	
	
	/*
	 * Back to main modal
	 */

	$('.to-modal-main').click(function(e){
		/* clear errors */
		$('.icon-alert,.icon-check').addClass('hidden');
		$('input').removeClass('error');
		DOM.inviteCode.val('');
		
		if(!/bg_hero\.jpg/.test(DOM.hero.css('background'))) {
			DOM.hero.fadeOut(CONST.ms, function(){
				$('#hero-bg').css('background', 'url(assets/img/onboarding/bg_hero.jpg) top center');
				$(this).fadeIn();
			});
		}
		DOM.modal.fadeOut(CONST.ms);
		DOM.modal.find('.row:not(:hidden)').slideUp(CONST.ms,function(){
			DOM.intro.fadeIn(CONST.ms);
		});
		e.preventDefault();
	});
	
	/*
	 * To invite modal
	 */

	$('.to-invite').click(function(e){
		DOM.modal.fadeOut(CONST.ms);
		DOM.modal.find('.row:not(:hidden)').slideUp(CONST.ms,function(){
			DOM.invite.fadeIn(CONST.ms);
		});
		
		e.preventDefault();
	});
	
	
	/*
	 * Map
	 */
	
	$('.map-btn-back').click(function(e){
		$('#steps-nav ol li.active').removeClass('active').prev().addClass('active');
		$('#steps-map').animate({
			"top": '+=740'
		}, 750);
		e.preventDefault();
	});
	
	$('.map-btn-next').click(function(e){
		$('#steps-nav ol li.active').removeClass('active').next().addClass('active');
		$('#steps-map').animate({
			"top": '-=740'
		}, 750);
		e.preventDefault();
	});
	
	
	/*
	 * How it works transition
	 */
	 
	 $('#factory-direct ul li').mouseover(function(){
		 $(this).addClass('big').siblings().removeClass('big');
	 });
	 
	 /*
		* Carousels
		*/
		
		var carou_nav_items = $('#products-slider-nav ul li');
		$('#carousel-products').cycle({
			fx: 'scrollLeft', 
			prev:	 '#carousel-products-left', 
			next:	 '#carousel-products-right',
			speed: 1500,
				timeout: 0,
			before: function(){
			  carou_nav_items.eq($(this).index()).addClass('active').siblings().removeClass('active');
			}
		});
	 
		carou_nav_items.click(function(e){
				$('#carousel-products').cycle($(this).index());
			e.preventDefault();
		}); 

		$('#team-slider-inner').cycle({
			fx: 'scrollHorz', 
			prev:	 '#carousel-team-left', 
			next:	 '#carousel-team-right',
			speed: 1500,
			slideResize: 0
		}).hover(function(){$('#team-slider-inner').cycle('pause')},function(){$('#team-slider-inner').cycle('resume')});

		$('#carousel-quotes-inner').cycle({
			fx: 'fade',
			next:	 '#carousel-quotes-right',
			speed: 750
		});
		
		
		$('#carousel-team-left').hover(function(e){
		$('#carousel-team-left-bg').fadeIn('fast');
		},function(e){
		$('#carousel-team-left-bg').fadeOut('fast');
		});
		$('#carousel-team-right').hover(function(e){
		$('#carousel-team-right-bg').fadeIn('fast');
		},function(e){
		$('#carousel-team-right-bg').fadeOut('fast');
		});
	
	/*
	 * Login
	 */

	$('#mltb-left a').click(function(e){
		$('#ml-forgot').addClass('enabled');
		e.preventDefault();
	});

	$('#mltb-right a').click(function(e){
		if(!/^.+@.+\..+/.test($('#ml-form-email').val())) 
			$('#ml-form-email').addClass('invalid').prev().show();
		if(!$('#ml-form-password').val()) 
			$('#ml-form-password').addClass('invalid').prev().show();

		e.preventDefault();
	});

	$('#ml-form-email, #ml-form-password').keyup(function(){
		$(this).removeClass('invalid').prev().hide();
	});

	$('#ml-forgot a').click(function(e){
		if(!/^.+@.+\..+/.test($('#ml-forgot').find('input').val())) 
			$('#ml-forgot').find('input').addClass('invalid').prev().prev().show();
		e.preventDefault();
	});
	
	$('#ml-forgot').find('input').keyup(function(){
		$(this).removeClass('invalid').prev().prev().hide();
	});
 
});
