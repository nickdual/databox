/* Author:

*/

$(function(){
	
	$('#my-profile select, .panel.shipping select, .panel.ccard select, .panel.baccount select').uniform();
	$('#account-settings .panel-content').setEqualHeight();
	
	/* set input masks */
	$('.input-phone-1, .input-phone-2, .input-phones-1, .input-phones-2, .input-phone-3, .input-phones-3, .input-zip, .input-cc-number, .input-cc-cvc').numeric();
	
  /*
   * Slider
   */
	
  $('.carousel-inner').cycle({
	 fx: 'scrollLeft',
	 prev:   '.carousel-control.left',
	 next:   '.carousel-control.right',
	 speed: 1500,
   timeout: 0,
	 before: function(){
    $('.carousel-control-pager li').eq($(this).index()).addClass('active').siblings().removeClass('active');
	 }
  });

	$('.carousel-control-pager li').click(function(e){
	  $('.carousel-inner').cycle($(this).index());
		e.preventDefault();
	});


  /*
   * Billing logic
   */

  /*
   * Bank account logic
   */
  
  var validateBank = function(bedit) {
    var valid = true;
    bedit.find('input[type=text]').each(function(){
      if(!$(this).val().length) {
        valid = false;
        $(this).addClass('error');
      } else {
        $(this).removeClass('error');
      }
    });

    bedit.find('input[type=checkbox]').each(function(){
      if(!$(this).is(':checked')) {
        valid = false;
        $(this).siblings('.icon-alert').show();
      } else {
        $(this).siblings('.icon-alert').hide();
      }
    });

    if(!/[0-9]+/.test($('#account-number').val())) {
      valid = false;
      $('#account-number').addClass('error');
    } else {
      $('#account-number').removeClass('error');
    }

    if(!/[0-9]{9,9}/.test($('#routing-number').val())) {
      valid = false;
      $('#routing-number').addClass('error');
    } else {
      $('#routing-number').removeClass('error');
    }

    if($('#confirm-account-number').val() != $('#account-number').val()) {
      valid = false;
      $('#confirm-account-number').addClass('error');
    } else {
      $('#confirm-account-number').removeClass('error');
    }

    if($('#confirm-routing-number').val() != $('#routing-number').val()) {
      valid = false;
      $('#confirm-routing-number').addClass('error');
    } else {
      $('#confirm-routing-number').removeClass('error');
    }
     
    return valid;
  };

  
  $('.baccount').find('.cancel').live('click', function(e){
    $(this).parents('.baccount').slideUp();
    $('#add-account').removeClass('btn-disabled');
    e.preventDefault();
  });

	var baccount_template = $('.baccount:first').clone();
  $('#add-account').live('click', function(e){
    $(this).addClass('btn-disabled');
    var baccount_clone = baccount_template.clone();
    $(baccount_clone).find('.panel-view').hide().siblings('.panel-edit').show();
	  $(baccount_clone).insertAfter($(this).parent()).slideDown();
    e.preventDefault();
  });

  $('.baccount').find('.save').live('click', function(e){
    if(validateBank($(this).parents('.baccount'))) {
      $(this).parents('.panel-edit').slideUp(function(){
       $(this).parents('.baccount').find('.panel-view.state1').slideDown();
       $('#add-account').removeClass('btn-disabled');
      });
    }

    e.preventDefault();
  });

  /*
   * Credit card logic
   */

  var validateCcard = function(pedit) {
    pedit.find('input,select').removeClass('error');
    var valid = true; 
    if(!pedit.find('.input-cc-full-name').val() || pedit.find('.input-cc-full-name').val().length < 3) {
      pedit.find('.input-cc-full-name').addClass('error');
      valid = false;
    } if(pedit.find('.input-cc-number').size()>0){
		if(!/^\d{16,16}$/.test(pedit.find('.input-cc-number').val())){ 
      		pedit.find('.input-cc-number').addClass('error');
      		valid = false;
		}
    } if(pedit.find('.select-cc-type option:selected').size() > 0){
		if(pedit.find('.select-cc-type option:selected').val() == '') {
      		pedit.find('.select-cc-type').addClass('error');
      		valid = false;
		}
    } if(pedit.find('.input-cc-cvc').size() > 0){
		if(!/^\d{3,4}$/.test(pedit.find('.input-cc-cvc').val())) {
      		pedit.find('.input-cc-cvc').addClass('error');
		    valid = false;
		}
    } if(!pedit.find('.input-cc-address-1').val() || pedit.find('.input-cc-address-1').val().length < 3) {
      pedit.find('.input-cc-address-1').addClass('error');
      valid = false;
    } if(!pedit.find('.input-city').val() || pedit.find('.input-city').val().length < 3) {
      pedit.find('.input-city').addClass('error');
      valid = false;
    } if(pedit.find('.input-select-state option:selected').val() == '') {
      pedit.find('.input-select-state').parents('.select-state').addClass('error');
      valid = false;
    } if(!/^\d{3,15}$/.test(pedit.find('.input-zip').val())) {
      pedit.find('.input-zip').addClass('error');
      valid = false;
    }
    return valid;
  };
	
  $('.input-cc-number').live('keyup', function(){
    var len = $(this).val().length;
    var val = '';
	var numbers = '';
	var masked = '';
    if(len < 12) {
      for(var i = 0; i < len; i++) {if(i % 4 == 0) val += ' ';val += 'X';}
    } else {
      for(var i = 0; i < 12; i++){ if(i % 4 == 0) val += ' '; val += 'X';}
      val += ' ';
      val += $(this).val().substr(12);
    }
	masked = val.substr(0,15);
	numbers = $(this).val().substr(12);
    $(this).parents('.ccard-back').find('.panel-header h3 .cc-hide').text(masked);
    $(this).parents('.ccard-back').find('.panel-header h3 .cc-number').text(numbers);
  });

  var ccard_template = $('.ccard:first').html();
  var ccard_back_template = $('.ccard-back:first').clone();

  $('#add-card').click(function(e){
	var ccard_template_clone = $(ccard_back_template).clone();
	$(ccard_template_clone).find('select').uniform();
	$(ccard_template_clone).find('.input-zip, .input-cc-number, .input-cc-cvc').numeric();
	$(ccard_template_clone).insertAfter($(this).parent()).slideDown('normal',function(e){$(ccard_template_clone).css('min-height',$(ccard_template_clone).height());});
    e.preventDefault();
  });

  $('.ccard').find('.edit').live('click', function(e){
    var card = $(this).parents('.ccard');
	  card.find('.panel-view').slideUp('normal',function(e){
		  card.find('.panel-edit').slideDown();
	  });
    e.preventDefault();
  });
  
  $('.ccard').find('.cancel').live('click', function(e){
    var card = $(this).parents('.ccard');
	  card.find('.panel-edit').slideUp('normal',function(e){
		  card.find('.panel-view').slideDown();
	  });
    e.preventDefault();
  });
  
  $('.ccard').find('.save').live('click', function(e){
    var card = $(this).parents('.ccard');
    if(validateCcard(card)){
		  card.find('.panel-edit').fadeTo('fast',0.05,function(e){
			  card.find('.panel-prompt .prompt-save').show();
			  card.find('.panel-prompt').fadeIn(function(){
          var self = this;
          setTimeout(function(){
	          card.find('.panel-edit').slideUp('slow',function(e){
			        card.find('.panel-edit').fadeTo('fast',1);
			        card.find('.panel-edit').hide();
			        card.find('.panel-prompt .prompt-save').hide();
			        card.find('.panel-prompt').hide();
		          card.find('.panel-view').slideDown('slow');
	          });

            $(self).fadeOut('fast');
          }, 400);
		    });
      });
    }
    e.preventDefault();
  });

  $('.ccard').find('.delete').live('click', function(e){
    var card = $(this).parents('.ccard');
	card.find('.panel-view').fadeTo('normal',0.05,function(e){
		card.find('.panel-prompt .prompt-delete').show();
		card.find('.panel-prompt').fadeIn();
	});
	  //card.fadeTo('normal',0,function(e){card.slideUp('normal',function(e){card.remove();})});
      e.preventDefault();
    });

	$('.ccard .panel-prompt .prompt-delete .btn-yes').live('click', function(e){
		var card = $(this).parents('.ccard');
		card.fadeTo('normal',0,function(e){card.slideUp('normal',function(e){card.remove();})});
		e.preventDefault();
	});
	
	$('.ccard .panel-prompt .prompt-delete .btn-no').live('click', function(e){
		var card = $(this).parents('.ccard');
		card.find('.panel-prompt').hide();
		card.find('.panel-prompt .prompt-delete').hide();
		card.find('.panel-view').fadeTo('fast',1);
		e.preventDefault();
	});

  $('.ccard-back').find('.cancel').live('click', function(e){
    var back = $(this).parents('.ccard-back');
	back.fadeTo('fast',0,function(e){
		back.slideUp('normal',function(e){
			back.remove();
		});
	});
      e.preventDefault();
    });

  $('.ccard-back').find('.save').live('click', function(e){
    var back = $(this).parents('.ccard-back');
    if(validateCcard(back)) {
		back.find('.panel-edit').fadeTo('fast',0.05,function(e){
			back.find('.panel-prompt .prompt-save').show();
			back.find('.panel-prompt').fadeIn().delay(400).fadeOut('fast',function(e){back.find('.panel-prompt').hide();back.find('.panel-prompt .prompt-save').hide();});
		});
	    back.find('.panel-edit').slideUp('slow',function(e){
			back.find('.panel-edit').fadeTo('fast',1);
			 back.find('.panel-edit').hide();
		    back.removeClass('ccard-back').addClass('ccard');
		    back.find('.panel-edit .panel-header h3').html('<img src="assets/img/backroom/ico_cc-visa.png" /> <span class="txt-light cc-hide">XXXX XXXX XXXX</span> 0101');
		    back.find('.panel-edit .panel-header save').text('Save Changes');
		    back.find('.panel-view').slideDown('slow',function(e){
				back.css('min-height','0');
				back.css('min-height',back.height());
			});
	    });
    }
    e.preventDefault();
  });
  
	$('.select-cc-type select').live('change',function(e){
		var sel = $(this);
		var panel_header = $(this).parents('.panel-edit').find('.panel-header h3');
		
		if(sel.val()=='') {
			panel_header.find('img').remove();
		} else {
			if(!panel_header.find('img').size()) {
				panel_header.prepend('<img src="assets/img/backroom/ico_cc-'+sel.val()+'.png" />');
			} else {
				panel_header.find('img').attr('src','assets/img/backroom/ico_cc-'+sel.val()+'.png');
			}
		}
	});
  

  /*
   * Shipping logic
   */

  var validateShipping = function(ctn) {
    var valid = true;  

    if(!ctn.find('.input-sh-name').val() || ctn.find('.input-sh-name').val().length < 3) {
      ctn.find('.input-sh-name').addClass('error');
      valid = false;
    } if(!ctn.find('.input-sh-address-1').val() || ctn.find('.input-sh-name').val().length < 3) {
      ctn.find('.input-sh-address-1').addClass('error');
      valid = false;
    } if(!ctn.find('.input-city').val() || ctn.find('.input-city').val().length < 3) {
      ctn.find('.input-city').addClass('error');
      valid = false;
    } if(ctn.find('.input-select-state option:selected').val() == '') {
      ctn.find('.input-select-state').parents('.select-state').addClass('error');
      valid = false;
    } if(!/^\d{3,16}$/.test(ctn.find('.input-zip').val())) {
      ctn.find('.input-zip').addClass('error');
      valid = false;
    }
	return valid;
  };

  var shipping_template = $('.shipping:first').html();
  var shipping_back_template = $('.shipping-back:first').clone();

  $('#add-shipping').click(function(e){
	var shipping_template_clone = $(shipping_back_template).clone();
	$(shipping_template_clone).find('select').uniform();
	$(shipping_template_clone).find('.input-zip').numeric();
	$(shipping_template_clone).insertAfter($(this).parent()).slideDown('normal',function(e){$(shipping_template_clone).css('min-height',$(shipping_template_clone).height());});
    e.preventDefault();
  });

  $('.shipping').find('.edit').live('click', function(e){
    var shipping = $(this).parents('.shipping');
	shipping.find('.panel-view').slideUp('normal',function(e){
		shipping.find('.panel-edit').slideDown();
	});
    e.preventDefault();
  });
  
  $('.shipping').find('.cancel').live('click', function(e){
    var shipping = $(this).parents('.shipping');
	shipping.find('.panel-edit').slideUp('normal',function(e){
		shipping.find('.panel-view').slideDown();
	});
    e.preventDefault();
  });
  
  $('.shipping').find('.save').live('click', function(e){
    var shipping = $(this).parents('.shipping');
    if(validateShipping(shipping)) {
		  shipping.find('.panel-edit').fadeTo('fast',0.05,function(e){
			  shipping.find('.panel-prompt .prompt-save').show();
			  shipping.find('.panel-prompt').fadeIn(function(){
          $(this).fadeOut('fast',function(e){
				    shipping.find('.panel-prompt .prompt-save').hide();
			    });
	    
          shipping.find('.panel-edit').slideUp('slow',function(e){
			      shipping.find('.panel-edit').fadeTo('fast',1);
			      shipping.find('.panel-edit').hide();
		        shipping.find('.panel-view').slideDown('slow');
	        });

		    });
      });
    }
    e.preventDefault();
  });

  $('.shipping').find('.delete').live('click', function(e){
    var shipping = $(this).parents('.shipping');
	shipping.find('.panel-view').fadeTo('normal',0.05,function(e){
		shipping.find('.panel-prompt .prompt-delete').show();
		shipping.find('.panel-prompt').fadeIn();
	});
    e.preventDefault();
  });
  
	$('.shipping .panel-prompt .prompt-delete .btn-yes').live('click', function(e){
		var shipping = $(this).parents('.shipping');
		shipping.fadeTo('normal',0,function(e){shipping.slideUp('normal',function(e){shipping.remove();})});
		e.preventDefault();
	});
	
	$('.shipping .panel-prompt .prompt-delete .btn-no').live('click', function(e){
		var shipping = $(this).parents('.shipping');
		shipping.find('.panel-prompt').hide();
		shipping.find('.panel-prompt .prompt-delete').hide();
		shipping.find('.panel-view').fadeTo('fast',1);
		e.preventDefault();
	});

  $('.shipping-back').find('.cancel').live('click', function(e){
    var back = $(this).parents('.shipping-back');
	back.fadeTo('fast',0,function(e){
		back.slideUp('normal',function(e){
			back.remove();
		});
	});
    e.preventDefault();
  });

  $('.shipping-back').find('.save').live('click', function(e){
    var back = $(this).parents('.shipping-back');
    if(validateShipping(back)){
		back.find('.panel-edit').fadeTo('fast',0.05,function(e){
			back.find('.panel-prompt .prompt-save').show();
			back.find('.panel-prompt').fadeIn().delay(400).fadeOut('fast',function(e){back.find('.panel-prompt').hide();back.find('.panel-prompt .prompt-save').hide();});
		});
	    back.find('.panel-edit').slideUp('normal',function(e){
			back.find('.panel-edit').fadeTo('fast',1);
			back.find('.panel-edit').hide();
		    back.removeClass('shipping-back').addClass('shipping');
		    back.find('.panel-edit .panel-header save').text('Save Changes');
		    back.find('.panel-view').slideDown();
			back.find('.panel-view').slideDown('slow',function(e){
				back.css('min-height','0');
				back.css('min-height',back.height());
			});
	    });
    }
    e.preventDefault();
  });

  $('.shipping-back').find('.edit').live('click', function(e){
    var back = $(this).parents('.shipping-back');
	back.find('.panel-view').slideUp('normal',function(e){
													   
		back.find('.panel-edit').slideDown();
	});
    e.preventDefault();
  });

  /*
   * Account settings logic
   */

  var validateAccount = function(set){
    var valid = true;
    if(!/^.+@.+\..+$/.test(set.find('.input-email-1').val())) {
      set.find('.input-email-1').addClass('error');
      valid = false;
    } if(!set.find('.input-pass').val()) {
      set.find('.input-pass').addClass('error');
      valid = false;
    } if(set.find('.input-pass').val() != set.find('.input-pass-confirm').val()) {
      set.find('.input-pass-confirm').addClass('error');
      valid = false;
    } if(!set.find('.input-name-1').val() || set.find('.input-name-1').val().length < 3){
      set.find('.input-name-1').addClass('error');
      valid = false;
    } if(!/^\d{3,3}$/.test(set.find('.input-phone-1').val())) {
      set.find('.input-phone-1').addClass('error');
      valid = false;
   } if(!/^\d{3,3}$/.test(set.find('.input-phone-2').val())) {
      set.find('.input-phone-2').addClass('error');
      valid = false;
   } if(!/^\d{4,4}$/.test(set.find('.input-phone-3').val())) {
      set.find('.input-phone-3').addClass('error');
      valid = false;
    } if(!/^.+@.+\..+$/.test(set.find('.input-email-2').val())) {
      set.find('.input-email-2').addClass('error');
      valid = false;
    } if(!set.find('.input-address-1').val() || set.find('.input-address-1').val().length < 3){
      set.find('.input-address-1').addClass('error');
      valid = false;
    } if(!set.find('.input-city').val() || set.find('.input-city').val().length < 3){
      set.find('.input-city').addClass('error');
      valid = false;
   } if(set.find('.input-select-state option:selected').val() == ''){
	   set.find('.input-select-state').parents('.select-state').addClass('error');
	   valid = false;
   } if(!set.find('.input-zip').val() || set.find('.input-zip').val().length < 3){
      set.find('.input-zip').addClass('error');
      valid = false;
    } if(!/^\d{3,3}$/.test(set.find('.input-phones-1').val())) {
      set.find('.input-phones-1').addClass('error');
      valid = false;
   } if(!/^\d{3,3}$/.test(set.find('.input-phones-2').val())) {
      set.find('.input-phones-2').addClass('error');
      valid = false;
   } if(!/^\d{4,4}$/.test(set.find('.input-phones-3').val())) {
      set.find('.input-phones-3').addClass('error');
      valid = false;
   }

    return valid;
  };

  $('#account-settings').find('.save').click(function(e){
    var settings = $('#account-settings');
    settings.find('input,select').removeClass('error');
    if(validateAccount(settings)){
      //show success 
    }
    e.preventDefault();
  });
	
	$('input').live('focus',function(e){$(this).removeClass('error');});
	$('select').live('change',function(e){$(this).parents('.error').removeClass('error');});
});