#= require jquery
#= require jquery-ui
#= require jquery_ujs
#= require_tree ./js/plugins
#= require ./js/custom/a-general
#= require_tree ./libs
#= require_directory ./
#= require ./i18n/i18n
#= require ./i18n/translations

$ ->
     #OrderWithMe.global.initActivityIndicator()
  Core.init()