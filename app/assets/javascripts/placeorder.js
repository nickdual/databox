/* Author:
*
*/

$(function(){
	//var owmCart = new OWMCART();
	
	$('.pill').prepend('<span class="grad"></span>');
	
	$('.filter').live('mouseenter',function(){
    $(this).find('.dropdown').toggle();
  }).live('mouseleave',function(){
    $(this).find('.dropdown').hide();
  });
  
  
  
	
	$('.fancybox').fancybox({
		closeBtn: false,
		modal: true
	});
	$('#note a').click(function(e){
		e.preventDefault();
		if($(this).attr('href','#save')) {
			/* save note */
		}
		$.fancybox.close();
	});
	
	//$('.panel select').uniform();
	$('.input-cc-number, .input-cc-cvc, .input-zip, .input-amount').numeric();
	
	$('.toggle:not(.disabled):not(.btn-disabled)').live('click',function(e){
		$(this).parents('.panel').find('.panel-view:not('+$(this).attr('href')+'), .panel-add:not('+$(this).attr('href')+')').slideUp();
		$($(this).attr('href')).slideToggle();
		e.preventDefault();
	});
	
	$('.cell-color, .cell-size, .cell-qty, .cell-item, .cell-specs').live('mouseenter',function(){
		$(this).find('.dropdown').toggle();
	}).live('mouseleave',function(){
		$(this).find('.dropdown').hide();
	});
	
	$('.swatch a').live('click', function(e){
		var swatch = $(this).parents('.swatch');
		swatch.parents('.cell-color').find('.pill').addClass('modified');
		swatch.parents('.cell-color').find('.pill img').attr('src',$(this).data('chip'));
		itemModified(swatch);
		e.preventDefault();
	});
	
	$('.size').find('li').live('click', function(){
		var size = $(this);
		size.parents('.cell-size').find('.size-value').text(size.text());
		size.parents('.cell-size').find('.pill').addClass('modified');
		itemModified(size);
	});
	
	$('.cell-qty input').numeric();
	$('.cell-qty input').live('keyup',function(e){
		var qty = $(this);
		qty.next('.qty').hide();
		qty.parents('.cell-qty').find('.pill').addClass('modified');
		itemModified(qty);
	});
	
	$('.qty').find('li').live('click', function(){
		var qty = $(this);
		qty.parents('.cell-qty').find('input').val(qty.text());
		qty.parents('.cell-qty').find('.pill').addClass('modified');
		itemModified(qty);
	});
	
	function itemModified(item){
		owmCart.itemModified($(item).parents('tr'));
		$(item).parents('tr').find('.remove-button').hide().next().show();
	}
	
	$('.save-button').live('click', function(e){
		e.preventDefault();
		var button = this;
		$(this).parents('tr').animate({opacity: 0.3}, function(){
			var self = this;
			$(self).find('.cell-remove span').append('<div class="loader"></div>');
			setTimeout(function(){
				$(self).find('.modified').removeClass('modified');
				$(self).find('.cell-remove span .loader').remove();
				$(button).hide().prev().show();
				$(self).animate({opacity: 1});
				owmCart.cartSaved();
			}, 750);
		});
	});
	
	/* remove row */
	$('.remove-button').live('click',function(e){
		e.preventDefault();
		var row = $(this).parents('tr');
		row.fadeOut('slow',function(){
			if(row.hasClass('last-group-item')) {
				row.prev().addClass('last-group-item');
			}
			if(row.hasClass('first-group-item')) {
				row.next().addClass('first-group-item');
			}
			row.remove();
		});
	});
	
	
	/* place order */
	$('#agree').on('change',function(e){
		if($(this).attr('checked')){
			$($(this).data('target')).removeClass('btn-disabled');
		} else {
			$($(this).data('target')).addClass('btn-disabled');
		}
		e.preventDefault();
	});
	
	$('.input-qty').live('blur',function(e){
		if($(this).val()<1){$(this).val(1)}
	});
	
	$('.input-amount').live('blur',function(e){
		if($(this).val() < $(this).data('minimum')){
			$(this).val($(this).data('minimum'));
		}
	});
	
	
	
	$('input').live('focus',function(e){$(this).removeClass('error');});
	$('select').live('change',function(e){$(this).parents('.error').removeClass('error');});
	
	
	
	  /*
   * Billing logic
   */

 var validateCcard = function(pedit) {
	 log('validate card');
    pedit.find('input,select').removeClass('error');
    var valid = true; 
    if(!pedit.find('.input-cc-full-name').val() || pedit.find('.input-cc-full-name').val().length < 3) {
      pedit.find('.input-cc-full-name').addClass('error');
      valid = false;
    } if(pedit.find('.input-cc-number').size()>0){
		if(!/^\d{16,16}$/.test(pedit.find('.input-cc-number').val())){ 
      		pedit.find('.input-cc-number').addClass('error');
      		valid = false;
		}
    } if(pedit.find('.select-cc-type option:selected').size() > 0){
		if(pedit.find('.select-cc-type option:selected').val() == '') {
      		pedit.find('.select-cc-type').addClass('error');
      		valid = false;
		}
    } if(pedit.find('.input-cc-cvc').size() > 0){
		if(!/^\d{3,4}$/.test(pedit.find('.input-cc-cvc').val())) {
      		pedit.find('.input-cc-cvc').addClass('error');
		    valid = false;
		}
    } if(!pedit.find('.input-cc-address-1').val() || pedit.find('.input-cc-address-1').val().length < 3) {
      pedit.find('.input-cc-address-1').addClass('error');
      valid = false;
    } if(!pedit.find('.input-city').val() || pedit.find('.input-city').val().length < 3) {
      pedit.find('.input-city').addClass('error');
      valid = false;
    } if(pedit.find('.input-select-state option:selected').val() == '') {
      pedit.find('.input-select-state').parents('.select-state').addClass('error');
      valid = false;
    } if(!/^\d{3,15}$/.test(pedit.find('.input-zip').val())) {
      pedit.find('.input-zip').addClass('error');
      valid = false;
    }
    return valid;
  };
  
  
/*
   * Shipping logic
   */

  var validateShipping = function(ctn) {
    var valid = true;  

	if(!ctn.find('.input-sh-address-1').val()) {
      ctn.find('.input-sh-address-1').addClass('error');
      valid = false;
    } if(!ctn.find('.input-city').val() || ctn.find('.input-city').val().length < 3) {
      ctn.find('.input-city').addClass('error');
      valid = false;
    } if(ctn.find('.input-select-state option:selected').val() == '') {
      ctn.find('.input-select-state').parents('.select-state').addClass('error');
      valid = false;
    } if(!/^\d{3,16}$/.test(ctn.find('.input-zip').val())) {
      ctn.find('.input-zip').addClass('error');
      valid = false;
    }
	return valid;
  };
  
  var addcard = $('#add-card');
  var addbaccount = $('#add-baccount');
  var addaddress = $('#add-address');
  
	$('#btn-checkout').live('click', function(e){
		if(addcard.is(':visible')) {
			if(validateCcard(addcard)) {
			}
		}
		if(addbaccount.is(':visible')) {
			if(validateBaccount(addbaccount)) {
			}
		}
		if(addaddress.is(':visible')) {
			if(validateShipping(addaddress)) {
			}
		}
		e.preventDefault();
	});

	
	$('.tooltip-toggle').live('mouseenter',function(e){
		$(this).find('.tooltip-box').show();
	});

	$('.tooltip-toggle').live('mouseleave',function(e){
		$(this).find('.tooltip-box').hide();
	});

  /*
   * Manage billing forms
   */

  var changeBilling = function(opt){
    if(opt.hasClass('new-baccount-option')) {
      $('#add-baccount').fadeIn();
      //disable same as billing address
      $('#input-same-as-billing-address').attr('disabled', 'disabled').removeAttr('checked');
    } else if(opt.hasClass('new-ccard-option')){
      $('#add-card').fadeIn();
      //enable same as billing address
      $('#input-same-as-billing-address').removeAttr('disabled');
    } else if(opt.hasClass('old-ccard-option')) {
      $('#ccard-panel-'+ opt.attr('id').split('-')[1]).fadeIn();
    } else if(opt.hasClass('old-baccount-option')) {
      $('#baccount-panel-'+ opt.attr('id').split('-')[1]).fadeIn();
      $('#input-same-as-billing-address').attr('disabled', 'disabled').removeAttr('checked');
    } else {
      //enable same as billing address
      $('#input-same-as-billing-address').removeAttr('disabled');
    }
  };

  $('#select-billing').change(function(){
    var opt = $(this).children('option:selected');
    if($(this).parents('.panel').children('div:visible').length) {
      $(this).parents('.panel').children('div:visible').fadeOut(function(){
        changeBilling(opt);
      });
    } else {
      changeBilling(opt);
    }
  });

  $('.bank-details').click(function(e){
    var self = this;
    $(this).parents('.panel-view').find('.panel-content').slideToggle(function(){
      if($(this).is(':visible')) 
        $(self).text($(self).text().replace('Show', 'Hide'));
      else
        $(self).text($(self).text().replace('Hide', 'Show'));
    });
    e.preventDefault();
  });

  $('.ccard-details').click(function(e){
    var self = this;
    $(this).parents('.panel-view').find('.panel-content').slideToggle(function(){
      if($(this).is(':visible')) 
        $(self).text($(self).text().replace('Show', 'Hide'));
      else
        $(self).text($(self).text().replace('Hide', 'Show'));
    });
    e.preventDefault();
  });


  /*
   * Manage shipping form
   */

  $('#input-same-as-billing-address').change(function(){
    if($(this).is(':checked')) {
      $('#select-shipping').attr('disabled', 'disabled');    
      $('#add-address').fadeOut();
    } else {
      $('#select-shipping').removeAttr('disabled');
      if($('.new-shipping-option:selected').length)
        $('#add-address').fadeIn();    
    }
  });

  $('#select-shipping').change(function(){
    var opt = $(this).children('option:selected');
    if(opt.hasClass('new-shipping-option')) {
      $('#add-address').fadeIn();
    } else {
      $('#add-address').fadeOut();
    }
  });

	
	if($('#sidebar').length > 0) {
		
		var jqWindow = $(window);
		var productContext = $('#main-content');
		var sidebar = productContext.find('#sidebar .floaty');
		
		jqWindow.data('height', jqWindow.height());
		sidebar.data('positionedWRT', 'parent');
		// Store some info about the page and the sidebar to make scrolling the sidebar with the page easier
		productContext.data('areaHeight', productContext.outerHeight());
		productContext.data('origOffsetY', productContext.offset().top);
		sidebar.data('origOffsetY', sidebar.offset().top);
		sidebar.data('height', sidebar.outerHeight());
		
		// Handle scrolling for the add-to-cart section, which follows the window
		var sidebarTopSpacing = 0;
		var sidebarScrollHandler = function() {
			var amtScrolledY = jqWindow.scrollTop();
			var encroachmentOnSidebar = amtScrolledY - (sidebar.data('origOffsetY') - sidebarTopSpacing);
			
			if (encroachmentOnSidebar > 0) {
				
				// Unless setNewTop ends up true, there's no reason to update the sidebar's CSS
				var setNewTop = (sidebar.data('positionedWRT') === 'parent') ? true : false;
				
				sidebar.data('positionedWRT', 'viewport');
				
				var sidebarNewTop = sidebarTopSpacing + 10;
			
				// Prevents the sidebar from running off the bottom of the page
				var unscrolledContentHeight = (productContext.data('areaHeight') + productContext.data('origOffsetY')) - amtScrolledY;
				if (unscrolledContentHeight < sidebar.data('height') + sidebarTopSpacing) {
					sidebarNewTop = unscrolledContentHeight - sidebar.data('height') + (sidebarTopSpacing * 0.5);
					setNewTop = true;
				}
				
				if (setNewTop) {
					sidebar.css({
						position:   'fixed',
						top:        sidebarNewTop,
						right:      getSidebarPosition(productContext)
					});
				}
			}
			else if (sidebar.data('positionedWRT') === 'viewport') {
				// Check first that sidebar.data('positionedWRT') is 'viewport' because if it's
				// already 'parent', then there's no reason that the sidebar's CSS would need to change.
				var newTop = 0;
				if(productContext.find('#sidebar .sub-head').length > 0){
					newTop = 50;
				}
				sidebar.data('positionedWRT', 'parent');
				sidebar.css({
					position:   'absolute',
					top:        newTop,
					right:      0
				});
			}
		};
		jqWindow.scroll(sidebarScrollHandler);
		
		// Returns the "right" setting that the sidebar should use. pageWrapper should be a jQuery object.
		function getSidebarPosition(pageWrapper) {
			return document.documentElement.clientWidth - pageWrapper.offset().left - pageWrapper.outerWidth(true);
		};
		
	}

  /*
   * Bank account validation
   */
  
  var validateBaccount = function(bedit) {
    var valid = true;
    bedit.find('input[type=text]').each(function(){
      if(!$(this).val().length) {
        valid = false;
        $(this).addClass('error');
      } else {
        $(this).removeClass('error');
      }
    });

    bedit.find('input[type=checkbox]').each(function(){
      if(!$(this).is(':checked')) {
        valid = false;
        $(this).siblings('.icon-alert').show();
      } else {
        $(this).siblings('.icon-alert').hide();
      }
    });

    if(!/[0-9]+/.test($('#account-number').val())) {
      valid = false;
      $('#account-number').addClass('error');
    } else {
      $('#account-number').removeClass('error');
    }

    if(!/[0-9]{9,9}/.test($('#routing-number').val())) {
      valid = false;
      $('#routing-number').addClass('error');
    } else {
      $('#routing-number').removeClass('error');
    }

    if($('#confirm-account-number').val() != $('#account-number').val()) {
      valid = false;
      $('#confirm-account-number').addClass('error');
    } else {
      $('#confirm-account-number').removeClass('error');
    }

    if($('#confirm-routing-number').val() != $('#routing-number').val()) {
      valid = false;
      $('#confirm-routing-number').addClass('error');
    } else {
      $('#confirm-routing-number').removeClass('error');
    }
     
    return valid;
  };


});