# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$(document).ready ->
  $("#update_promotion_rule").live 'click', (e) ->
    id = $(this).attr("data-id")
    el = $("#edit_promotion_rule_" + id)
    el.validate(
      errorLabelContainer: el.find('#validate_js')
      errorClass: "invalid"
      errorElement: "p"
      rules:
        'promotion_rule[name]':
          required: true
      messages:
        "promotion_rule[name]":
          required: I18n.t("promotion_rule.name.required")
    )

    if el.valid()
      el.find('#validate_rails').removeClass('msgsuccess')
      el.find('#validate_rails').removeClass('msgerror')
      loading = el.find("#loading")
      loading.show()

      $.ajax
        type: "POST"
        url: "/promotion_rules/update_promotion_rule"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        data:
          "promotion_rule[id]": el.find("#promotion_rule_id").val()
          "promotion_rule[name]": el.find("#promotion_rule_name").val()
        dataType: "json"
        success: (data) ->
          msg = el.find('#validate_rails')

          if data.success == true
            msg.addClass('msgsuccess')
            msg.find('p').html(data.message)
            msg.show()
            msg.delay(5000).fadeOut(2000)
          else

            msg.addClass('msgerror')
            msg.find('p').html(data.message)
            msg.show()
          loading.hide()
        error: (data) ->
          loading.hide()
    else
      el.find('#validate_rails').hide()
    e.preventDefault()

  $("#add_promotion_rule").live 'click', (e) ->

    el = $("#new_promotion_rule")
    loading = el.find("#loading")
    el.validate(
      errorLabelContainer: el.find('#validate_js')
      errorClass: "invalid"
      errorElement: "p"
      rules:
        'promotion_rule[name]':
          required: true
      messages:
        "promotion_rule[name]":
          required: I18n.t("promotion_rule.name.required")
    )

    if el.valid()
      el.find('#validate_rails').removeClass('msgsuccess')
      el.find('#validate_rails').removeClass('msgerror')
      loading.show()

      $.ajax
        type: "POST"
        url: "/promotion_rules/add_promotion_rule"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        data:
          "promotion_rule[product_promotion_id]": $(this).closest('.container').attr('id').substr(7)
          "promotion_rule[name]": el.find("#promotion_rule_name").val()
        dataType: "json"
        success:(data) ->
          msg = el.find('#validate_rails')
          if data.success == true
            $("#list_promotion_rules").append(data.data)
            tag_input = $("#list_promotion_rules").find('.rules-container:last').find('input[type=checkbox]')
            $.each(tag_input, (index, value) ->
              if ($(value).attr('checked') != null && $(value).attr('checked') == 'checked')
                $(value).wrap('<span class="checkbox checked"></span>');
              else
                $(value).wrap('<span class="checkbox"></span>');
            )

            msg.addClass('msgsuccess')
            msg.find('p').html(data.message)
            msg.show()
            msg.delay(5000).fadeOut(2000)
          else
            msg.addClass('msgerror')
            msg.find('p').html(data.message)
            msg.show()
          loading.hide()

        error: (e) ->
          alert e
          loading.hide()
    else
      el.find('#validate_rails').hide()

    e.preventDefault()

  $("#add_rule_condition").live 'click', (e) ->
    id = $(this).attr("data-rule-id")
    el = $("#new_promotion_rule_condition_" + id)
    loading = el.find("#loading")

    el.validate(
      errorLabelContainer: el.find('#validate_js')
      errorClass: "invalid"
      errorElement: "p"
      rules:
        'promotion_rule_condition[value]':
          required: true
          number: true
        'promotion_rule_condition[other]':
          required: true
          number: true
      messages:
        'promotion_rule_condition[value]':
          required: I18n.t("promotion_rule_condition.value.required")
          number: I18n.t("promotion_rule_condition.value.number")
        'promotion_rule_condition[other]':
          required: I18n.t("promotion_rule_condition.other.required")
          number: I18n.t("promotion_rule_condition.other.number")
    )

    if el.valid()
      el.find('#validate_rails').removeClass('msgsuccess')
      el.find('#validate_rails').removeClass('msgerror')
      loading.show()
      $.ajax
        type: "POST"
        url: "/promotion_rules/add_rule_condition"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        data:
          "promotion_rule_condition[promotion_rule_id]": el.find("#promotion_rule_condition_promotion_rule_id").val()
          "promotion_rule_condition[promotion_rule_condition_name]": el.find("#promotion_rule_condition_promotion_rule_condition_name").val()
          "promotion_rule_condition[operator_id]": el.find("#promotion_rule_condition_operator_id").val()
          "promotion_rule_condition[value]": el.find("#promotion_rule_condition_value").val()
          "promotion_rule_condition[other]": el.find("#promotion_rule_condition_other").val()
          "promotion_rule_condition[shipment_method_id]": el.find("#promotion_rule_condition_shipment_method_id").val()
        dataType: "json"
        success:(data) ->
          msg = el.find('#validate_rails')
          if data.success == true
            $("#list_promotion_rule_conditions_rule_" + id).append(data.data)
            msg.addClass('msgsuccess')
            msg.find('p').html(data.message)
            msg.show()
            msg.delay(5000).fadeOut(2000)
          else
            msg.addClass('msgerror')
            msg.find('p').html(data.message)
            msg.show()

          loading.hide()
        error: (e) ->
          alert e
          loading.hide()
    else
      el.find('#validate_rails').hide()
    e.preventDefault()

  $("#promotion_rule_condition_save").live 'click',(e) ->
    id = $(this).attr("data-id")
    el = $("#edit_promotion_rule_condition_" + id)
    loading = el.find("#loading")
    el.validate(
      errorLabelContainer: el.find('#validate_js')
      errorClass: "invalid"
      errorElement: "p"
      rules:
        'promotion_rule_condition[value]':
          required: true
          number: true
        'promotion_rule_condition[other]':
          required: true
          number: true
      messages:
        'promotion_rule_condition[value]':
          required: I18n.t("promotion_rule_condition.value.required")
          number: I18n.t("promotion_rule_condition.value.number")
        'promotion_rule_condition[other]':
          required: I18n.t("promotion_rule_condition.other.required")
          number: I18n.t("promotion_rule_condition.other.number")
    )
    if el.valid()
      el.find('#validate_rails').removeClass('msgsuccess')
      el.find('#validate_rails').removeClass('msgerror')
      loading.show()
      $.ajax
        type: "POST"
        url: "/promotion_rules/update_rule_condition"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        data:
          "id": id
          "promotion_rule[id]": el.find("#promotion_rule_id").val()
          "promotion_rule_condition[promotion_rule_condition_name]": el.find("#promotion_rule_condition_promotion_rule_condition_name").val()
          "promotion_rule_condition[operator_id]": el.find("#promotion_rule_condition_operator_id").val()
          "promotion_rule_condition[value]": el.find("#promotion_rule_condition_value").val()
          "promotion_rule_condition[other]": el.find("#promotion_rule_condition_other").val()
          "promotion_rule_condition[shipment_method_id]": el.find("#promotion_rule_condition_shipment_method_id").val()
        dataType: "json"
        success: (data) ->
          msg = el.find('#validate_rails')

          if data.success == true
            msg.addClass('msgsuccess')
            msg.find('p').html(data.message)
            msg.show()
            msg.delay(5000).fadeOut(2000)
          else
            msg.addClass('msgerror')
            msg.find('p').html(data.message)
            msg.show()
          loading.hide()
        error: (e) ->
          loading.hide()
          alert 'error'
    else
      el.find('#validate_rails').hide()
    e.preventDefault()

  $("#promotion_rule_condition_delete").live 'click', (e) ->
    id = $(this).attr("data-id")
    el = $("#edit_promotion_rule_condition_" + id)
    loading = el.find("#loading_promotion_rule_condition")
    loading.show()

    $.ajax
      type: "POST"
      url: "/promotion_rules/delete_rule_condition"
      beforeSend: (xhr) ->
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      data:
        "id": id
      dataType: "json"
      success: (data) ->
        loading.hide()
        el.closest('.rule-child-tab').remove()
      error: (e) ->
        alert 'error'
        loading.hide()
    e.preventDefault()

  $('#add_promotion_category').live 'click', (e) ->
    $("#loading_add_promotion_category").show()
    if $("#promotion_category").val() != null
      $.ajax
        type: "POST"
        url: "/promotion_rules/add_promotion_category"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        data:
          "promotion_category[product_category_id]": $("#promotion_category").val()
          "promotion_category[group_id]": $("#promotion_category_group").val()
          "promotion_category[sub_category_status]": $("#promotion_category_sub_category_status").val()

        dataType: "json"
        success: (data) ->
          console.log(data)
          $("div#list_promotion_category").prepend(data.data)
          $("#loading_add_promotion_category").hide()
          $("#promotion_category").val('')
          $("#promotion_category_group").val('')
        error: (e) ->
          alert 'aaaa'
          $("#loading_add_promotion_category").hide()
          return
    else
      $("#loading_add_promotion_category").hide()
      $('#content-messages_error_category').show()

    e.preventDefault()

  $('#add_promotion_product').live 'click', (e) ->
    $("#loading_add_promotion_product").show()
    product_id = $("#promotion_product").val()
    if product_id != null
      $.ajax(
        type: "POST"
        url: "/promotion_rules/add_promotion_product"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        data:
          "promotion_product[product_id]": product_id
          "promotion_product[sub_category_status]": $("#promotion_product_sub_category_status").val()
        dataType: "json"
        success: (data) ->
          $("#list_promotion_product").prepend(data.data)
          $("#loading_add_promotion_product").hide()
          $("#promotion_product").val('')

        error: (e) ->
          console.log(e)
          $("#loading_add_promotion_product").hide()
          return
      )
    else
      $("#loading_add_promotion_product").hide()
      $('#content-messages_error_product').show()
    e.preventDefault()

  $("#delete_category").live 'click', (e) ->
    id = $(this).attr('data-id')
    parent = $(this).parent()
    parent = $(this).parent()
    loading = parent.find("#loading_delete_category")
    loading.show()
    $.ajax
      type: "POST"
      url: "/promotion_rules/delete_promotion_category"
      beforeSend: (xhr) ->
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      data:
        "id": id
      dataType: "json"
      success: (data) ->
        parent.remove()
        loading.hide()
      error: (e) ->

    e.preventDefault()

  $("#delete_promotion_product").live 'click', (e) ->
    id = $(this).attr('data-id')
    parent = $(this).parent()
    parent = $(this).parent()
    loading = parent.find("#loading_delete_promotion_product")
    loading.show()
    $.ajax
      type: "POST"
      url: "/promotion_rules/delete_promotion_product"
      beforeSend: (xhr) ->
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      data:
        "id": id
      dataType: "json"
      success: (data) ->
        parent.remove()
        loading.hide()
      error: (e) ->
    e.preventDefault()

  $("#add_promotion_rule_condition_category").live 'click', (e) ->
    id = $(this).attr('data-id')
    el = $("#new_promotion_rule_condition_category_" + id)

    el.validate(
      errorLabelContainer: el.find('#validate_js')
      errorClass: "invalid"
      errorElement: "p"
      rules:
        'promotion_rule_condition_category[promotion_rule_condition_id]':
          required: true
        'promotion_rule_condition_category[product_category_id]':
          required: true
        "promotion_rule_condition_category[sub_category_status]":
          required: true
      messages:
        'promotion_rule_condition_category[promotion_rule_condition_id]':
          required: I18n.t("promotion_rule_condition_category.promotion_rule_condition_id.required")
        'promotion_rule_condition_category[product_category_id]':
          required: I18n.t("promotion_rule_condition_category.product_category_id.required")
        "promotion_rule_condition_category[sub_category_status]":
          required: I18n.t("promotion_rule_condition_category.sub_category_status.required")
    )
    if el.valid()
      el.find('#validate_rails').removeClass('msgsuccess')
      el.find('#validate_rails').removeClass('msgerror')
      loading = el.find("#loading")
      loading.show()
      $.ajax
        type: "POST"
        url: "/promotion_rules/add_rule_condition_category"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        data:
          "promotion_rule_condition_category[promotion_rule_condition_id]": el.find("#promotion_rule_condition_category_promotion_rule_condition_id").val()
          "promotion_rule_condition_category[product_category_id]": el.find("#promotion_rule_condition_category_product_category_id").val()
          "promotion_rule_condition_category[sub_category_status]": el.find("#promotion_rule_condition_category_sub_category_status").val()
        dataType: "json"
        success: (data) ->
          loading.hide()
          el.closest('.rule-child-tab').find("#msg-category").hide()
          msg = el.find('#validate_rails')
          if data.success == true
            $('#condition-category-' + id).prepend(data.data)
            msg.addClass('msgsuccess')
            msg.find('p').html(data.message)
            msg.show()
            msg.delay(5000).fadeOut(2000)
          else
            msg.addClass('msgerror')
            msg.find('p').html(data.message)
            msg.show()
        error: (e) ->
          alert 'error'
          loading.hide()
    else
      el.find('#validate_rails').hide()

    e.preventDefault()

  $("#update_promotion_rule_condition_category").live 'click', (e) ->
    id = $(this).attr("data-id")
    el = $("#edit_promotion_rule_condition_category_" + id)
    loading = el.find("#loading")
    loading.show()
    $.ajax
      type: "POST"
      url: "/promotion_rules/update_rule_condition_category"
      beforeSend: (xhr) ->
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      data:
        "id": id
        "promotion_rule_condition_category[promotion_rule_condition_id]": el.find("#promotion_rule_condition_category_promotion_rule_condition_id").val()
        "promotion_rule_condition_category[product_category_id]": el.find("#promotion_rule_condition_category_product_category_id").val()
        "promotion_rule_condition_category[sub_category_status]": el.find("#promotion_rule_condition_category_sub_category_status").val()
      dataType: "json"
      success: (data) ->
        loading.hide()
      error: (e) ->
        alert 'error'
        loading.hide()
    e.preventDefault()

  $("#delete_promotion_rule_condition_category").live 'click', (e) ->
    id = $(this).attr("data-id")
    el = $("#edit_promotion_rule_condition_category_" + id)
    loading = el.find("#loading")
    loading.show()
    $.ajax
      type: "POST"
      url: "/promotion_rules/delete_rule_condition_category"
      beforeSend: (xhr) ->
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      data:
        "id": id

      dataType: "json"
      success: (data) ->
        loading.hide()
        el.remove()
      error: (e) ->
        alert 'error'
        loading.hide()
    e.preventDefault()

  # Product condition
  $("#add_rule_condition_product").live 'click', (e) ->
    id = $(this).attr("data-id")
    el = $("#new_promotion_rule_condition_product_" + id)
    el.validate(
      errorLabelContainer: el.find('#validate_js')
      errorClass: "invalid"
      errorElement: "p"
      rules:
        'promotion_rule_condition_product[promotion_rule_condition_id]':
          required: true
        'promotion_rule_condition_product[product_id]':
          required: true
        "promotion_rule_condition_product[sub_category_status]":
          required: true
      messages:
        'promotion_rule_condition_product[promotion_rule_condition_id]':
          required: "condition id"
        'promotion_rule_condition_product[product_id]':
          required: "product id"
        "promotion_rule_condition_product[sub_category_status]":
          required: "sub"
    )
    if el.valid()
      el.find('#validate_rails').removeClass('msgsuccess')
      el.find('#validate_rails').removeClass('msgerror')
      loading = el.find("#loading")
      loading.show()
      $.ajax
        type: "POST"
        url: "/promotion_rules/add_rule_condition_product"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        data:
          "promotion_rule_condition_product[promotion_rule_condition_id]": el.find("#promotion_rule_condition_product_promotion_rule_condition_id").val()
          "promotion_rule_condition_product[product_id]": el.find("#promotion_rule_condition_product_product_id").val()
          "promotion_rule_condition_product[sub_category_status]": el.find("#promotion_rule_condition_product_sub_category_status").val()
        dataType: "json"
        success: (data) ->

          loading.hide()
          $("#msg-product").hide()
          msg = el.find('#validate_rails')
          if data.success == true
            $("#condition-product-" + id).prepend(data.data)
            msg.addClass('msgsuccess')
            msg.find('p').html(data.message)
            msg.show()
            msg.delay(5000).fadeOut(2000)
          else
            msg.addClass('msgerror')
            msg.find('p').html(data.message)
            msg.show()
        error: (e) ->
          alert 'error'
          loading.hide()
    else
      el.find('#validate_rails').hide()

    e.preventDefault()

  $("#update_rule_condition_product").live 'click', (e) ->
    id = $(this).attr("data-id")
    el = $("#edit_promotion_rule_condition_product_" + id)
    loading = el.find("#loading")
    loading.show()
    $.ajax
      type: "POST"
      url: "/promotion_rules/update_rule_condition_product"
      beforeSend: (xhr) ->
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      data:
        "id": id
        "promotion_rule_condition_product[promotion_rule_condition_id]": el.find("#promotion_rule_condition_product_promotion_rule_condition_id").val()
        "promotion_rule_condition_product[product_id]": el.find("#promotion_rule_condition_product_product_id").val()
        "promotion_rule_condition_product[sub_category_status]": el.find("#promotion_rule_condition_product_sub_category_status").val()
      dataType: "json"
      success: (data) ->
        loading.hide()
      error: (e) ->
        alert 'error'
        loading.hide()
    e.preventDefault()

  $("#delete_rule_condition_product").live 'click', (e) ->
    id = $(this).attr("data-id")
    el = $("#edit_promotion_rule_condition_product_" + id)
    loading = el.find("#loading")
    loading.show()
    $.ajax
      type: "POST"
      url: "/promotion_rules/delete_rule_condition_product"
      beforeSend: (xhr) ->
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      data:
        "id": id

      dataType: "json"
      success: (data) ->
        loading.hide()
        el.remove()
      error: (e) ->
        alert 'error'
        loading.hide()
    e.preventDefault()

  $('.edit_promotion_rule_action #promotion_rule_action_save').live "click",(e) ->
    form = $(this).closest('form')
    id = form.attr('id').substr(27)
    form.validate(
      errorLabelContainer: form.find('#validate_js')
      errorClass: "invalid"
      errorElement: "p"
      rules:
        'promotion_rule_action[quantity]':
          required: true
          number: true
        "promotion_rule_action[amount]":
          required: true
          number: true
        "promotion_rule_action[service_name]":
          required: true
        "promotion_rule_action[item_id]":
          required: true
          number: true
        "promotion_rule_action[party_id]":
          required: true

      messages:
        'promotion_rule_action[quantity]':
          required: I18n.t("promotion_rule_action.quantity.required")
          number: I18n.t("promotion_rule_action.quantity.number")
        "promotion_rule_action[amount]":
          required: I18n.t("promotion_rule_action.amount.required")
          number: I18n.t("promotion_rule_action.quantity.number")
        "promotion_rule_action[service_name]":
          required: I18n.t("promotion_rule_action.service_name.required")
        "promotion_rule_action[item_id]":
          required: I18n.t("promotion_rule_action.item_id.required")
          number: I18n.t("promotion_rule_action.quantity.number")
        "promotion_rule_action[party_id]":
          required: I18n.t("promotion_rule_action.party_id.required")

    )
    if form.valid()
      form.find('#validate_rails').removeClass('msgsuccess')
      form.find('#validate_rails').removeClass('msgerror')
      $.ajax
        type: "POST"
        url: "/promotion_rules/update_rule_action"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
          form.find(".loading-button").show()
        data:
          "id": id
          "promotion_rule_action[promotion_rule_action_name]": form.find("#promotion_rule_action_promotion_rule_action_name").val()
          "promotion_rule_action[quantity]": form.find("#promotion_rule_action_quantity").val()
          "promotion_rule_action[amount]": form.find("#promotion_rule_action_amount").val()
          "promotion_rule_action[service_name]": form.find("#promotion_rule_action_service_name").val()
          "promotion_rule_action[item_id]": form.find("#promotion_rule_action_item_id").val()
          "promotion_rule_action[party_id]": form.find("#promotion_rule_action_party_id").val()
          "promotion_rule_action[use_cart_quantity]": form.find("#promotion_rule_action_use_cart_quantity").val()
        dataType: "json"
        success: (data) ->
          form.find(".loading-button").hide()
          msg = form.find('#validate_rails')
          if data.success == true
            form.closest('.lra-container').children('.lr-actions').append(data.data)
            msg.addClass('msgsuccess')
            msg.find('p').html(data.message)
            msg.show()
            msg.delay(5000).fadeOut(2000)
          else
            msg.addClass('msgerror')
            msg.find('p').html(data.message)
            msg.show()
        error: (e) ->

    else
      form.find('#validate_rails').hide()
    e.preventDefault()


  $(document).on('click', '.edit_promotion_rule_action #promotion_rule_action_delete', (e) ->
    form = $(this).closest('form')
    id = form.attr('id').substr(27)
    $.ajax
      type: "POST"
      url: "/promotion_rules/delete_rule_action"
      beforeSend: (xhr) ->
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        form.find(".loading-button").show()
      data:
        "id": id
      dataType: "json"
      success: (response) ->
        form.find(".loading-button").hide()
        form.closest('.rule-action').remove()
      error: (e) ->
    e.preventDefault()
  )

  $(document).on('click', '.edit-rule-action-category .btn-add', (e) ->
    form = $(this).closest('form')
    id = form.attr('id').substr(36)
    form.validate(
      errorLabelContainer: form.find('#validate_js')
      errorClass: "invalid"
      errorElement: "p"
      rules:
        'promotion_rule_action_category[product_category_id]':
          required: true
        "promotion_rule_action_category[sub_category_status]":
          required: true
        "promotion_rule_action_category[group_id]":
          required: true
        "promotion_rule_action_category[promotion_rule_action_id]":
          required: true
    )
    if form.valid()
      form.find('#validate_rails').removeClass('msgsuccess')
      form.find('#validate_rails').removeClass('msgerror')
      $.ajax
        type: "POST"
        url: "/promotion_rules/add_rule_action_category"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
          form.find(".loading-button").show()
        data:
          "id": id
          "promotion_rule_action_category[product_category_id]": form.find("select[name=\"promotion_rule_action_category[product_category_id]\"]").val()
          "promotion_rule_action_category[sub_category_status]": form.find("#promotion_rule_action_category_sub_category_status").val()
          "promotion_rule_action_category[group_id]": form.find("#promotion_rule_action_category_group_id").val()
          "promotion_rule_action_category[promotion_rule_action_id]": form.closest('.rule-action').attr('id').substr(7)
        dataType: "json"
        success: (data) ->
          form.find(".loading-button").hide()
          msg = form.find('#validate_rails')
          if data.success == true
            form.closest('.rule-action').find('.list-rule-action-categories .lra-body').prepend(data.data)
            msg.addClass('msgsuccess')
            msg.find('p').html(data.message)
            msg.show()
            msg.delay(5000).fadeOut(2000)
          else
            msg.addClass('msgerror')
            msg.find('p').html(data.message)
            msg.show()
        error: (e) ->
      e.preventDefault()
    else
      form.find('#validate_rails').hide()
  )

  $(document).on('click', '.list-rule-action-categories .btn-delete', (e) ->
    tag = $(this).closest('.rule-action-category')
    id = tag.attr('id').substr(7)
    $.ajax
      type: "POST"
      url: "/promotion_rules/delete_rule_action_category"
      beforeSend: (xhr) ->
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        tag.find(".loading-button").show()
      data: {"id": id}
      dataType: "json"
      success: (data) ->
        tag.find(".loading-button").hide()
        tag.remove()
      error: (e) ->
    e.preventDefault()
  )

  $('.edit-rule-action-product .btn-add').live 'click', (e) ->
    form = $(this).closest('form')
    id = form.closest('.rule-action').attr('id').substr(7)
    form.validate(
      errorLabelContainer: form.find('#validate_js')
      errorClass: "invalid"
      errorElement: "p"
      rules:
        'promotion_rule_action_product[product_id]':
          required: true
          number: true
        "promotion_rule_action_product[sub_category_status]":
          required: true
        "promotion_rule_action_category[group_id]":
          required: true
        "promotion_rule_action_product[promotion_rule_action_id]":
          required: true
        messages:
          'promotion_rule_action_product[product_id]':
            required: I18n.t("promotion_rule_action_product.product_id.required")
            number: I18n.t("promotion_rule_action_product.product_id.number")
          "promotion_rule_action_product[sub_category_status]":
            required: I18n.t("promotion_rule_action_product.sub_category_status.required")

          "promotion_rule_action_category[group_id]":
            required: I18n.t("promotion_rule_action_category.group_id_error.required")
          "promotion_rule_action_product[promotion_rule_action_id]":
            required: I18n.t("promotion_rule_action_product.promotion_rule_action_id.required")

    )
    if form.valid()
      form.find('#validate_rails').removeClass('msgsuccess')
      form.find('#validate_rails').removeClass('msgerror')
      $.ajax
        type: "POST"
        url: "/promotion_rules/add_rule_action_product"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
          form.find(".loading-button").show()
        data:
          "id": id
          "promotion_rule_action_product[product_id]": form.find("[name=\"promotion_rule_action_product[product_id]\"]").val()
          "promotion_rule_action_product[sub_category_status]": form.find("[name=\"promotion_rule_action_product[sub_category_status]\"]").val()
          "promotion_rule_action_product[promotion_rule_action_id]": form.closest('.rule-action').attr('id').substr(7)
        dataType: "json"
        success: (data) ->
          form.find(".loading-button").hide()

          msg = form.find('#validate_rails')
          if data.success == true
            $('.list-rule-action-products .lra-body').prepend(data.data)
            msg.addClass('msgsuccess')
            msg.find('p').html(data.message)
            msg.show()
            msg.delay(5000).fadeOut(2000)
          else
            msg.addClass('msgerror')
            msg.find('p').html(data.message)
            msg.show()
        error: (e) ->

    else
      form.find('#validate_rails').hide()
    e.preventDefault()


  $(document).on('click', '.list-rule-action-products .btn-delete', (e) ->
    tag = $(this).closest('.rule-action-product')
    id = tag.attr('id').substr(7)
    $.ajax
      type: "POST"
      url: "/promotion_rules/delete_rule_action_product"
      beforeSend: (xhr) ->
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        tag.find(".loading-button").show()
      data: {"id": id}
      dataType: "json"
      success: (data) ->
        tag.find(".loading-button").hide()
        tag.remove()
      error: (e) ->
    e.preventDefault()
  )

  $('#promotion_rule_action_add').live "click",(e) ->
    form = $(this).closest('form')
    id = form.closest('.rules-container').attr('id').substr(7)
    form.validate(
      errorLabelContainer: form.find('#validate_js')
      errorClass: "invalid"
      errorElement: "p"
      rules:
        'promotion_rule_action[quantity]':
          required: true
          number: true
        "promotion_rule_action[amount]":
          required: true
          number: true
        "promotion_rule_action[service_name]":
          required: true
        "promotion_rule_action[item_id]":
          required: true
          number: true
        "promotion_rule_action[party_id]":
          required: true

      messages:
        'promotion_rule_action[quantity]':
          required: I18n.t("promotion_rule_action.quantity.required")
          number: I18n.t("promotion_rule_action.quantity.number")
        "promotion_rule_action[amount]":
          required: I18n.t("promotion_rule_action.amount.required")
          number: I18n.t("promotion_rule_action.quantity.number")
        "promotion_rule_action[service_name]":
          required: I18n.t("promotion_rule_action.service_name.required")
        "promotion_rule_action[item_id]":
          required: I18n.t("promotion_rule_action.item_id.required")
          number: I18n.t("promotion_rule_action.quantity.number")
        "promotion_rule_action[party_id]":
          required: I18n.t("promotion_rule_action.party_id.required")

    )
    if form.valid()
      form.find('#validate_rails').removeClass('msgsuccess')
      form.find('#validate_rails').removeClass('msgerror')
      $.ajax
        type: "POST"
        url: "/promotion_rules/insert_rule_action"
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
          form.find(".loading-button").show()
        data:
          "promotion_rule_action[promotion_rule_action_name]": form.find("#promotion_rule_action_promotion_rule_action_name").val()
          "promotion_rule_action[quantity]": form.find("#promotion_rule_action_quantity").val()
          "promotion_rule_action[amount]": form.find("#promotion_rule_action_amount").val()
          "promotion_rule_action[service_name]": form.find("#promotion_rule_action_service_name").val()
          "promotion_rule_action[item_id]": form.find("#promotion_rule_action_item_id").val()
          "promotion_rule_action[party_id]": form.find("#promotion_rule_action_party_id").val()
          "promotion_rule_action[use_cart_quantity]": form.find("#promotion_rule_action_use_cart_quantity").is(':checked')
          "promotion_rule_action[promotion_rule_id]": id
        dataType: "json"
        success: (data) ->
          form.find(".loading-button").hide()
          msg = form.find('#validate_rails')
          if data.success == true
            form.closest('.lra-container').children('.lr-actions').append(data.data)
            msg.addClass('msgsuccess')
            msg.find('p').html(data.message)
            msg.show()
            msg.delay(5000).fadeOut(2000)
          else
            msg.addClass('msgerror')
            msg.find('p').html(data.message)
            msg.show()
          $(".chosen").chosen();
          form.closest('.lra-container').children('.lr-actions').children('div:last').find('input[type=checkbox]').each(() ->
            console.log($(this).attr('checked'))
            if ($(this).attr('checked') != null && $(this).attr('checked') == 'checked')
              $(this).wrap('<span class="checkbox checked"></span>');
            else
              $(this).wrap('<span class="checkbox"></span>');
          );
        error: (e) ->
          console.log(e)
    else
      form.find('#validate_rails').hide()
    e.preventDefault()

  $('.rules-container .btn-conditions').click(() ->
    container = $(this).closest('.rules-container')
    lrc = container.find('.lrc-container')
    if (lrc.is(":hidden"))
      lrc.slideDown("fast");
    else
      lrc.slideUp("fast")
    return false;
  )

  $('.rules-container .btn-actions').click(() ->
    container = $(this).closest('.rules-container')
    lra = container.find('.lra-container')
    if (lra.is(":hidden"))
      lra.slideDown("fast");
    else
      lra.slideUp("fast")
    return false;
  )

  $(document).on('click', '.tab-header', () ->
    body = $(this).parent().children('.tab-body')
    if (body.is(":hidden"))
      body.slideDown("fast", () ->
        body.css('display', 'inline-block')
      );
    else
      body.slideUp("fast")
  )
