/* Author:

*/

$(function(){

	$('.btn-disabled').live('click',function(e){e.preventDefault();});
	var DOM = {
		main: $('#modal-main'),
		hero: $('#hero'),
		modal: $('#hero').find('.modal'),
		inner: $('#hero').find('.inner'),
		getStarted: $('#modal-get-started'),
		createAccount: $('#modal-create-account'),
    signup_agree: $('#modal-signup-agree'),
    signup_phone: $('#modal-signup-phone'),
		co_op: $('#modal-create-account-co-op'),
    body_html: $('body, html'),
    steps_inner: $('#steps-inner')
	};

  var CONST = {
		ms: 500
  };

  /*
	 * Smooth top navbar
	 */

	 $('#header a').click(function(e){
	 	var top = $($(this).attr('href')).offset().top;
	 	var sih = DOM.steps_inner.height();
	 	DOM.steps_inner.height(0);
		DOM.body_html.animate({
			scrollTop: top
		}, 1500, function(){
			DOM.steps_inner.height(sih);
		});
		e.preventDefault();
	});
	
	$('.tohead').click(function(e){
	 	var sih = DOM.steps_inner.height();
	 	DOM.steps_inner.height(0);
		DOM.body_html.animate({
			scrollTop: 0
		}, 1500, function(){
	 	  DOM.steps_inner.height(sih);
    });
		e.preventDefault();
	});

	/* toggle active on General Page template */
	$('#sidebar .topics li').click(function(e){
		$(this).parent('.topics').find('li').removeClass('active')
		$(this).addClass('active');
	});
	
	/*var ins = false;
	$(window).scroll(function(){
		if(!ins && $(this).scrollTop()> 230){ $('#sidebar').css({'position': 'fixed','top': '0'}); ins = true; }
		else if(ins && $(this).scrollTop()<= 230) { ins = false; $('#sidebar').css({'position': 'static'}); }
	});*/
	
	$(' [placeholder] ').defaultValue();
	
	$('.icon-tooltip').wrapInner('<div class="tooltip-txt"></div>');
	$('.icon-tooltip').wrapInner('<div class="tooltip-box"></div>');
	$('.icon-tooltip .tooltip-box').append('<div class="tooltip-pointer"></div>');
	$('.icon-tooltip').live('mouseenter',function(){
		$(this).find('.tooltip-box, h6, p').show();
		$(this).live('mouseleave',function(){$(this).find('.tooltip-box').hide();});
	});
});

/**
 *  Style Switcher 
 */

$('.style-select').click(function(){
  style = $(this).attr('id').split('-');
  if(style[1] == 'default') {
    $('#branded-styles').remove();
  } else {
    if($('#branded-styles').length == 0){
      $('head').append('<link id="branded-styles" rel="stylesheet" href="assets/css/'+style[1]+'.css">');
    } else {
      $('#branded-styles').attr('href','assets/css/'+style[1]+'.css' );
    }
  }
});

$('#switcher-toggle').click(function(){
  $('#style-options').toggle(200);
})




