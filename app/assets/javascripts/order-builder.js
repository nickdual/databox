
/*
 * Order builder
 */

$(function(){
	var CART = {
		container: $('#cart'),
		link: $('#cart a'),
		loader: $('#cart a .icon-loader-pink').clone(),
		growl: $('#cart #cart-growl').clone(),
		diffType: 'added',
		items: 0,
		itemsEl: $('#items h5'),
		estPrice: 0,
		estPriceElCont: $('#estimated-price'),
		estPriceEl: $('#estimated-price h5'),
		minDue: 0,
		minDueElCont: $('#minimum-due'),
		minDueEl: $('#minimum-due h5')
	};
	$('#cart a .icon-loader-pink').remove();
	$('#cart #cart-growl').remove();
	

	/* temporary append index number */
	$('.product').each(function(){
		$(this).find('.product-id').text($(this)
    .find('.product-id').text() + '-' + $(this).index());
	});
	
	
/**
 * Fancybox 
 */	
	
  $('.fancybox').fancybox({
    closeBtn: false
  });
  $('.icon-overlay-close').click(function(e){
    e.preventDefault();
    $.fancybox.close();
  });



/*
   * Product silder
   */
  $('.item-carousel').cycle({
    fx: 'scrollLeft', 
    prev:  '.icon-arrow-carousel-left', 
    next:  '.icon-arrow-carousel-right',
    speed: 750,
    timeout: 0,
    before: function(curr, next, opts){
      var img = $(next).find('img');
      if(typeof img.attr('data-original') != "undefined") {
        //alert('cool');
        img.attr('src', img.attr('data-original')).removeAttr('data-original');
      }
    }
  });


/*
 * Infinite scroll
 */

	var $productsGrid = $('#products-grid');
	var loading = false;
	
//	$(window).scroll(function(evt){
//		if(!loading && $(window).scrollTop() > $(document).height() - $(window).height() - 200) {
//			loading = true;
//			var cat = $('.categories.active a').attr('data-category');
//			var prds = '';
//			var product = '<div id="product-1" class="product '+cat.substr(1)+'">' + $productsGrid.find(cat).html() + '</div>';
//
//			for(var i = 0; i < 6; i++) prds += product;
//			prds = $(prds);
//			$productsGrid.isotope('insert', prds);
//			setTimeout(function(){
//				loading = false;
//			}, 2000);
//		}
//	});

/*
 * Isotope products display
 */
	
	/** Custom layout modes for centering
	 * 
	 * See: http://isotope.metafizzy.co/custom-layout-modes/centered-masonry.html
	 * 
	 */
		$.Isotope.prototype._getCenteredMasonryColumns = function() {
		this.width = this.element.width();
		
		var parentWidth = this.element.parent().width();
		
									// i.e. options.masonry && options.masonry.columnWidth
		var colW = this.options.masonry && this.options.masonry.columnWidth ||
									// or use the size of the first item
									this.$filteredAtoms.outerWidth(true) ||
									// if there's no items, use size of container
									parentWidth;
		
		var cols = Math.floor( parentWidth / colW );
		cols = Math.max( cols, 1 );

		// i.e. this.masonry.cols = ....
		this.masonry.cols = cols;
		// i.e. this.masonry.columnWidth = ...
		this.masonry.columnWidth = colW;
	};
	
	$.Isotope.prototype._masonryReset = function() {
		// layout-specific props
		this.masonry = {};
		// FIXME shouldn't have to call this again
		this._getCenteredMasonryColumns();
		var i = this.masonry.cols;
		this.masonry.colYs = [];
		while (i--) {
			this.masonry.colYs.push( 0 );
		}
	};

	$.Isotope.prototype._masonryResizeChanged = function() {
		var prevColCount = this.masonry.cols;
		// get updated colCount
		this._getCenteredMasonryColumns();
		return ( this.masonry.cols !== prevColCount );
	};
	
	$.Isotope.prototype._masonryGetContainerSize = function() {
		var unusedCols = 0,
				i = this.masonry.cols;
		// count unused columns
		while ( --i ) {
			if ( this.masonry.colYs[i] !== 0 ) {
				break;
			}
			unusedCols++;
		}
		
		return {
			height : Math.max.apply( Math, this.masonry.colYs ),
			// fit container to columns that have been used;
			width : (this.masonry.cols - unusedCols) * this.masonry.columnWidth
		};
	};

	$(window).load(function(){
		$productsGrid.isotope({
			itemSelector : '.product',
			animationEngine: 'jquery',
			getSortData: {
				featured: function($el){
					return $el.find('.featured').length;
				}
			}
		});
		$('#main-content').animate({
			opacity: 1
		});
	});

/*
 * Sidebar behavior
 */

	/*
	 * resize
	 */
  var sidebarResize = function(){
    $('#ob-sidebar-wrapper, #ob-sidebar').height($(window).height()-105);
    $('#ob-sidebar').niceScroll().resize();
  };
	$('#ob-sidebar-wrapper, #ob-sidebar').height($(window).height()-105);
	$('#ob-sidebar-wrapper').fadeTo('fast',1);
	var bar = $('#ob-sidebar').niceScroll();
	bar.resize();
	$(window).resize(sidebarResize);

	/*
	 * Show/Hide sidebar
	 */

	var side_hidden = false;
	$('#sidebar-button').click(function(e){
		if(!side_hidden) {
			$('#ob-sidebar-wrapper').animate({
				'left': '-200'
			}, 500, 'easeInOutQuint', function(e){
				$(this).addClass('collapsed');
        bar.hide();
			});
	
		$('#main-content').animate({'margin-left': 30},500,'easeInOutQuint',function(e){$productsGrid.isotope({});});
	
		} else {
	
			$('#ob-sidebar-wrapper').animate({
				'left': '0'
			}, 500, 'easeInOutQuint', function(e){
				$(this).removeClass('collapsed');
        bar.show();
			});
	
			$('#main-content').animate({'margin-left': 230},500,'easeInOutQuint',function(e){$productsGrid.isotope({});});
		}
		side_hidden = !side_hidden;
	});


	/*
	 * Category filter
	 */

	$('.categories').click(function(e){
		var self = this;
		$(this).addClass('active').siblings('.active').removeClass('active');
		$('.category-loader').show();
		$productsGrid.animate({opacity: 0}, function(){
			var selector = $(self).children('a').attr('data-category');
			$productsGrid.isotope({ filter: selector});
			setTimeout(function(){$productsGrid.animate({opacity: 1});$('.category-loader').hide();}, 500);
		});
		e.preventDefault();
	});

	/*
	 * Sort by featured
	 */

	$('#sort-by-featured').click(function(e){
		if($(this).hasClass('hover')) {
			$productsGrid.isotope({sortBy: 'original-order', sortAscending: true});
			$(this).removeClass('hover');
		} else {
			$productsGrid.isotope({sortBy: 'featured', sortAscending: false});
			$(this).addClass('hover');
		}
		e.preventDefault();
	});

	/*
	 * Filter accordion
	 */
	$('.filters-top').click(function(e){
		var li = $(this).parents('li');
		li.toggleClass('collapsed').toggleClass('expanded');
	if(li.hasClass('collapsed')){
		li.find('.filters-bottom').slideUp(sidebarResize);
	} else {
		li.find('.filters-bottom').slideDown(sidebarResize);
	}
	});
				 
  /*
   * Back to top
   */

  $('#back-to-top').click(function(e){
    $('html, body').animate({
      scrollTop: 0
    }, 1000);
    e.preventDefault()
  });


/*
 * Product boxes
 */
	
	
	
	/*
	 * Add row
	 */

  $(document).click(function(e){
    if($('.info-miss').has(e.target).length == 0)
      $('.info-miss').hide();
  });


  $('.add-button.btn-disabled').live('click', function(e){
    $(this).next().show();
    e.stopPropagation();
  });

	$('.add-button:not(.btn-disabled)').live('click', function(e){

    // Prevent add if qty exceed max
    var qty = $(this).parents('.product-bottom').find('.input-qty');
    if(qty.val() > 500) {qty.siblings('.qty-exceeded').show();  return false;}
    else qty.siblings('.qty-exceeded').hide();


		if($(this).parents('.product-bottom').find('.default.hidden').length == 3) {
			var og_product_bottom = $(this).parents('.product-bottom');
			var product_bottom =	$(this).parents('.product-bottom').clone();
			
			/** Check to see if we should be updating an existing or add a new row */
			var dupeEl;
			//What we are trying to add
			var currVals = orderValuesFromMarkup(product_bottom.find('.pb-region'));
			
			//Compare to Previously added
			var prevRows = $(this).parents('.product').find('.product-bottom.added');
			prevRows.each(function(i,v){
				var vals = orderValuesFromMarkup(v);
				
				if (vals.swatch == currVals.swatch && vals.size == currVals.size){
					console.log({curr:currVals, dupe: vals, dupeEl: this}); 
					dupeEl = $(this);
					return dupeEl;
				}
			});
				
			if(dupeEl){
				/** Update quantity */
				var qtyEl = $(dupeEl).find('.qty').siblings('input');
				qtyEl.val(Number(qtyEl.val()) + Number(currVals.qty));
				
				// Indicate action outcome to user
				dupeEl.find('.remove-button').hide().next().show()
			} else { 
				/** Add the new row */
				product_bottom.addClass('added expanded');
				product_bottom.find('.pb-region:last').append('<div class="remove-button"></div><div class="save-button hidden">SAVE</div>');
				product_bottom.find('.pb-region.second .pill, .pb-region.third .pill').addClass('pill-trans');
				product_bottom.find('.pb-region:last .pill').remove();
				product_bottom.hide();
				$(this).parents('.product-bottom').after(product_bottom);
				product_bottom.slideDown('fast','swing',function(e){$productsGrid.isotope({});});
			}
			
			/** reset row **/
			og_product_bottom.find('.default.hidden').removeClass('hidden');
			og_product_bottom.find('.pb-region.first .pill img').hide();
			og_product_bottom.find('.pb-region.third .pill-input').show();
			og_product_bottom.find('.pb-region.third .input-qty').show();
			og_product_bottom.find('.add-button').addClass('btn-disabled');
			
			/** Cart Update **/
			cartUpdate('added');
		}
	});
			
	$('.pill-trans').live('mouseenter',function(){
		if(!$(this).hasClass('modified')) {
			$(this).removeClass('pill-trans');
		}
		$(this).live('click',function(e){
			$(this).removeClass('pill-trans');
			$(this).die('mouseenter');
			$(this).die('mouseleave');
			e.preventDefault();
		});
	});
	$('.added .pb-region.second .pill, .added .pb-region.third .pill').live('mouseleave',function(){
		if(!$(this).hasClass('modified')) {
			$(this).addClass('pill-trans');
		}
	});
	
	var orderValuesFromMarkup = function(els){
		var vals = {};
		
		$(els).each(function(i,v){
			var col = $(v);
			//filter out what we don't care about
			if(col.find('.add-button').length){ return;}
			
			//Markup could use hints about semantics and improved consistency
			
			// Handle color
			if (col.find('.swatch').length){
					vals.swatch = col.find('.pill > img').attr('src');
			}
			
			// Handle size
			if (col.find('.size').length){
					vals.size = col.find('.pill > .size-value').text();
			}
			
			// Hande qty
			if (col.find('.qty').length){
					vals.qty = col.find('input').val();
			}
		});
		
		return vals;
	};
	
	/*
	 * Darker border
	 */
	
	$('.pb-region').live('mouseenter', function(){
		$(this).find('.add-button').addClass('hover').parents('.product-bottom').prev().addClass('hover');
	});
	$('.pb-region').live('mouseleave', function(){
		$(this).find('.add-button').removeClass('hover').parents('.product-bottom').prev().removeClass('hover');
	});
	
	/*$('.add-button').live('mouseenter', function(){
		  $(this).addClass('hover').parents('.product-bottom').prev().addClass('hover');
	});
	$('.add-button').live('mouseleave', function(){
		$(this).removeClass('hover').parents('.product-bottom').prev().removeClass('hover');
	});*/

	/*
	 * Highlight add button
	 */
		
	$('.product-body').live('mouseenter', function(){
		$(this).next().find('.add-button').addClass('hover');
	});
	$('.product-body').live('mouseleave', function(){
		$(this).next().find('.add-button').removeClass('hover');
	});


  /*
   * Clear input-qty on click
   */

  $('.input-qty').live('click', function(e){
	$(this).data('val',$(this).val());
    $(this).val('');
	if($(this).parents('.added').size()) {
    	$(this).addClass('modified');
	}
    e.preventDefault();
  });

  /*
   * qty exceed
   */

  $('.input-qty').live('change', function(){
    if(!$(this).val().length || $(this).val() <= 500) $(this).siblings('.qty-exceeded').hide();
  });

  $('.input-qty').live('keyup', function(){
    if(!$(this).val().length || $(this).val() <= 500) $(this).siblings('.qty-exceeded').hide();
  });
  
  $('.added .input-qty').live('blur', function(){
    if(!$(this).val().length || $(this).val() <= 500){ $(this).siblings('.qty-exceeded').hide(); }
  });
  
  $('.added .input-qty').live('keyup', function(){
	if($(this).data('val') != '' && (isNaN($(this).val()) || $(this).val() < 1)) {
		$(this).val($(this).data('val'));
	}
  });
  
  $('.added .input-qty').live('blur', function(){
	if($(this).data('val') != '') {
		if(isNaN($(this).val()) || $(this).val() < 1){
			if($(this).val() == $(this).data('val')) {
				$(this).removeClass('modified').addClass('pill-trans');
			}
			$(this).val($(this).data('val'));
			//$(this).removeClass('modified').addClass('pill-trans');
		}
	}
  });

	/*
	 * Remove row
	 */

	$('.remove-button').live('click', function(e){
    var qty = $(this).parents('.product-bottom').find('.input-qty');
    if(qty.val() > 500) { qty.siblings('.qty-exceeded').show();return false;}
		$(this).parents('.product-bottom').remove();
		$productsGrid.isotope({});
		cartUpdate('removed');
		e.preventDefault();
	});

	$('.save-button').live('click', function(){
		var button = this;
		var qty = $(this).parents('.product-bottom').find('.input-qty');
		if(qty.val() > 500) {qty.siblings('.qty-exceeded').show();  return false;}
		$(this).parents('.product-bottom').animate({opacity: 0.3}, function(){
			var self = this;
			$(this).append('<div class="loader"></div>');
			$(this).find('.pb-region.first .pill, .pb-region.second .pill, .pb-region.third .pill').removeClass('modified');
			$(this).find('.pb-region.second .pill, .pb-region.third .pill').addClass('pill-trans');
			setTimeout(function(){
				$(self).children('.loader').remove();
				$(button).hide().prev().show();
				$(self).animate({opacity: 1});
		
			}, 750);
		});
		cartUpdate('update');
	});

	/*
	 * Select a color swatch
	 */

	$('.pb-region.first').live('mouseenter', function(){
		$(this).children('.swatch').toggle();
		$(this).addClass('hover');
	}).live('mouseleave',function(){
		$(this).children('.swatch').hide();
		$(this).removeClass('hover');
	});
	
	$('.swatch').find('a').live('click', function(e){
		var swatch = $(this).parents('.swatch');
		swatch.parents('.pb-region').find('.pill img').attr('src',$(this).data('chip'));
		swatch.parents('.pb-region').find('.pill .default').addClass('hidden');
		swatch.parents('.pb-region').find('.pill img').show();
		swatch.parents('.product-bottom').find('.remove-button').hide().next().show();
		if($(this).parents('.added').size()){
			$(this).parents('.added').find('.first .pill').addClass('modified');
		}
		updateAddButton($(this).parents('.product-bottom'));
		e.preventDefault();
	});

	/*
	 * Select a size
	 */

	$('.pb-region.second').live('mouseenter', function(){
		$(this).children('.size').toggle();
		$(this).addClass('hover');
	}).live('mouseleave',function(){
		$(this).children('.size').hide();
		$(this).removeClass('hover');
	});
	
	$('.size').find('li').live('click', function(){
		$(this).parents('.pb-region').find('.default').addClass('hidden');
		$(this).parents('.pb-region').find('.size-value').text($(this).text());
		$(this).parents('.product-bottom').find('.remove-button').hide().next().show();
		if($(this).parents('.added').size()){
			$(this).parents('.added').find('.second .pill-trans').removeClass('pill-trans').addClass('modified');
		}
		updateAddButton($(this).parents('.product-bottom'));
	});

	/*
	 * Select a quantity
	 */
	
	$('.pb-region.third input').numeric();
	
	$('.pb-region.third').live('mouseenter', function(e){
		//$(this).children('.pill-input').remove();
		//$(this).children('.input-qty').show();
		$(this).children('.qty').show();
		$(this).addClass('hover');
	}).live('mouseleave',function(){
		$(this).children('.qty').hide();
		$(this).removeClass('hover');
	});

	$('.pb-region.third .pill-input').live('click',function(e){
		$(this).hide();
		$(this).find('.default').addClass('hidden');
		$(this).next().val('').addClass('modified').show().focus();
	});

	$('.pb-region.third input').live('keyup',function(e){
		$(this).next('.qty').hide();
		$(this).parents('.product-bottom').find('.remove-button').hide().next().show();
		updateAddButton($(this).parents('.product-bottom'));
	});

	$('.added .pb-region.third input').live('keyup',function(e){
		$(this).addClass('pill-trans');
		updateAddButton($(this).parents('.product-bottom'));
	});
	
	$('.added .pb-region.third input').live('click',function(e){
		$(this).parents('.added').find('.remove-button').hide().next().show();
	});

	$('.qty').find('li').live('click', function(){
		$(this).parents('.pb-region').find('.default').addClass('hidden');
		$(this).parents('.pb-region').find('.pill-input').hide();
		$(this).parents('.pb-region').children('input').val($(this).text());
		$(this).parents('.pb-region').children('input').show();
		$(this).parents('.product-bottom').find('.remove-button').hide().next().show();
		$(this).parents('.added').find('.third .pill-trans').removeClass('pill-trans').addClass('modified');
		updateAddButton($(this).parents('.product-bottom'));
	});

	function updateAddButton(row) {
		if(row.find('.default.hidden').length==3) {
			row.find('.add-button').removeClass('btn-disabled');
		}
	}
	
	/*
	 * Product silder
	 */
	$('.product-body').live('mouseenter', function(){
		var self = this;
		$(this).find('.product-carousel').cycle({
			fx: 'scrollLeft', 
			prev:	 '.carousel-control.left', 
			next:	 '.carousel-control.right',
			speed: 750,
			timeout: 500,
      before: function(curr, next, opts){
        var img = $(next).find('img');
        if(typeof img.attr('data-original') != "undefined") {
          img.attr('src', img.attr('data-original')).removeAttr('data-original');
        }

				$(self).parents('.product').find('.carousel-bullet').eq($(this).index()).addClass('active').siblings().removeClass('active');
      }
		});
	});
	
	$('.product-body').live('mouseleave', function(){
		try {
		$(this).find('.product-carousel').cycle('pause');
		} catch(e) {};
	});
	
	$('.pill:not(input):not(.pill-input)').prepend('<span class="grad"></span>');
	
	$('.product-bottom:not(.added) .input-qty').live('blur',function(e){
		log('blur not added');
		if(isNaN($(this).val()) || $(this).val() < 1){
			//$(this).val('Qty');
			$(this).val('1').hide();
			$(this).siblings('.pill-input').show().find('.hidden').removeClass('hidden');
		}
    	$(this).removeClass('modified');
	});

  /*
   * turn plus button to pink on hover
   */	

  $('.product-bottom .pb-region').hover(function(){
    $(this).siblings().find('.add-button').removeClass('btn-disabled');
  }, function(){
    if($(this).siblings().find('.default:not(.hidden)').length)
      $(this).siblings().find('.add-button').addClass('btn-disabled');
  });


	/*
	 * Cart Update
	 */
	function cartUpdate(type) {
		var newCount = 0;
		var newEstPrice = 0;
		var newMinDue = 0;
		
		$('.added').each(function(){
			newCount += parseInt($(this).find('.input-qty').val());
			newEstPrice += parseInt($(this).find('.input-qty').val()) * parseFloat($(this).parents('.product').find('.product-price').text());
		});
		newEstPrice = Math.round(newEstPrice*100)/100;
		newMinDue = Math.round(parseFloat(newEstPrice*.3)*100)/100;
		
		var diffType = 'added';
		var diffCount = 0;
		
		/* if CART is NOT empty */
		if(CART.items > 0) {
			if(CART.items < newCount) {
				/* Added Items */
				diffType = 'added';
			} else {
				/* Removed Items */
				diffType = 'removed';
			}
			diffCount = CART.items - newCount;
		/* if CART IS empty */
		} else {
			diffCount = newCount;
		}
		
		cartUpdateData({
			diffType: diffType,
			diffCount: Math.abs(diffCount),
			oldItems: CART.items,
			oldEstPrice: CART.estPrice,
			oldMinDue: CART.minDue,
			newCount: newCount,
			newEstPrice: newEstPrice,
			newMinDue: newMinDue
		});
	}
	
	function cartUpdateData(options) {
		CART.diffType = options.diffType;
		CART.items = options.newCount;
		CART.estPrice = options.newEstPrice;
		CART.minDue = options.newMinDue;
		
		if(CART.items > 0) {
			if(CART.estPriceElCont.hasClass('hidden')) {
				CART.estPriceElCont.removeClass('hidden').animate({
					width: 121
				},function(){$(this).css('min-width',$(this).width()).width('auto');});
				CART.minDueElCont.removeClass('hidden').animate({
					width: 115
				},function(){$(this).css('min-width',$(this).width()).width('auto');});
			}
		} else {
			CART.estPriceElCont.animate({
				width: 0
			},function(){$(this).addClass('hidden').css('min-width',0)})
			CART.minDueElCont.animate({
				width: 0
			},function(){$(this).addClass('hidden').css('min-width',0)});
		}
		
		CART.itemsEl.fadeOut('fast',function(){
			CART.itemsEl.text(options.newCount);
			CART.itemsEl.fadeIn('fast');
		});
		
		CART.estPriceEl.fadeOut('fast',function(){
			CART.estPriceEl.text(options.newEstPrice);
			CART.estPriceEl.fadeIn('fast');
		});
		
		CART.minDueEl.fadeOut('fast',function(){
			CART.minDueEl.text(options.newMinDue);
			CART.minDueEl.fadeIn('fast');
		});
		
		cartGrowl({
			type: options.diffType,
			count: options.diffCount
		});
	}
	
	function cartGrowl(options) {
		var type = options.type;
		var count = options.count;
		var s = count > 1 ? 's' : '';

		/* remove any existing growls */
		CART.container.find('#cart-added').fadeOut('fast',function(){$(this).remove();});
		
		/* create new growl */
		var growl = CART.growl.clone();

		growl.html('<em><span>' + count + '</span> unit' + s + ' ' + type + '.</em><img src="fpo/p_cart-item-added.jpg" /> <i class="icon-cart-added-caret"></i>');
		
		CART.container.append(growl);
		
		if(!CART.link.find('.icon-loader-pink').size()){
			CART.link.append(CART.loader.clone().removeClass('hidden').hide().fadeIn('fast'));
		}
		
		growl.fadeIn('normal',function(){
			setTimeout(function(){
				growl.fadeOut('normal',function(){growl.remove();});
				CART.link.find('.icon-loader-pink').fadeOut('normal',function(){$(this).remove();});
			}, 3000);
		});
	}
});
