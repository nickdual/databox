/* Author:

*/

$(function(){
  
  var CART = {
    container: $('#cart'),
    link: $('#cart a'),
    loader: $('#cart a .icon-loader-pink').clone(),
    growl: $('#cart #cart-growl').clone(),
    diffType: 'added',
    items: 0,
    itemsEl: $('#items h5'),
    estPrice: 0,
    estPriceElCont: $('#estimated-price'),
    estPriceEl: $('#estimated-price h5'),
    minDue: 0,
    minDueElCont: $('#minimum-due'),
    minDueEl: $('#minimum-due h5')
  };
  $('#cart a .icon-loader-pink').remove();
  $('#cart #cart-growl').remove();
 

  
  $('.pill').each(function(){
	try{
		$(this).prepend('<span class="grad"></span>');
	} catch(e) { log(e); }
  });
  
  $('.filter').live('mouseenter',function(){
    $(this).find('.dropdown').toggle();
  }).live('mouseleave',function(){
    $(this).find('.dropdown').hide();
  });

  /*
   * Prevent <= 0 qty
   */
   $('.input-qty').live('blur',function(e){
    if ($(this).val() != '' && $(this).val() < 1) {
      $(this).val('');
    }
  });

  /*
   * Clear input-qty on click
   */

  $('.input-qty').live('click', function(){
    $(this).val('');
    $(this).addClass('border-pink');
  });

  $('.input-qty').live('blur', function(){
    $(this).removeClass('border-pink');
  });

  /*
   * qty exceed
   */

  $('.input-qty').live('change', function(){
    if(!$(this).val() || $(this).val() <= 500) $(this).siblings('.qty-exceeded').hide();
  });

  $('.input-qty').live('keyup', function(){
    if(!$(this).val() || $(this).val() <= 500) $(this).siblings('.qty-exceeded').hide();
  });


  $('.cell-color, .viewby-filter, .color-filter, .cell-size, .cell-qty, .cell-specs, .cell-item').live('mouseenter',function(){
    $(this).find('.dropdown:not(.qty-exceeded)').toggle();
    $(this).addClass('hover');
  }).live('mouseleave',function(){
    $(this).find('.dropdown:not(.qty-exceeded)').hide();
    $(this).removeClass('hover');
  });
  
  
  $('.more').live('mouseenter',function(){
    $(this).find('.dropdown').toggle();
  }).live('mouseleave',function(){
    $(this).find('.dropdown').hide();
  });
  
  $('.swatch a').live('click', function(e){
    var swatch = $(this).parents('.swatch');
    swatch.parents('.cell-color').addClass('scd').find('.pill img').attr('src',$(this).data('chip'));
    e.preventDefault();
  });
  
  $('.size').find('li').live('click', function(){
    var size = $(this);
    size.parents('.cell-size').find('.size-value').text(size.text());
    //rowUpdated($(this).parents('tr'));
  });
  
  $('.cell-qty input').numeric();
  $('.cell-qty input').live('keyup',function(e){
    var qty = $(this);
    qty.next('.qty').hide();
    //rowUpdated($(this).parents('tr'));
  });
  
  $('.qty').find('li').live('click', function(){
    var qty = $(this);
    qty.parents('.cell-qty').find('input').val(qty.text());
    //rowUpdated($(this).parents('tr'));
  });
  
  $('.add-button:not(.btn-disabled)').live('click',function(e){
    e.preventDefault();
    // Prevent add if qty exceed max
    var qty = $(this).parents('tr').find('.input-qty');
    if(qty.val() > 500) {qty.siblings('.qty-exceeded').show();  return false;}
    else qty.siblings('.qty-exceeded').hide();

    if(!$(this).hasClass('btn-disabled')){
      var img = $(this).parents('tr').find('.cell-item img').attr('src');
      var color = $(this).parents('tr').find('.cell-color .pill img').attr('src');
      var qty = $(this).parents('tr').find('.input-qty').val();
      var price = $(this).parents('tr').find('.cell-price span').text().replace('$','');
      
      $('.items-added').append('<li class="added hidden"><input type="hidden" class="input-price" value="'+price+'" /><span class="thumb"><img src="'+img+'"></span><span class="chip pill"><img src="'+color+'"></span><span class="qty">('+qty+')<input type="hidden" class="input-qty" value="'+qty+'" /></span><a href="#" class="remove-button"><i class="icon-remove"></i></a></li>');
      
      $('.items-added li.hidden').slideDown().removeClass('hidden');
      
      cartUpdate('added');
      
      resetRow($(this).parents('tr'));
    }
  });

  $(document).click(function(e){
    if($('.info-miss').has(e.target).length == 0)
      $('.info-miss').hide();
  });

  $('.add-button.btn-disabled').live('click', function(e){
    $('.info-miss').hide();
    $(this).next().show();
    e.stopPropagation();
    e.preventDefault();
  });
  
  function resetRow(row){
    row.find('.cell-color').removeClass('scd');
    row.find('.cell-color .pill img').attr('src',row.find('.cell-color .swatch ul li:eq(0) a').data('chip'));
    row.find('.cell-size .size-value').text('');
    row.find('.cell-qty .input-qty').val('');
    row.find('.cell-remove .add-button').addClass('btn-disabled');
  }
  
  $('.remove-button').live('click',function(e){
    e.preventDefault();
    
    $(this).parents('li').slideUp('normal',function(){
      $(this).remove();
      cartUpdate('remove');
    });
  });
  
  
  /*
   * Cart Update
   */
  function cartUpdate(type) {
    var newCount = 0;
    var newEstPrice = 0;
    var newMinDue = 0;
    
    $('.added').each(function(){
      newCount += parseInt($(this).find('.input-qty').val());
      log(parseInt($(this).find('.input-qty').val()));
      newEstPrice += parseInt($(this).find('.input-qty').val()) * parseFloat($(this).find('.input-price').val());
    });
    newEstPrice = Math.round(newEstPrice*100)/100;
    newMinDue = Math.round(parseFloat(newEstPrice*.3)*100)/100;
    
    var diffType = 'added';
    var diffCount = 0;
    
    /* if CART is NOT empty */
    if(CART.items > 0) {
      if(CART.items < newCount) {
        /* Added Items */
        diffType = 'added';
      } else {
        /* Removed Items */
        diffType = 'removed';
      }
      diffCount = CART.items - newCount;
    /* if CART IS empty */
    } else {
      diffCount = newCount;
    }
    
    cartUpdateData({
      diffType: diffType,
      diffCount: Math.abs(diffCount),
      oldItems: CART.items,
      oldEstPrice: CART.estPrice,
      oldMinDue: CART.minDue,
      newCount: newCount,
      newEstPrice: newEstPrice,
      newMinDue: newMinDue
    });
  }
  
  function cartUpdateData(options) {
    CART.diffType = options.diffType;
    CART.items = options.newCount;
    CART.estPrice = options.newEstPrice;
    CART.minDue = options.newMinDue;
    
    if(CART.items > 0) {
      if(CART.estPriceElCont.hasClass('hidden')) {
        CART.estPriceElCont.removeClass('hidden').animate({
          width: 121
        },function(){$(this).css('min-width',$(this).width()).width('auto');});
        CART.minDueElCont.removeClass('hidden').animate({
          width: 115
        },function(){$(this).css('min-width',$(this).width()).width('auto');});
      }
    } else {
      CART.estPriceElCont.animate({
        width: 0
      },function(){$(this).addClass('hidden').css('min-width',0)})
      CART.minDueElCont.animate({
        width: 0
      },function(){$(this).addClass('hidden').css('min-width',0)});
    }
    
    CART.itemsEl.fadeOut('fast',function(){
      CART.itemsEl.text(options.newCount);
      CART.itemsEl.fadeIn('fast');
    });
    
    CART.estPriceEl.fadeOut('fast',function(){
      CART.estPriceEl.text(options.newEstPrice);
      CART.estPriceEl.fadeIn('fast');
    });
    
    CART.minDueEl.fadeOut('fast',function(){
      CART.minDueEl.text(options.newMinDue);
      CART.minDueEl.fadeIn('fast');
    });
    
    cartGrowl({
      type: options.diffType,
      count: options.diffCount
    });
  }
  
  function cartGrowl(options) {
    var type = options.type;
    var count = options.count;
    var s = count > 1 ? 's' : '';

    /* remove any existing growls */
    CART.container.find('#cart-added').fadeOut('fast',function(){$(this).remove();});
    
    /* create new growl */
    var growl = CART.growl.clone();
    
    growl.html('<em><span>' + count + '</span> unit' + s + ' ' + type + '.</em><img src="fpo/p_cart-item-added.jpg" /> <i class="icon-cart-added-caret"></i>');
    
    CART.container.append(growl);
    
    if(!CART.link.find('.icon-loader-pink').size()){
      CART.link.append(CART.loader.clone().removeClass('hidden').hide().fadeIn('fast'));
    }
    
    growl.fadeIn('normal',function(){
      setTimeout(function(){
        growl.fadeOut('normal',function(){growl.remove();});
        CART.link.find('.icon-loader-pink').fadeOut('normal',function(){$(this).remove();});
      }, 3000);
    });
  }
  
  /*
   * Product silder
   */
  $('.item-carousel').cycle({
    fx: 'scrollLeft', 
    prev:  '.icon-arrow-carousel-left', 
    next:  '.icon-arrow-carousel-right',
    speed: 750,
    timeout: 0,
    before: function(){
    }
  });

  /*
   * Back to top
   */

  $('#back-to-top').click(function(e){
    $('html, body').animate({
      scrollTop: 0
    }, 1000);
    e.preventDefault()
  });


  /*
   * turn plus button to pink on hover
   */ 

  $('.pill:not(.add-button), .dropdown.qty, .dropdown.size, .dropdown.swatch, .thumb').hover(function(){
    $(this).parents('tr').find('.add-button').removeClass('btn-disabled');
  }, function(){
    if(!$(this).parents('tr').find('.size-value').text().length || !$(this).parents('tr').find('.input-qty').val() || !$(this).parents('tr').find('.cell-color').hasClass('scd'))
      $(this).parents('tr').find('.add-button').addClass('btn-disabled');
  });


  /*
   * Category filter
   */

  $('.categories li').click(function(e){
    var self = this;
    $('#order-builder-table').fadeOut('slow', function(){
      $(this).addClass('active').siblings('.active').removeClass('active');
      $(this).parent().siblings('a').text($(self).children('a').text());
    });
  });

 /*
   * Reset color and view by
   */

  $('.dropdown-clear').click(function(){
    $(this).parents('.pb-region').find('.pill img').addClass('hidden');
    $('.order-builder-table tbody').find('tr').fadeIn('slow');
  });

  /*
   * Dropdown swatch filter
   */

 
  $('.color-filter').find('li').live('click', function(e){
    $(this).parents('.pb-region').find('.pill img').attr('src', $(this).find('a img').attr('src')).removeClass('hidden');
  });

  $('.color-filter .dropdown.swatch, .dropdown.viewby, .categories').find('li').click(function(e){
    var self = this;
    $('.category-loader').show();
    $('.order-builder-table').animate({opacity: 0}, function(){
      setTimeout(function(){ $('.order-builder-table').animate({opacity: 1}); $('.category-loader').hide(); }, 800);
    });
    e.preventDefault();
  });

  /*
   * Sticky table header
   */

  var $tableHeader = $('.order-builder-table thead')
    , $productHeader = $('#product-header-filter-container, #products-header');

  $productHeader.waypoint(function(e, direction){
    if(direction === "down") $tableHeader.addClass('sticky');
    else $tableHeader.removeClass('sticky');
  }, {'offset': 45});

  if($('#sidebar').length > 0) {
    
    var jqWindow = $(window);
    var productContext = $('#main-content');
    var sidebar = productContext.find('#sidebar .floaty');
    
    jqWindow.data('height', jqWindow.height());
    sidebar.data('positionedWRT', 'parent');
    // Store some info about the page and the sidebar to make scrolling the sidebar with the page easier
    productContext.data('areaHeight', productContext.outerHeight());
    productContext.data('origOffsetY', productContext.offset().top);
    sidebar.data('origOffsetY', sidebar.offset().top);
    sidebar.data('height', sidebar.outerHeight());
    
    // Handle scrolling for the add-to-cart section, which follows the window
    var sidebarTopSpacing = 0;
    var sidebarScrollHandler = function() {
      var amtScrolledY = jqWindow.scrollTop();
      var encroachmentOnSidebar = amtScrolledY - (sidebar.data('origOffsetY') - sidebarTopSpacing);
      
      if (encroachmentOnSidebar > 0) {
        
        // Unless setNewTop ends up true, there's no reason to update the sidebar's CSS
        var setNewTop = (sidebar.data('positionedWRT') === 'parent') ? true : false;
        
        sidebar.data('positionedWRT', 'viewport');
        
        var sidebarNewTop = sidebarTopSpacing + 10;
      
        // Prevents the sidebar from running off the bottom of the page
        var unscrolledContentHeight = (productContext.data('areaHeight') + productContext.data('origOffsetY')) - amtScrolledY;
        if (unscrolledContentHeight < sidebar.data('height') + sidebarTopSpacing) {
          sidebarNewTop = unscrolledContentHeight - sidebar.data('height') + (sidebarTopSpacing * 0.5);
          setNewTop = true;
        }
        
        if (setNewTop) {
          sidebar.css({
            position:   'fixed',
            top:        sidebarNewTop,
            right:      getSidebarPosition(productContext)
          });
        }
      }
      else if (sidebar.data('positionedWRT') === 'viewport') {
        // Check first that sidebar.data('positionedWRT') is 'viewport' because if it's
        // already 'parent', then there's no reason that the sidebar's CSS would need to change.
        var newTop = 0;
        if(productContext.find('#sidebar .sub-head').length > 0){
          newTop = 50;
        }
        sidebar.data('positionedWRT', 'parent');
        sidebar.css({
          position:   'absolute',
          top:        newTop,
          right:      0
        });
      }
    };
    jqWindow.scroll(sidebarScrollHandler);
    
    // Returns the "right" setting that the sidebar should use. pageWrapper should be a jQuery object.
    function getSidebarPosition(pageWrapper) {
      return document.documentElement.clientWidth - pageWrapper.offset().left - pageWrapper.outerWidth(true);
    };
  }
  
/*
 * Item Details Overlay
 */
	var DETAILS = {
		container: $('#item-overlay'),
		username: $('#item-overlay').data('username'),
		closeBtn: $('.icon-overlay-close'),
		ratingsEl: $('.item-reviews-count'),
		ratingBtn: $('.item-reviews-count li'),
		isRated: ($('.item-reviews-count li.rated').size() > 0),
		ratedMsg: $('.item-image-message'),
		moreCommentsBtn: $('.link-more-comments'),
		closeMoreCommentsBtn: $('.link-all-comments-close'),
		allCommentsEl: $('.item-all-comments'),
		allCommentsReviewsEl: $('.item-all-comments .item-reviews-container'),
		allCommentsImageName: $('.item-all-comments .item-image-name'),
		allCommentsAdd: $('.item-all-comments .item-comment-add'),
		addCommentEl: $('.item-comment-add'),
		addCommentTextarea: $('.item-comment-add textarea'),
		addCommentBtn: $('.item-comment-add .btn-post'),
		addCommentCharCount: $('.item-comment-add .item-comment-count'),
		commentTemplate: Handlebars.compile($("#template-comment").html()),
		commentsEl: $('.item-comments'),
		reviewsEl: $('.item-reviews'),
		ratingTemplate: Handlebars.compile($("#template-comment-rating").html())
	};
	
	$('.open-item-details').fancybox({
		closeBtn: false,
		fitToView: false,
		scrolling: 'no',
		afterShow: function(){
			//$('.item-details .item-reviews-container').css('min-height',$('.item-details .item-reviews-container').height());
		}
	});
	
	DETAILS.closeBtn.click(function(e){
		e.preventDefault();
		$.fancybox.close();
	});
	
	// review click
	DETAILS.ratingBtn.live('click',function(e){
		e.preventDefault();
		if(!DETAILS.isRated){
			var oldCount = $(this).find('.item-review-count').text();
			var newCount = ++oldCount;
			$(this).find('.item-review-count').text(newCount);
			$(this).addClass('rated');
			DETAILS.ratedMsg.fadeIn('slow',function(){
				setTimeout(function(){DETAILS.ratedMsg.fadeOut();},2000);
			});
			$(this).parent().addClass('disabled');
			DETAILS.isRated = true;
			//logic needed to update current user comments with rating
		}
	});
	
	// more comments slide up
	DETAILS.moreCommentsBtn.live('click',function(e){
		e.preventDefault();
		
		// init for scrollbar height
		DETAILS.allCommentsEl
			.css('position','absolute')
			.css('visibility','hidden')
			.removeClass('hidden')
			.height(DETAILS.container.height());
		
		// set scrollbar
		var heightOffset = 120;
		if(DETAILS.container.height()<DETAILS.allCommentsReviewsEl.height()+DETAILS.allCommentsImageName.height()+DETAILS.allCommentsAdd.height()+heightOffset) {
			DETAILS.allCommentsReviewsEl.height(DETAILS.container.height()-DETAILS.allCommentsImageName.height()-DETAILS.allCommentsAdd.height()-heightOffset);
			DETAILS.allCommentsReviewsEl.mCustomScrollbar();
		}
		
		// hide again
		DETAILS.allCommentsEl
			.css('visibility','visible')
			.height(0);
		
		// slide up
		$('.fancybox-overlay').animate({scrollTop:0}, 'normal');
		DETAILS.allCommentsEl
			.animate({
				height: DETAILS.container.height()
			});
	});
	
	// more comments slide down
	DETAILS.closeMoreCommentsBtn.live('click',function(e){
		e.preventDefault();
		DETAILS.allCommentsEl
			.animate({
				height:0
			},500,function(){
				DETAILS.allCommentsReviewsEl.height(0);
				DETAILS.allCommentsReviewsEl.mCustomScrollbar("destroy");
				DETAILS.allCommentsEl.addClass('hidden');
			});
	});
	
	// comments textarea logic
	DETAILS.addCommentTextarea.live('focus',function(e){
		DETAILS.addCommentBtn.slideDown('fast');
	}).live('keyup',function(e){
		DETAILS.addCommentTextarea.val($(this).val());
		DETAILS.addCommentCharCount.text(parseInt(DETAILS.addCommentTextarea.attr('maxlength'))-DETAILS.addCommentTextarea.val().length);
		if(DETAILS.addCommentTextarea.val().length > 0) {
			DETAILS.addCommentBtn.removeClass('btn-disabled');
		} else {
			DETAILS.addCommentBtn.addClass('btn-disabled');
		}
	}).live('blur',function(e){
		if(DETAILS.addCommentTextarea.val().length == 0) {
			DETAILS.addCommentBtn.addClass('hidden');
		}
	});
	
	// post comment button click
	DETAILS.addCommentBtn.live('click',function(e){
		if(!$(this).hasClass('btn-disabled')) {
			postComment($(this).parents('.item-comments').find('.item-reviews'));
		}
	});
	
	function postComment(reviews){
		var html = DETAILS.commentTemplate({username: DETAILS.username, comment: DETAILS.addCommentTextarea.val()});
		html = $(html).prepend(getRating()).hide();
		reviews.prepend(html);
		$(html).slideDown('normal',function(){
			// need logic to add comment to other screen
			if(reviews.parents('.item-details').size()){
				// update comments screen
				//$('.item-all-comments .item-comments .item-reviews').prepend(html.clone());
			} else {
				// update details screen
				//$('.item-details .item-comments .item-reviews').prepend(html.clone());
			}
		});
		var heightOffset = 120;
		if(DETAILS.container.height()<DETAILS.allCommentsReviewsEl.height()+DETAILS.allCommentsImageName.height()+DETAILS.allCommentsAdd.height()+heightOffset+50) {
			DETAILS.allCommentsReviewsEl.height(DETAILS.container.height()-DETAILS.allCommentsImageName.height()-DETAILS.allCommentsAdd.height()-heightOffset);
			if(DETAILS.allCommentsReviewsEl.hasClass('mCustomScrollbar')) {
				DETAILS.allCommentsReviewsEl.mCustomScrollbar('update');
			} else {
				DETAILS.allCommentsReviewsEl.mCustomScrollbar();
			}
		}
		
		DETAILS.addCommentTextarea.val('');
		DETAILS.addCommentBtn.addClass('btn-disabled').addClass('hidden');
		DETAILS.addCommentCharCount.text(DETAILS.addCommentTextarea.attr('maxlength'));
		
		// limit comments to 5 in Item Details view
		$('.item-details .item-reviews .item-review:gt(4)').slideUp('normal',function(e){
			$(this).remove();
		});
		
	}
	
	function getRating(){
		if(DETAILS.isRated){
			return DETAILS.ratingTemplate({rating: DETAILS.ratingsEl.find('.rated i').data('rating')});
		} else {
			return;
		}
	}
});
