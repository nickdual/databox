# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$(document).ready ->

  $('#new_promotion_code').validate(
    errorLabelContainer: $("div#content-messages")
    errorClass: "invalid"
    errorElement: "div"
    rules:
      'promotion_code[use_limit_per_code]':
        number: true
      'promotion_code[use_limit_per_customer]':
        number: true
    messages:
      "promotion_code[use_limit_per_code]":
        number: I18n.t("promotion_code.use_limit_per_code.number")
      "promotion_code[use_limit_per_customer]":
        number: I18n.t("promotion_code.use_limit_per_customer.number")

    )

  $('#form-add-party-id').validate(
    errorLabelContainer: $("div#content-messages.party-id")
    errorClass: "invalid"
    errorElement: "div"
    rules:
      "promotion_code_party[party_id]":
        number: true
    messages:
      "promotion_code[use_limit_per_code]":
        number: I18n.t("promotion_code_party.error")
  )
  $('#form-add-email').validate(
    errorLabelContainer: $("div#content-messages.add-email")
    errorClass: "invalid"
    errorElement: "div"
    rules:
      "promotion_code_email[email]":
        email: true
    messages:
      "promotion_code_email[email]":
        email: I18n.t("promotion_code_email.email.email_error")
  )

  $('#add_promotion_code_email').live 'click', (e) ->
    $("#loading_add_email").show()
    $.ajax(
      type: "POST"
      url: "/promotion_codes/add_email"
      beforeSend: (xhr) ->
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      data:
        "promotion_code_email[product_promotion_code_id]": $("#promotion_code_email_product_promotion_code_id").val()
        "promotion_code_email[email]": $("#promotion_code_email_email").val()

      dataType: "json"
      success: (data) ->

        el = '<div>'
        el += '<a class="btn btn4 btn_trash" data-id="'
        el +=  data.id
        el += '" id="delete_email" style="background-color: rgb(247, 247, 247);"></a>'
        el += '<span>'
        el += data.email
        el += '</span><img src="/assets/img/loader5.gif" id="loading_delete_email" class="hide"></div>'

        $("#list_promotion_code_email").prepend(el)
        $("#loading_add_email").hide()
        $("#promotion_code_email_email").val('')
      error: (e) ->
        console.log(e)
        $("#loading_add_email").hide()
        return
    )
    e.preventDefault()

  $('#promotion_code_party_party_id').live 'input', (e) ->
    $("#error-messages_label").hide()
    $("#loading_add_party").hide()

  $('#add_promotion_code_party').live 'click', (e) ->
    $("#loading_add_party").show()
    $.ajax
      type: "POST"
      url: "/promotion_codes/check_party_id"
      beforeSend: (xhr) ->
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      data:
        "check_party_id": $("#promotion_code_party_party_id").val()

      dataType: "json"
      success: (data) ->
        if (data)
          $.ajax
            type: "POST"
            url: "/promotion_codes/add_party"
            beforeSend: (xhr) ->
              xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
            data:
              "promotion_code_party[product_promotion_code_id]": $("#promotion_code_party_product_promotion_code_id").val()
              "promotion_code_party[party_id]": $("#promotion_code_party_party_id").val()
            dataType: "json"
            success: (data) ->
              el = '<div>'
              el += '<a class="btn btn4 btn_trash" data-id="'
              el +=  data.id
              el += '" id="delete_party" style="background-color: rgb(247, 247, 247);"></a>'
              el += '<span>'
              el += data.party_id
              el += '</span><img src="/assets/img/loader5.gif" id="loading_delete_party" class="hide"></div>'
              $("#list_promotion_code_party").prepend(el)
              $("#loading_add_party").hide()
              $("#promotion_code_party_party_id").val('')
            error: (e) ->
              console.log(e)
              $("#loading_add_party").hide()
              return
        else
          $("div#error-messages_label").show()

    e.preventDefault()

  $("#delete_email").live 'click', (e) ->
    id = $(this).attr('data-id')
    parent = $(this).parent()
    parent = $(this).parent()
    loading = parent.find("#loading_delete_email")
    loading.show()
    $.ajax
      type: "POST"
      url: "/promotion_codes/delete_email"
      beforeSend: (xhr) ->
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      data:
        "id": id
      dataType: "json"
      success: (data) ->
        parent.remove()
        loading.hide()
      error: (e) ->
        console.log(e)

    e.preventDefault()

  $("#delete_party").live 'click', (e) ->

    id = $(this).attr('data-id')
    parent = $(this).parent()
    loading = parent.find("#loading_delete_party")
    loading.show()
    $.ajax
      type: "POST"
      url: "/promotion_codes/delete_party"
      beforeSend: (xhr) ->
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      data:
        "id": id
      dataType: "json"
      success: (data) ->
        parent.remove()
        loading.hide()
      error: (e) ->
        console.log(e)

    e.preventDefault()

  #Promotion Code
  if $('#pc-massinsert').length > 0
    $('#pc-massinsert').validate({
      errorLabelContainer: "#messageBox",
      errorClass: "invalid"
      errorElement: "div"
      rules:
        "quantity":
          required: true
          number: true
        "code_length":
          required: true
          number: true
        "limit_per_code":
          required: true
          number: true
        limit_per_customer:
          required: true
          number: true
      messages:
        "quantity":
          required: "Please enter the number of promotion codes"
          number: "promotion codes must be number"
        "code_length":
          required: "Please enter length of codes"
          number: "code length must be number"
        "limit_per_code":
          required: "Please enter limit per code"
          number: "limit per code must be number"
        "limit_per_customer":
          required: "Please enter limit per customer"
          number: "limit per customer must be number"
    })
  fetch_promotion = (page, per) ->
    content = $('#dyntable_filter').children('input').val();
    type = $('#dyntable_filter').find('select').val();
    data = {page: page, per: per};
    if (content != '')
      data['type'] = type;
      data['content'] = content;
    $.ajax({
    type: 'POST',
    url: '/promotion_codes/promotion_codes_per_page',
    async: true,
    jsonpCallback: 'jsonCallback',
    dataType: 'json',
    data: data,
    beforeSend: () ->
      $('.stdtable > tbody').css('opacity', '0.25');
      width = $('.stdtable').parent().width()
      height = $('.stdtable').parent().height()
      $('.stdtable').prepend('<img src="/assets/loading_128.gif" style="position: absolute; left: '+(width/2-64)+'px; top: 128px" class="img-loading"/>');
    success: (response) ->
      $('.stdtable .img-loading').remove();
      $('.stdtable > tbody').empty();
      $('.stdtable > tbody').css('opacity', '1');
      showResults(response, page, per)
    });

  showResults = (response, page, per) ->
    if (per != undefined)
      $('#dyntable_paginate').empty();
      num = response.page;
      if (num > 1)
        $('#dyntable_paginate').append('<span class="first paginate_button" id="dyntable_first">'+I18n.t("common.first")+'</span><span class="previous paginate_button" id="dyntable_previous">'+I18n.t("common.previous")+'</span>')
        $('#dyntable_paginate').append('<span class="paginate-body" style="max-width: 256px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; vertical-align: middle;"><span class="paginate-body-inner"></span></span>')
        for i in [1..num]
          if (i == page)
            $('#dyntable_paginate > .paginate-body > .paginate-body-inner').append('<span class="paginate_active">'+i+'</span>');
          else
            $('#dyntable_paginate > .paginate-body > .paginate-body-inner').append('<span class="paginate_button">'+i+'</span>');
        $('#dyntable_paginate').append('<span class="next paginate_button" id="dyntable_next">'+I18n.t("common.next")+'</span><span class="last paginate_button" id="dyntable_last">'+I18n.t("common.last")+'</span>');
    else
      $('#dyntable_paginate > .paginate-body').children('.paginate-body-inner').children('.paginate_active').removeClass('paginate_active').addClass('paginate_button');
      $('#dyntable_paginate > .paginate-body').children('.paginate-body-inner').children('span:nth-child('+page+')').removeClass('paginate_button').addClass('paginate_active');
    if (response.data.length > 0)
      showInTable(response.data)
    else
      $('.stdtable > tbody').append('<tr><td colspan="6" style="text-align: center">Not Found Results</td></tr>');

  showInTable = (data) ->
    if (data == undefined || data == null || data.length == 0)
      return
    $.each(data, (index, value) ->
      $('.stdtable > tbody').append('<tr id="proid-'+value['product_promotion_code_id']+'" ref="/promotion_codes/'+value['product_promotion_code_id']+'/edit">
                                      <td class="center">
                                        <span><span class="checkbox">
                                          <input type="checkbox">
                                        </span></span>
                                      </td>
                                      <td>
                                        '+value['product_promotion_code_id']+'
                                      </td>
                                      <td>
                                        '+value['user_entered']+'
                                      </td>
                                      <td>
                                        '+value['require_email_or_party']+'
                                      </td>
                                      <td>
                                        '+value['use_limit_per_code']+'
                                      </td>
                                      <td>
                                        '+value['use_limit_per_customer']+'
                                      </td>
                                      <td class="wrap-content">
                                        '+value['from_date']+'
                                      </td>
                                      <td class="wrap-content">
                                        '+value['thru_date']+'
                                      </td>
                                      <td class="wrap-content">
                                        '+value['created_at']+'
                                      </td>
                                      <td>
                                        '+value['created_by_user_login']+'
                                      </td>
                                      <td class="wrap-content">
                                        '+value['updated_at']+'
                                      </td>
                                      <td>
                                        '+value['last_modified_by_user_login']+'
                                      </td>
                                    </tr>')
    )
  searchPress = (content) ->
    type = $('select.search-content').val()
    $.ajax({
    type: 'POST',
    url: '/promotion_codes/promotion_codes_per_page',
    async: true,
    jsonpCallback: 'jsonCallback',
    dataType: 'json',
    data: {type: type, content: content},
    beforeSend: () ->
      $('.stdtable > tbody').css('opacity', '0.25');
      width = $('.stdtable').parent().width()
      height = $('.stdtable').parent().height()
      $('.stdtable').prepend('<img src="/assets/loading_128.gif" style="position: absolute; left: '+(width/2-64)+'px; top: 128px" class="img-loading"/>');
    success: (response) ->
      $('.stdtable .img-loading').remove();
      $('.stdtable > tbody').empty();
      $('.stdtable > tbody').css('opacity', '1');
      if (response.data.length == 0)
        if (type == 'product_promotion_code_id')
          $('#dyntable_paginate').empty();
          $('.stdtable > tbody').empty();
          $('.stdtable > tbody').css('opacity', '1');
          $('.stdtable > tbody').append('<tr><td colspan="12" style="text-align: center">Not Found Results <a href="/promotion_codes/new?product_promotion_code_id='+content+'">Want to Create?</a></td></tr>');
        else
          showResults(response, 1, response.per)
      else
        showResults(response, 1, response.per);
    });

  $('#list-prdcode .btn-search').click(() ->
    searchPress($(this).parent().children('input').val())
  )

  $('#list-prdcode #promotions-per-page').change(() ->
    per = $(this).val();
    fetch_promotion(1, per);
  )

  $('#list-prdcode #btn-delete').click(() ->
    list = []
    $.each($('.stdtable > tbody').find('span.checkbox input[type=checkbox][checked=checked]'), (index, value) ->
      tr = $(value).closest('tr');
      list.push(tr.attr('id').substr(6));
    );
    if (list.length > 0)
      jConfirm(I18n.t('common.dialog.confirm.content'), I18n.t('common.dialog.confirm.title'), (r) ->
        if (r == true)
          $.each($('.stdtable > tbody').find('span.checkbox input[type=checkbox][checked=checked]'), (index, value) ->
            tr = $(value).closest('tr');
            tr.remove();
          );
          page_active = $('.dataTables_wrapper #dyntable_paginate .paginate-body-inner > .paginate_active').text()
          $.ajax({
            type: 'POST',
            url: '/promotion_codes/delete_promotion_codes',
            async: true,
            jsonpCallback: 'jsonCallback',
            dataType: 'json',
            data: {list: list, page: page_active, content: $('#dyntable_filter > input').val(), type: $('#dyntable_filter .search-content').val()},
            success: (response) ->
              $.fn.paginate.remove_lastpage(response.page)
              showInTable(response.data)
          });
      );
  )

  $('#list-prdcode #promo-search').keyup((e) ->
    content = $(this).val()
    if (e.which == 13)
      searchPress(content)
    else
      return;
      $.each($('.stdtable > tbody > tr'), (index, value) ->
        if ($(value).children('td:nth-child(3)').text().indexOf(content) == -1)
          $(value).hide()
        else
          $(value).show()
      );
  )
  if $('#list-prdcode .paginate_button').length > 0
    $('#list-prdcode .paginate_button').paginate({callback: fetch_promotion})

  $('#list-prdcode .search-content').change(() ->
    value = $(this).children('option[selected="selected"]').text()
    $('.search-select > div').text(value);
    $('#dyntable_filter > input').css('padding-left', ($('.search-select').outerWidth(true) + 4)+ 'px');
  )

