# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$(document).ready ->
  $(document).on('click', '#shipment_rates .field-country ul li', () ->
    code = $(this).attr('tag')
    parent = $(this).closest('.row')
    $(this).closest('.field-country').children('input').attr('tag', code)
    $.ajax({
      type: 'POST',
      url: '/facility/ship_rates/get_for_country',
      async: true,
      jsonpCallback: 'jsonCallback',
      dataType: 'json',
      data: {type: 'country', id: code},
      beforeSend: () ->
        parent.children('div:not(.field-width1)').remove()
        parent.append('<img src="/assets/img/loader5.gif" style="margin: 3px 0px;"/>');
      success: (response) ->
        parent.children('img').remove()
        parent.children('div:not(.field-width1)').remove()
        if (response.data == null)
          html = '
            <div class="field-width2">
              <div class="label">' + I18n.t('ship_rate.city') + '</div>
              <input id="city_autocomplete" class="autocomplete_event" name="ship_rate[city]"/>
            </div>
            <div class="field-width3">
              <div class="label">' + I18n.t('ship_rate.postal_code') + '</div>
              <input id="postal_code"  name="ship_rate[postal_code]">
            </div>'
          parent.append(html)
        else
          html = '<option value=""></option>'
          $.each(response.data, (index, content) ->
            html += '<option value="' + content['ascii_name'] + '">' + content['name'] + '</option>'
          )
          html = '
            <div class="field-width2">
              <div class="label">' + I18n.t('ship_rate.city') + '</div>
              <select class="chosen" name="ship_rate[city]">' + html + '</select>
            </div>
            <div class="field-width3">
              <div class="label"> ' + I18n.t('ship_rate.postal_code') + ' </div>
              <input name="ship_rate[postal_code]">
            </div>'
          parent.append(html)
          parent.find('.chosen').chosen()
    })
  )

  $(document).on('click', '#shipment_rates #get_rates', () ->
    el = $('#Ship_rate')
    el_length_start = $('.starting-point .field-width2 input').length
    el_length_end = $('.starting-point .field-width2 input').length

    if el_length_start == 0
      $('#validate_error1').show()
      return false
    else if el_length_end == 0
      $('#validate_error1').hide()
      $('#validate_error2').show()
      return false
    else
      $('#validate_error1').hide()
      $('#validate_error2').hide()
      el.validate(
        errorLabelContainer: el.find('#validate_js')
        errorClass: "invalid"
        errorElement: "p"
        rules:
          'ship_rate[country_start]':
            required: true
          'ship_rate[postal_code]':
            required: true
          'ship_rate[country_end]':
            required: true
          'ship_rate[city]':
            required: true
          'ship_rate[weight_package]':
            required: true
          'ship_rate[box_height]':
            required: true
          'ship_rate[box_length]':
            required: true
          'ship_rate[box_width]':
            required: true

        messages:
          'ship_rate[country_start]':
            required: I18n.t("ship_rate.country_start.required")
          'ship_rate[postal_code]':
            required: I18n.t("ship_rate.postal_code_required.required")
          'ship_rate[city]':
            required: I18n.t("ship_rate.city_required.required")
          'ship_rate[country_end]':
            required: I18n.t("ship_rate.country_end.required")
          'ship_rate[weight_package]':
            required: I18n.t("ship_rate.weight_package.required")
          'ship_rate[box_width]':
            required: I18n.t("ship_rate.box_width.required")
          'ship_rate[box_height]':
            required: I18n.t("ship_rate.box_height.required")
          'ship_rate[box_length]':
            required: I18n.t("ship_rate.box_length.required")

      )

      if el.valid()
        parent = $(this).parent()
        starting_point = get_location($('.starting-point'))
        ending_point = get_location($('.ending-point'))
        weight = $('#sr_weight').val()
        height = $('#sr_height').val()
        length = $('#sr_length').val()
        width = $('#sr_width').val()
        $.ajax({
          type: 'POST',
          url: '/facility/ship_rates/get_rates',
          async: true,
          jsonpCallback: 'jsonCallback',
          dataType: 'json',
          data: {starting_point: starting_point, ending_point: ending_point, weight: weight, width: width, height: height, length: length},
          beforeSend: () ->
            parent.empty()
            parent.append('<img src="/assets/img/loader5.gif" style="margin: 38px 0px 21px 76px"/>');
          success: (response) ->
            parent.children('img').remove()
            parent.append('<button id="get_rates">' + I18n.t('ship_rate.button') + '</button>')
            if (response.data == null)
              $('#quote_details').html('<div class="notification msgerror">
                                              <a class="close"></a>
                                              <p>' + response.text + '</p>
                                          </div>')
            else
              $('#quote_details').empty()
              current_page = null;
              tag_pag = $('.dataTables_wrapper #dyntable_paginate .paginate-body-inner')
              active_page = null
              $.each(tag_pag.children(), (index, value) ->
                current_page++;
                if ($(value).attr('class') == 'paginate_active')
                  active_page = index + 1
              )
              if (current_page != null && current_page < response.page)
                tag_pag.append('<span class="paginate_button">
                                      '+response.page+'
                                    </span>')
              else if (current_page == null && response.page > 1)
                $('.dataTables_wrapper #dyntable_paginate').append('
                                <span class="first paginate_button" id="dyntable_first">
                                  ' + I18n.t('common.first') + '
                                </span>
                                <span class="previous paginate_button" id="dyntable_previous">
                                  ' + I18n.t('common.previous') + '
                                </span>
                                <span class="paginate-body">
                                  <span class="paginate-body-inner">
                                    <span class="paginate_active">
                                      1
                                    </span>
                                    <span class="paginate_button">
                                      2
                                    </span>
                                  </span>
                                </span>
                                <span class="next paginate_button" id="dyntable_next">
                                  ' + I18n.t('common.next') + '
                                </span>
                                <span class="last paginate_button" id="dyntable_last">
                                  ' + I18n.t('common.last') + '
                                </span>')
              else if (active_page == current_page)
                $('.stdtable > tbody').append('
                        <tr id="srid-' + response.data.id + '" ref="">
                            <td class="center">
                              <span>
                                <span class="checkbox"><input id="check_delete_1" type="checkbox"></span>
                              </span>
                            </td>
                            <td class="center">
                              <span>
                                ' + response.data.id + '
                              </span>
                            </td>
                            <td class="center">
                              <span>
                                <input id="dimession_' + response.data.id + '" style="width: 128px" value="' + length + ', ' + width + ', ' + height + '">
                              </span>
                            </td>
                            <td class="center">
                              <span>
                                <input id="weight_' + response.data.id + '" style="width: 64px" value="' + weight + '">
                              </span>
                            </td>
                            <td class="center">
                              <span>
                                <input id="starting_point_' + response.data.id + '" value="' + response.data['country_start_code'] + ', ' + starting_point['city_name'] + ', ' + starting_point['postcode'] + '">
                              </span>
                            </td>
                            <td class="center">
                              <span>
                                <input id="ending_point_' + response.data.id + '"  value="' + response.data['country_end_code'] + ', ' + ending_point['city_name'] + ', ' + ending_point['postcode'] + '">
                              </span>
                            </td>
                            <td class="center">
                              <button class="stdbtn btn-update" id="update-' + response.data.id + '" >
                                ' + I18n.t('common.update') + '
                              </button>
                              <button class="stdbtn btn-quote" id="quote-' + response.data.id + '"  >
                                ' + I18n.t('common.quote') + '
                              </button>
                            </td>
                          </tr>'
                )
          error: () ->

          })
        return false;
    )

  $('#dwsrates #btn-delete').click(() ->
    list = []
    $.each($('.stdtable > tbody').find('span.checkbox input[type=checkbox][checked=checked]'), (index, value) ->
      tr = $(value).closest('tr');
      list.push(tr.attr('id').substr(5));
    );
    if (list.length > 0)
      jConfirm(I18n.t('common.dialog.confirm.content'), I18n.t('common.dialog.confirm.title'), (r) ->
        if (r == true)
          $.each($('.stdtable > tbody').find('span.checkbox input[type=checkbox][checked=checked]'), (index, value) ->
            tr = $(value).closest('tr');
            tr.remove();
          );
          page_active = $('.dataTables_wrapper #dyntable_paginate .paginate-body-inner > .paginate_active').text()
          $.ajax({
            type: 'POST',
            url: '/facility/ship_rates/delete_shipment',
            async: true,
            jsonpCallback: 'jsonCallback',
            dataType: 'json',
            data: {list: list, page: page_active, content: $('#dyntable_filter > input').val(), type: $('#dyntable_filter .search-content').val()},
            success: (response) ->
              $.fn.paginate.remove_lastpage(response.page)
              showInTable(response.data)
          });
      )
  )

  $(document).on('click', '#dwsrates .btn-update', () ->
    parent = $(this).parent()
    tag_tr = $(this).closest('tr')
    id = tag_tr.attr('id').substr(5)
    dimension = tag_tr.children('td:nth-child(3)').find('input').val()
    weight = tag_tr.children('td:nth-child(4)').find('input').val()
    starting_point = tag_tr.children('td:nth-child(5)').find('input').val()
    ending_point = tag_tr.children('td:nth-child(6)').find('input').val()

    value = dimension.split(',')
    if ($.isNumeric(value[0]) == false || $.isNumeric(value[1]) == false || $.isNumeric(value[2]) == false || $.isNumeric(weight) == false)
      return false;
    dimension = [value[0], value[1], value[2]]
    value = starting_point.split(',')
    if (value[1] == undefined)
      value.push('')
    if (value[2] == undefined)
      value.push('')

    starting_point = {country_code: value[0].trim(), city_name: value[1].trim(), postcode: value[2].trim()}
    value = ending_point.split(',')
    if (value[1] == undefined)
      value.push('')
    if (value[2] == undefined)
      value.push('')
    ending_point = {country_code: value[0].trim(), city_name: value[1].trim(), postcode: value[2].trim()}
    $.ajax({
    type: 'POST',
    url: '/facility/ship_rates/update_shipment',
    async: true,
    jsonpCallback: 'jsonCallback',
    dataType: 'json',
    data: {id: id, starting_point: starting_point, ending_point: ending_point, weight: weight, width: dimension[1], height: dimension[2], length: dimension[0]},
    beforeSend: () ->
      parent.children('.btn-update').remove()
      parent.append('<img src="/assets/img/loader5.gif" style="margin: 4px 12px 0px 38px; width: 24px; height: 24px; float: left"/>');
    success: (response) ->
      parent.children('img').remove()
      parent.prepend('<button class="stdbtn btn-update" id="update-'+id+'" style="opacity: 0.75;">
                      ' + I18n.t('common.update') + '
                    </button>')
      if (response.status == 'error')
        $('#quote_details').html('<div class="notification msgerror">
                                                    <a class="close"></a>
                                                    <p>' + response.text + '</p>
                                                </div>')
      else
        $('#quote_details').html('<div class="notification msgsuccess">
                                                     <a class="close"></a>
                                                     <p>' + response.text + '</p>
                                                </div>')
    });
  )

  $(document).on('click', '#dwsrates .btn-quote', () ->
    parent = $(this).parent()
    id = $(this).closest('tr').attr('id').substr(5)
    $.ajax({
      type: 'POST',
      url: '/facility/ship_rates/get_quote',
      async: true,
      jsonpCallback: 'jsonCallback',
      dataType: 'json',
      data: {id: id},
      beforeSend: () ->
        parent.children('.btn-quote').remove()
        parent.append('<img src="/assets/img/loader5.gif" style="margin: 4px 38px 0px 8px; width: 24px; height: 24px; float: right"/>');
      success: (response) ->
        parent.children('img').remove()
        parent.append('<button class="stdbtn btn-quote" id="quote-' + id + '" style="opacity: 0.75;">
                        ' + I18n.t('common.quote') + '
                      </button>')
        html = ''
        $.each(response.data, (index, value) ->
          if (value[4] == null)
            value[4] = ''
          if (value[3] != null && value[3] != '')
            value[3] = value[3] / 1000
          html += '
              <div>
                <div>' + I18n.t('ship_rate.service_name') + ' ' + value[0] + ' - ' + value[2]+ '</div>
                <div>
                  <p>' + I18n.t('ship_rate.total_price') + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + value[3] + ' ' + value[1] + '</p>
                  <p' + I18n.t('ship_rate.delivery_date') + ' ' + value[4] + '</p>
                </div>
              </div>'
        )
        href = location.protocol + '//' + location.host + '/facility/ship_rates/estimate?starting_country='+response.location['starting_point']['country'] +
                '&starting_city='+response.location['starting_point']['city'] +
                '&starting_postal='+response.location['starting_point']['postal_code'] +
                '&ending_country='+response.location['ending_point']['country'] +
                '&ending_city='+response.location['ending_point']['city'] +
                '&ending_postal='+response.location['ending_point']['postal_code'] +
                '&weight='+response.weight +
                '&length='+response.dimension['length'] +
                '&height='+response.dimension['height'] +
                '&width='+response.dimension['width']
        html = '<div class="notification msginfo">
                  <a class="close"></a>
                  <div style="font-weight: bold;">Service: </div>
                  ' + html + '
                  <div style="font-weight: bold; color: black">Link Detail: <a href="' + href + '">Click Here</a></div>
                </div>'
        $('#quote_details').html(html)
      error: (response) ->
        parent.children('img').remove()
        parent.append('<button class="stdbtn btn-quote" id="quote-' + id + '"  style="opacity: 0.75;">
                                ' + I18n.t('common.quote') + '
                              </button>')
        $('#quote_details').html('<div class="notification msgerror">
                                <a class="close"></a>
                                <p>' + I18n.t('ship_rate.invalid_service') + '</p>
                            </div>')
    });
  )

  $('.field-country > input').focusout(() ->
    parent = $(this).parent()
    $.ajax({
    type: 'POST',
    url: '/facility/ship_rates/get_for_country_autocomplete',
    async: true,
    jsonpCallback: 'jsonCallback',
    dataType: 'json',
    data: {content: $(this).val(), type: 'correct'},
    beforeSend: () ->
    success: (response) ->
      if (response.data == undefined || response.data == null || response.data == 0)
        parent.closest('.row').children('div:not(.field-width1)').remove()
    })
  )
  $('#dwsrates #promo-search').keyup((e) ->
    content = $(this).val()
    if (e.which == 13)
      searchPress(content)
  )
  $(document).on('click', '.notification > .close', () ->
    $(this).parent().remove()
  )
  $(document).on('click', '.contenttitle', () ->
    next = $(this).next()
    if (next.css('display') == 'none')
      next.slideDown()
    else
      next.slideUp()
  )
  showInTable = (data) ->
    if (data == undefined || data == null || data.length == 0)
      return;
    $.each(data, (index, value) ->
      $('.stdtable > tbody').append('
                          <tr id="srid-' + value['id'] + '" ref="">
                                  <td class="center">
                                    <span>
                                      <span class="checkbox"><input id="check_delete_1" type="checkbox"></span>
                                    </span>
                                  </td>
                                  <td class="center">
                                    <span>
                                      ' + value['id'] + '
                                    </span>
                                  </td>
                                  <td class="center">
                                    <span>
                                      <input id="dimession_ '+ value['id'] + '" style="width: 128px" value="' + value['box_length'] + ', ' + value['box_width'] + ', ' + value['box_height'] + '">
                                    </span>
                                  </td>
                                  <td class="center">
                                    <span>
                                      <input id="weight_' + value['id'] + '" style="width: 64px" value="' + value['weight'] + '">
                                    </span>
                                  </td>
                                  <td class="center">
                                    <span>
                                      <input id="starting_point_' + value['id'] + '" value="' + value['starting_point']['country_code'] + ', ' + value['starting_point']['city_name'] + ', ' + value['starting_point']['postcode'] + '">
                                    </span>
                                  </td>
                                  <td class="center">
                                    <span>
                                      <input id="ending_point_' + value['id'] + '" value="' + value['ending_point']['country_code'] + ', ' + value['ending_point']['city_name'] + ', ' + value['ending_point']['postcode'] + '">
                                    </span>
                                  </td>
                                  <td class="center">
                                    <button class="stdbtn btn-update" id="update-' + value['id'] + '">
                                      ' + I18n.t('common.update') + '
                                    </button>
                                    <button class="stdbtn btn-quote" id="quote-' + value['id'] + '">
                                      ' + I18n.t('common.quote') + '
                                    </button>
                                  </td>
                                </tr>'
      )
    )

  get_location = (el) ->
    p1 = el.find('> .field-width1 > .chzn-container > a > span').attr('tag')
    if (p1 == undefined || p1 == null || p1 == '')
      p1 = el.find('> .field-width1 > input').val()
    p2 = el.find('> .field-width2 > .chzn-container > a > span').attr('tag')
    if (p2 == undefined || p2 == null || p2 == '')
      p2 = el.find('> .field-width2 > input').val()
    p3 = el.find('> .field-width3 > input').val()
    return {country_code: p1, city_name: p2.trim(), postcode: p3.trim()}

  cities_autocomplete = (evt) ->
    value = $(this).val()
    parent = $(this).parent()
    country_code = $(this).closest('.row').find('> .field-width1 > .chzn-container > a > span').attr('tag')
    if (country_code == undefined || value == undefined)
      country_code = $(this).closest('.row').find('> .field-width1 > input').val()
      if (country_code == undefined)
        return
    $.ajax({
      type: 'POST',
      url: '/facility/ship_rates/get_for_city_autocomplete',
      async: true,
      jsonpCallback: 'jsonCallback',
      dataType: 'json',
      data: {country_code: country_code, content: value, type: 'name'},
      beforeSend: () ->

      success: (response) ->
        if (response.data == null)
          return;
        parent.children('.autocomplete').remove()
        html = ''
        $.each(response.data, (index, value) ->
          html += '<li>' + value['name'] + '</li>'
        )
        html = '<ul class="autocomplete">' + html + '</ul>'
        parent.append(html)
    })

  countries_autocomplete = (evt) ->
    value = $(this).val()
    parent = $(this).parent()
    $.ajax({
      type: 'POST',
      url: '/facility/ship_rates/get_for_country_autocomplete',
      async: true,
      jsonpCallback: 'jsonCallback',
      dataType: 'json',
      data: {content: value},
      beforeSend: () ->
      success: (response) ->
        parent.children('.autocomplete').remove()
        if (response.data == undefined || response.data == null || response.data.length == 0)
          parent.closest('.row').children('div:not(.field-width1)').remove()
        else
          html = ''
          $.each(response.data, (index, value) ->
            html += '<li tag="' + value['iso_code_two_letter'] + '">' + value['name'] + '</li>'
          )
          html = '<ul class="autocomplete">' + html + '</ul>'
          parent.append(html)
          if (parent.closest('.row').children('.field-width2').length == 0)
            html = '
                    <div class="field-width2">
                      <div class="label">' + I18n.t('ship_rate.city') + '</div>
                      <input id="city_autocomplete" class="autocomplete_event" name="ship_rate[city]"/>
                    </div>
                    <div class="field-width3">
                      <div class="label">' + I18n.t('ship_rate.postal_code') + '</div>
                      <input id="postal_code"  name="ship_rate[postal_code]">
                    </div>'
            parent.closest('.row').append(html)
    })

  fetch_shipment = (page, per) ->
    content = $('#dyntable_filter').children('input').val();
    type = $('#dyntable_filter').find('select').val();
    data = {page: page, per: per};
    if (content != '')
      data['type'] = type;
      data['content'] = content;
    $.ajax({
    type: 'POST',
    url: '/facility/ship_rates/shipment_per_page',
    async: true,
    jsonpCallback: 'jsonCallback',
    dataType: 'json',
    data: data,
    beforeSend: () ->
      $('.stdtable > tbody').css('opacity', '0.25');
      width = $('.stdtable').parent().width()
      height = $('.stdtable').parent().height()
      $('.stdtable').prepend('<img src="/assets/loading_128.gif" style="position: absolute; left: '+(width/2-64)+'px; top: 128px" class="img-loading"/>');
    success: (response) ->
      $('.stdtable .img-loading').remove();
      $('.stdtable > tbody').empty();
      $('.stdtable > tbody').css('opacity', '1');
      showResults(response, page, per)
    });

  showResults = (response, page, per) ->
    if (per != undefined)
      $('#dyntable_paginate').empty();
      num = response.page;
      if (num > 1)
        $('#dyntable_paginate').append('<span class="first paginate_button" id="dyntable_first">'+I18n.t("common.first")+'</span><span class="previous paginate_button" id="dyntable_previous">'+I18n.t("common.previous")+'</span>')
        $('#dyntable_paginate').append('<span class="paginate-body" style="max-width: 256px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; vertical-align: middle;"><span class="paginate-body-inner"></span></span>')
        for i in [1..num]
          if (i == page)
            $('#dyntable_paginate > .paginate-body > .paginate-body-inner').append('<span class="paginate_active">'+i+'</span>');
          else
            $('#dyntable_paginate > .paginate-body > .paginate-body-inner').append('<span class="paginate_button">'+i+'</span>');
        $('#dyntable_paginate').append('<span class="next paginate_button" id="dyntable_next">'+I18n.t("common.next")+'</span><span class="last paginate_button" id="dyntable_last">'+I18n.t("common.last")+'</span>');
    else
      $('#dyntable_paginate > .paginate-body').children('.paginate-body-inner').children('.paginate_active').removeClass('paginate_active').addClass('paginate_button');
      $('#dyntable_paginate > .paginate-body').children('.paginate-body-inner').children('span:nth-child('+page+')').removeClass('paginate_button').addClass('paginate_active');
    if (response.data.length > 0)
      showInTable(response.data)
    else
      $('.stdtable > tbody').append('<tr><td colspan="7" style="text-align: center">'+I18n.t("common.no_result")+'</td></tr>');

  searchPress = (content, type) ->
    if (type == undefined)
      type = $('select.search-content').val()
    $.ajax({
    type: 'POST',
    url: '/facility/ship_rates/shipment_per_page',
    async: true,
    jsonpCallback: 'jsonCallback',
    dataType: 'json',
    data: {type: type, content: content},
    beforeSend: () ->
      $('.stdtable > tbody').css('opacity', '0.25');
      width = $('.stdtable').parent().width()
      height = $('.stdtable').parent().height()
      $('.stdtable').prepend('<img src="/assets/loading_128.gif" style="position: absolute; left: '+(width/2-64)+'px; top: 128px" class="img-loading"/>');
    success: (response) ->
      $('.stdtable .img-loading').remove();
      $('.stdtable > tbody').empty();
      $('.stdtable > tbody').css('opacity', '1');
      if (response.data.length == 0)
        if (type == 'id')
          $('#dyntable_paginate').empty();
          $('.stdtable > tbody').empty();
          $('.stdtable > tbody').css('opacity', '1');
          $('.stdtable > tbody').append('<tr><td colspan="7" style="text-align: center">' + I18n.t('common.no_result') + ' <a href="#">' + I18n.t('common.want_to_create') + '</a></td></tr>');
        else
          showResults(response, 1, response.per)
      else
        showResults(response, 1, response.per);
    });

  $('.field-country > input').db_autocomplete({callback: countries_autocomplete})
  $('.field-width2 > input').db_autocomplete({callback: cities_autocomplete})
  $('#dwsrates .paginate_button').paginate({callback: fetch_shipment})


(($) ->
  $.fn.db_autocomplete = (options) ->
    defaults = {
      callback: () ->
    }
    opts = $.extend(defaults, options);
    $(document).on('keyup', this.selector, () ->
      opts.callback.call($(this), $(this))
    )

    $(document).on('click', this.selector, (evt) ->
      return false;
    )

    $(document).on('focusin', this.selector, (evt) ->
      auto = $(this).parent().children('.autocomplete')
      if (auto.css('display') == 'none')
        $('.autocomplete').hide()
        auto.show()
      else
        auto.hide()

      evt.preventDefault()
      evt.stopPropagation()
      return false;
    )

    $(document).on('click', '.autocomplete li', (evt) ->
      $(this).parent().hide()
      text = $(this).text()
      $(this).parent().parent().children('input').val(text)
      evt.preventDefault()
    )

    $(document).on('click', 'body', () ->
      auto = $('.autocomplete')
      if (auto.length > 0)
        auto.hide()
    )
    $(document).on('focusin', 'input[class!=autocomplete_event]', () ->
      auto = $('.autocomplete')
      if (auto.length > 0)
        auto.hide()
    )
)(jQuery);
