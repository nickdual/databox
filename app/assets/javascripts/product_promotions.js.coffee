# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$(document).ready ->
  $('#new_product_promotion').validate(
    errorLabelContainer: $("div#content-messages")
    errorClass: "invalid"
    errorElement: "div"
    rules:
#      'product_promotion[promotion_name]':
#        required: true
#      'product_promotion[promotion_text]':
#        required: true
      'product_promotion[use_limit_per_customer]':
        number: true
      'product_promotion[use_limit_per_order]':
        number: true
      'product_promotion[use_limit_per_promotion]':
        number: true
      'product_promotion[billback_factor]':
        number: true
      'product_promotion[override_org_party_id]':
        number: true

    messages:
      "product_promotion[promotion_name]":
        required: I18n.t("product_promotion.promotion_name_error.required")
      "product_promotion[promotion_text]":
        required: I18n.t("product_promotion.promotion_text_error.required")
      "product_promotion[use_limit_per_customer]":
        number: I18n.t("product_promotion.use_limit_per_customer.number")
      "product_promotion[use_limit_per_order]":
        number: I18n.t("product_promotion.use_limit_per_order.number")
      "product_promotion[use_limit_per_promotion]":
        number: I18n.t("product_promotion.use_limit_per_promotion.number")
      "product_promotion[billback_factor]":
        number: I18n.t("product_promotion.billback_factor.number")
      "product_promotion[override_org_party_id]":
        number: I18n.t("product_promotion.override_org_party_id.number")

  )
  $('#error_explanation>h2').hide()
  $('#error_explanation').addClass('notification msgerror')
  $('#error_explanation').append('<a class="close close-error-explanation"></a>')

  $('.close-error-explanation').live 'click', (e)->
    $('#error_explanation').hide()


  $('#content-messages').live 'click', (e) ->
    $(this).hide()
    e.preventDefault()


  $(document).on('click', 'table.stdtable tr', () ->
    ref = $(this).attr('ref')
    if (ref != undefined && ref != null && ref != '')
      window.location.href = $(this).attr('ref')
  )

  #Promotion
  fetch_promotion = (page, per) ->
    content = $('#dyntable_filter').children('input').val();
    type = $('#dyntable_filter').find('select').val();
    data = {page: page, per: per};
    if (content != '')
      data['type'] = type;
      data['content'] = content;
    $.ajax({
    type: 'POST',
    url: '/product_promotions/promotions_per_page',
    async: true,
    jsonpCallback: 'jsonCallback',
    dataType: 'json',
    data: data,
    beforeSend: () ->
      $('.stdtable > tbody').css('opacity', '0.25');
      width = $('.stdtable').parent().width()
      height = $('.stdtable').parent().height()
      $('.stdtable').prepend('<img src="/assets/loading_128.gif" style="position: absolute; left: '+(width/2-64)+'px; top: 128px" class="img-loading"/>');
    success: (response) ->
      $('.stdtable .img-loading').remove();
      $('.stdtable > tbody').empty();
      $('.stdtable > tbody').css('opacity', '1');
      showResults(response, page, per)
    });

  showResults = (response, page, per) ->
    if (per != undefined)
      $('#dyntable_paginate').empty();
      num = response.page;
      if (num > 1)
        $('#dyntable_paginate').append('<span class="first paginate_button" id="dyntable_first">'+I18n.t("common.first")+'</span><span class="previous paginate_button" id="dyntable_previous">'+I18n.t("common.previous")+'</span>')
        $('#dyntable_paginate').append('<span class="paginate-body" style="max-width: 256px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; vertical-align: middle;"><span class="paginate-body-inner"></span></span>')
        for i in [1..num]
          if (i == page)
            $('#dyntable_paginate > .paginate-body > .paginate-body-inner').append('<span class="paginate_active">'+i+'</span>');
          else
            $('#dyntable_paginate > .paginate-body > .paginate-body-inner').append('<span class="paginate_button">'+i+'</span>');
        $('#dyntable_paginate').append('<span class="next paginate_button" id="dyntable_next">'+I18n.t("common.next")+'</span><span class="last paginate_button" id="dyntable_last">'+I18n.t("common.last")+'</span>');
    else
      $('#dyntable_paginate > .paginate-body').children('.paginate-body-inner').children('.paginate_active').removeClass('paginate_active').addClass('paginate_button');
      $('#dyntable_paginate > .paginate-body').children('.paginate-body-inner').children('span:nth-child('+page+')').removeClass('paginate_button').addClass('paginate_active');
    if (response.data.length > 0)
      showInTable(response.data)
    else
      $('.stdtable > tbody').append('<tr><td colspan="6" style="text-align: center">Not Found Results</td></tr>');

  showInTable = (data) ->
    if (data == undefined || data == null || data.length == 0)
      return
    $.each(data, (index, value) ->
      $('.stdtable > tbody').append('<tr id="proid-'+value['id']+'" ref="/product_promotions/'+value['id']+'/edit"><td class="center"><span><span class="checkbox"><input type="checkbox"></span><span></span></td><td>'+value['id']+'</td><td>'+value['promotion_name']+'</td><td>'+value['promotion_text']+'</td><td>'+value['require_code']+'</td><td class="wrap-content">'+value['updated_at']+'</td></tr>')
    )
  searchPress = (content, type) ->
    if (type == undefined)
      type = $('select.search-content').val()
    $.ajax({
    type: 'POST',
    url: '/product_promotions/promotions_per_page',
    async: true,
    jsonpCallback: 'jsonCallback',
    dataType: 'json',
    data: {type: type, content: content},
    beforeSend: () ->
      $('.stdtable > tbody').css('opacity', '0.25');
      width = $('.stdtable').parent().width()
      height = $('.stdtable').parent().height()
      $('.stdtable').prepend('<img src="/assets/loading_128.gif" style="position: absolute; left: '+(width/2-64)+'px; top: 128px" class="img-loading"/>');
    success: (response) ->
      $('.stdtable .img-loading').remove();
      $('.stdtable > tbody').empty();
      $('.stdtable > tbody').css('opacity', '1');
      if (response.data.length == 0)
        if (type == 'id')
          $('#dyntable_paginate').empty();
          $('.stdtable > tbody').empty();
          $('.stdtable > tbody').css('opacity', '1');
          $('.stdtable > tbody').append('<tr><td colspan="6" style="text-align: center">Not Found Results <a href="/product_promotions/new?id='+content+'">Want to Create?</a></td></tr>');
        else
          showResults(response, 1, response.per)
      else
        showResults(response, 1, response.per);
    });

  $('#lists-prdpro .btn-search').click(() ->
    value = $(this).parent().find('.search-content').children('option[selected="selected"]').attr('value')
    if (value == 'user_entered')
      searchPress(true);
    else if (value == 'imported_only')
      searchPress(false, 'user_entered');
    else
      searchPress($(this).parent().children('input').val())
  )

  $('#lists-prdpro #promotions-per-page').change(() ->
    per = $(this).val();
    fetch_promotion(1, per);
  )

  $('#lists-prdpro #btn-delete').click(() ->
    list = []
    $.each($('.stdtable > tbody').find('span.checkbox input[type=checkbox][checked=checked]'), (index, value) ->
      tr = $(value).closest('tr');
      list.push(tr.attr('id').substr(6));
    );
    if (list.length > 0)
      jConfirm(I18n.t('common.dialog.confirm.content'), I18n.t('common.dialog.confirm.title'), (r) ->
        if (r == true)
          $.each($('.stdtable > tbody').find('span.checkbox input[type=checkbox][checked=checked]'), (index, value) ->
            tr = $(value).closest('tr');
            tr.remove();
          );
          page_active = $('.dataTables_wrapper #dyntable_paginate .paginate-body-inner > .paginate_active').text()
          $.ajax({
            type: 'POST',
            url: '/product_promotions/delete_promotions',
            async: true,
            jsonpCallback: 'jsonCallback',
            dataType: 'json',
            data: {list: list, page: page_active, content: $('#dyntable_filter > input').val(), type: $('#dyntable_filter .search-content').val()},
            success: (response) ->
              $.fn.paginate.remove_lastpage(response.page)
              showInTable(response.data)
          });
      )
  )

  $('#lists-prdpro #promo-search').keyup((e) ->
    content = $(this).val()
    if (e.which == 13)
      searchPress(content)
    else
      return;
      $.each($('.stdtable > tbody > tr'), (index, value) ->
        if ($(value).children('td:nth-child(3)').text().indexOf(content) == -1)
          $(value).hide()
        else
          $(value).show()
      );
  )
  if $('#lists-prdpro .paginate_button').length > 0
    $('#lists-prdpro .paginate_button').paginate({callback: fetch_promotion})

  $('#dyntable_filter .search-content').change(() ->
    value = $(this).children('option[selected="selected"]').text()
    $('.search-select > div').text(value);
    value = $(this).children('option[selected="selected"]').attr('value')
    if (value == 'user_entered' || value == 'imported_only')
      $(this).closest('.dataTables_filter').children('input').css('display', 'none')
      $(this).closest('.dataTables_filter').children('.search-select').css({'position': 'static', 'margin-right': '28px'})
    else
      $(this).closest('.dataTables_filter').children('input').css('display', 'block')
      $(this).closest('.dataTables_filter').children('.search-select').css({'position': 'absolute', 'margin-right': '0'})
    $('#dyntable_filter > input').css('padding-left', ($('.search-select').outerWidth(true) + 4)+ 'px');
  )

  #Promotion Store
  fetch_promotion_store = (page, per) ->
    content = $('#dyntable_filter').children('input').val();
    type = $('#dyntable_filter').find('select').val();
    data = {page: page, per: per};
    if (content != '')
      data['type'] = type;
      data['content'] = content;
    $.ajax({
    type: 'POST',
    url: '/product_promotions/stores_per_page',
    async: true,
    jsonpCallback: 'jsonCallback',
    dataType: 'json',
    data: data,
    beforeSend: () ->
      $('.stdtable > tbody').css('opacity', '0.25');
      width = $('.stdtable').parent().width()
      height = $('.stdtable').parent().height()
      $('.stdtable').prepend('<img src="/assets/loading_128.gif" style="position: absolute; left: '+(width/2-64)+'px; top: 128px" class="img-loading"/>');
    success: (response) ->
      $('.stdtable .img-loading').remove();
      $('.stdtable > tbody').empty();
      $('.stdtable > tbody').css('opacity', '1');
      showResult_Stores(response, page, per)
      $('.datepicker').datetimepicker({
        showSecond: true,
        showMillisec: true,
        showButtonPanel: false,
        dateFormat: 'yy-mm-dd',
        timeFormat: 'HH:mm:ss.l',
        millisecText: 'Millisec',
      });
    });

  showResult_Stores = (response, page, per) ->
    if (per != undefined)
      $('#dyntable_paginate').empty();
      num = response.page;
      if (num > 1)
        $('#dyntable_paginate').append('<span class="first paginate_button" id="dyntable_first">'+I18n.t("common.first")+'</span><span class="previous paginate_button" id="dyntable_previous">'+I18n.t("common.previous")+'</span>')
        $('#dyntable_paginate').append('<span class="paginate-body" style="max-width: 256px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; vertical-align: middle;"><span class="paginate-body-inner"></span></span>')
        for i in [1..num]
          if (i == page)
            $('#dyntable_paginate > .paginate-body > .paginate-body-inner').append('<span class="paginate_active">'+i+'</span>');
          else
            $('#dyntable_paginate > .paginate-body > .paginate-body-inner').append('<span class="paginate_button">'+i+'</span>');
        $('#dyntable_paginate').append('<span class="next paginate_button" id="dyntable_next">'+I18n.t("common.next")+'</span><span class="last paginate_button" id="dyntable_last">'+I18n.t("common.last")+'</span>');
    else
      $('#dyntable_paginate > .paginate-body').children('.paginate-body-inner').children('.paginate_active').removeClass('paginate_active').addClass('paginate_button');
      $('#dyntable_paginate > .paginate-body').children('.paginate-body-inner').children('span:nth-child('+page+')').removeClass('paginate_button').addClass('paginate_active');
    if (response.data.length > 0)
      showStoresInTable(response.data)
    else
      $('.stdtable > tbody').append('<tr><td colspan="6" style="text-align: center">Not Found Results</td></tr>');

  showStoresInTable = (data) ->
    if (data == undefined || data == null || data.length == 0)
      return
    $.each(data, (index, value) ->
      $('.stdtable > tbody').append('
                      <tr id="prstid-'+value['id']+'" ref="">
                        <td class="center">
                          <span><span class="checkbox">
                            <input type="checkbox">
                          </span></span>
                        </td>
                        <td>
                          '+value['store_name']+' ['+value['product_store_id']+']
                        </td>
                        <td class="wrap-content">
                          '+value['from_date']+'
                        </td>
                        <td class="center">
                          <input class="datepicker" style="width: 160px" value="'+value['thru_date']+'">
                        </td>
                        <td class="center">
                          <input class="ip-sequence" style="width: 160px" value="'+value['sequence_num']+'">
                        </td>
                        <td class="center">
                          <button class="stdbtn btn-update" style="padding: 5px 10px 6px 10px; margin-left: 4px">
                            Update
                          </button>
                        </td>
                      </tr>')
    )
  searchPress_Stores = (content) ->
    type = $('select.search-content').val()
    $.ajax({
    type: 'POST',
    url: '/product_promotions/stores_per_page',
    async: true,
    jsonpCallback: 'jsonCallback',
    dataType: 'json',
    data: {type: type, content: content},
    beforeSend: () ->
      $('.stdtable > tbody').css('opacity', '0.25');
      width = $('.stdtable').parent().width()
      height = $('.stdtable').parent().height()
      $('.stdtable').prepend('<img src="/assets/loading_128.gif" style="position: absolute; left: '+(width/2-64)+'px; top: 128px" class="img-loading"/>');
    success: (response) ->
      $('.stdtable .img-loading').remove();
      $('.stdtable > tbody').empty();
      $('.stdtable > tbody').css('opacity', '1');
      if (response.data.length == 0)
        if (type == 'product_store_id')
          $('#dyntable_paginate').empty();
          $('.stdtable > tbody').empty();
          $('.stdtable > tbody').css('opacity', '1');
          $('.stdtable > tbody').append('<tr><td colspan="6" style="text-align: center">Not Found Results <a href="/product_promotions/stores/new?product_store_id='+content+'">Want to Create?</a></td></tr>');
        else
          showResult_Stores(response, 1, response.per)
      else
        showResult_Stores(response, 1, response.per);
    });

  $('#lpp-store #promotions-per-page').change(() ->
    per = $(this).val();
    fetch_promotion_store(1, per);
  )

  $('#lpp-store .btn-search').click(() ->
    searchPress_Stores($(this).parent().children('input').val())
  )

  $('#lpp-store #promo-search').keyup((e) ->
    content = $(this).val()
    if (e.which == 13)
      searchPress_Stores(content)
    else
      return;
      $.each($('.stdtable > tbody > tr'), (index, value) ->
        if ($(value).children('td:nth-child(3)').text().indexOf(content) == -1)
          $(value).hide()
        else
          $(value).show()
      );
  )
  $('#lpp-store .paginate_button').paginate({callback: fetch_promotion_store})

  $('#lpp-store #btn-delete').click(() ->
    list = []
    $.each($('.stdtable > tbody').find('span.checkbox input[type=checkbox][checked=checked]'), (index, value) ->
      tr = $(value).closest('tr');
      list.push(tr.attr('id').substr(7));
    );
    if (list.length > 0)
      jConfirm(I18n.t('common.dialog.confirm.content'), I18n.t('common.dialog.confirm.title'), (r) ->
        if (r == true)
          $.each($('.stdtable > tbody').find('span.checkbox input[type=checkbox][checked=checked]'), (index, value) ->
            tr = $(value).closest('tr');
            tr.remove();
          );
          page_active = $('.dataTables_wrapper #dyntable_paginate .paginate-body-inner > .paginate_active').text()
          $.ajax({
            type: 'POST',
            url: '/product_promotions/delete_stores',
            async: true,
            jsonpCallback: 'jsonCallback',
            dataType: 'json',
            data: {list: list, page: page_active, content: $('#dyntable_filter > input').val(), type: $('#dyntable_filter .search-content').val()},
            success: (response) ->
              $.fn.paginate.remove_lastpage(response.page)
              showStoresInTable(response.data)
          });
      )
  )

  $('#pps-insert .btn-update').click(() ->
    tag_parent = $(this).parent()
    tag_from_date = tag_parent.children('input.from_date');
    tag_thru_date = tag_parent.children('input.thru_date');
    tag_store_id = tag_parent.find('select')
    id = tag_store_id.val()
    from_date = tag_from_date.val()
    thru_date = tag_thru_date.val()
    store_name = tag_store_id.find("option:selected").text()
    $.ajax({
      type: 'POST',
      url: '/product_promotions/insert_stores',
      async: true,
      jsonpCallback: 'jsonCallback',
      dataType: 'json',
      data: {id: tag_store_id.val(),from_date: from_date, thru_date: thru_date},
      beforeSend: () ->
        tag_parent.children('button').hide()
        tag_parent.append('<img src="/assets/img/loader5.gif" alt="">')
      success: (response) ->
        showMessage('Insert was successful...')
        tag_parent.children('img').remove()
        tag_parent.children('button').show()
        current_page = null;
        tag_pag = $('.dataTables_wrapper #dyntable_paginate .paginate-body-inner')
        active_page = null
        $.each(tag_pag.children(), (index, value) ->
          current_page++;
          if ($(value).attr('class') == 'paginate_active')
            active_page = index + 1
        )
        console.log(current_page + ' ' + response.page)
        if (current_page != null && current_page < response.page)
          tag_pag.append('<span class="paginate_button">
                      '+response.page+'
                    </span>')
        else if (current_page == null && response.page > 1)
          $('.dataTables_wrapper #dyntable_paginate').append('
                <span class="first paginate_button" id="dyntable_first">
                  First
                </span>
                <span class="previous paginate_button" id="dyntable_previous">
                  Previous
                </span>
                <span class="paginate-body">
                  <span class="paginate-body-inner">
                    <span class="paginate_active">
                      1
                    </span>
                    <span class="paginate_button">
                      2
                    </span>
                  </span>
                </span>
                <span class="next paginate_button" id="dyntable_next">
                  Next
                </span>
                <span class="last paginate_button" id="dyntable_last">
                  Last
                </span>')
        else if (active_page == current_page)
          $('.stdtable > tbody').append('
                  <tr id="prstid-'+response.data.id+'" ref="">
                    <td class="center">
                      <span>
                        <span class="checkbox"><input type="checkbox"></span>
                      </span>
                    </td>
                    <td>
                      <span>
                        '+store_name+'
                      </span>
                    </td>
                    <td class="wrap-content">
                      <span>
                        '+thru_date+'
                      </span>
                    </td>
                    <td class="center">
                      <span>
                        <input class="datepicker" value="'+from_date+'">
                      </span>
                    </td>
                    <td class="center">
                      <span>
                        <input class="ip-sequence" value="0">
                      </span>
                    </td>
                    <td class="center">
                      <span>
                        <button class="stdbtn btn-update" style="opacity: 1;">
                          Update
                        </button>
                      </span>
                    </td>
                  </tr>')

      error: () ->
        showMessage('Insert was not successful', 'error')
    });
  )

  $(document).on('click', '#lpp-store .btn-update', () ->
    tag_parent = $(this).parent()
    tag_tr = $(this).closest('tr')
    tag_input_sequence = tag_tr.find('.ip-sequence');
    tag_input_thru_date = $(this).closest('tr').find('.datepicker')
    value = tag_input_sequence.val()
    if (/^[0-9]+$/.test(value))
      $.ajax({
      type: 'POST',
      url: '/product_promotions/update_stores',
      async: true,
      jsonpCallback: 'jsonCallback',
      dataType: 'json',
      data: {id: tag_tr.attr('id').substr(7),thru_date: tag_input_thru_date.val(), sequence_num: value},
      beforeSend: () ->
        tag_parent.children('button').hide()
        tag_parent.append('<img src="/assets/img/loader5.gif" alt="">')
      success: (response) ->
        tag_parent.children('img').remove()
        tag_parent.children('button').show()
        tag_input_sequence.css('color', 'black')
        showMessage('Update was sucessful...')
      error: () ->
        showMessage('Update was not successful...', 'error')
      });
    else
      tag_input_sequence.css('color', 'red')
  )

  showMessage = (contain, type) ->
    if (type == undefined || type == 'success')
      type = 'msgsuccess'
    else
      type = 'msgerror'
    if ($('.messagebox').css('display') == 'none')
      $('.messageBox').html('<div class="notification ' + type + '">
                    <a class="close"></a>
                    <p>' + (contain) + '</p>
                  </div>')
      $('.messageBox').delay(500).fadeIn('normal', () ->
        $(this).delay(2500).fadeOut();
      );
    else
      $('.messageBox').find('p').text(contain)



(($) ->
  $.fn.paginate = (options) ->
    defaults = {
      callback: () ->
    }
    opts = $.extend(defaults, options);
    $(document).on('click', this.selector, () ->
      tag_pag_body = $(this).closest('#dyntable_paginate')
      tag_page_inner = tag_pag_body.find('.paginate-body-inner')
      pag_width = tag_page_inner.outerWidth(true) - parseInt(tag_page_inner.css('margin-left').substr(0, tag_page_inner.css('margin-left').length - 2))
      if ($(this).closest('.paginate-body').length != 0)
        opts.callback.call($(this), $(this).text())
        movement_pagination($(this), pag_width)
      else
        type = $(this).attr('class')
        if (type.indexOf('first') != -1)
          opts.callback.call($(this), 1);
          movement_pagination($('.paginate-body > .paginate-body-inner > span:first'), pag_width)
        else if (type.indexOf('last') != -1)
          opts.callback.call($(this), $('.paginate-body > .paginate-body-inner > span:last').text())
          movement_pagination($('.paginate-body > .paginate-body-inner > span:last'), pag_width)
        else if (type.indexOf('previous') != -1)
          number = parseInt($('.paginate-body > .paginate-body-inner > .paginate_active').text());
          if (number > 1)
            opts.callback.call($(this), number - 1)
            movement_pagination($('.paginate-body > .paginate-body-inner > .paginate_active').prev(), pag_width)
        else if (type.indexOf('next') != -1)
          number = parseInt($('.paginate-body > .paginate-body-inner > .paginate_active').text());
          last = parseInt($('.paginate-body > .paginate-body-inner > span:last').text());
          if (number < last)
            opts.callback.call($(this), number + 1)
            movement_pagination($('.paginate-body > .paginate-body-inner > .paginate_active').next(), pag_width)
    )
    movement_pagination = (obj, pag_width) ->
      parent_width = obj.closest('.paginate-body').outerWidth(true)
      if (pag_width >= parent_width)
        left = obj.position().left
        margin_left = obj.parent().css('margin-left')
        margin_left = parseInt(margin_left.substr(0, margin_left.length - 2))
        margin_left -= left - parent_width / 2
        if (left > 3 * parent_width / 4)
          if ((parent_width - pag_width) > margin_left)
            margin_left = parent_width - pag_width
          obj.parent().animate({"margin-left": margin_left + "px"})
        else if (left < parent_width / 4)
          if (margin_left > 0)
            margin_left = 0
          obj.parent().animate({"margin-left": margin_left + "px"})
    $.fn.paginate.remove_lastpage = (page) ->
      inner = $('.dataTables_wrapper #dyntable_paginate .paginate-body-inner')
      if (page < inner.children('span').length)
        if (inner.children('span').length > 1)
          inner.children('span:last').remove()
)(jQuery);