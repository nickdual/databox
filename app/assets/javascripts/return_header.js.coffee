$(document).ready ->
  $('#return_header_form > .stdform').validate(
    errorLabelContainer: $("div#content-messages")
    errorClass: "invalid"
    errorElement: "div"
    rules:
      'return_header[return_id]':
        required: true
        number: true
      'return_header[from_party_id]':
        required: true
        number: true
      'return_header[to_party_id]':
        required: true
        number: true
      'return_header[fin_account_id]':
        required: true
        number: true
      'return_header[origin_contact_mech_id]':
        required: true
        number: true
      'return_header[supplier_rma_id]':
        required: true
        number: true
    messages:
      'return_header[return_id]':
        required: "The field Return Header Type Id is required."
        number: "The field Return Header Type Id must be number."
      'return_header[from_party_id]':
        required: "The field From Party Id is required."
        number: "The field From Party Id must be number."
      'return_header[to_party_id]':
        required: "The field To Party Id is required."
        number: "The field To Party Id must be number."
      'return_header[fin_account_id]':
        required: "The field Fin Account Id is required."
        number: "The field Fin Account Id must be number."
      'return_header[origin_contact_mech_id]':
        required: "The field Origin Contact Mech Id is required."
        number: "The field Origin Contact Mech Id must be number."
      'return_header[supplier_rma_id]':
        required: "The field Supplier Rma Id is required."
        number: "The field Supplier Rma must be number."

  )

  #Return Headers
  fetch_return_header = (page, per) ->
    content = $('#dyntable_filter').children('input').val();
    type = $('#dyntable_filter').find('select').val();
    data = {page: page, per: per};
    if (content != '')
      data['type'] = type;
      data['content'] = content;
    $.ajax({
    type: 'POST',
    url: '/return_headers/return_headers_per_page',
    async: true,
    jsonpCallback: 'jsonCallback',
    dataType: 'json',
    data: data,
    beforeSend: () ->
      $('.stdtable > tbody').css('opacity', '0.25');
      width = $('.stdtable').parent().width()
      height = $('.stdtable').parent().height()
      $('.stdtable').prepend('<img src="/assets/loading_128.gif" style="position: absolute; left: '+(width/2-64)+'px; top: 128px" class="img-loading"/>');
    success: (response) ->
      $('.stdtable .img-loading').remove();
      $('.stdtable > tbody').empty();
      $('.stdtable > tbody').css('opacity', '1');
      showResults(response, page, per)
    });

  showResults = (response, page, per) ->
    if (per != undefined)
      $('#dyntable_paginate').empty();
      num = response.page;
      if (num > 1)
        $('#dyntable_paginate').append('<span class="first paginate_button" id="dyntable_first">'+I18n.t("common.first")+'</span><span class="previous paginate_button" id="dyntable_previous">'+I18n.t("common.previous")+'</span>')
        $('#dyntable_paginate').append('<span class="paginate-body" style="max-width: 256px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; vertical-align: middle;"><span class="paginate-body-inner"></span></span>')
        for i in [1..num]
          if (i == page)
            $('#dyntable_paginate > .paginate-body > .paginate-body-inner').append('<span class="paginate_active">'+i+'</span>');
          else
            $('#dyntable_paginate > .paginate-body > .paginate-body-inner').append('<span class="paginate_button">'+i+'</span>');
        $('#dyntable_paginate').append('<span class="next paginate_button" id="dyntable_next">'+I18n.t("common.next")+'</span><span class="last paginate_button" id="dyntable_last">'+I18n.t("common.last")+'</span>');
    else
      $('#dyntable_paginate > .paginate-body').children('.paginate-body-inner').children('.paginate_active').removeClass('paginate_active').addClass('paginate_button');
      $('#dyntable_paginate > .paginate-body').children('.paginate-body-inner').children('span:nth-child('+page+')').removeClass('paginate_button').addClass('paginate_active');
    if (response.data.length > 0)
      showInTable(response.data)
    else
      $('.stdtable > tbody').append('<tr><td colspan="6" style="text-align: center">Not Found Results</td></tr>');

  showInTable = (data) ->
    if (data == undefined || data == null || data.length == 0)
      return
    $.each(data, (index, value) ->
      $('.stdtable > tbody').append('
        <tr id="rthdr-' + value['id'] + '" ref="/return_headers/' + value['id'] + '/edit">
                <td class="center">
                  <span>
                    <span class="checkbox"><input type="checkbox"></span>
                  </span>
                </td>
                <td class="center">
                  <span>
                    ' + value['id'] + '
                  </span>
                </td>
                <td>
                  <span>
                    ' + value['return_header_type_id'] + '
                  </span>
                </td>
                <td>
                  <span>
                    ' + value['status_id'] + '
                  </span>
                </td>
                <td class="center">
                  <span>
                    ' + value['from_party_id'] + '
                  </span>
                </td>
                <td class="center">
                  <span>
                    ' + value['to_party_id'] + '
                  </span>
                </td>
                <td class="center">
                  <span>
                    ' + value['payment_method_id'] + '
                  </span>
                </td>
                <td class="center">
                  <span>
                    ' + value['fin_account_id'] + '
                  </span>
                </td>
                <td class="center">
                  <span>
                    ' + value['billing_account_id'] + '
                  </span>
                </td>
                <td class="wrap-content center">
                  <span>
                    ' + value['entry_date'] + '
                  </span>
                </td>
                <td class="center">
                  <span>
                    ' + value['origin_contact_mech_id'] + '
                  </span>
                </td>
                <td class="center">
                  <span>
                    ' + value['destination_facility_id'] + '
                  </span>
                </td>
                <td class="center">
                  <span>
                    ' + value['needs_inventory_receive'] + '
                  </span>
                </td>
                <td class="center">
                  <span>
                    ' + value['currency_uom_id'] + '
                  </span>
                </td>
                <td class="center">
                  <span>
                    ' + value['supplier_rma_id'] + '
                  </span>
                </td>
              </tr>')
    )
  searchPress = (content, type) ->
    if (type == undefined)
      type = $('select.search-content').val()
    $.ajax({
    type: 'POST',
    url: '/return_headers/return_headers_per_page',
    async: true,
    jsonpCallback: 'jsonCallback',
    dataType: 'json',
    data: {type: type, content: content},
    beforeSend: () ->
      $('.stdtable > tbody').css('opacity', '0.25');
      width = $('.stdtable').parent().width()
      height = $('.stdtable').parent().height()
      $('.stdtable').prepend('<img src="/assets/loading_128.gif" style="position: absolute; left: '+(width/2-64)+'px; top: 128px" class="img-loading"/>');
    success: (response) ->
      $('.stdtable .img-loading').remove();
      $('.stdtable > tbody').empty();
      $('.stdtable > tbody').css('opacity', '1');
      if (response.data.length == 0)
        if (type == 'id')
          $('#dyntable_paginate').empty();
          $('.stdtable > tbody').empty();
          $('.stdtable > tbody').css('opacity', '1');
          $('.stdtable > tbody').append('<tr><td colspan="6" style="text-align: center">Not Found Results <a href="/return_headers/new?id='+content+'">Want to Create?</a></td></tr>');
        else
          showResults(response, 1, response.per)
      else
        showResults(response, 1, response.per);
    });

  $('#lists-rtnhdr .btn-search').click(() ->
    value = $(this).parent().find('.search-content').children('option[selected="selected"]').attr('value')
    searchPress($(this).parent().children('input').val())
  )

  $('#lists-rtnhdr #promotions-per-page').change(() ->
    per = $(this).val();
    fetch_return_header(1, per);
  )

  $('#lists-rtnhdr #btn-delete').click(() ->
    list = []
    $.each($('.stdtable > tbody').find('span.checkbox input[type=checkbox][checked=checked]'), (index, value) ->
      tr = $(value).closest('tr');
      list.push(tr.attr('id').substr(6));
    );
    if (list.length > 0)
      jConfirm(I18n.t('common.dialog.confirm.content'), I18n.t('common.dialog.confirm.title'), (r) ->
        if (r == true)
          $.each($('.stdtable > tbody').find('span.checkbox input[type=checkbox][checked=checked]'), (index, value) ->
            tr = $(value).closest('tr');
            tr.remove();
          );
          page_active = $('.dataTables_wrapper #dyntable_paginate .paginate-body-inner > .paginate_active').text()
          $.ajax({
          type: 'POST',
          url: '/return_headers/delete_return_headers',
          async: true,
          jsonpCallback: 'jsonCallback',
          dataType: 'json',
          data: {list: list, page: page_active, content: $('#dyntable_filter > input').val(), type: $('#dyntable_filter .search-content').val()},
          success: (response) ->
            $.fn.paginate.remove_lastpage(response.page)
            showInTable(response.data)
          });
      )
  )

  $('#lists-rtnhdr #promo-search').keyup((e) ->
    content = $(this).val()
    if (e.which == 13)
      searchPress(content)
    else
      return;
      $.each($('.stdtable > tbody > tr'), (index, value) ->
        if ($(value).children('td:nth-child(3)').text().indexOf(content) == -1)
          $(value).hide()
        else
          $(value).show()
      );
  )
  if $('#lists-rtnhdr .paginate_button').length > 0
    $('#lists-rtnhdr .paginate_button').paginate({callback: fetch_return_header})

  $('#dyntable_filter .search-content').change(() ->
    value = $(this).children('option[selected="selected"]').text()
    $('.search-select > div').text(value);
    value = $(this).children('option[selected="selected"]').attr('value')
    if (value == 'user_entered' || value == 'imported_only')
      $(this).closest('.dataTables_filter').children('input').css('display', 'none')
      $(this).closest('.dataTables_filter').children('.search-select').css({'position': 'static', 'margin-right': '28px'})
    else
      $(this).closest('.dataTables_filter').children('input').css('display', 'block')
      $(this).closest('.dataTables_filter').children('.search-select').css({'position': 'absolute', 'margin-right': '0'})
    $('#dyntable_filter > input').css('padding-left', ($('.search-select').outerWidth(true) + 4)+ 'px');
  )