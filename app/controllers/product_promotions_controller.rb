class ProductPromotionsController < ApplicationController
  before_filter :authenticate_user!
  layout "product_promotions"

  # GET /product_promotions
  # GET /product_promotions.json
  def index
    session[:promo_page] = 10 if session[:promo_page].blank?
    session.delete(:product_promotion_id)
    @product_promotions = ProductPromotion.paginate(:page => 1, :per_page => session[:promo_page])
    @total = ProductPromotion.count
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @product_promotions }
    end
  end

  # GET /product_promotions/1
  # GET /product_promotions/1.json
  def show
    @product_promotion = ProductPromotion.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @product_promotion }
    end
  end

  # GET /product_promotions/new
  # GET /product_promotions/new.json
  def new
    @product_promotion = ProductPromotion.new
    #str = ''
    #@promotion_id = ' '
    #if params[:promotion_id].present?
    #  str = params[:promotion_id]
    #  @promotion_id = ProductPromotion.check_id(str)
    #end
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @product_promotion }
    end
  end

  # GET /product_promotions/1/edit
  def edit
    session[:product_promotion_id] = params[:id]
    @product_promotion = ProductPromotion.find(params[:id])

  end

  # POST /product_promotions
  # POST /product_promotions.json
  def create
    @product_promotion = ProductPromotion.create(params[:product_promotion])
    @product_promotion.created_by_user_login = current_user
    @product_promotion.last_modified_by_user_login = current_user
    @product_promotion.update_attribute(:user_id, current_user.id) if @product_promotion.user_id.blank?
    respond_to do |format|
      if @product_promotion.save
        format.html { redirect_to edit_product_promotion_path(@product_promotion.id), notice: 'Product promotion was successfully created.'  }
        format.json { render json: @product_promotion, status: :created, location: @product_promotion }
      else
        format.html { render action: "new" }
        format.json { render json: @product_promotion.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /product_promotions/1
  # PUT /product_promotions/1.json
  def update
    @product_promotion = ProductPromotion.find(params[:id])
    respond_to do |format|
      if @product_promotion.update_attributes(params[:product_promotion])
        format.html { redirect_to  '/product_promotions', notice: 'Product promotion was successfully updated.'}
        format.json { render json: @product_promotion }
      else
        format.html { render action: "edit" }
        format.json { render json: @product_promotion.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_promotions/1
  # DELETE /product_promotions/1.json
  def destroy
    @product_promotion = ProductPromotion.find(params[:id])
    @product_promotion.destroy

    respond_to do |format|
      format.html { redirect_to product_promotions_url }
      format.json { head :no_content }
    end
  end

  def promotions_per_page
    session[:promo_page] = params['per'] if !params['per'].blank?
    params[:page] = 1 if params[:page].blank?

    @product_promotions = ProductPromotion.simple_search(params)
    @product_promotion = @product_promotions.paginate(:page => params['page'], :per_page => session[:promo_page])
    render json: {"data" => @product_promotion, "page" => (@product_promotions.count / session[:promo_page].to_f).ceil, "per" => session[:promo_page]}
  end

  def delete_promotions
    session[:promo_page] = params['per'] if !params['per'].blank?
    params[:page] = 1 if params[:page].blank?

    ProductPromotion.destroy_all(["id IN (?)", params['list']])
    length = params['list'].length
    @product_promotions = ProductPromotion.simple_search(params)
    @product_promotion = @product_promotions.paginate(:page => params['page'], :per_page => session[:promo_page])

    render json: {data: @product_promotion[(session[:promo_page].to_i - length)..(session[:promo_page].to_i)], page: (@product_promotions.count / session[:promo_page].to_f).ceil}
  end

  def stores
    session[:promo_page] = 10 if session[:promo_page].nil?
    @promotion_stores = PromotionStore.joins(:product_store).select("product_stores.store_name, promotion_stores.*").where(:product_promotion_id => session[:product_promotion_id]).paginate(:page => 1, :per_page => session[:promo_page])
    @stores = ProductStore.select("id, store_name")
    @total = @promotion_stores.count
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @promotion_stores }
    end
  end

  def stores_per_page
    session[:promo_page] = params['per'] if !params['per'].nil?
    params[:page] = 1 if params[:page].nil?

    params[:product_promotion_id] = session[:product_promotion_id]
    @promotion_stores = PromotionStore.simple_search(params)
    @promotion_store_temp = @promotion_stores.paginate(:page => params['page'], :per_page => session[:promo_page])
    @promotion_store = []
    @promotion_store_temp.each do |promotion_store|
      thru_date = ''
      from_date = ''
      thru_date = promotion_store['thru_date'].strftime("%Y-%m-%d %H:%M:%S:%L") if !promotion_store['thru_date'].blank?
      from_date = promotion_store['from_date'].strftime("%Y-%m-%d %H:%M:%S:%L") if !promotion_store['from_date'].blank?
      item = ActiveSupport::JSON.decode(promotion_store.to_json)
      item['thru_date'] = thru_date
      item['from_date'] = from_date
      @promotion_store.push(item)
    end
    render json: {"data" => @promotion_store, "page" => (@promotion_stores.count / session[:promo_page].to_f).ceil, "per" => session[:promo_page]}
  end

  def update_stores
    store = PromotionStore.find(params["id"])
    store.update_attributes({'thru_date' => params['thru_date'], 'sequence_num' => params['sequence_num']}) if !store.blank?
    render json: params
  end

  def insert_stores
    session[:promo_page] = params['per'] if !params['per'].nil?
    store = PromotionStore.new({"product_store_id" => params['id'], "from_date" => params['from_date'], "thru_date" => params['thru_date'], "sequence_num" => 0, "product_promotion_id" => session[:product_promotion_id]})
    if (store.save)
      render json: {"data" => store, "page" => (PromotionStore.where("product_promotion_id" => session[:product_promotion_id]).count / session[:promo_page].to_f).ceil, "per" => session[:promo_page]}
    else
      render json: {}
    end
  end

  def delete_stores
    session[:promo_page] = params['per'] if !params['per'].blank?
    params[:page] = 1 if params[:page].blank?

    PromotionStore.destroy_all(["id IN (?)", params['list']])
    length = params['list'] ? params['list'].length  : 0
    params[:product_promotion_id] = session[:product_promotion_id]
    @stores = PromotionStore.simple_search(params)
    @store = @stores.paginate(:page => params['page'], :per_page => session[:promo_page])

    render json: {data: PromotionStore.standard_data(@store[(session[:promo_page].to_i - length)..(session[:promo_page].to_i)]), page: (@stores.count / session[:promo_page].to_f).ceil}
  end

end
