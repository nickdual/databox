#Controller for optional static pages.
class PagesController < ApplicationController
  def error
    #render :layout  => "error"
  end

  def facility_inventory
    p params[:facility_id]
  end


  def edit_facility

  end

  def facility_selection
    @facility = Facility.all
  end

  def verify_pick

  end


  def receive_return
    p params[:return_id]
  end

  def receive_inventory
  end

  def pick_move_stock
  end

  def picklist_options
  end

  
end
