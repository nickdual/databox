class OrdersController < ApplicationController
  before_filter :authenticate_user!

  def index
     #render all orders page on index
  end

  def new
    @stores = ProductStore.all
    @saleschannels  = SalesChannel.all

  end

  def create

  end

  # Show order details.
  def show
    #  Not in use.
  end

  def view
    # Just render a view for sample order. For now.
  end

  # Action to find returns of the orders.
  def find_return
     # Currently, this action will just render a view for find-return
  end

end