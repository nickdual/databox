class CustomersController < ApplicationController

	before_filter :authenticate_user!

	layout "customers"
	
	NUMBER_OF_ROWS_ON_PAGE = 15

	def index
		if params[:search_order].blank?
			@orders = OrderHeader.all
		else
			
			@orders = if params[:search_order]
				OrderHeader.joins(:person).where("people.first_name LIKE (?) OR people.last_name LIKE (?) OR order_id = ?", "%#{params[:search_order]}%", "%#{params[:search_order]}%", params[:search_order]) 
		  end
				name = params[:search_order].split(" ")

				person = if(name.size.eql?(1))
					puts name
					Person.where("first_name LIKE (?) OR last_name LIKE (?)", "%#{params[:search_order]}%", "%#{params[:search_order]}%" ).map(&:party_id)
				else
					Person.where("first_name LIKE (?) AND last_name LIKE (?)", "%#{name[0]}%", "%#{name[1]}%").map(&:party_id)
				end

				unless person.empty?
					order_part = OrderPart.where("customer_party_id IN (?)", person).map(&:order_id).uniq
					OrderHeader.where("order_id IN (?)", order_part)
				else
					OrderHeader.where("order_id = ?", params[:search_order])
				end
			end

		end
	
	def admin_customers
	  starting_index = 0
	  returned_data = customers(starting_index, nil)
	  searchvalue = ''
	  
	  @rows_count = returned_data[:total_count]
	  @customers = returned_data[:customers]
	  @first_index = 1
    @last_index = last_index(@rows_count, starting_index)
    
    render "customers/_admin_customers", :locals => {:rows_count => @rows_count, :customers => @customers, :first_index => @first_index, :last_index => @last_index, :searchvalue => searchvalue}
	end
	
	def next_page
	  starting_index = params[:lastindex].to_i
	  issearch = params[:issearch]
	  searchvalue = params[:searchvalue]
	  if issearch === true
	    returned_data = customers(starting_index, searchvalue)
	  else
	    returned_data = customers(starting_index, nil)
	  end
	  
	  @rows_count = returned_data[:total_count]
    @customers = returned_data[:customers]
    @first_index = starting_index + 1
    @last_index = last_index(@rows_count, starting_index)
    @issearch = params[:issearch]
    @render = render_to_string :partial => "customers/admin_customers", :locals => {:rows_count => @rows_count, :customers => @customers, :first_index => @first_index, :last_index => @last_index, :issearch => @issearch, :searchvalue => searchvalue}
    render :inline => @render
	end
	
	def previous_page
    starting_index = params[:firstindex].to_i - NUMBER_OF_ROWS_ON_PAGE - 1
    issearch = params[:issearch]
    searchvalue = params[:searchvalue]
    if issearch === true
      returned_data = customers(starting_index, searchvalue)
    else
      returned_data = customers(starting_index, nil)
    end
    
    @rows_count = returned_data[:total_count]
    @customers = returned_data[:customers]
    @first_index = starting_index + 1
    @last_index = last_index(@rows_count, starting_index)
    @issearch = params[:issearch]
    @render = render_to_string :partial => "customers/admin_customers", :locals => {:rows_count => @rows_count, :customers => @customers, :first_index => @first_index, :last_index => @last_index, :issearch => @issearch, :searchvalue => searchvalue}
    render :inline => @render
  end
  
  def search_customer
    starting_index = 0
    searchvalue = params[:searchvalue]
    returned_data = customers(starting_index, searchvalue)
    
    @rows_count = returned_data[:total_count]
    @customers = returned_data[:customers]
    @first_index = 1
    @last_index = last_index(@rows_count, starting_index)
    @issearch = true
    render "customers/_admin_customers", :locals => {:rows_count => @rows_count, :customers => @customers, :first_index => @first_index, :last_index => @last_index, :issearch => @issearch, :searchvalue => searchvalue}
  end
	
	def admin_customers_detail
	  
	  render "customers/_admin_customers_detail"
	end
	
	private
  	def customers(starting_index, searchvalue)
  	  @customers = Array.new
      partyidowm = '34'
      if searchvalue
        #all_customers = Person.select('*').where("(first_name LIKE :search) or (last_name LIKE :search) or (concat(last_name, ',', first_name) LIKE :search)", :search => "%#{searchvalue}%")
        all_customers = Person.select('*').where("(first_name LIKE :search) or (last_name LIKE :search)", :search => "%#{searchvalue}%")
      else
        all_customers = Person.select('*').joins(:party)
      end
      
      @rows_count = all_customers.count
      
      all_customers.limit(NUMBER_OF_ROWS_ON_PAGE).offset(starting_index).each do |customer|

        total_order = 0
        customer.party.customer_order_parties.where("order_parts.vendor_party_id = ?", partyidowm).each do |order_party|
          logger.info order_party.order_header.status.status_id
          if order_party.order_header.status.status_id != 'ORDER_REJECTED' and order_party.order_header.status.status_id != 'ORDER_CANCELLED'
            total_order += order_party.order_header.grand_total
          else
            logger.info "Can't sum"
          end
        end
        
        # email = customer.party.party_contact_meches.find(:first, :conditions => ["contact_mechanism_types.name = 'EMAIL_ADDRESS'"],  :joins => [:contact_mech => :contact_mech_type], :select => "contact_meches.info_string").value
        # address = customer.party.party_contact_meches.find(:first, :conditions => ["contact_mechanism_types.name = 'POSTAL_ADDRESS'"],  :joins => [:contact_mech => :contact_mech_type], :select => "contact_meches.info_string").value
        # phone = customer.party.party_contact_meches.find(:first, :conditions => ["contact_mechanism_types.name = 'TELECOM_NUMBER'"],  :joins => [:contact_mech => :contact_mech_type], :select => "contact_meches.info_string").value

        element = {
          :customer_name => customer.first_name + ' ' + customer.last_name,
          :marketplace => 'TODO',
          :store => 'TODO',
          :email => 'TODO',
          :address => 'TODO',
          :phone => 'TODO',
          :total_ordered => total_order
        }
        
        @customers<<element
      end
      
      return {:customers =>@customers, :total_count => @rows_count}
  	end
  	
  	def last_index(rows_count, starting_index)
  	  if rows_count < starting_index + NUMBER_OF_ROWS_ON_PAGE
        last_index = rows_count
      else
        last_index = starting_index + NUMBER_OF_ROWS_ON_PAGE
      end
    end
end
