class FactoriesController < ApplicationController
	before_filter :authenticate_user!

	layout "factories"
	
	def index
  #  Where can I get data for Factories?
  end

  def all_factories
    #@factories = OrderHeader.all
    if params[:search_order].blank?
    	@factories = OrderHeader.paginate(:page => params[:page], :per_page => 25)
    else
    	@factories = if params[:search_order]
    		organization = Organization.where("organization_name LIKE (?)", "%#{params[:search_order]}%").map(&:party_id)
        unless organization.empty?
          order_part = OrderPart.where("customer_party_id IN (?)", organization).map(&:order_id).uniq
          OrderHeader.where("order_id IN (?)", order_part).paginate(:page => params[:page], :per_page => 25)
        else
    			OrderHeader.where("order_id = ?", params[:search_order]).paginate(:page => params[:page], :per_page => 25)
    		end
    	end
    end
  end


	def show
	end

  def edit
  end

  def new
  end

  def create
  end

  def factory_detail
  	status = OrderItemStatus.where("description IN (?)", ["ORDER_REJECTED", "ORDER_CANCELLED"]).map(&:status_id)
  	party = Party.find(params[:party_id]) rescue nil
    unless party.nil?
      @organization = Organization.where("party_id = ?", params[:party_id]).first
      order = party.order_part_parties.pluck(:order_id)
      @factory = OrderItem.where("order_id IN (?) AND status_id NOT IN (?)", order, status)
    else
      redirect_to all_factories_path, notice: 'Sorry this party_id is not found'
    end
  	
  end

end
