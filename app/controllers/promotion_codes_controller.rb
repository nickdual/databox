class PromotionCodesController < ApplicationController
  # GET /promotion_codes
  # GET /promotion_codes.json
  before_filter :authenticate_user!
  layout "product_promotions"
  def index
    session[:promo_page] = 10 if session[:promo_page].nil?

    @promotion_codes = PromotionCode.where(:product_promotion_id => session[:product_promotion_id]).paginate(:page => 1, :per_page => session[:promo_page])
    @total = PromotionCode.count

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @promotion_codes }
    end
  end

  # GET /promotion_codes/1
  # GET /promotion_codes/1.json
  def show
    @promotion_code = PromotionCode.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @promotion_code }
    end
  end

  # GET /promotion_codes/new
  # GET /promotion_codes/new.json
  def new
    @promotion_code = PromotionCode.new
    @product_promotion_id_collection = ProductPromotion.select("id, promotion_name")
    str = ''
    @product_promo_code_id=' '
    if params[:product_promotion_code_id].present?
      str = params[:product_promotion_code_id]
      @product_promo_code_id = PromotionCode.check_id(str)
    end
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @promotion_code }
    end
  end

  # GET /promotion_codes/1/edit
  def edit
    @promotion_code = PromotionCode.find(params[:id])
    @product_promo_code_id = @promotion_code.product_promotion_code_id
    @product_promotion_id_collection = ProductPromotion.select("id, promotion_name")
    @user_role_create = RolesUser.joins(:role).where("user_id=#{@promotion_code.created_by_user_login.id}").select("roles.name").first
    @user_role_modified = RolesUser.joins(:role).where("user_id=#{@promotion_code.last_modified_by_user_login.id}").select("roles.name").first
    @name_create = (@user_role_create.blank?) ? @promotion_code.created_by_user_login.email : @user_role_create.name
    @name_modified = (@user_role_modified.blank?) ? @promotion_code.last_modified_by_user_login.email : @user_role_modified.name
    @promotion_code_emails = @promotion_code.promotion_code_emails
    @promotion_code_parties = @promotion_code.promotion_code_parties
    respond_to do |format|
      format.html # edit.html.erb
      format.json { render json: @promotion_code }
    end
  end

  # POST /promotion_codes
  # POST /promotion_codes.json
  def create
    uuid = UUID.new
    params[:promotion_code][:product_promotion_code_id] = params[:product_promotion_code_id].present? ? params[:product_promotion_code_id] : uuid.generate
    params[:promotion_code][:product_promotion_id] = session[:product_promotion_id]
    @promotion_code = PromotionCode.new(params[:promotion_code])
    @promotion_code.created_by_user_login = current_user
    @promotion_code.last_modified_by_user_login = current_user
    respond_to do |format|
      if @promotion_code.save
        format.html { redirect_to edit_promotion_code_path(@promotion_code.id), notice: 'Promotion code was successfully created.' }
        format.json { render json: @promotion_code, status: :created, location: @promotion_code }
      else
        format.html { render action: "new" }
        format.json { render json: @promotion_code.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /promotion_codes/1
  # PUT /promotion_codes/1.json
  def update
    @promotion_code = PromotionCode.find(params[:id])

    respond_to do |format|
      if @promotion_code.update_attributes(params[:promotion_code])
        @promotion_code.last_modified_by_user_login = current_user
        @promotion_code.save
        format.html { redirect_to '/promotion_codes', notice: 'Promotion code was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @promotion_code.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /promotion_codes/1
  # DELETE /promotion_codes/1.json
  def destroy
    @promotion_code = PromotionCode.find(params[:id])
    @promotion_code.destroy

    respond_to do |format|
      format.html { redirect_to promotion_codes_url }
      format.json { head :no_content }
    end
  end

  # ADD promotion code email  /promotion_codes/add_email
  def add_email
    @promotion_code_email = PromotionCodeEmail.new(params[:promotion_code_email])
    respond_to do |format|
      if @promotion_code_email.save
        format.json { render json: @promotion_code_email.to_json}
      else
        format.json { render json: @promotion_code_email.errors}
      end
    end
  end


  # ADD promotion code email  /promotion_codes/add_email
  def add_party
    @promotion_code_party = PromotionCodeParty.new(params[:promotion_code_party])
    respond_to do |format|
      if @promotion_code_party.save
        format.json { render json: @promotion_code_party}
      else
        format.json { render json: @promotion_code_party.errors}
      end
    end
  end

  def delete_email
    @promotion_code_email = PromotionCodeEmail.find(params[:id])
    @promotion_code_email.destroy

    respond_to do |format|

      format.json { head :no_content }
    end
  end

  def check_party_id
    p_id = ''
    p_id = params[:check_party_id].to_i
    if Party.where(:id => p_id).count != 0
      render :text => 'true'
    else
      render :text => 'false'
    end

  end

  def delete_party
    @promotion_code_party = PromotionCodeParty.find(params[:id])
    @promotion_code_party.destroy

    respond_to do |format|

      format.json { head :no_content }
    end
  end
  def mass_insert
    if request.post?
      str = nil
      if (params['user_entered'].nil?)
        params['user_entered'] = false
      else
        params['user_entered'] = true
      end
      if (params['require'].nil?)
        params['require'] = false
      else
        params['require'] = true
      end
      if (params['code_length'].present? && params['code_length'].to_i > 0)
        params['quantity'].to_i.times do
          str = PromotionCode.random_string(params['code_layout'], params['code_length'].to_i, str)
          PromotionCode.create({"product_promotion_code_id" => str, "user_entered" => params['user_entered'], "require_email_or_party" => params['require'],
                                "use_limit_per_code" => params['limit_per_code'], "use_limit_per_customer" => params['limit_per_customer'], "product_promotion_id" => session[:product_promotion_id], "created_by_user_login" => current_user, "last_modified_by_user_login" => current_user})
          flash[:success] = "success created!"
        end
      else
        flash[:error] = "error in created!"
      end
    end
  end

  def promotion_codes_per_page
    session[:promo_page] = params['per'] if !params['per'].nil?
    params[:page] = 1 if params[:page].nil?
    params[:product_promotion_id] = session[:product_promotion_id]
    @promotion_codes = PromotionCode.simple_search(params)
    @promotion_code = @promotion_codes.paginate(:page => params['page'], :per_page => session[:promo_page])
    render json: {"data" => @promotion_code, "page" => (@promotion_codes.count / session[:promo_page].to_f).ceil, "per" => session[:promo_page]}
  end

  def delete_promotion_codes
    session[:promo_page] = params['per'] if !params['per'].nil?
    params[:page] = 1 if params[:page].nil?
    length = params['list'].present? ? params['list'].length : 0
    PromotionCode.destroy_all(["id IN (?)", params['list']])
    params[:product_promotion_id] = session[:product_promotion_id]
    @promotion_codes = PromotionCode.simple_search(params)
    @promotion_code = @promotion_codes.paginate(:page => (params['page'].present? ? params['page'].to_i : 1), :per_page => session[:promo_page])

    render json: {data: @promotion_code[(session[:promo_page].to_i - length)..(session[:promo_page].to_i)], page: (@promotion_codes.count / session[:promo_page].to_f).ceil}
  end

  def check_promotion_code_party_id

  end

end
