class FacilitiesController < ActionController::Base

   layout '_facility'

   def main
      @facility_names = Facility.get_all_names
   end

   def edit
      facility_id = params[:id] if defined?( params[:id] )

      @facility = Facility.get_one( facility_id )
   end
   
   def update
      
   end
   
   def edit_facility_inventory_item
      @facility = Facility.first
   end
   
end