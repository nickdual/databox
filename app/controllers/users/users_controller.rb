class Users::UsersController < Devise::RegistrationsController
  before_filter :authenticate_user!
  load_and_authorize_resource
  layout 'parties'
  def index
    if params[:filter]
      @users = User.where("email LIKE '%#{params[:filter]}%'").page(params[:page]).per_page(20)
    else
      @users  = User.order("id").page(params[:page]).per_page(20)
    end
    @roles = Role.all
  end

  def adduser
    user = User.create(params[:user])
    if user.id
      roles = params[:roles]
      if roles
        roles.each do |role|
          user.add_role(role)
        end
      end
      flash[:success] = "Party has been successfully added to the system."
    else
      flash[:error] = "Party could not be added to the system."
    end
    redirect_to party_path
  end

  def status
     @users = User.all
     @roles = Role.all
  end

  def update_user
      party  = User.find(params[:user][:userid])
      party.update_attributes(:approval=>true)
      flash[:notice] = "Party has been authenticated!"
      redirect_to part_account_status_path
  end

  def assign_role
    party = User.find(params[:user][:userid])
    role  = params[:user][:role]
    party.add_role(role)
    redirect_to party_account_status_path
  end
  def check_email
    email = params[:email]
    user = User.where({:email => email}).first
    if user.blank?
      render :text => false
    else
      render :text => true
    end
  end
end