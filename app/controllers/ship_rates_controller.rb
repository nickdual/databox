require 'will_paginate/array'

class ShipRatesController < ApplicationController
  layout "product_promotions"
  before_filter :authenticate_user!

  def index
    #@countries = Country.all
    session[:promo_page] = 10 if session[:promo_page].blank?
    @ships = Shipment.paginate(:page => 1, :per_page => session[:promo_page])
    @total = Shipment.count
  end
  def create
    render json: { :text => :ok }
  end
  def get_for_country
    type = params[:type]
    id = params[:id]
    if (type == 'country')
      render json: {:data => nil}
    elsif (type == 'city')
    end
  end

  def get_rates
    weight = params[:weight].to_f
    dimension = {}
    dimension['length'] = params[:length].to_f
    dimension['width'] = params[:width].to_f
    dimension['height'] = params[:height].to_f
    starting_point = params[:starting_point]
    ending_point = params[:ending_point]

    country_start = Country.where('lower(name) like ?', starting_point['country_code'].downcase).first
    if (country_start.blank?)
      render json: {:data => nil, :text => I18n.t('ship_rate.invalid_starting_country')}
      return
    end
    #if (City.where('lower(ascii_name) like ? AND country_iso_code_two_letters = ?', starting_point['city_name'].downcase, country_start['iso_code_two_letter']).count == 0)
    #  render json: {:data => nil, :text => I18n.t('ship_rate.invalid_starting_city')}
    #  return
    #end

    country_end = Country.where('lower(name) like ?', ending_point['country_code'].downcase).first
    if (country_end.blank?)
      render json: {:data => nil, :text => I18n.t('ship_rate.invalid_ending_country')}
      return
    end
    #if (City.where('lower(ascii_name) like ? AND country_iso_code_two_letters = ?', ending_point['city_name'].downcase, country_end['iso_code_two_letter']).count == 0)
    #  render json: {:data => nil, :text => I18n.t('ship_rate.invalid_ending_city')}
    #  return
    #end

    pd1 = PostalAddress.create({:access_code => country_start['iso_code_two_letter'], :city => starting_point['city_name'], :postal_code => starting_point['postcode']})
    pd2 = PostalAddress.create({:access_code => country_end['iso_code_two_letter'], :city => ending_point['city_name'], :postal_code => ending_point['postcode']})
    shipment = Shipment.create({:origin_contact_mech_id => pd1[:id], :destination_contact_mech_id => pd2[:id]})
    ShipmentPackage.create({:shipment_id => shipment[:id], :box_length => dimension['length'], :box_height => dimension['height'], :box_width => dimension['width'], :weight => weight})

    render json: {"data" => {:id => shipment.id, :country_start_code => country_start['iso_code_two_letter'], :country_end_code => country_end['iso_code_two_letter']},"page" => (Shipment.count / session[:promo_page].to_f).ceil, "per" => session[:promo_page]}
  end

  def get_for_city_autocomplete
    if (params[:type].blank? || params[:type] == 'code')
      country_code = {:iso_code_two_letter => params[:country_code]}
    else
      country_code = params[:country_code]
      country = Country.where({:name => country_code}).first
      if (country.blank?)
        render json: {:data => nil}
        return
      end
    end
    content = params[:content].downcase
    cities = City.where('lower(ascii_name) like ? AND country_iso_code_two_letters = ?', '%' + content + '%', country.iso_code_two_letter).order('cities.ascii_name ASC').select('cities.id, cities.name, cities.ascii_name').limit(50)
    render json: {:data => cities}
  end

  def get_for_country_autocomplete
    if (params['type'].blank?)
      content = params[:content].downcase
      countries = Country.where('lower(name) like ?', '%' + content + '%').order('countries.name ASC').select('countries.id, countries.name, countries.iso_code_two_letter').limit(50)
      render json: {:data => countries}
    else
      content = params[:content].downcase
      countries = Country.where('lower(name) like ?', content).count
      render json: {:data => countries}
    end
  end

  def get_quote

    shipment = Shipment.find(params[:id])
    ship_package = shipment.shipment_package
    starting_point = shipment.origin_contact_mech
    ending_point = shipment.destination_contact_mech
    packages = [Package.new(ship_package['weight'] * 1000, [ship_package['box_length'] / 100, ship_package['box_width'] / 100, ship_package['box_height'] / 100], :units => :metric)]

    # You live in Beverly Hills, he lives in Ottawa
    origin = Location.new(      :country => starting_point['access_code'],
                                :city => starting_point['city'],
                                :postal_code => starting_point['postal_code'])

    destination = Location.new( :country => ending_point['access_code'],
                                :city => ending_point['city'],
                                :postal_code => ending_point['postal_code'])

    fedex = FedEx.new(:login => ENV['FEDIX_LOGIN'], :password => ENV['FEDIX_PASSWORD'], :key => ENV['FEDIX_KEY'],
                      :account => ENV['FEDIX_ACCOUNT'], :meter => ENV['FEDIX_METER'], :test => true)
    response = fedex.find_rates(origin, destination, packages)
    ups_rates = response.rates.sort_by(&:price).collect {|rate| [rate.carrier, rate.currency, rate.service_name, rate.total_price, rate.delivery_date, rate.delivery_range]}
    render json: {
        :data => ups_rates,
        :location => {
            :starting_point => {:country => starting_point['access_code'], :city => starting_point['city'], :postal_code => starting_point['postal_code']},
            :ending_point => {:country => ending_point['access_code'], :city => ending_point['city'], :postal_code => ending_point['postal_code']}
        },
        :weight => ship_package['weight'],
        :dimension => {
            :length => ship_package['box_length'],
            :width => ship_package['box_width'],
            :height => ship_package['box_height'],
        }
    }

  end

  def shipment_per_page
    session[:promo_page] = params['per'] if !params['per'].blank?
    params[:page] = 1 if params[:page].blank?
    params[:promo_page] = session[:promo_page]
    result = Shipment.simple_search(params)
    render json: {"data" => result[:data], "page" => (result[:count] / session[:promo_page].to_f).ceil, "per" => session[:promo_page]}
  end

  def delete_shipment
    session[:promo_page] = params['per'] if !params['per'].blank?
    params[:page] = 1 if params[:page].blank?
    Shipment.where(["id IN (?)", params['list']]).each do |shipment|
      shipment.origin_contact_mech.delete
      shipment.destination_contact_mech.delete
      shipment.delete
    end
    length = params['list'].length
    result = Shipment.simple_search(params)
    render json: {data: result[:data][(session[:promo_page].to_i - length)..(session[:promo_page].to_i - 1)], page: (result[:count] / session[:promo_page].to_f).ceil}
  end

  def update_shipment
    weight = params[:weight].to_f
    dimension = {}
    dimension['length'] = params[:length].to_f
    dimension['width'] = params[:width].to_f
    dimension['height'] = params[:height].to_f
    starting_point = params[:starting_point]
    ending_point = params[:ending_point]

    country_start = Country.where('lower(iso_code_two_letter) like ?', starting_point['country_code'].downcase).first
    if (country_start.blank?)
      render json: {:status => "error", :text => I18n.t('ship_rate.invalid_starting_country')}
      return
    end
    #city_start = City.where('lower(ascii_name) like ? AND country_iso_code_two_letters = ?', starting_point['city_name'].downcase, country_start['iso_code_two_letters']).first
    #if (city_start.blank?)
    #  render json: {:status => "error", :text => I18n.t('ship_rate.invalid_starting_city')}
    #  return
    #end

    country_end = Country.where('lower(iso_code_two_letter) like ?', ending_point['country_code'].downcase).first
    if (country_end.blank?)
      render json: {:status => "error", :text => I18n.t('ship_rate.invalid_ending_country')}
      return
    end
    #city_end = City.where('lower(ascii_name) like ? AND country_iso_code_two_letters = ?', ending_point['city_name'].downcase, country_end['iso_code_two_letter']).first
    #if (city_end.blank?)
    #  render json: {:status => "error", :text => I18n.t('ship_rate.invalid_ending_city')}
    #  return
    #end
    shipment = Shipment.find(params[:id])
    shipment.shipment_package.update_attributes({"box_height" => dimension['height'], "box_width" => dimension['width'], "box_length" => dimension['length'], "weight" => weight})
    shipment.origin_contact_mech.update_attributes({:access_code => country_start['iso_code_two_letter'], :city => starting_point['city_name'], :postal_code => starting_point['postcode']})
    shipment.destination_contact_mech.update_attributes({:access_code => country_end['iso_code_two_letter'], :city => ending_point['city_name'], :postal_code => ending_point['postcode']})
    render json: {:status => "ok", :text => I18n.t('common.update_success')}
  end

  def estimate
    begin
    @starting_point = {"access_code" => params["starting_country"], "city" => params["starting_city"], "postal_code" => params["starting_postal"]}
    @ending_point = {"access_code" => params["ending_country"], "city" => params["ending_city"], "postal_code" => params["ending_postal"]}
    @weight = params["weight"].to_f
    @dimension = {"length" => params["length"].to_f, "height" => params["height"].to_f, "width" => params["width"].to_f}
    packages = [Package.new(@weight * 1000, [@dimension["length"] / 100, @dimension["height"] / 100, @dimension["width"] / 100], :units => :metric)]
    origin = Location.new(      :country => @starting_point['access_code'],
                                :city => @starting_point['city'],
                                :postal_code => @starting_point['postal_code'])

    destination = Location.new( :country => @ending_point['access_code'],
                                :city => @ending_point['city'],
                                :postal_code => @ending_point['postal_code'])

    fedex = FedEx.new(:login => ENV['FEDIX_LOGIN'], :password => ENV['FEDIX_PASSWORD'], :key => ENV['FEDIX_KEY'],
                      :account => ENV['FEDIX_ACCOUNT'], :meter => ENV['FEDIX_METER'], :test => true)
    response = fedex.find_rates(origin, destination, packages)
    @ups_rates = response.rates.sort_by(&:price)
    rescue => ex
      logger.error ex.message
    end
  end
end