class ProductsController < ApplicationController
  #to remove devise authentication, comment following before_filter function out.
  before_filter :authenticate_user!

  layout "products"
  
  def index
    if params[:query]
      @products = Product.where("product_name LIKE '%#{params[:query]}%' OR sku LIKE '%#{params[:query]}%'").page(params[:page]).per_page(30)
      @total  = @products.count
    else
      @products = Product.page(params[:page]).per_page(10)
      @total  = @products.count
    end
  end

  def show
    id = params[:id]
    @product = Product.find(id)
    @contents = @product.product_contents
    @history  = @product.versions
  end

  # Update Product information
  def update_product
    product =  params[:product]
    @product = Product.find(params[:product][:id])
    @product.update_attributes(product)
    redirect_to "/products/"+params[:product][:id]
  end

  def update_image
    if params[:product_content]
      #ProductContent.
      ProductContent.create(params[:product_content])
      redirect_to "/products/"+params[:product_content][:product_id]
    end
  end

  def update_factory
    #puts params
    @product = Product.find(params[:product][:id])
    @product.product_parties.first.party.organizations.first.update_attributes(params[:organization])
    @product.supplier_products.first.update_attributes(params[:supplier_product])
    redirect_to "/products/#{@product.id}"
  end

  def new
    #Just render form for creating a new product.
  end
  # Create a new product
  def create
  if params[:product_content]
    @product=Product.new()
    @product[:product_name] = "Sample Product Name w Img"
    @product[:product_type_id] = "1"
    @product[:sku] = "XIMGX"
    @product.save()

    content = params[:product_content]
    @product.product_contents.create(content)
  end


  if params[:product]
    @product= Product.new(params[:product])
    @product[:product_type_id] = "1"
    @product[:sku] = "XXXX"
    @product.save()
  else
    if params[:factory_info]
      @product=Product.new()
      @product[:product_name] = "Sample Product Name"
      @product[:product_type_id] = "1"
      @product[:sku] = "XXXX"
      @product.save()

      ##Other Relative Entites
      #asset_product = AssetProduct.new()
      #asset_product[:product_id]=@product.id
      ##Temporary assignment of Asset id
      #asset_product[:asset_id]="1"
      #asset_product[:quantity_on_hand_total]="00"
      #asset_product.save()
      #
      #cost_component =  @product.cost_components.new()
      #cost_component[:cost] = "0000"
      #cost_component.save()
      #
      ##@product.asset_products.first.asset.quantity_on_hand_total
      #
      #prod_price  = @product.product_prices.new()
      #prod_price[:price] = "0000"
      #prod_price.save()
      #
      #supp_prod = @product.supplier_products.new()
      #supp_prod[:standard_lead_time_days] = params[:factory_info][:standard_lead_time_days]
      #supp_prod.save()
      @factoryInfo  = params[:factory_info]
    end
    end
    #Other Relative Entites
    asset_product = AssetProduct.new()
    asset_product[:product_id]=@product.id
    #Temporary assignment of Asset id
    asset_product[:asset_id]="1"
    asset_product[:quantity_on_hand_total]="00"
    asset_product.save()

    cost_component =  @product.cost_components.new()
    cost_component[:cost] = "0000"
    cost_component.save()

    #@product.asset_products.first.asset.quantity_on_hand_total

    prod_price  = @product.product_prices.new()
    prod_price[:price] = "0000"
    prod_price.save()

    prod_party = ProductParty.new()
    prod_party[:party_id] = 1
    prod_party[:product_id] = @product.id
    prod_party.save()

    supp_prod = @product.supplier_products.new()
    if params[:factory_info]
      supp_prod[:standard_lead_time_days] = params[:factory_info][:standard_lead_time_days]
    else
      supp_prod[:standard_lead_time_days] = "0"
    end
    supp_prod.save()
  redirect_to "/products/#{@product.id}"

  end


  def find
    #Just render form for Product Search
  end

  # Search a product wit given search parameters
  def results
    #@products = Product.where("id = ? OR supplier_id = ? OR product_name = ? OR description = ?", params[:product][:product_id],params[:product][:supplier_id], params[:product][:product_name], params[:product][:description])

    @products = Product.where("id = ? AND product_name = ? AND description = ?", params[:product][:product_id], params[:product][:product_name], params[:product][:description])
  end
  
  def overview
    sku = params[:id]
    @product  = Product.find_by_id(sku)
    #@dimention = @product.product_dimentions.find(:conditions => "dimension_type_id = 1")
  end

  def photos
    @product = Product.where("sku = ? ", params[:sku]).first
  end
  
  def copy
  
  end
  
  def color
  
  end
  
  def pricing
    find_product_by_sku
    ap @product
  end
  
  def moq_cost
  
  end
  
  def weight_size
    find_product_by_sku
    ap @product
  end

  def find_product_by_sku
     @product = Product.where("sku = ? ", params[:sku]).first
  end
end
