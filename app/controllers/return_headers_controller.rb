class ReturnHeadersController < ApplicationController
  before_filter :authenticate_user!
  layout "product_promotions"

  # GET /return_headers
  # GET /return_headers.json
  def index
    session[:promo_page] = 10 if session[:promo_page].blank?
    @return_headers = ReturnHeader.paginate(:page => 1, :per_page => session[:promo_page])
    @total = ReturnHeader.count
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @return_headers }
    end
  end

  # GET /return_headers/1
  # GET /return_headers/1.json
  def show
    @return_header = ReturnHeader.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @return_header }
    end
  end

  # GET /return_headers/new
  # GET /return_headers/new.json
  def new
    @return_header = ReturnHeader.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @return_header }
    end
  end

  # GET /return_headers/1/edit
  def edit
    @return_header = ReturnHeader.find(params[:id])
  end

  # POST /return_headers
  # POST /return_headers.json
  def create
    @return_header = ReturnHeader.new(params[:return_header])

    respond_to do |format|
      if @return_header.save
        format.html { redirect_to edit_return_header_path(@return_header), notice: 'Return header was successfully created.' }
        format.json { render json: @return_header, status: :created, location: @return_header }
      else
        format.html { render action: "new" }
        format.json { render json: @return_header.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /return_headers/1
  # PUT /return_headers/1.json
  def update
    @return_header = ReturnHeader.find(params[:id])

    respond_to do |format|
      if @return_header.update_attributes(params[:return_header])
        format.html { redirect_to edit_return_header_path(@return_header), notice: 'Return header was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @return_header.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /return_headers/1
  # DELETE /return_headers/1.json
  def destroy
    @return_header = ReturnHeader.find(params[:id])
    @return_header.destroy

    respond_to do |format|
      format.html { redirect_to return_headers_url }
      format.json { head :no_content }
    end
  end

  def return_headers_per_page
    session[:promo_page] = params['per'] if !params['per'].blank?
    params[:page] = 1 if params[:page].blank?

    @return_headers = ReturnHeader.simple_search(params)
    @return_header_temp = @return_headers.paginate(:page => params['page'], :per_page => session[:promo_page])
    @return_header = []
    @return_header_temp.each do |header|
      header_json = ActiveSupport::JSON.decode(header.to_json)
      header_json['return_header_type_id'] = header.return_header_type.description
      header_json['status_id'] = header.status.description
      header_json['payment_method_id'] = header.payment_method.description
      header_json['billing_account_id'] = header.billing_account.billing_account_id.to_s
      header_json['destination_facility_id'] = header.destination_facility.facility_name
      @return_header.push(header_json)
    end

    render json: {"data" => @return_header, "page" => (@return_headers.count / session[:promo_page].to_f).ceil, "per" => session[:promo_page]}
  end

  def delete_return_headers
    session[:promo_page] = params['per'] if !params['per'].blank?
    params[:page] = 1 if params[:page].blank?

    ReturnHeader.destroy_all(["id IN (?)", params['list']])
    length = params['list'].length
    @return_headers = ReturnHeader.simple_search(params)
    @return_header_temp = @return_headers.paginate(:page => params['page'], :per_page => session[:promo_page])
    @return_header = []
    @return_header_temp.each do |header|
      header_json = ActiveSupport::JSON.decode(header.to_json)
      header_json['return_header_type_id'] = header.return_header_type.description
      header_json['status_id'] = header.status.description
      header_json['payment_method_id'] = header.payment_method.description
      header_json['billing_account_id'] = header.billing_account.billing_account_id.to_s
      header_json['destination_facility_id'] = header.destination_facility.facility_name
      @return_header.push(header_json)
    end
    render json: {data: @return_header[(session[:promo_page].to_i - length)..(session[:promo_page].to_i)], page: (@return_headers.count / session[:promo_page].to_f).ceil}
  end
end
