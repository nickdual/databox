class PromotionRulesController < ApplicationController
  layout "product_promotions"

  def index

  end

  def show

  end
  def new
    @product_promotion = ProductPromotion.find(session[:product_promotion_id])
    @promotion_rule = @product_promotion.promotion_rules.build()
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @promotion_rule}
    end
  end
  def edit
    @promotion_rule = PromotionRule.find(params[:id])
    @promotion_rule_conditions = @promotion_rule.promotion_rule_conditions
    @promotion_rule_condition_category = PromotionRuleConditionCategory.new()
    @promotion_rule_condition_product = PromotionRuleConditionProduct.new()

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @promotion_rule }
    end
  end

  def destroy

  end

  def create
    @promotion_rule = PromotionRule.new(params[:promotion_rule])

    respond_to do |format|
      if @promotion_rule.save
        format.html { redirect_to '/promotion_rules/manage/' + @promotion_rule.product_promotion_id }
        format.json { render json: @promotion_rule, status: :created, location: @promotion_rule }
      else
        format.html { render action: "new" }
        format.json { render json: @promotion_rule.errors, status: :unprocessable_entity }
      end
    end
  end

  def update

  end

  def manage
    @product_promotion = ProductPromotion.find(params[:id].to_s)
    session[:product_promotion_id] = params[:id]
    if @product_promotion
       @promotion_rules = @product_promotion.promotion_rules
       @promotion_rule_new = PromotionRule.new(:id => @product_promotion.id.to_s)
    end
    @promotion_category = PromotionCategory.new()
    @product_category_list = ProductCategory.all
    @promotion_category_list = @product_promotion.promotion_categories.all
    @products_list = Product.all
    @promotion_products_list = @product_promotion.promotion_products.all
  end

  def update_promotion_rule
    @promotion_rule = PromotionRule.find(params[:promotion_rule][:id])
    @promotion_rule.name =   params[:promotion_rule][:name]
    if @promotion_rule.save
      render :json => {:success => true,:message => I18n.t("message.save_rule")}
    else
      render :json => {:success => false , :message => I18n.t("message.save_rule_error") + " " + @promotion_rule.errors.full_messages.to_s}
    end

  end

  def add_promotion_rule
    @promotion_rule = PromotionRule.new(params[:promotion_rule])
    if @promotion_rule.save
      puts 'aa'
      @render = render_to_string :partial => "promotion_rules/form", :object => @promotion_rule
      render :json => {:success => true,:data => @render, :message => "Rule " + I18n.t("message.create_success") }
    else
      puts 'bbb'
      render :json => {:success => false, :message => "Rule " +  I18n.t("message.create_error")  + " " + @promotion_rule.errors.full_messages.to_s }
    end
  end

  def add_rule_condition

    @promotion_rule_condition = PromotionRuleCondition.new(params[:promotion_rule_condition])
    if @promotion_rule_condition.save
      @render = render_to_string :partial => "promotion_rules/form_condition_rule", :object => @promotion_rule_condition
      render :json => {:success => true,:data => @render, :message => "Rule Condition " + I18n.t("message.save_success") }
    else
      render :json => {:success => false, :message => "Rule Condition " +  I18n.t("message.save_error")  + " " + @promotion_rule_condition.errors.full_messages.to_s }

    end
  end

  def update_rule_condition
    @promotion_rule_condition = PromotionRuleCondition.find(params[:id])

    if @promotion_rule_condition.update_attributes(params[:promotion_rule_condition])
      render :json => {:success => true, :message => "Rule Condition " + I18n.t("message.save_success") }
    else
      render :json => {:success => false, :message => "Rule Condition " +  I18n.t("message.save_error")  + " " + @promotion_rule_condition.errors.full_messages.to_s }
    end
  end

  def delete_rule_condition
    @promotion_rule_condition = PromotionRuleCondition.find(params[:id])
    @promotion_rule_condition.promotion_rule_condition_categories.destroy_all()
    @promotion_rule_condition.promotion_rule_condition_products.destroy_all()
    @promotion_rule_condition.destroy()
    render :json => {:status => :ok}

  end

  def add_rule_condition_category
    @promotion_rule_condition_category = PromotionRuleConditionCategory.new(params[:promotion_rule_condition_category])

    if @promotion_rule_condition_category.save
      @render = render_to_string :partial => "promotion_rules/form_rule_condition_category", :object => @promotion_rule_condition_category
      render :json => {:success => true,:data => @render, :message => "Rule Condition Category " + I18n.t("message.save_success") }
    else
      render :json => {:success => false, :message => "Rule Condition Category " +  I18n.t("message.save_error")  + " " + @promotion_rule_condition_category.errors.full_messages.to_s }
    end
  end

  def update_rule_condition_category
    @promotion_rule_condition_category = PromotionRuleConditionCategory.find(params[:id])

    if @promotion_rule_condition_category.update_attributes(params[:promotion_rule_condition_category])
      render :json => {:status => :ok}
    else
      render :json => {:status => 403}
    end
  end

  def delete_rule_condition_category
    @promotion_rule_condition_category = PromotionRuleConditionCategory.find(params[:id])
    @promotion_rule_condition_category.destroy()

    render :json => {:status => :ok}
  end

  def delete_rule_condition_product
    @promotion_rule_condition_product = PromotionRuleConditionProduct.find(params[:id])
    @promotion_rule_condition_product.destroy()

    render :json => {:status => :ok}
  end

  def add_rule_condition_product
    @promotion_rule_condition_product = PromotionRuleConditionProduct.new(params[:promotion_rule_condition_product])

    if @promotion_rule_condition_product.save
      @render = render_to_string :partial => "promotion_rules/form_rule_condition_product", :object => @promotion_rule_condition_product
      render :json => {:success => true,:data => @render, :message => "Rule Condition Product " + I18n.t("message.save_success") }
    else
      render :json => {:success => false, :message => "Rule Condition Product  " +  I18n.t("message.save_error")  + " " + @promotion_rule_condition_product.errors.full_messages.to_s }
    end
  end

  def update_rule_condition_product
    @promotion_rule_condition_product = PromotionRuleConditionProduct.find(params[:id])

    if @promotion_rule_condition_product.update_attributes(params[:promotion_rule_condition_product])
      render :json => {:status => :ok}
    else
      render :json => {:status => 403}
    end
  end

  def delete_rule_action_category
    @promotion_rule_action_category = PromotionRuleActionCategory.find(params[:id])
    @promotion_rule_action_category.destroy
    render :json => {}
  end

  def add_rule_action_category
    @promotion_rule_action_category = PromotionRuleActionCategory.new(params[:promotion_rule_action_category])
    if @promotion_rule_action_category.save
      @render = render_to_string(:partial => "rule_action_category", :locals => {:rule_action_category => @promotion_rule_action_category})
      render :json => {:success => true,:data => @render, :message => "Action Category " + I18n.t("message.save_success") }
    else
      render :json => {:success => false, :message => "Action Category " +  I18n.t("message.save_error")  + " " + @promotion_rule_action_category.errors.full_messages.to_s }
    end
  end

  def update_rule_action
    promotion_rule_action = PromotionRuleAction.find(params[:id])
    if promotion_rule_action.update_attributes(params[:promotion_rule_action])
      render :json => {:success => true, :message => "Action " + I18n.t("message.save_success") }
    else
      render :json => {:success => false, :message => "Action  " +  I18n.t("message.save_error")  + " " + promotion_rule_action.errors.full_messages.to_s }
    end
  end

  def delete_rule_action
    promotion_rule_action = PromotionRuleAction.find(params[:id])
    promotion_rule_action.destroy
    render :json => {:status => :ok}
  end

  def delete_rule_action_product
    promotion_rule_action_product = PromotionRuleActionProduct.find(params[:id])
    promotion_rule_action_product.destroy
    render :json => {:status => :ok}
  end

  def add_rule_action_product
    promotion_rule_action_product = PromotionRuleActionProduct.new(params[:promotion_rule_action_product])
    if promotion_rule_action_product.save
      @render = render_to_string(:partial => "rule_action_product", :locals => {:rule_action_product => promotion_rule_action_product})
      render :json => {:success => true,:data => @render, :message => "Action Product " + I18n.t("message.save_success") }
    else
      render :json => {:success => false, :message => "Action Product " +  I18n.t("message.save_error")  + " " + promotion_rule_action_product.errors.full_messages.to_s }
    end
  end

  def insert_rule_action
    promotion_rule_action = PromotionRuleAction.new(params[:promotion_rule_action])
    if promotion_rule_action.save
      @render = render_to_string(:partial => "rule_action_form", :locals => {:promotion_rule_action => promotion_rule_action})
      render :json => {:success => true,:data => @render, :message => "Rule Action " + I18n.t("message.save_success") }
    else
      render :json => {:success => false, :message => "Rule Action " +  I18n.t("message.save_error")  + " " + promotion_rule_action.errors.full_messages.to_s }
    end
  end

  def add_promotion_category
    params[:id] = session[:product_promotion_id]
    @product_promotion = ProductPromotion.find(params[:id])
    promotion_category = @product_promotion.promotion_categories.new(params[:promotion_category])
    if promotion_category.save
      str = render_to_string(:partial => "promotion_category_form", :locals => {:promotion_category => promotion_category })
      render :json => {"data" => str, "status" => :ok}
    else
      render :json => {"status" => 403}
    end
  end

  def add_promotion_product
    params[:id] = session[:product_promotion_id]
    @product_promotion = ProductPromotion.find(params[:id])
    @promotion_product = @product_promotion.promotion_products.new(params[:promotion_product])
    if @promotion_product.save
      @product = Product.find(@promotion_product.product_id)
      str = render_to_string(:partial => "promotion_product_form", :locals => {:promotion_product => @promotion_product, :product => @product })
      render :json => {"data" => str, "status" => :ok}
    else
      render :json => {"status" => 403}
    end
  end

  def delete_promotion_category
    @promotion_category = PromotionCategory.find(params[:id])
    @promotion_category.destroy

    render :json => {:status => :ok}
  end

  def delete_promotion_product
    @promotion_product = PromotionProduct.find(params[:id])
    @promotion_product.destroy

    render :json => {:status => :ok}
  end

end
