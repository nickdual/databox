module CustomersHelper
	def customer_name(order_head)
		order_part = OrderPart.where("order_id = ?",order_head.order_id).first
		unless order_part.nil?
			customer_party = order_part.customer_party 
			unless customer_party.blank?
				customer = customer_party.people
				unless customer.empty?
					customer.first.full_name
				end
			end
		end
	end

	def order_header_status(status_id)
		status =  OrderHeaderStatus.where("status_id = ?", status_id).first
		status.description
	end
end
