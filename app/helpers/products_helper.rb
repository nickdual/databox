module ProductsHelper
  def pipeline?
    params[:controllers] == 'pipelines'
  end

  def products?
    params[:controllers] == 'products'
  end

  def orders?
    params[:controllers] == 'orders'
  end

  def customers?
    params[:controllers] == 'customers'
  end

  def suppliers?
    params[:controllers] == 'suppliers'
  end

  def dashboard?
    params[:controllers] == 'dashboard'
  end

  def wiki?
    params[:controllers] == 'wiki'
  end
  
  def overview?
    params[:controllers] == 'products' && params[:action] == 'overview'
  end
  
  def photos?
    params[:controllers] == 'products' && params[:action] == 'photos'
  end
  
  def copy?
    params[:controllers] == 'products' && params[:action] == 'copy'
  end
  
  def color?
    params[:controllers] == 'products' && params[:action] == 'color'
  end
  
  def volume?
    params[:controllers] == 'products' && params[:action] == 'weight_size'
  end
  
  def pricing?
    params[:controllers] == 'products' && params[:action] == 'pricing'
  end
  
  def cost?
    params[:controllers] == 'products' && params[:action] == 'moq_cost'
  end
  
  def child?
    params[:controllers] == 'products' && params[:action] == 'child'
  end
end
