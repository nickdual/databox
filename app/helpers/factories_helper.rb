module FactoriesHelper
	def factory_name(order_head)
		order_part = OrderPart.where("order_id = ?",order_head.order_id).first
		unless order_part.nil?
			customer_party = order_part.customer_party
			organization = customer_party.organizations
			unless organization.empty?
				organization.first.organization_name
			end
		end
	end
	def factory_party_id(order_head)
		order_part = OrderPart.where("order_id = ?",order_head.order_id).first
		unless order_part.nil?
			customer_party = order_part.customer_party
			organization = customer_party.organizations
			unless organization.empty?
				organization.first.party_id
			end
		end
	end

	def order_item_status(status_id)
		status =  OrderHeaderStatus.where("status_id = ?", status_id).first
		status.description
	end
end
