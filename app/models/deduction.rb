class Deduction < ActiveRecord::Base
  attr_accessible :amount, :deduction_id, :deduction_type_enum_id, :payment_id
  
  belongs_to :payment
end
