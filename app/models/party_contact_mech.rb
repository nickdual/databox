class PartyContactMech < ActiveRecord::Base
  attr_accessible :allow_solicitation, :comments, :contact_mech_id, :contact_mech_purpose_id, :extension, :from_date, :party_id, :thru_date, :used_since, :verify_code
  
  belongs_to :party
  belongs_to :contact_mech
end
