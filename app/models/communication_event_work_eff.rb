class CommunicationEventWorkEff < ActiveRecord::Base
  attr_accessible :communication_event_id, :description, :work_effort_id
  # Composite primary keys 
  # self.primary_keys = :work_effort_id, :communication_event_id
  # Association
  belongs_to :work_effort
  belongs_to :communication_event
end
