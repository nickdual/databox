class AssetMaintenanceStatus < ActiveRecord::Base
  attr_accessible :description, :name, :sequence_num
  # Association
  has_many :asset_maintenances, :class_name => 'AssetMaintenance', :foreign_key => :status_id
  has_many :status_valids, :class_name => 'AssetMaintenanceStatusValid', :foreign_key => :status_id
  has_many :to_status_valids, :class_name => 'AssetMaintenanceStatusValid', :foreign_key => :to_status_id
end
