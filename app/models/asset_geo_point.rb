class AssetGeoPoint < ActiveRecord::Base
  attr_accessible :asset_id, :from_date, :geo_point_id, :thru_date
  # Composite primary keys
  self.primary_keys = :asset_id, :geo_point_id, :from_date
  # Association
  belongs_to :asset
end
