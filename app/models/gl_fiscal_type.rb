class GlFiscalType < ActiveRecord::Base
  attr_accessible :description, :name
  has_many :acctg_trans
end
