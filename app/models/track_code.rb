class TrackCode < ActiveRecord::Base
  attr_accessible :billable_lifetime, :comments, :description, :from_date, :group_id, :marketing_campaign_id, :redirect_url, :subgroup_id, :thru_date, :trackable_lifetime, :tracking_code_id, :tracking_code_type_enum_id

  
  belongs_to :marketing_campaign
  belongs_to :tracking_code_type_enum, :class_name => "TrackingCodeType", :foreign_key => "tracking_code_type_enum_id"  

  # Association
  has_many :sales_opportunity_trackings, :class_name => 'SalesOpportunityTracking', :foreign_key => :tracking_code_id

end
