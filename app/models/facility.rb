class Facility < ActiveRecord::Base
  attr_accessible :closed_date, :default_days_to_ship, :description, :facility_name, :facility_size, :facility_size_uom_id, :facility_type_enum_id, :inventory_item_type, :geo_point_id, :opened_date, :owner_party_id, :parent_facility_id, :weight_uom_id
  # Association
  belongs_to :facility_type_enum, :class_name => "FacilityType", :foreign_key => :facility_type_enum_id
  
  belongs_to :weight_uom
  belongs_to :inventory_item_type
  belongs_to :facility_size_uom

  has_many :destination_return_headers, :class_name => 'ReturnHeader', :foreign_key => :destination_facility_id

  has_one :delivery
  has_one :picklist
  has_one :shipment
  has_one :shipment_route_segment

  has_many :order_parties
  has_many :work_efforts
  has_many :inventory_product_stores, :class_name => 'ProductStore', :foreign_key => :inventory_facility_id
  has_many :product_store_facilities
  has_many :product_average_costs
  has_many :assets
  has_many :containers
  
  has_many :product_facilities, :class_name => 'ProductFacility', :foreign_key => :product_id

  def self.get_one(id)
     return self.find(id)
  end
  
  def self.get_all_names
     rows = self.find(:all, :select => 'id,facility_name')
     return rows
  end

  def typename
    return facility_type_enum.name
  end

  def name
    return :facility_name
  end
  
  def weight_unit
     return :weight_uom_id
  end
  
  def inventory_type
     return :inventory_item_type_id   
  end
  
  def facility_size_unit
     return :facility_size_uom_id
  end
  
end
