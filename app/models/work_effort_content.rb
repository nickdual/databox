class WorkEffortContent < ActiveRecord::Base
  attr_accessible :content_location, :content_type_id, :from_date, :thru_date, :work_effort_id
  # Composite primary keys
  # self.primary_keys = :work_effort_id, :content_location, :content_type_id
  # Association
  belongs_to :work_effort
  belongs_to :work_effort_content_types
end
