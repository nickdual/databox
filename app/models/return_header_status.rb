class ReturnHeaderStatus < ActiveRecord::Base
  attr_accessible :description, :sequence_num, :status_id
  # Association
  has_many :return_headers, :class_name => 'ReturnHeader', :foreign_key => :status_id
  has_many :status_valids, :class_name => 'ReturnHeaderStatusValid', :foreign_key => :status_id
  has_many :to_status_valids, :class_name => 'ReturnHeaderStatusValid', :foreign_key => :to_status_id
end
