class GlReconciliation < ActiveRecord::Base
	attr_accessible :gl_reconciliation_id, :gl_reconciliation_name, :description, :gl_account_id, :status_id,
	:organization_party_id, :reconciled_balance, :opening_balance, :reconciled_date

	belongs_to :gl_account
	belongs_to :party
  belongs_to :gl_reconciliation_status, :foreign_key => :status_id
	has_one :gl_reconciliation_entry
end
