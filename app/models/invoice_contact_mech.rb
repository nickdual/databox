class InvoiceContactMech < ActiveRecord::Base
  attr_accessible :contact_mech_id, :contact_mech_purpose_enum_id, :invoice_id
  
  belongs_to :invoice
  belongs_to :contact_mech_purpose_enum, :class_name => "ContactMechPurpose", :foreign_key => "contact_mech_purpose_enum_id"
  belongs_to :contact_mech
end
