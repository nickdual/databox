class OrderContactMech < ActiveRecord::Base
  attr_accessible :contact_mech_id, :contact_mech_purpose_id, :order_id
  # Composite primary keys
  # self.primary_keys = :order_id, :contact_mech_purpose_id, :contact_mech_id
  # Association
  belongs_to :order_header, :class_name => 'OrderHeader', :foreign_key => :order_id
  belongs_to :contact_mech_purpose
  belongs_to :contact_mech
end
