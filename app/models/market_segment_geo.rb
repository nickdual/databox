class MarketSegmentGeo < ActiveRecord::Base
  attr_accessible :geo_id, :market_segment_id
  
  belongs_to :market_segment
end
