class PicklistBinItem < ActiveRecord::Base
  attr_accessible :asset_id, :order_id, :order_item_seq_id, :picklist_bin_id, :picklist_bin_item_seq_id, :quantity, :status_id
  # Composite primary keys
  self.primary_keys = :picklist_bin_id, :picklist_bin_item_seq_id
  # Association
  belongs_to  :picklist_bin
  belongs_to :order_item, :foreign_key => [:order_id, :order_item_seq_id]
end
