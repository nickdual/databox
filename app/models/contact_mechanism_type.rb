class ContactMechanismType < ActiveRecord::Base
  attr_accessible :description, :name, :parent_id
  # Association
  belongs_to :parent , :class_name => 'ContactMechanismType'
  has_many :children ,:class_name => 'ContactMechanismType'
  has_many :communication_events
  has_many :contact_mechanism_type_communication_event_types, :class_name => 'CommunicationEventType', :foreign_key => 'contact_mech_type_id'
  
end
