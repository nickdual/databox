class ProductCalculatedInfo < ActiveRecord::Base
  attr_accessible :average_customer_rating, :product_id, :total_quantity_ordered, :total_times_viewed
  # self.primary_key :product_id
  # Association
  belongs_to :product
end
