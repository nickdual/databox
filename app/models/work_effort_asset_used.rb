class WorkEffortAssetUsed < ActiveRecord::Base
  attr_accessible :asset_id, :quantity, :status_id, :work_effort_id
  # Composite primary keys
  # self.primary_keys = :work_effort_id, :asset_id
  # Association
  belongs_to :work_effort
  belongs_to :asset
  has_many :asset_details, :foreign_key => [:work_effort_id, :asset_id]
end
