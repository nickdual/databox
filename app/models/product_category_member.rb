class ProductCategoryMember < ActiveRecord::Base
  attr_accessible :comments, :from_date, :product_category_id, :product_id, :quantity, :sequence_num, :thru_date
  # Composite primary keys
  self.primary_keys = :product_category_id, :product_id, :from_date
  # Association
  belongs_to :product
  belongs_to :product_category
end
