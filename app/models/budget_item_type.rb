class BudgetItemType < ActiveRecord::Base
  attr_accessible :description, :name
  has_many :budget_items
end
