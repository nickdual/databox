class WorkEffortBilling < ActiveRecord::Base
  attr_accessible :invoice_id, :invoice_item_seq_id, :percentage, :work_effort_id
  # Composite primary keys
  self.primary_keys = :work_effort_id, :invoice_id, :invoice_item_seq_id
  # Association
  belongs_to :work_effort
  belongs_to :invoice_item, :foreign_key => [:invoice_id, :invoice_item_seq_id]
end
