class ShipmentPackage < ActiveRecord::Base
  attr_accessible :box_height, :box_length, :box_width, :date_created, :dimension_uom_id, :insured_value, :shipment_box_type_id, :shipment_id, :shipment_package_seq_id, :weight, :weight_uom_id

  has_one :shipment_receipt
  has_one :shipment_package_content
  has_one :shipment_package_route_seg

  belongs_to :shipment, :foreign_key => :shipment_id
  belongs_to :shipment_box_type
end
