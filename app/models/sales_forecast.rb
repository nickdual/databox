class SalesForecast < ActiveRecord::Base
  attr_accessible :best_case_amount, :closed_amount, :currency_uom_id, :forecast_amount, :internal_party_id, :organization_party_id, :parent_sales_forecast_id, :percent_of_quota_closed, :percent_of_quota_forecast, :pipeline_amount, :quota_amount, :time_period_id
  # Association
  belongs_to :parent, :class_name => 'SalesForecast', :foreign_key => :parent_sales_forecast_id
  belongs_to :organization, :class_name => 'Party', :foreign_key => :organization_party_id
  belongs_to :internal, :class_name => 'Party', :foreign_key => :internal_party_id
  belongs_to :time_period
  has_many :children, :class_name => 'SalesForecast', :foreign_key => :parent_sales_forecast_id
  has_many :sales_forecast_details
end
