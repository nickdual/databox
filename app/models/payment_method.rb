class PaymentMethod < ActiveRecord::Base
  attr_accessible :description, :fin_account_id, :from_date, :gl_account_id, :owner_party_id, :payment_fraud_evidence_id, :payment_method_id, :payment_method_type_enum_id, :postal_contact_mech_id, :telecom_contact_mech_id, :thru_date, :trust_level_enum_id
  
  belongs_to :payment_method_type_enum, :class_name => "PaymentMethodType", :foreign_key => "payment_method_type_enum_id"
  belongs_to :owner_party, :class_name => "Party", :foreign_key => "owner_party_id"
  belongs_to :postal_contact_mech, :class_name => "ContactMech", :foreign_key => "postal_contact_mech_id"
  belongs_to :postal_contact_mech, :class_name => "PostalAddress", :foreign_key => "postal_contact_mech_id"
  belongs_to :telecom_contact_mech, :class_name => "ContactMech",  :foreign_key => "telecom_contact_mech_id"
  belongs_to :telecom_contact_mech, :class_name => "TelecomNumber", :foreign_key => "telecom_contact_mech_id"
  belongs_to :trust_level_enum, :class_name=> "ContactPaymentTrustLevel", :foreign_key => "trust_level_enum_id"
  belongs_to :payment_fraud_evidence
  belongs_to :gl_account
  belongs_to :financial_account
  has_many :return_headers, :class_name => "ReturnHeader"
end
