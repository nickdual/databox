class ProductConfigOptionProduct < ActiveRecord::Base
  attr_accessible :config_item_id, :config_option_seq_id, :product_id, :quantity, :sequence_num
  # Composite primary keys
  self.primary_keys = :config_item_id, :config_option_seq_id, :product_id
  # Association
  belongs_to :product_config_option, :foreign_key => [:config_item_id, :config_option_seq_id]
  belongs_to :product
end
