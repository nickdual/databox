class ContactListParty < ActiveRecord::Base
  attr_accessible :contact_list_id, :from_date, :opt_in_verify_code, :party_id, :preferred_contact_mech_id, :status_id, :thru_date
  
  belongs_to :contact_list
  belongs_to :party
  belongs_to :contact_list_party, :class_name => "ContactListPartyStatus"
  belongs_to :status, :class_name => "ContactListPartyStatus", :foreign_key => :status_id
  belongs_to :preferred_contact_mech, :class_name =>"ContactMech", :foreign_key => "preferred_contact_mech_id"

#  attr_accessible :contact_list_id, :party_id, :preferred_contact_mech_id, :status_id
end
