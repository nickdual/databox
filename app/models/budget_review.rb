class BudgetReview < ActiveRecord::Base
	attr_accessible :budgetId, :party_id, :budget_review_result_enum_id, :review_date

	enum_attr :budget_review_result_enum_id, %w(DATA_1 DATA_2)

	belongs_to :budget
	belongs_to :party
	belongs_to :budget_review_result, :foreign_key => :budget_review_result_enum_id
end
