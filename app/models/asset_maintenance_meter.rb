class AssetMaintenanceMeter < ActiveRecord::Base
  attr_accessible :asset_maintenance_id, :meter_value, :product_meter_type_id
  # Composite primary keys
  self.primary_keys = :asset_maintenance_id, :product_meter_type_id
  # Association
  belongs_to :asset_maintenances
  belongs_to :product_meter_types
end
