class MarketingCampaign < ActiveRecord::Base
  attr_accessible :actual_cost, :budgeted_cost, :campaign_name, :campaign_summary, :converted_leads, :cost_uom_id, :created_at, :estimated_cost, :expected_response_percent, :expected_revenue, :from_date, :is_active, :marketing_campaign_id, :num_sent, :parent_campaign_id, :start_date, :status_id, :thru_date, :updated_at
  
  belongs_to :parent, :class_name => "MarketingCampaign", :foreign_key => "parent_campaign_id"
  belongs_to :status, :class_name => "MarketingCampaignStatus", :foreign_key => "status_id"
  belongs_to :marketing_campaign_status, :class_name => "MarketingCampaignStatus"
  # Association
  has_many :sales_opportunities
end
