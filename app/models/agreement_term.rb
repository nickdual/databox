class AgreementTerm < ActiveRecord::Base
  attr_accessible :agreement_id, :agreement_item_seq_id, :from_date, :settlement_term_id, :term_type_id, :thru_date
  # Association
  belongs_to :agreement
  belongs_to :agreement_item, :foreign_key => [:agreement_id, :agreement_item_seq_id]
  belongs_to :settlement_term
  belongs_to :term_type
end
