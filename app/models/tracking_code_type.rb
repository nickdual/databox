class TrackingCodeType < ActiveRecord::Base
  attr_accessible :description, :id, :status_id
  # Association
  has_many :tracking_codes
end
