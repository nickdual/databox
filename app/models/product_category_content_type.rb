class ProductCategoryContentType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :product_category_contents, :class_name => 'ProductCategoryContent', :foreign_key => :category_content_type_id
end
