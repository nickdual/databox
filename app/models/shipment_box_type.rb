class ShipmentBoxType < ActiveRecord::Base
  attr_accessible :box_height, :box_length, :box_weight, :box_width, :description, :dimension_uom_id, :weight_uom_id

  has_one :carrier_shipment_box_type
  has_one :shipment_package
  has_many :default_products, :class_name => 'Product', :foreign_key => :default_shipment_box_type_id
end
