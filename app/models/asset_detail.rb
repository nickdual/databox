class AssetDetail < ActiveRecord::Base
  attr_accessible :asset_detail_seq_id, :asset_id, :asset_id, :asset_maintenance_id, :asset_reservation_id, :available_to_promise_diff, :description, :effective_date, :item_issuance_id, :physical_inventory_id, :quantity_on_hand_diff, :receipt_id, :return_id, :return_item_seq_id, :shipment_id, :shipment_item_seq_id, :unit_cost, :variance_reason_id, :work_effort_id
  # Composite primary keys
  self.primary_keys = :asset_id, :asset_detail_seq_id
  # Association
  belongs_to :asset
  belongs_to :work_effort
  belongs_to :asset_reservations
  belongs_to :asset_maintenance
  belongs_to :item_issuance
  belongs_to :work_effort_asset_used, :foreign_key => [:work_effort_id, :asset_id]
  belongs_to :work_effort_asset_produced, :foreign_key => [:work_effort_id, :asset_id]
  belongs_to :physical_inventory
  belongs_to :variance_reason, :class_name => 'InventoryVarianceReason', :foreign_key => :variance_reason_id
end
