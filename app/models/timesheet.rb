class Timesheet < ActiveRecord::Base
  attr_accessible :client_party_id, :comments, :from_date, :party_id, :status_id, :thru_date
  # Association
  belongs_to :party
  belongs_to :client, :class_name => 'Party', :foreign_key => :client_party_id
  belongs_to :status, :class_name => 'TimesheetStatus', :foreign_key => :status_id
  has_many :time_entries
  has_many :timesheet_parties
  
end
