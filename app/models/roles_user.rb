class RolesUser < ActiveRecord::Base
  attr_accessible :role_id, :user_id
  belongs_to :role
  belongs_to :databox_user
  belongs_to :databox_role
end
