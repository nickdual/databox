class SupplierProduct < ActiveRecord::Base
  attr_accessible :can_drop_ship, :comments, :preferred_order_id, :product_id, :quantity_included, :quantity_increment, :quantity_uom_id, :standard_lead_time_days, :supplier_commission_percent, :supplier_party_id, :supplier_product_name, :supplier_rating_type_id
  # Composite primary keys
  #self.primary_keys = :supplier_party_id, :product_id
  # Association
  belongs_to :supplier, :class_name => 'Party', :foreign_key => :supplier_party_id
  belongs_to :product
  belongs_to :preferred_order, :class_name => 'SupplierPreferredOrder', :foreign_key => :preferred_order_id
end
