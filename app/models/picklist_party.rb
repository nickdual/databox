class PicklistParty < ActiveRecord::Base
  attr_accessible :from_date, :party_id, :picklist_id, :role_type_id, :thru_date

  belongs_to :picklist
  belongs_to :role_type
end
