class AgreementItemGeo < ActiveRecord::Base
  attr_accessible :agreement_id, :agreement_item_seq_id, :geo_id
  # Composite primary keys
  self.primary_keys = :agreement_id, :agreement_item_seq_id, :geo_id
  # Association
  belongs_to :agreement_item, :foreign_key => [:agreement_id, :agreement_item_seq_id]
end
