class VarianceReasonGlAccount < ActiveRecord::Base
	attr_accessible :organization_party_id, :variance_reason_enum_id, :gl_account_id

	belongs_to :inventory_variance_reason
	belongs_to :party
	belongs_to :gl_account
end
