class Asset < ActiveRecord::Base
  attr_accessible :acquire_order_id, :acquire_order_item_seq_id, :activation_number, :activation_valid_thru, :actual_end_of_life, :asset_name, :asset_type_id, :available_to_promise_total, :calendar_id, :class_id, :comments, :container_id, :date_acquired, :date_last_serviced, :date_manufactured, :date_next_service, :depreciation, :depreciation_type_id, :expected_end_of_life, :facility_id, :has_quantity, :instance_of_product_id, :location_seq_id, :lot_id, :owner_party_id, :parent_asset_id, :production_capacity, :production_capacity_uom_id, :purchase_cost, :purchase_cost_uom_id, :quantity_on_hand_total, :receive_date, :salvage_value, :serial_number, :soft_identifier, :status_id
  # Association
  has_one :acctg_trans
  has_one :acctg_trans_entry
  has_one :asset_type_gl_account
  #
  has_one :delivery
  has_one :item_issuance
  has_one :shipment_receipt
  #
  belongs_to :asset_type
  belongs_to :parent, :class_name => 'Asset', :foreign_key => :parent_asset_id
  belongs_to :instance_of, :class_name => 'Product', :foreign_key => :instance_of_product_id

  #Culprit Association
  #belongs_to :class, :class_name => 'AssetClass', :foreign_key => :class_id

  belongs_to :status, :class_name => 'AssetStatus', :foreign_key => :status_id
  belongs_to :owner, :class_name => 'Party', :foreign_key => :owner_party_id

  belongs_to :acquire, :class_name => 'OrderItem', :foreign_key => [:acquire_order_id, :acquire_order_item_seq_id]
  belongs_to :facility
  belongs_to :container
  belongs_to :lot
  has_many :asset_details
  #
  has_many :children, :class_name => 'Asset', :foreign_key => :parent_asset_id
  has_many :from_order_items, :class_name => 'OrderItem', :foreign_key => :from_asset_id
  has_many :work_effort_asset_assigns
  has_many :work_effort_asset_produceds
  has_many :work_effort_asset_useds
  has_many :asset_maintenances
  has_many :asset_meters
  has_many :asset_registrations
  has_many :cost_components
  has_many :asset_geo_points
  has_many :asset_identifications
  has_many :asset_products
  has_many :asset_standard_costs
  has_many :asset_party_assignments
  has_many :asset_reservations
  has_many :accommodation_maps
  has_many :accommodation_spots
end
