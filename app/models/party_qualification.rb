class PartyQualification < ActiveRecord::Base
  attr_accessible :description, :from_date, :party_id, :qualification_type_enum_id, :status_id, :thru_date, :title, :verification_status_id
  # Association
  belongs_to :qualification_type, :foreign_key => :qualification_type_enum_id
  belongs_to :party_qualification_status, :foreign_key => :status_id
end
