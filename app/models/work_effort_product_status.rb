class WorkEffortProductStatus < ActiveRecord::Base
  attr_accessible :description, :name, :sequence_num
  # Association
  has_many :work_effort_products, :class_name => 'WorkEffortProduct', :foreign_key => :status_id
end
