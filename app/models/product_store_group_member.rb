class ProductStoreGroupMember < ActiveRecord::Base
  attr_accessible :from_date, :product_store_group_id, :product_store_id, :sequence_num, :thru_date
  # Composite primary keys
  # self.primary_key :product_store_group_id, :product_store_id, :from_date
  # Association
  belongs_to :product_store
  belongs_to :product_store_group
end
