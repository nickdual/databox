class PromotionRuleActionProduct < ActiveRecord::Base
  attr_accessible :product_id, :promotion_rule_action_id, :sub_category_status
  enum_attr :sub_category_status, %w(include exclude always_include) do
    label :include => 'Include'
    label :exclude => 'Exclude'
    label :always_include => 'Always Include'

  end
  validates :product_id, :presence => true
  # Association
  belongs_to :product
  belongs_to :promotion_rule_action
end
