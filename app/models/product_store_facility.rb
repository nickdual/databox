class ProductStoreFacility < ActiveRecord::Base
  attr_accessible :facility_id, :from_date, :product_store_id, :sequence_num, :thru_date
  # Composite primary keys
  # self.primary_keys = :product_store_id, :facility_id, :from_date
  # Association
  belongs_to :product_store
  belongs_to :facility
end
