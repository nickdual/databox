class WorkEffortAssoc < ActiveRecord::Base
  attr_accessible :from_date, :sequence_num, :thru_date, :to_work_effort_id, :work_effort_assoc_type_id, :work_effort_id
  # Composite primary keys
  # self.primary_keys = :work_effort_id, :to_work_effort_id, :work_effort_assoc_type_id, :from_date
  # Association
  belongs_to :work_effort
  belongs_to :to_work_effort, :class_name => 'WorkEffort', :foreign_key => :to_work_effort_id
  belongs_to :work_effort_assoc_type
end
