class Organization < ActiveRecord::Base
  attr_accessible :annual_revenue, :comments, :num_employees, :office_site_name, :organization_name, :party_id
  # set_primary_key :party_id
  # Association
  belongs_to :party
end
