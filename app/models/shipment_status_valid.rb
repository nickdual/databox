class ShipmentStatusValid < ActiveRecord::Base
  attr_accessible :status_id, :to_status_id, :transition_name
end
