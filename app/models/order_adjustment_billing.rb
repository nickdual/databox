class OrderAdjustmentBilling < ActiveRecord::Base
  attr_accessible :amount, :invoice_id, :invoice_item_seq_id, :order_adjustment_id
  # Composite primary keys
  self.primary_keys = :order_adjustment_id, :invoice_id, :invoice_item_seq_id
  # Association
  belongs_to :order_adjustment
  belongs_to :invoice_item, :foreign_key => [:invoice_id, :invoice_item_seq_id]
end
