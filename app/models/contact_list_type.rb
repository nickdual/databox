class ContactListType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association 
  has_many :contact_lists
end
