class WorkEffortAssetAssignStatus < ActiveRecord::Base
  attr_accessible :description, :name, :sequence_num
  # Association
  has_many :work_effort_asset_assigns, :class_name => 'WorkEffortAssetAssign', :foreign_key => :status_id
end
