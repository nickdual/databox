class MarketingCampaignParty < ActiveRecord::Base
  attr_accessible :from_date, :marketing_campaign_id, :party_id, :role_type_id, :thru_date
  
  belongs_to :marketing_campaign
  belongs_to :party
  belongs_to :role_type
end
