class MaintenanceType < ActiveRecord::Base
  attr_accessible :description, :name, :parent_id
  # Association
  belongs_to :parent, :class_name => 'MaintenanceType', :foreign_key => :parent_id
  has_many :children, :class_name => 'MaintenanceType', :foreign_key => :parent_id
  has_many :asset_maintenances
  has_many :product_maintenances
end
