class ShipmentItemBilling < ActiveRecord::Base
  attr_accessible :invoice_id, :invoice_item_seq_id, :shipment_id, :shipment_item_seq_id

  belongs_to :shipment_item
  belongs_to :invoice_item
end
