class PromotionCodeEmail < ActiveRecord::Base
  attr_accessible :email, :product_promotion_code_id
  # Association
  belongs_to :promotion_code, :foreign_key => :product_promotion_code_id
end
