class ShipmentItem < ActiveRecord::Base
  attr_accessible :description, :product_id, :quantity, :shipment_id, :shipment_item_seq_id
  # Association
  has_many :return_item_shipments , :class_name => 'ReturnItemShipment', :foreign_key => :shipment_id
  has_many :seq_return_item_shipments, :class_name => 'ReturnItemShipment', :foreign_key => :shipment_item_seq_id


	has_one :item_issuance
	has_one :shipment_receipt
	has_one :shipment_item_billing
	has_one :shipment_package_content

	belongs_to :shipment
	belongs_to :product

  has_many :seq_order_shipments, :class_name => 'OrderShipment', :foreign_key => :shipment_item_seq_id
  has_many :shipment_item_sources
end
