class Picklist < ActiveRecord::Base
  attr_accessible :description, :facility_id, :picklist_date, :shipment_method_id, :status_id
  # Association
  belongs_to :facility
  belongs_to :shipment_method  

  has_one :picklist_bin
  has_one :picklist_party
end
