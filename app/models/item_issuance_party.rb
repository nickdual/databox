class ItemIssuanceParty < ActiveRecord::Base
  attr_accessible :item_issuance_id, :party_id, :role_type_id

  belongs_to :item_issuance
  belongs_to :party
  belongs_to :role_type
end
