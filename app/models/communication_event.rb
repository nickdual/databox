class CommunicationEvent < ActiveRecord::Base
  attr_accessible :communication_event_type_id, :contact_list_id, :contact_mech_type_id, :content, :content_type, :datetime_ended, :datetime_started, :email_message_id, :entry_date, :from_contact_mech_id, :from_party_id, :from_role_type_id, :note, :original_comm_event_id, :parent_comm_event_id, :reason_id, :status_id, :subject, :to_contact_mech_id, :to_party_id, :to_role_type_id
  # Association
  belongs_to :communication_event_type
  belongs_to :contact_mechanism_type, :foreign_key => "contact_mech_type_id"
  belongs_to :communication_event_status, :foreign_key => "status_id"
  belongs_to :from_contact_mech, :class_name => "ContactMech", :foreign_key => 'from_contact_mech_id'
  belongs_to :to_contact_mech, :class_name => "ContactMech", :foreign_key => 'to_contact_mech_id'
  belongs_to :from_party, :class_name => "Party", :foreign_key => 'from_party_id'  
  belongs_to :to_party, :class_name => "Party", :foreign_key => 'to_party_id'  
  belongs_to :from_role_type, :class_name => "RoleType", :foreign_key => 'from_role_type_id'
  belongs_to :to_role_type, :class_name => "RoleType", :foreign_key => 'to_role_type_id'
  belongs_to :contact_list
  
  has_many :communication_event_contents
  has_many :communication_event_products
  has_many :communication_event_purposes
  has_many :communication_event_parties
  has_many :order_communication_events
  has_many :communication_event_work_effs
  has_many :subscription_deliveries
end
