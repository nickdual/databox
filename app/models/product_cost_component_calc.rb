class ProductCostComponentCalc < ActiveRecord::Base
  attr_accessible :cost_component_calc_id, :cost_component_type_id, :from_date, :product_id, :sequence_num, :thru_date
  # Composite primary keys
  self.primary_keys = :product_id, :cost_component_type_id, :from_date
  # Association
  belongs_to :product
  belongs_to :cost_component_type
  belongs_to :cost_component_calc
end
