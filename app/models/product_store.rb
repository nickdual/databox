class ProductStore < ActiveRecord::Base
  attr_accessible :default_currency_uom_id, :default_locale, :default_sales_channel_id, :inventory_facility_id, :organization_party_id, :require_customer_role, :requirement_method_id, :reservation_order_id, :store_name
  # Association
  belongs_to :organization, :class_name => 'Party', :foreign_key => :organization_party_id
  belongs_to :inventory, :class_name => 'Facility', :foreign_key => :inventory_facility_id
  belongs_to :reservation_order, :class_name => 'AssetReservationOrder', :foreign_key => :reservation_order_id
  belongs_to :requirement_method
  has_many :order_headers
  has_many :product_store_categories
  has_many :product_store_emails
  has_many :product_store_facilities
  has_many :product_store_parties
  has_many :product_store_group_members
  has_many :product_prices
  has_many :product_reviews
  has_many :promotion_stores
end
