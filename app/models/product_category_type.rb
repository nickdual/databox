class ProductCategoryType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :product_categories
end
