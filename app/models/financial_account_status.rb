class FinancialAccountStatus < ActiveRecord::Base
  attr_accessible :description, :sequence_num, :status_id
end
