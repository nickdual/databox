class WorkEffortPartyExpectation < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :expectation_work_effort_parties, :class_name => 'WorkEffortParty', :foreign_key => :expectation_id
  
end
