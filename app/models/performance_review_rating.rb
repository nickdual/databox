class PerformanceReviewRating < ActiveRecord::Base
  attr_accessible :description, :name, :sequence_num
  has_many :performance_review_items
end
