class RequestItemWorkEffort < ActiveRecord::Base
  attr_accessible :request_id, :request_item_seq_id, :work_effort_id
  
  belongs_to :request_item, :foreign_key => [:request_id, :request_item_seq_id]
  belongs_to :request
  belongs_to :work_effort
end
