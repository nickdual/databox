class WorkEffort < ActiveRecord::Base
	attr_accessible :actual_completion_date, :actual_setup_time, :actual_start_date, :actual_work_time, :description, :estimated_completion_date, :estimated_setup_time, :estimated_start_date, :estimated_work_time, :facility_id, :info_url, :location, :parent_work_effort_id, :percent_complete, :priority, :purpose_id, :recurrence_info_id, :revision_number, :send_notification_email, :source_reference_id, :status_id, :time_uom_id, :total_time_allowed, :universal_id, :visibility_id, :work_effort_name, :work_effort_type_id
  # Association
	has_one :acctg_trans
	has_one :shipment
  belongs_to :parent, :class_name => 'WorkEffort', :foreign_key => :parent_work_effort_id
  belongs_to :work_effort_type
  belongs_to :purpose, :class_name => 'WorkEffortPurpose', :foreign_key => :purpose_id
  belongs_to :visibility, :class_name => 'WorkEffortVisibility', :foreign_key => :visibility_id
  belongs_to :status, :class_name => 'WorkEffortStatus', :foreign_key => :status_id
  belongs_to :facility, :class_name => 'Facility'
  has_many :children, :class_name => 'WorkEffort', :foreign_key => :parent_work_effort_id
  has_many :order_item_work_efforts
  has_many :time_entries
  has_many :communication_event_work_effs
  has_many :work_effort_asset_assigns
  has_many :work_effort_asset_neededs
  has_many :work_effort_asset_produceds
  has_many :work_effort_asset_useds
  has_many :work_effort_assocs
  has_many :to_work_effort_assocs, :class_name => 'WorkEffortAssoc', :foreign_key => :to_work_effort_id
  has_many :work_effort_billings
  has_many :work_effort_contact_meches
  has_many :work_effort_contents
  has_many :work_effort_deliverable_prods
  has_many :work_effort_notes
  has_many :work_effort_parties
  has_many :work_effort_products
  has_many :work_effort_skill_standards
  has_many :sales_opportunity_work_efforts
  has_many :routing_product_assocs, :class_name => 'ProductAssoc', :foreign_key => :routing_work_effort_id
  has_many :task_asset_maintenances, :class_name => 'AssetMaintenance', :foreign_key => :task_work_effort_id
  has_many :template_product_maintenances, :class_name => 'ProductMaintenance', :foreign_key => :template_work_effort_id
  has_many :cost_components
  has_many :asset_details
  has_many :agreement_item_work_efforts
end
