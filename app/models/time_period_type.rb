class TimePeriodType < ActiveRecord::Base
  attr_accessible :description, :period_length
  # Composite primary keys
  # self.primary_key = :time_period_type_id
  # Association
  belongs_to :length_uom

end
