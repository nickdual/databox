class PartyClassificationAppl < ActiveRecord::Base
  attr_accessible :from_date, :party_classification_id, :party_id, :thru_date
  # Composite primary keys
  # self.primary_keys = :party_id, :party_classification_id, :from_date
  # Association
  belongs_to :party
  belongs_to :party_classification
end
