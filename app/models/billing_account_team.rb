class BillingAccountTeam < ActiveRecord::Base
  attr_accessible :billing_account_id, :billing_account_term_id, :term_type_enum_id, :term_uom_id, :term_value
  
  belongs_to :term_type_enum, :class_name =>"BillingAccountTermType", :foreign_key => "term_type_enum_id"
  belongs_to :billing_account
end
