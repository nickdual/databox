class InvoiceItemAssoc < ActiveRecord::Base
  attr_accessible :amount, :from_date, :from_party_id, :invoice_id, :invoice_item_assoc_id, :invoice_item_assoc_type_enum_id, :invoice_item_seq_id, :quantity, :thru_date, :to_invoice_id, :to_invoice_item_seq_id, :to_party_id
  
  belongs_to :invoice_item_assoc_type_enum, :class_name => "InvoiceItemAssocType", :foreign_key => "invoice_item_assoc_type_enum_id"
  belongs_to :invoice, :class_name => "InvoiceItem", :foreign_key => "invoice_id"
  belongs_to :invoice_item_seq, :class_name => "InvoiceItem", :foreign_key => "invoice_item_seq_id"
  belongs_to :from_party, :class_name => "Party", :foreign_key => "from_party_id"
  belongs_to :to_party, :class_name => "Party", :foreign_key => "to_party_id"
end
