class OrderItem < ActiveRecord::Base
  attr_accessible :budget_id, :budget_item_seq_id, :cancel_quantity, :comments, :external_item_id, :from_asset_id, :is_modified_price, :is_promo, :item_description, :item_type_id, :order_id, :order_item_seq_id, :order_part_seq_id, :override_gl_account_id, :product_category_id, :product_config_saved_id, :product_id, :quantity, :sales_opportunity_id, :selected_amount, :shopping_list_id, :shopping_list_item_seq_id, :status_id, :subscription_id, :supplier_product_id, :unitList_price, :unit_price
  validates_uniqueness_of :order_id, :scope => :order_item_seq_id
  # Association
  belongs_to :order_header, :class_name => 'OrderHeader', :foreign_key => :order_id
  belongs_to :order_part, :foreign_key => [:order_id, :order_part_seq_id]
  belongs_to :status, :class_name => 'OrderItemStatus', :foreign_key => :status_id
  belongs_to :product
  belongs_to :product_config_saved
  belongs_to :from_asset, :class_name => 'Asset', :foreign_key => :from_asset_id
  belongs_to :override_gl_account,:class_name => 'GlAccount', :foreign_key => :override_gl_account_id
  belongs_to :sales_opportunity
  has_many :return_items, :class_name => 'ReturnItem',  :foreign_key => :order_id
  has_many :seq_return_items, :class_name => 'ReturnItem', :foreign_key => :order_item_seq_id
  has_many :order_adjustments, :class_name => 'OrderAdjustment', :foreign_key => [:order_id, :order_item_seq_id]


  has_one :item_issuance
  has_one :shipment_receipt 

  has_many :order_item_billings, :foreign_key => [:order_id, :order_item_seq_id]
  has_many :seq_order_shipments, :class_name => 'OrderShipment', :foreign_key => :order_item_seq_id
  has_many :seq_order_terms, :class_name => 'OrderTerm', :foreign_key => :order_item_seq_id
  has_many :subscriptions, :class_name => 'Subscription', :foreign_key => [:order_id, :order_item_seq_id]
  has_many :asset_maintenance_order_items, :class_name => 'AssetMaintenanceOrderItem', :foreign_key => [:order_id, :order_item_seq_id]
  has_many :acquire_assets, :class_name => 'Asset', :foreign_key => [:acquire_order_id, :acquire_order_item_seq_id]
  has_many :seq_asset_reservations, :class_name => 'AssetReservation', :foreign_key => :order_item_seq_id
  has_many :picklist_bin_items, :foreign_key => [:order_id, :order_item_seq_id]
  has_many :shipment_item_sources, :foreign_key => :order_id
  has_many :seq_shipment_item_sources,:class_name => 'ShipmentItemSource',:foreign_key => :order_item_seq_id
end
