class PartyRelationship < ActiveRecord::Base
  attr_accessible :comments, :from_date, :from_party_id, :from_role_type_id, :permissions_id, :priority_type_id, :relationship_name, :relationship_type_id, :status_id, :thru_date, :to_party_id, :to_role_type_id
  # Association
  belongs_to :from_party ,:class_name => 'Party', :foreign_key => :from_party_id
  belongs_to :to_party ,:class_name => 'Party', :foreign_key => :to_party_id
  belongs_to :from_role_type, :class_name => 'RoleType', :foreign_key => :from_role_type_id
  belongs_to :to_role_type, :class_name => 'RoleType', :foreign_key => :to_role_type_id
  belongs_to :status , :class_name => 'PartyRelationshipStatus', :foreign_key => :status_id
  belongs_to :relationship_type, :class_name => 'PartyRelationshipType', :foreign_key =>  :relationship_type_id
end
