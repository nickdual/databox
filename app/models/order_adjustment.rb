class OrderAdjustment < ActiveRecord::Base
  attr_accessible :amount, :amount_already_included, :comments, :corresponding_product_id, :customer_reference_id, :description, :exempt_amount, :include_in_shipping, :include_in_tax, :item_type_id, :order_id, :order_item_seq_id, :order_part_seq_id, :original_adjustment_id, :override_gl_account_id, :primary_geo_id, :product_feature_id, :recurring_amount, :secondary_geo_id, :source_percentage, :source_reference_id, :tax_auth_geo_id, :tax_auth_party_id, :tax_authority_rate_id
  # Association
  belongs_to :order_header, :class_name => 'OrderHeader', :foreign_key => :order_id
  belongs_to :order_item, :class_name => 'OrderItem', :foreign_key => [:order_id, :order_item_seq_id]
  belongs_to :order_part, :class_name => 'OrderPart', :foreign_key => [:order_id, :order_part_seq_id]
  belongs_to :tax_auth, :class_name => 'TaxAuthority', :foreign_key => [:tax_auth_geo_id, :tax_auth_party_id]
  belongs_to :override_gl_account, :class_name => 'GlAccount', :foreign_key => :override_gl_account_id
  belongs_to :tax_authority_rate
  belongs_to :original_adjustment, :class_name => 'OrderAdjustment', :foreign_key => :original_adjustment_id
  has_many :children, :class_name => 'OrderAdjustment', :foreign_key => :original_adjustment_id
  has_many :order_adjustment_billings
end
