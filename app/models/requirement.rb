class Requirement < ActiveRecord::Base
  attr_accessible :asset_id, :deliverable_id, :description, :estimated_budget, :facility_id, :product_id, :quantity, :reason, 
                  :requirement_start_date, :requirement_type_enum_id, :status_id, :use_case
end
