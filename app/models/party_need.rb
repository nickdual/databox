class PartyNeed < ActiveRecord::Base
  attr_accessible :communication_event_id, :datetime_recorded, :description, :need_type_id, :party_id, :product_category_id, :product_id, :role_type_id, :visit_id
  # Association
  belongs_to :party
  belongs_to :role_type
  belongs_to :communication_event
  belongs_to :product
  belongs_to :product_category
end
