class OrderHeaderStatus < ActiveRecord::Base
  attr_accessible :description, :sequence_num, :status_id
  # Association
  has_many :order_headers, :class_name => 'OrderHeader', :foreign_key => :status_id
  has_many :status_valids, :class_name => 'OrderHeaderStatusValid', :foreign_key => :status_id
  has_many :to_status_valids, :class_name => 'OrderHeaderStatusValid', :foreign_key => :to_status_id
  
end
