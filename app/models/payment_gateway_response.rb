class PaymentGatewayResponse < ActiveRecord::Base
  attr_accessible :alt_reference, :amount, :amount_uom_id, :gateway_avs_result, :gateway_code, :gateway_cv_result, :gateway_flag, :gateway_message, :gateway_score_result, :payment_gateway_response_id, :payment_method_id, :payment_service_type_enum_id, :reference_num, :result_bad_card_number, :result_bad_expire, :result_declined, :result_nsf, :sub_reference, :trans_code_enum_id, :transaction_date
  
  belongs_to :payment
  belongs_to :payment_method
end
