class SalesOpportunityQuote < ActiveRecord::Base
  attr_accessible :order_id, :sales_opportunity_id
  # Composite primary keys
  # self.primary_keys = :sales_opportunity_id, :order_id
  # Association
  belongs_to :order_header, :class_name => 'OrderHeader', :foreign_key => :order_id
  belongs_to :sales_opportunity
end
