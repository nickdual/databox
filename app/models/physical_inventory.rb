class PhysicalInventory < ActiveRecord::Base
  attr_accessible :general_comments, :party_id, :physical_inventory_date
  # Association
  has_one :acctg_trans
  has_many :asset_details
end
