class ShipmentRouteSegmentStatusValid < ActiveRecord::Base
  attr_accessible :status_id, :to_status_id, :transition_name
  belongs_to :status, :class_name => 'TimesheetStatus', :foreign_key => :status_id
  belongs_to :to_status, :class_name => 'TimesheetStatus', :foreign_key => :to_status_id
end
