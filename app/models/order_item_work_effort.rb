class OrderItemWorkEffort < ActiveRecord::Base
  attr_accessible :order_id, :order_item_seq_id, :work_effort_id
  # Composite primary keys
  # self.primary_keys = :order_id, :order_item_seq_id, :work_effort_id
  # Association
  belongs_to :order_item_seq, :class_name => 'OrderItem', :foreign_key => :order_item_seq_id
  belongs_to :work_effort
end
