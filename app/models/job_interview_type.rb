class JobInterviewType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :job_interviews
end
