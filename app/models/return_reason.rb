class ReturnReason < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :return_items
end
