class ProductAssoc < ActiveRecord::Base
  attr_accessible :from_date, :instruction, :product_assoc_type_id, :product_id, :quantity, :reason, :routing_work_effort_id, :scrap_factor, :sequence_num, :thru_date, :to_product_id
  # Composite primary keys
  # self.primary_keys = :product_id, :to_product_id, :product_assoc_type_id, :from_date
  # Association
  belongs_to :product_assoc_type
  belongs_to :product
  belongs_to :to_product, :class_name => 'Product', :foreign_key => :to_product_id
  belongs_to :routing, :class_name => 'WorkEffort', :foreign_key => :routing_work_effort_id
end
