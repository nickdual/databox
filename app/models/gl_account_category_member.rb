class GlAccountCategoryMember < ActiveRecord::Base
  attr_accessible :gl_account_id, :gl_account_category_id, :from_date, :thru_date, :amount_percentage
  
  belongs_to :gl_account
  belongs_to :gl_account_category
end
