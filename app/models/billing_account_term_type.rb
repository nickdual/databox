class BillingAccountTermType < ActiveRecord::Base
  attr_accessible :description, :name
end
