class BudgetRevision < ActiveRecord::Base
  attr_accessible :budget_id, :revision_seq_id, :date_revision, :description

  belongs_to :budget
  has_one :budget_revision_impact
end
