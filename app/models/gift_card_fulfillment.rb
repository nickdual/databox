class GiftCardFulfillment < ActiveRecord::Base
  attr_accessible :amount, :auth_code, :card_number, :fulfillment_date, :fulfillment_id, :merchant_id, :order_id, :order_item_seq_id, :party_id, :pin_number, :reference_num, :response_code, :survey_response_id, :type_enum_id
  
  belongs_to :party, :foreign_key => "party_id"
  belongs_to :order_header, :foreign_key => "order_id"
  belongs_to :order, :class_name => "OrderItem", :foreign_key => "order_id"
  belongs_to :order_item_seq, :class_name => "OrderItem", :foreign_key => "order_item_seq"
end
