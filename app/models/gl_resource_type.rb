class GlResourceType < ActiveRecord::Base
  attr_accessible :description, :name
  has_many :gl_accounts
end
