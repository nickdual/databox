class SalesOpportunityWorkEffort < ActiveRecord::Base
  attr_accessible :sales_opportunity_id, :work_effort_id
  # Composite primary keys
  # self.primary_keys = :sales_opportunity_id, :work_effort_id
  # Association
  belongs_to :sales_opportunity
  belongs_to :work_effort
end
