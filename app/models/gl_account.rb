class GlAccount < ActiveRecord::Base
	attr_accessible :parent_gl_account_id, :gl_account_type_enum_id, :gl_account_class_enum_id,
	:gl_resource_type_enum_id, :gl_xbrl_class_enum_id, :account_code, :account_name, :description, :product_id,
	:external_id, :posted_balance

	enum_attr :gl_account_class_enum_id, %w(DEBIT CREDIT RESOURCE ASSET CURRENT_ASSET CASH_EQUIVALENT INVENTORY_ASSET LONGTERM_ASSET LIABILITY 
		CURRENT_LIABILITY LONGTERM_LIABILITY EQUITY OWNERS_EQUITY RETAINED_EARNINGS DISTRIBUTION RETURN_OF_CAPITAL DIVIDEND REVENUE CONTRA_REVENUE 
		INCOME CASH_INCOME NON_CASH_INCOME EXPENSE CASH_EXPENSE INTEREST_EXPENSE COGS_EXPENSE SGA_EXPENSE NON_CASH_EXPENSE 
		DEPRECIATION AMORTIZATION INVENTORY_ADJUST CONTRA_ASSET ACCUM_DEPRECIATION ACCUM_AMORTIZATION NON_POSTING)
	
	enum_attr :gl_account_type_enum_id, %w(ACCOUNTS_RECEIVABLE ACCOUNTS_PAYABLE BALANCE_ACCOUNT BANK_STLMNT_ACCOUNT UNDEPOSITED_RECEIPTS MRCH_STLMNT_ACCOUNT 
	CURRENT_ASSET FIXED_ASSET FIXED_ASSET_MAINT OTHER_ASSET CREDIT_CARD CURRENT_LIABILITY LONG_TERM_LIABILITY CUSTOMER_ACCOUNT SUPPLIER_ACCOUNT PRODUCT_ACCOUNT 
	INVENTORY_ACCOUNT INV_ADJ_AVG_COST INV_ADJ_VAL RAWMAT_INVENTORY WIP_INVENTORY TAX_ACCOUNT PROFIT_LOSS_ACCOUNT SALES_ACCOUNT SALES_RETURNS DISCOUNTS_ACCOUNT 
	COGS_ACCOUNT COGS_ADJ_AVG_COST PURCHASE_ACCOUNT INV_CHANGE_ACCOUNT EXPENSE OPERATING_EXPENSE OTHER_EXPENSE OWNERS_EQUITY RETAINED_EARNINGS CUSTOMER_DEPOSIT 
	CUSTOMER_CREDIT CUSTOMER_GC_DEPOSIT UNINVOICED_SHIP_RCPT PURCHASE_PRICE_VAR INCOME OTHER_INCOME INTEREST_INCOME INTRSTINC_RECEIVABLE PREPAID_EXPENSES INVENTORY_XFER_OUT 
	INVENTORY_XFER_IN ACCPAYABLE_UNAPPLIED ACCREC_UNAPPLIED COMMISSION_EXPENSE COMMISSIONS_PAYABLE OTHER_EXPENSE ACCTRECV_WRITEOFF ACCTPAY_WRITEOFF COMMISSIONS_WRITEOFF 
	INTRSTINC_WRITEOFF FX_GAIN_LOSS_ACCT FX_GAIN_ACCOUNT FX_LOSS_ACCOUNT)

	enum_attr :gl_resource_type_enum_id, %w(GLRT_MONEY GLRT_RAW_MATERIALS GLRT_LABOR GLRT_SERVICES GLRT_FINISHED_GOODS GLRT_DELIVERED_GOODS)

	enum_attr :gl_xbrl_class_enum_id, %w(US_GAAP IAS)
  # Association
	belongs_to :gl_account
	belongs_to :gl_account_type, :foreign_key => :gl_account_type_enum_id
	belongs_to :gl_account_class, :foreign_key => :gl_account_class_enum_id
	belongs_to :gl_resource_type, :foreign_key => :gl_resource_type_enum_id
	belongs_to :gl_xbrl_class, :foreign_key => :gl_xbrl_class_enum_id

	has_one :gl_account_category_member
	has_one :gl_account_group_member
	has_one :gl_account_history
	has_one :gl_account_organization
	has_one :gl_account_party
	has_one :gl_budget_xrefs
	has_one :gl_reconciliation
	has_one :acctg_trans
	has_one :asset_type_gl_account
	has_one :credit_card_type_gl_account
	has_one :financial_account_type_gl_account
	has_one :gl_account_type_party_default
	has_one :gl_account_type_default
	has_one :item_type_gl_account
	has_one :payment_method_type_gl_account
	has_one :product_category_gl_account
	has_one :product_gl_account
	has_one :tax_authority_gl_account
	has_one :variance_reason_gl_account
	has_one :party_acctg_preference
  has_many :override_order_adjustments, :class_name => 'OrderAdjustment', :foreign_key => :override_gl_account_id
  has_many :override_order_items, :class_name => 'OrderItem', :foreign_key => :override_gl_account_id
end
