class WorkEffortAssetProduced < ActiveRecord::Base
  attr_accessible :asset_id, :work_effort_id
  # Composite primary keys 
  # self.primary_keys = :asset_id, :work_effort_id
  # Association
  belongs_to :work_effort
  belongs_to :asset
  has_many :asset_details, :foreign_key => [:work_effort_id, :asset_id]
end
