class PartyNote < ActiveRecord::Base
  attr_accessible :note_date, :note_text, :party_id
  # Composite primary keys
  # self.primary_keys = :party_id, :note_date
  # Association
  belongs_to :party
end
