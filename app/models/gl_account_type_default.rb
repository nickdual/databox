class GlAccountTypeDefault < ActiveRecord::Base
  attr_accessible :gl_account_type_enum_id, :organization_party_id, :gl_accouny_id

  belongs_to :party  
  belongs_to :gl_account_type
  belongs_to :gl_account
end
