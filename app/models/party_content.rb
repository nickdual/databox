class PartyContent < ActiveRecord::Base
  attr_accessible :content_location, :from_date, :party_content_type_id, :party_id, :thru_date
  # Composite primary keys
  # self.primary_keys = :party_id, :content_location, :party_content_type_id, :from_date
  # Association
  belongs_to :party
  belongs_to :party_content_type
end
