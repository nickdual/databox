class ReturnContactMech < ActiveRecord::Base
  attr_accessible :contact_mech_id, :contact_mech_purpose_id, :return_id
  # Composite primary key
  # self.primary_keys = :return_id, :contact_mech_purpose_id, :contact_mech_id
  # Association
  belongs_to :return_header, :class_name => 'ReturnHeader', :foreign_key => :return_id
  belongs_to :contact_mech
  belongs_to :contact_mech_purpose
end
