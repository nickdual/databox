class FinancialAccountAuth < ActiveRecord::Base
  attr_accessible :amount, :amount_uom_id, :authorization_date, :fin_account_auth_id, :fin_account_id, :from_date, :thru_date
  
  belongs_to :financial_account
end
