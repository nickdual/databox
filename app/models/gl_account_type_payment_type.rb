class GlAccountTypePaymentType < ActiveRecord::Base
  attr_accessible :payment_type_enum_id, :organization_party_id, :gl_account_type_enum_id

  belongs_to :payment_type
  belongs_to :party
  belongs_to :gl_account_type
end
	