class ProductFeature < ActiveRecord::Base
  attr_accessible :abbrev, :default_amount, :default_sequence_num, :description, :id_code, :number_specified, :number_uom_id, :product_feature_type_id
  # Association
  belongs_to :product_feature_type
  has_many :product_feature_appls
  has_many :product_feature_group_appls
  has_many :from_product_feature_iactns, :class_name => 'ProductFeatureIactn', :foreign_key => :from_product_feature_id
  has_many :to_product_feature_iactns, :class_name => 'ProductFeatureIactn', :foreign_key => :to_product_feature_id
end
