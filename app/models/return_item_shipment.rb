class ReturnItemShipment < ActiveRecord::Base
  attr_accessible :quantity, :return_id, :return_item_seq_id, :shipment_id, :shipment_item_seq_id
  # Composite primary keys
  # self.primary_keys = :return_id, :return_item_seq_id, :shipment_id, :shipment_item_seq_id
  # Association
  belongs_to :return_item , :class_name => 'ReturnItem', :foreign_key => :return_id
  belongs_to :return_item_seq, :class_name => 'ReturnItem', :foreign_key => :return_item_seq_id
  belongs_to :shipment_item , :class_name => 'ShipmentItem', :foreign_key => :shipment_id
  belongs_to :shipment_item_seq, :class_name => 'ShipmentItem', :foreign_key => :shipment_item_seq_id
    
end
