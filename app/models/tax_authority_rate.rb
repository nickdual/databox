class TaxAuthorityRate < ActiveRecord::Base
  attr_accessible :tax_auth_geo_id, :tax_auth_party_id, :rate_type_enum_id, :product_store_id, :product_category_id,
  :title_transfer_enum_id, :min_item_price, :min_purchase, :tax_shipping, :tax_percentage, :tax_promotions, 
  :from_date, :thru_date, :description

	enum_attr :rate_type_enum_id, %w(SALES_TAX USE_TAX VAT_TAX INCOME_TAX EXPORT_TAX IMPORT_TAX)
  # Association
  belongs_to :tax_authority_rate_type, :foreign_key => :rate_type_enum_id
  belongs_to :tax_authority
  belongs_to :product_store
  belongs_to :product_category
  has_many :order_adjustments
end
