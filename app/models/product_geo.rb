class ProductGeo < ActiveRecord::Base
  attr_accessible :description, :geo_id, :product_geo_purpose_id, :product_id
  # Composite primary keys
  # self.primary_keys = :product_id, :geo_id
  # Association
  belongs_to :product
  belongs_to :product_geo_purpose
end
