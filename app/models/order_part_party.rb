class OrderPartParty < ActiveRecord::Base
  attr_accessible :order_id, :order_part_seq_id, :party_id, :role_type_id
  # Composite primary keys
  # self.primary_keys = :order_id, :order_part_seq_id, :party_id, :role_type_id
  # Association
  belongs_to :order_part_seq, :class_name => 'OrderPart', :foreign_key => :order_part_seq_id
  belongs_to :party
  belongs_to :role_type
end
