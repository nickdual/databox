class CommunicationPurpose < ActiveRecord::Base
  attr_accessible :description, :name
  # Association 
  has_many :communication_event_purposes, :class_name => 'CommunicationEventPurpose', :foreign_key => 'purpose_id'
end
