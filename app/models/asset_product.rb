class AssetProduct < ActiveRecord::Base
  attr_accessible :asset_id, :asset_product_type_id, :comments, :from_date, :product_id, :quantity, :quantity_uom_id, :sequence_num, :thru_date, :file
  # Composite primary keys
  self.primary_keys = :asset_id, :product_id, :asset_product_type_id, :from_date
  # Association
  belongs_to :product
  belongs_to :asset
  belongs_to :asset_product_type
end
