class PartyQualificationStatus < ActiveRecord::Base
  attr_accessible :description, :sequence_num, :status_id
  # Association 
  has_many :party_qualifications
end
