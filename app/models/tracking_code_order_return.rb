class TrackingCodeOrderReturn < ActiveRecord::Base
  attr_accessible :affiliate_referred_time_stamp, :has_exported, :is_billable, :order_id, :order_item_seq_id, :return_id, :side_id, :tracking_code_id, :tracking_code_type_enum_id
  
  belongs_to :tracking_code_type_enum, :class_name => "TrackingCodeType", :foreign_key => "tracking_code_type_enum_id"
  belongs_to :return_header
  belongs_to :order_header
  belongs_to :tracking_code
end
