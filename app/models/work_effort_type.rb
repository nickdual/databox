class WorkEffortType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :work_efforts
end
