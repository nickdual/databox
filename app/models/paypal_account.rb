class PaypalAccount < ActiveRecord::Base
  attr_accessible :avs_addr, :avs_zip, :correlation_id, :expresss_checkout_token, :payer_id, :payer_status, :payment_method_id, :transaction_id
  
  belongs_to :payment_method
end
