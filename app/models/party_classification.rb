class PartyClassification < ActiveRecord::Base
  attr_accessible :classification_type_id, :description, :parent_classification_id
  # Association
  belongs_to :classification_type, :class_name => 'PartyClassificationType', :foreign_key => 'classification_type_id'
  belongs_to :parent , :class_name => 'PartyClassification', :foreign_key => 'parent_classification_id'
  has_many :children , :class_name => 'PartyClassification', :foreign_key => 'parent_classification_id'
  has_many :party_classification_appls
end
