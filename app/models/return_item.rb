class ReturnItem < ActiveRecord::Base
  attr_accessible :billing_account_id, :description, :expected_inventory_status_id, :fin_account_trans_id, :item_type_id, :order_adjustment_id, :order_id, :order_item_seq_id, :original_payment_id, :product_id, :received_quantity, :refund_payment_id, :replacement_order_id, :response_amount, :response_date, :return_id, :return_item_seq_id, :return_price, :return_quantity, :return_reason_id, :return_response_id, :status_id
  # Composite primary keys 
  # self.primary_keys = :return_id, :return_item_seq_id
  # Association
  has_one :shipment_receipt
  belongs_to :return_header , :class_name => 'ReturnHeader', :foreign_key => :return_id
  belongs_to :return_reason
  belongs_to :return_response
  belongs_to :order_item , :class_name => 'OrderItem', :foreign_key => :order_id
  belongs_to :order_item_seq, :class_name => 'OrderItem', :foreign_key => :order_item_seq_id
  belongs_to :product
  belongs_to :replacement_order, :class_name => 'OrderHeader', :foreign_key => :replacement_order_id
  has_many :return_item_billings, :class_name => 'ReturnItemBilling', :foreign_key => :return_id
  has_many :seq_return_item_billings, :class_name => 'ReturnItemBilling', :foreign_key => :return_item_seq_id
  has_many :return_item_shipments, :class_name => 'ReturnItemShipment', :foreign_key => :return_id
  has_many :seq_return_item_shipments, :class_name => 'ReturnItemShipment', :foreign_key => :return_item_seq_id
end
