class FacilityGroup < ActiveRecord::Base
  attr_accessible :description, :facility_group_type_enum_id, :parent_group_id
  # Association
  belongs_to :facility_group_type, :foreign_key => :facility_group_type_enum_id
end
