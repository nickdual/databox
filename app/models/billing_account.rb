class BillingAccount < ActiveRecord::Base
  attr_accessible :account_limit, :account_limit_uom_id, :bill_from_id, :bill_to_party_id, :billing_account_id, :description, :external_account_id, :from_date, :postal_contact_mech_id, :thru_date
  
  belongs_to :bill_to_party, :class_name => "Party", :foreign_key => "bill_to_party_id"
  belongs_to :postal_contact_mech, :class_name => "ContactMech", :foreign_key => "postal_contact_mech_id"
  belongs_to :postal_address, :foreign_key => "postal_contact_mech_id"

  has_one :return_header, :class_name => "ReturnHeader"
end
