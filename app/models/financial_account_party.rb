class FinancialAccountParty < ActiveRecord::Base
  attr_accessible :fin_account_id, :from_date, :party_id, :role_type_id, :thru_date
  
  belongs_to :financial_account
  belongs_to :party
  belongs_to :role_type
end
