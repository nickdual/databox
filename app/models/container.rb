class Container < ActiveRecord::Base
  attr_accessible :container_type_id, :description, :facility_id, :geo_point_id
  # Association
  belongs_to :facility
  has_many :assets
  
end
