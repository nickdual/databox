class JobPostingType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :job_requisitions
end
