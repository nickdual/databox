class AssetMaintenanceOrderItem < ActiveRecord::Base
  attr_accessible :asset_maintenance_id, :order_id, :order_item_seq_id
  # Composite primary keys
  self.primary_keys = :asset_maintenance_id, :order_id, :order_item_seq_id
  # Association
  belongs_to :asset_maintenance
  belongs_to :order_header, :class_name => 'OrderHeader', :foreign_key => :order_id
  belongs_to :order_item, :foreign_key => [:order_id, :order_item_seq_id]
end
