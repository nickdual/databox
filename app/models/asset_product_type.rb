class AssetProductType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :asset_products
end
