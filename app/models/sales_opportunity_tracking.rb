class SalesOpportunityTracking < ActiveRecord::Base
  attr_accessible :received_date, :sales_opportunity_id, :tracking_code_id
  # Composite primary keys
  # self.primary_keys = :sales_opportunity_id, :tracking_code_id
  # Association
  belongs_to :sales_opportunity
  belongs_to :tracking_code, :class_name => 'TrackCode', :foreign_key => :tracking_code_id
end
