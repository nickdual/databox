class TelecomNumber < ActiveRecord::Base
  attr_accessible :area_code, :ask_for_name, :contact_mech_id, :contact_number, :country_code

  has_one :shipment
  has_one :shipment_route_segment
end
