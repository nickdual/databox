class RequirementRequestItem < ActiveRecord::Base
  attr_accessible :request_id, :request_item_seq_id, :requirement_id
end
