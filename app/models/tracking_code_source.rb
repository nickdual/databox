class TrackingCodeSource < ActiveRecord::Base
  attr_accessible :description, :id, :status_id
  # Association
  has_many :tracking_code_visits
end
