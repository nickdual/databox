class PaymentType < ActiveRecord::Base
  attr_accessible :description, :name, :parent_name
end
