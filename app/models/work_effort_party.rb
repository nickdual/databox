class WorkEffortParty < ActiveRecord::Base
  attr_accessible :availability_id, :comments, :delegate_reason_id, :expectation_id, :from_date, :must_rsvp, :party_id, :role_type_id, :status_id, :thru_date, :work_effort_id
  # Composite primary keys
  # self.primary_keys = :work_effort_id, :party_id, :role_type_id, :from_date
  # Association
  belongs_to :work_effort
  belongs_to :party
  belongs_to :role_type
  belongs_to :status, :class_name => 'WorkEffortPartyStatus', :foreign_key => :status_id
  belongs_to :availability, :class_name => 'WorkEffortPartyAvailability', :foreign_key => :availability_id
  belongs_to :delegate_reason, :class_name => 'WorkEffortPartyDelegateReason', :foreign_key => :delegate_reason_id
  belongs_to :expectation, :class_name => 'WorkEffortPartyExpectation', :foreign_key => :expectation_id
end
