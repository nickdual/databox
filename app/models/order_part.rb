class OrderPart < ActiveRecord::Base
  attr_accessible :auto_cancel_date, :carrier_party_id, :customer_agent_party_id, :customer_party_id, :customer_po_id, :dont_cancel_set_date, :dont_cancel_set_user_id, :estimated_delivery_date, :estimated_ship_date, :facility_id, :gift_message, :is_gift, :may_split, :order_id, :order_part_seq_id, :parent_part_seq_id, :part_name, :postal_contact_mech_id, :ship_after_date, :ship_before_date, :ship_to_party_id, :shipment_method_id, :shipping_instructions, :telecom_contact_mech_id, :tracking_number, :valid_from_date, :valid_thru_date, :vendor_agent_party_id, :vendor_party_id
  # Composite primary keys
  #self.primary_keys = :order_id, :order_part_seq_id
  # Association
  belongs_to :order_header, :class_name =>'OrderHeader', :foreign_key => :order_id
  belongs_to :parent_part, :class_name => 'OrderPart', :foreign_key => [:order_id, :parent_part_seq_id]
  belongs_to :vendor, :class_name => 'Party', :foreign_key => :vendor_party_id
  belongs_to :customer_party, :class_name => 'Party', :foreign_key => :customer_party_id
  belongs_to :carrier_party, :class_name => 'Party', :foreign_key => :carrier_party_id
  belongs_to :facility
  belongs_to :shipment_method
  belongs_to :postal, :class_name => 'ContactMech', :foreign_key => :postal_contact_mech_id
  belongs_to :telecom, :class_name => 'ContactMech', :foreign_key => :telecom_contact_mech_id
  has_many :children_part, :class_name => 'OrderPart', :foreign_key => [:order_id, :parent_part_seq_id]
  has_many :order_adjustments, :class_name => 'OrderAdjustment', :foreign_key => [:order_id, :order_part_seq_id]


  has_one :shipment

  has_many :order_items, :foreign_key => [:order_id, :order_part_seq_id]
  has_many :seq_order_part_parties, :class_name => 'OrderPartParty', :foreign_key => :order_part_seq_id
  has_many :primary_picklist_bin, :class_name => 'PicklistBin', :foreign_key => [:primary_order_id, :primary_order_part_seq_id]
end
