class ProductConfigItem < ActiveRecord::Base
  attr_accessible :config_item_id, :config_item_name, :config_item_type_id
  self.primary_key = :config_item_id
  # Association
  belongs_to :config_item_type, :class_name => 'ProductConfigItem', :foreign_key => :config_item_type_id
  has_many :type_config_items, :class_name => 'ProductConfigItem', :foreign_key => :config_item_type_id
  has_many :product_config_item_appls, :class_name => 'ProductConfigItemAppl', :foreign_key => :config_item_id
  has_many :product_config_item_contents, :class_name => 'ProductConfigItemContent', :foreign_key => :config_item_id
  has_many :product_config_options, :class_name => 'ProductConfigOption', :foreign_key => :config_item_id
  
end
