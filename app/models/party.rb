class Party < ActiveRecord::Base
	attr_accessible :data_source_id, :disabled, :external_id, :party_type_id, :user_id
  # Association
	has_one :gl_account_history
	has_one :gl_account_organization
	has_one :gl_account_party
	has_one :gl_reconciliation
	has_one :acctg_trans
	has_one :acctg_trans_entry
	has_one :gl_journal	
	has_one :asset_type_gl_account
	has_one :credit_card_type_gl_account
	has_one :financial_account_type_gl_account
	has_one :gl_account_type_payment_type
	has_one :gl_account_type_party_default
	has_one :gl_account_type_default
	has_one :item_type_gl_account
	has_one :payment_method_type_gl_account
	has_one :product_category_gl_account
	has_one :product_gl_account
	has_one :tax_authority_gl_account
	has_one :variance_reason_gl_account
	has_one :party_acctg_preference
	has_one :carrier_shipment_method
	has_one :carrier_shipment_box_type
	has_one :party_carrier_account
	has_one :item_issuance_party
	has_one :shipment_receipt_party
	has_one :shipment
	has_one :shipment_route_segment

  belongs_to :party_type
  belongs_to :user
  
  has_many :from_party_communication_events, :class_name => "CommunicationEvent", :foreign_key => 'from_party_id' 
  has_many :to_party_communication_events, :class_name => "CommunicationEvent", :foreign_key => 'to_party_id' 
  has_many :communication_event_parties
  has_many :organizations
  has_many :people
  has_many :user_accounts
  has_many :party_identifications
  has_many :party_geo_points
  has_many :party_classification_appls
  has_many :party_contents
  has_many :party_note
  has_many :from_party_relationships, :class_name => 'PartyRelationship', :foreign_key => :from_party_id
  has_many :to_party_relationships, :class_name =>'PartyRelationship', :foreign_key => :to_party_id
  has_many :from_return_headers, :class_name => 'ReturnHeader', :foreign_key => :from_party_id
  has_many :to_return_headers, :class_name => 'ReturnHeader', :foreign_key => :to_party_id
  has_many :customer_order_parties, :class_name => 'OrderPart', :foreign_key => :customer_party_id
  has_many :vendor_order_parties, :class_name => 'OrderPart', :foreign_key => :vendor_party_id
  has_many :carrier_order_parties, :class_name => 'OrderPart', :foreign_key => :carrier_party_id
  has_many :order_part_parties
  has_many :time_entries
  has_many :timesheets
  has_many :client_timesheets, :class_name => 'Timesheet', :foreign_key => :client_party_id
  has_many :timesheet_parties
  has_many :work_effort_parties
  has_many :sales_opportunity_parties
  has_many :party_needs
  has_many :organization_sales_forecast, :class_name => 'SalesForecast', :foreign_key => :organization_party_id
  has_many :internal_sales_forecast, :class_name => 'SalesForecast', :foreign_key => :internal_party_id
  has_many :supplier_products, :class_name => 'SupplierProduct', :foreign_key => :supplier_party_id
  has_many :subscriber_subscriptions, :class_name => 'Subscription', :foreign_key => :subscriber_party_id
  has_many :organization_product_stores, :class_name => 'ProductStore', :foreign_key => :organization_party_id
  has_many :product_store_parties
  has_many :product_store_group_parties
  has_many :product_parties
  has_many :gov_agency_asset_registrations, :class_name => 'AssetRegistration', :foreign_key => :gov_agency_party_id
  has_many :cost_components
  has_many :organization_product_average_costs, :class_name => 'ProductAverageCost', :foreign_key => :organization_party_id
  has_many :product_category_parties
  has_many :owner_assets, :class_name => 'Asset', :foreign_key => :owner_party_id
  has_many :asset_party_assignments
  has_many :from_agreements, :class_name => 'Agreement', :foreign_key => :from_party_id
  has_many :to_agreements, :class_name => 'Agreement', :foreign_key => :to_party_id
  has_many :agreement_parties
  has_many :agreement_item_parties
  has_many :party_contact_meches, :class_name => 'PartyContactMech', :foreign_key => 'party_id'
end
