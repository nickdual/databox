
class ContactMech < ActiveRecord::Base
  attr_accessible :contact_mech_type_id, :info_string, :payment_fraud_evidence_id, :trust_level_id
  # Association
  belongs_to :contact_mech_type, :class_name => "ContactMechanismType"
  
  has_one	:shipment_contact_mech
  has_many :from_contact_mech_communication_events, :class_name => "CommunicationEvent", :foreign_key => 'from_contact_mech_id'
  has_many :to_contact_mech_communication_events, :class_name => "CommunicationEvent", :foreign_key => 'to_contact_mech_id'
  has_many :communication_event_parties
  has_many :return_contact_meches
  has_many :origin_return_headers, :class_name => 'ReturnHeader', :foreign_key => :origin_contact_mech_id
  has_many :order_contact_meches
  has_many :postal_order_parties, :class_name => 'OrderPart', :foreign_key => :postal_contact_mech_id
  has_many :telecom_order_parties, :class_name => 'OrderPart', :foreign_key => :telecom_contact_mech_id
  has_many :work_effort_contact_meches
  has_many :deliver_to_subscriptions, :class_name => 'Subscription', :foreign_key => :deliver_to_contact_mech_id
  has_many :contact_payment_trust_levels, :foreign_key => :trust_level_id
end
