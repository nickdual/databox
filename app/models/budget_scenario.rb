class BudgetScenario < ActiveRecord::Base
  attr_accessible :description

  belongs_to :budget_scenario_application
  has_one :create_budget_scenario_rules
end
