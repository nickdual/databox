class PartyClassificationType < ActiveRecord::Base
  attr_accessible :description, :name, :parent_id
  # Association 
  belongs_to :parent, :class_name => 'PartyClassificationType', :foreign_key => 'parent_id'
  has_many :children, :class_name => 'PartyClassificationType', :foreign_key => 'parent_id'
  has_many :classification_type_party_classifications , :class_name => 'PartyClassification' , :foreign_key => 'classification_type_id'
end
