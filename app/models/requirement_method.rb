class RequirementMethod < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :product_stores
  has_many :products
end
