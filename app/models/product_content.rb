class ProductContent < ActiveRecord::Base
  has_paper_trail
  attr_accessible :content_location, :from_date, :product_content_type_id, :product_id, :sequence_num, :thru_date, :file, :file_content_type, :file_file_name, :file_file_size,:file_updated_at
  has_attached_file :file,
                    :styles => { :medium => "300x300>", :thumb => "182x182>" },
                    :convert_options =>
                    {
                      :medium => "-quality 80 -interlace Plane",
                      :thumb => "-quality 80 -interlace Plane"
                    },
                    :storage => :s3,
                    :s3_credentials => {
                      :access_key_id => ENV['S3_KEY'],
                      :secret_access_key => ENV['S3_SECRET'],
                      :bucket => ENV['S3_BUCKET']

                    }

  # Composite primary keys
  #self.primary_keys = :product_id, :content_location, :product_content_type_id, :from_date
  # Association
  belongs_to :product
  belongs_to :product_content_type
end