class TaxAuthorityAssocType < ActiveRecord::Base
  attr_accessible :description, :name
  has_many :tax_authority_assocs
end
