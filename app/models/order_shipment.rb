class OrderShipment < ActiveRecord::Base
  attr_accessible :order_id, :order_item_seq_id, :quantity, :shipment_id, :shipment_item_seq_id
  # Association
  belongs_to :order_header, :class_name => 'OrderHeader', :foreign_key => :order_id
  belongs_to :order_item_seq, :class_name => 'OrderItem', :foreign_key => :order_item_seq_id
  belongs_to :shipment
  belongs_to :shipment_item_seq, :class_name => 'ShipmentItem', :foreign_key => :shipment_item_seq_id
end
