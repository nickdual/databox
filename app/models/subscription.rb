class Subscription < ActiveRecord::Base
  attr_accessible :available_time, :available_time_uom_id, :description, :external_subscription_id, :from_date, :order_id, :order_item_seq_id, :product_id, :purchase_from_date, :purchase_thru_date, :subscriber_party_id, :subscriber_party_id, :subscription_resource_id, :subscription_type_id, :thru_date, :use_count_limit, :use_time, :use_time_uom_id, :deliver_to_contact_mech_id
  # Association
  belongs_to :subscription_type
  belongs_to :subscription_resource
  belongs_to :subscriber, :class_name => 'Party', :foreign_key => :subscriber_party_id
  belongs_to :deliver_to, :class_name => 'ContactMech', :foreign_key => :deliver_to_contact_mech_id
  belongs_to :order_item, :class_name => 'OrderItem', :foreign_key => [:order_id, :order_item_seq_id]
  belongs_to :product
  has_many :subscription_deliveries
end
