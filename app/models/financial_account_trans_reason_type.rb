class FinancialAccountTransReasonType < ActiveRecord::Base
  attr_accessible :description, :name
end
