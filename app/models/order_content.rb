class OrderContent < ActiveRecord::Base
  attr_accessible :content_location, :from_date, :order_content_type_id, :order_id, :order_item_seq_id, :thru_date
  # Association
  belongs_to :order_header ,:class_name => 'OrderHeader', :foreign_key => :order_id
end
