class OrderItemBilling < ActiveRecord::Base
  attr_accessible :amount, :invoice_id, :invoice_item_seq_id, :item_issuance_id, :order_id, :order_item_seq_id, :quantity, :shipment_receipt_id
  # Composite primary keys
  self.primary_keys = :order_id, :order_item_seq_id, :invoice_id, :invoice_item_seq_id
  # Association
  belongs_to :order_item, :foreign_key => [:order_id, :order_item_seq_id]
  belongs_to :item_issuance
  belongs_to :shipment_receipt
  belongs_to :invoice_item, :foreign_key => [:invoice_id, :invoice_item_seq_id]
end
