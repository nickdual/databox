class SettlementTermValue < ActiveRecord::Base
  attr_accessible :description, :term_id, :term_value, :term_value_uom_id
end
