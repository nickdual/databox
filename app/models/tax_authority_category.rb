class TaxAuthorityCategory < ActiveRecord::Base
  attr_accessible :tax_auth_geo_id, :tax_auth_party_id, :product_category_id
   	
  belongs_to :tax_authority
  belongs_to :product_category 	
end
