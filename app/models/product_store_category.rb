class ProductStoreCategory < ActiveRecord::Base
  attr_accessible :from_date, :product_category_id, :product_store_id, :sequence_num, :store_category_type_id, :thru_date
  # Composite primary keys
  self.primary_keys = :product_store_id, :product_category_id, :store_category_type_id, :from_date
  # Association
  belongs_to :product_store
  belongs_to :product_category
  belongs_to :store_category_type, :class_name => 'ProductStoreCategoryType', :foreign_key => :store_category_type_id
end
