class CostComponentCalc < ActiveRecord::Base
  attr_accessible :cost_gl_account_type_id, :cost_uom_id, :description, :fixed_cost, :offset_gl_account_type_id, :per_milli_second, :variable_cost
  # Association
  has_many :cost_components
  has_many :product_cost_component_calcs
end
