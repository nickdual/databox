class MarketSegment < ActiveRecord::Base
  attr_accessible :description, :market_segment_id, :market_segment_type_enum_id, :product_store_id
  
  belongs_to :market_segment_type_enum, :class_name => "MarketSegmentType", :foreign_key => "market_segment_type_enum_id"
  belongs_to :product_store
end
