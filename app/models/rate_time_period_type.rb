class RateTimePeriodType < ActiveRecord::Base
  attr_accessible :description, :length_uom_id, :period_length, :time_period_type_id
  # Association
  has_many :rate_amounts
end
