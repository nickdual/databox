class EmplPositionResponsibility < ActiveRecord::Base
  attr_accessible :comments, :empl_position_id, :from_date, :responsibility_enum_id, :thru_date
  # Association
  belongs_to :employment_responsibility, :foreign_key => :responsibility_enum_id
end
