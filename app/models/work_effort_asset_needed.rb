class WorkEffortAssetNeeded < ActiveRecord::Base
  attr_accessible :asset_product_id, :estimated_cost, :estimated_duration, :estimated_quantity, :work_effort_id
  # Composite primary keys
  # self.primary_keys = :work_effort_id, :asset_product_id
  # Association
  belongs_to :work_effort
  belongs_to :asset_product, :class_name => 'Product', :foreign_key => :asset_product_id
end
