class PromotionRuleAction < ActiveRecord::Base
  attr_accessible :amount, :item_id, :party_id, :promotion_rule_action_name, :promotion_rule_id, :quantity, :service_name, :use_cart_quantity

  validates :quantity, :numericality => true
  validates :quantity, :presence => true

  validates :amount, :numericality => true
  validates :amount, :presence => true

  validates :item_id, :numericality => true
  validates :item_id, :presence => true

  validates :party_id, :presence => true

  validates :service_name, :presence => true

  validates :promotion_rule_action_name, :presence => true

  # Association
  belongs_to :promotion_rule
  has_many :promotion_rule_action_categories, :dependent => :destroy
  has_many :promotion_rule_action_products, :dependent => :destroy
  enum_attr :promotion_rule_action_name, %w(PPRA_GIFT_WITH_PURCHASE PPRA_X_PRODUCT_Y_PECENT_DISCOUNT PPRA_X_PRODUCT_Y_DISCOUNT PPRA_X_PRODUCT_Y_PRICE PPRA_ORDER_PERCENT_DISCOUNT PPRA_ORDER_AMOUNT_FLAT PPRA_PRODUCT_FOR_PRICE PPRA_SHIPPING_X_DISCOUNT PPRA_CALL_SERVICE PPRA_TAX_DISCOUNT) do
    label :PPRA_GIFT_WITH_PURCHASE => 'Gift with Purchase'
    label :PPRA_X_PRODUCT_Y_PECENT_DISCOUNT => 'X Product for Y% Discount'
    label :PPRA_X_PRODUCT_Y_DISCOUNT =>  'X Product for Y Discount'
    label :PPRA_X_PRODUCT_Y_PRICE => 'X Product for Y Price'
    label :PPRA_ORDER_PERCENT_DISCOUNT => 'Order Percent Discount'
    label :PPRA_ORDER_AMOUNT_FLAT =>  'Order Amount Flat'
    label :PPRA_PRODUCT_FOR_PRICE =>  'Product for [Special Promo] Price'
    label :PPRA_SHIPPING_X_DISCOUNT =>  'Shipping X% Discount'
    label :PPRA_CALL_SERVICE => 'Call Service'
    label :PPRA_TAX_DISCOUNT =>  'Tax % Discount'
  end
end
