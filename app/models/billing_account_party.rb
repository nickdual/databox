class BillingAccountParty < ActiveRecord::Base
  attr_accessible :billing_account_id, :from_date, :party_id, :role_type_id, :thru_date
  
  belongs_to :billing_account
  belongs_to :party
  belongs_to :role_type
end
