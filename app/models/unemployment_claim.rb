class UnemploymentClaim < ActiveRecord::Base
  attr_accessible :description, :employment_id, :from_date, :status_id, :thru_date, :unemployment_claim_date
  # Association
  belongs_to :unemployment_claim_status, :foreign_key => :status_id
end
