class ProductReviewStatus < ActiveRecord::Base
  attr_accessible :description, :sequence_num, :status_id
  # Association
  has_many :product_reviews, :class_name => 'ProductReview', :foreign_key => :status_id
  has_many :status_valids, :class_name => 'ProductReviewStatusValid', :foreign_key => :status_id
  has_many :to_status_valids, :class_name => 'ProductReviewStatusValid', :foreign_key => :to_status_id
end
