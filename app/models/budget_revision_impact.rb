class BudgetRevisionImpact < ActiveRecord::Base
  attr_accessible :budget_id, :budget_item_seq_id, :revision_seq_id, :revised_amount, :add_delete_flag, :revision_reason

  belongs_to :budget
  belongs_to :budget_item
  belongs_to :budget_revision
end
