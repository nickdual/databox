class Division < ActiveRecord::Base
  attr_accessible :country_id, :code, :full_code, :name, :ascii_name, :geonames_id

  belongs_to :country
  has_many :cities
end