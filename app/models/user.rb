class User < ActiveRecord::Base
  rolify
  after_create :assign_role
  after_create :create_party
  devise :invitable, :database_authenticatable, :registerable,:omniauthable, :confirmable,
          :recoverable, :rememberable, :trackable, :validatable, :invite_for => 2.weeks
  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :approval, :sign_in_count, :created_at, :confirmed_at,:id
  #validate :approval, acceptance: true
  # Association
  has_many :product_promotions
  has_many :created_by_user_login_promotion_codes, :class_name => 'PromotionCode', :foreign_key => :created_by_user_login
  has_many :last_modified_by_user_login_promotion_codes, :class_name => 'PromotionCode', :foreign_key => :last_modified_by_user_login
  has_many :created_by_user_login_product_promotions, :class_name => 'ProductPromotion', :foreign_key => :created_by_user_login
  has_many :last_modified_by_user_login_product_promotions, :class_name => 'ProductPromotion', :foreign_key => :last_modified_by_user_login

  has_one :party


  # Assign a default role when a user is created.

  def assign_role
     self.add_role("visitor")
  end

  def create_party
    self.party= Party.create
    self.save
  end

  def self.find_for_google_oauth2(access_token, signed_in_resource=nil)
    data = access_token.info
    user = User.where(:email => data["email"]).first

    unless user
      user = User.create(email: data["email"],
                         password: Devise.friendly_token[0,20]
      )
    end
    user
  end
end
