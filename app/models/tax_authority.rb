class TaxAuthority < ActiveRecord::Base
  attr_accessible :tax_auth_geo_id, :tax_auth_party_id, :tax_auth_party_id, :tax_id_format_pattern, :include_tax_in_price
  # Composite primary keys
  self.primary_keys = :tax_auth_geo_id, :tax_auth_party_id
  # Association
	belongs_to :tax_auth
	has_one :tax_authority_assoc
	has_one :tax_authority_category	
	has_one :tax_authority_rate
	has_one :tax_authority_gl_account
  has_many :order_adjustments, :class_name => 'OrderAdjustment', :foreign_key =>  [:tax_auth_geo_id, :tax_auth_party_id]
  has_many :party_product_prices, :class_name => 'ProductPrice', :foreign_key => :tax_auth_party_id
  has_many :geo_product_prices, :class_name => 'ProductPrice', :foreign_key => :tax_auth_geo_id
end
