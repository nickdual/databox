class GiftCard < ActiveRecord::Base
  attr_accessible :card_number, :expire_date, :payment_method_id, :pin_number
  
  belongs_to :payment_method
end
