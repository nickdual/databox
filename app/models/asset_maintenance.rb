class AssetMaintenance < ActiveRecord::Base
  attr_accessible :asset_id, :interval_meter_type_id, :interval_quantity, :interval_uom_id, :maintenance_type_id, :product_maintenance_id, :purchase_order_id, :status_id, :task_work_effort_id
  # Association
  has_one :item_issuance
  belongs_to :asset
  belongs_to :status, :class_name => 'AssetMaintenanceStatus', :foreign_key => :status_id
  belongs_to :maintenance_type
  belongs_to :product_maintenance
  belongs_to :task, :class_name => 'WorkEffort', :foreign_key => :task_work_effort_id
  belongs_to :interval_meter_type, :class_name => 'ProductMeterType', :foreign_key => :interval_meter_type_id
  belongs_to :purchase_order, :class_name => 'OrderHeader', :foreign_key => :purchase_order_id
  has_many :asset_maintenance_meters
  has_many :asset_maintenance_order_items
  has_many :asset_meters
  has_many :asset_details
end
