class OrderRequirementCommitment < ActiveRecord::Base
  attr_accessible :order_id, :order_item_seq_id, :quantity, :requirement_id
end
