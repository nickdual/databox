
class ProductPromotion < ActiveRecord::Base
  attr_accessible :billback_factor, :override_org_party_id, :promotion_name, :promotion_show_to_customer, :promotion_text, :require_code, :use_limit_per_customer, :use_limit_per_order, :use_limit_per_promotion, :user_entered, :user_id, :created_by_user_login, :last_modified_by_user_login
  # Association
  belongs_to :user
  belongs_to :created_by_user_login, :class_name => 'User', :foreign_key =>  :created_by_user_login
  belongs_to :last_modified_by_user_login, :class_name => 'User', :foreign_key => :last_modified_by_user_login
  has_many :promotion_codes, :foreign_key => :product_promotion_id
  has_many :promotion_products, :foreign_key => :product_promotion_id
  has_many :promotion_categories, :foreign_key => :product_promotion_id
  has_many :promotion_rules
  has_many :promotion_stores, :foreign_key => :product_promotion_id
  validates :promotion_name, :presence => {:message => I18n.t("product_promotion.validate.promotion_name")}


  def self.promotion_by_user(user_id)
      @promotions = self.where(:user_id => user_id).order("id ASC")
      @promotions.map{ |item| ["[#{item.id}]#{item.promotion_name}", item.id] }
  end

  def self.simple_search(params)
    if (params['content'].blank?)
      product_promotions = ProductPromotion.where({})
    else
      type = params[:type]
      content = params[:content]
      if (type == 'promotion_name')
        product_promotions = ProductPromotion.where([type + ' like ?', '%' + content + '%'])
      elsif (type == 'user_entered')
        content = content.downcase
        if (content == 't' || content == 'true' || content == 1)
          content = 't'
        elsif (content == 'f' || content == 'false' || content == 0)
          content = 'f'
        end
        product_promotions = ProductPromotion.where({type => content})
      else
        product_promotions = ProductPromotion.where({type => content})
      end
    end
    return product_promotions
  end

  def self.check_id(previous=nil)
    str = ''
    str = previous.to_s
    if ProductPromotion.where(:promotion_id => str).count != 0
      str = ''
    end

    return str
  end

  def self.random_string(type, length, previous=nil)
    str = ''
    if (type == "smart")
      begin
        str = ApplicationHelper.random_friendly_string('abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789', length)
      end while ProductPromotion.where(:promotion_id => str).count != 0
    elsif (type == "sequence")
      if (previous == nil || previous == '')
        previous = ApplicationHelper.random_friendly_string('0123456789', length)
      end
      str = previous.to_i
      while ProductPromotion.where(:promotion_id => str.to_s).count != 0
        str += 1
      end
    else
      begin
        str = ApplicationHelper.random_friendly_string('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', length)
      end while ProductPromotion.where(:promotion_id => str).count != 0
    end
    return str
  end
end
