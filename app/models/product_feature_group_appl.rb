class ProductFeatureGroupAppl < ActiveRecord::Base
  attr_accessible :from_date, :product_feature_group_id, :product_feature_id, :sequence_num, :thru_date
  # Composite primary keys
  self.primary_keys = :product_feature_group_id, :product_feature_id, :from_date
  # Association
  belongs_to :product_feature_group
  belongs_to :product_feature
end
