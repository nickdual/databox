class ShipmentItemSource < ActiveRecord::Base
  attr_accessible :binLocation_number, :order_id, :order_item_seq_id, :product_id, :quantity, :quantity_not_packed, :return_id, :return_item_seq_id, :shipment_id, :status_id
  # Association
  belongs_to :shipment_item, :foreign_key => :shipment_id
  belongs_to :order_item, :foreign_key => :order_id
  belongs_to :order_item_seq, :class_name => 'OrderItem', :foreign_key => :order_item_seq_id
  belongs_to :return_header, :foreign_key => :return_id
  belongs_to :return_header_seq, :class_name => 'ReturnHeader', :foreign_key => :return_item_seq_id
  belongs_to :shipment_item_pick_status, :foreign_key => :status_id
end
