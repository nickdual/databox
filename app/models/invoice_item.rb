class InvoiceItem < ActiveRecord::Base
  attr_accessible :amount, :asset_id, :description, :invoice_id, :invoice_item_seq_id, :item_type_enum_id, :override_gl_account_id, :override_org_party_id, :parent_invoice_id, :parent_invoice_item_seq_id, :product_id, :quantity, :sales_opportunity_id, :tax_auth_geo_id, :tax_auth_party_id, :tax_authority_rate_id, :taxable_flag, :uom_id
  # Composite primary keys
  self.primary_keys = :invoice_id, :invoice_item_seq_id
  # Association
  belongs_to :item_type_enum, :class_name => "ItemType", :foreign_key => "item_type_enum_id"
  belongs_to :invoice
  belongs_to :asset
  belongs_to :product
  belongs_to :parent_invoice, :class_name => "InvoiceItem", :foreign_key => "parent_invoice_id"
  belongs_to :parent_invoice_item_seq, :class_name => "InvoiceItem", :foreign_key => "parent_invoice_item_seq"
  belongs_to :override_gl_account, :class_name => "GlAccount", :foreign_key => "override_gl_account_id"
  belongs_to :tax_auth_party, :class_name => "Party", :foreign_key => "tax_auth_party_id"
  belongs_to :tax_authority_rate
  belongs_to :override_gl_account, :class_name => "Party", :foreign_key => "override_gl_account_id"
  belongs_to :sales_opportunity
  has_many :work_effort_billings, :foreign_key => [:invoice_id, :invoice_item_seq_id]
  has_many :time_entries, :foreign_key => [:invoice_id, :invoice_item_seq_id]
  has_many :order_adjustment_billings, :foreign_key => [:invoice_id, :invoice_item_seq_id]
  has_many :order_item_billings, :foreign_key => [:invoice_id, :invoice_item_seq_id]
end