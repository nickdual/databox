class GlAccountCategoryType < ActiveRecord::Base
  attr_accessible :description, :name
  has_many :gl_account_category
end
