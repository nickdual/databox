class OrderCommunicationEvent < ActiveRecord::Base
  attr_accessible :communication_event_id, :order_id
  # Composite primary keys
  # self.primary_keys = :communication_event_id, :order_id
  # Association
  belongs_to :order_header, :class_name => 'OrderHeader', :foreign_key => :order_id
  belongs_to :communication_event
end
