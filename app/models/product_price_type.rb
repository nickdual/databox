class ProductPriceType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :product_prices, :class_name => 'ProductPrice', :foreign_key => :price_type_id
end
