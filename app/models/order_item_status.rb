class OrderItemStatus < ActiveRecord::Base
  attr_accessible :description, :sequence_num, :status_id
  # Association
  has_many :order_items, :class_name => 'OrderItem',  :foreign_key => :status_id
  has_many :status_valids, :class_name => 'OrderItemStatusValid', :foreign_key => :status_id
  has_many :to_status_valids, :class_name => 'OrderItemStatusValid', :foreign_key => :to_status_id
end
