class SubscriptionDelivery < ActiveRecord::Base
  attr_accessible :comments, :communication_event_id, :date_sent, :subscription_id
  # Composite primary keys
  # self.primary_keys = :subscription_id, :date_sent
  # Association
  belongs_to :subscription
  belongs_to :communication_event
end
