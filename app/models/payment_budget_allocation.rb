class PaymentBudgetAllocation < ActiveRecord::Base
  attr_accessible :amount, :budget_id, :budget_item_seq_id, :payment_id
  
  belongs_to :budget, :class_name => "BudgetItem", :foreign_key => "budget_id"
  belongs_to :budget_item_seq, :class_name => "BudgetItem", :foreign_key => "budget_item_seq_id"
  belongs_to :payment
end
