class PartyRelationshipType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :type_party_relationships, :class_name => 'PartyRelationship', :foreign_key => :relationship_type_id
end
