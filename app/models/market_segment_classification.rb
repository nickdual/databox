class MarketSegmentClassification < ActiveRecord::Base
  attr_accessible :market_segment_id, :party_classification_id
  
  belongs_to :market_segment
  belongs_to :party_classification
end
