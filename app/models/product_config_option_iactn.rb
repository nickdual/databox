class ProductConfigOptionIactn < ActiveRecord::Base
  attr_accessible :config_item_id, :config_option_seq_id, :description, :iactn_type_id, :to_config_item_id, :to_config_option_seq_id
  # Association
  belongs_to :product_config_option, :foreign_key => [:config_item_id, :config_option_seq_id]
  belongs_to :to_product_config_option, :class_name => 'ProductConfigOption', :foreign_key => [:to_config_item_id, :to_config_option_seq_id]  
end
