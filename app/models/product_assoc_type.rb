class ProductAssocType < ActiveRecord::Base
  attr_accessible :description, :name, :parent_id
  # Association
  belongs_to :parent, :class_name => 'ProductAssocType', :foreign_key => :parent_id
  has_many :children, :class_name => 'ProductAssocType', :foreign_key => :parent_id
  has_many :product_assocs
  
end
