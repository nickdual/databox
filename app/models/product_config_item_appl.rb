class ProductConfigItemAppl < ActiveRecord::Base
  attr_accessible :config_item_id, :config_type_id, :default_config_option_id, :description, :from_date, :is_mandatory, :product_id, :sequence_num, :thru_date
  # Composite primary keys
  self.primary_keys = :product_id, :config_item_id, :sequence_num, :from_date
  # Association
  belongs_to :product
  belongs_to :config_item, :class_name => 'ProductConfigItem', :foreign_key => :config_item_id
end
