class ShipmentReceiptParty < ActiveRecord::Base
  attr_accessible :party_id, :role_type_id, :shipment_receipt_id

  belongs_to :shipment_receipt
  belongs_to :party
  belongs_to :role_type
end
