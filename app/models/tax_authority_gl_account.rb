class TaxAuthorityGlAccount < ActiveRecord::Base
  attr_accessible :tax_auth_geo_id, :tax_auth_party_id, :organization_party_id, :gl_account_id

  belongs_to :tax_authority
  belongs_to :party
	belongs_to :gl_account
end
