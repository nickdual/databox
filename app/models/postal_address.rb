class PostalAddress < ActiveRecord::Base
  attr_accessible :access_code, :address1, :address2, :attn_name, :city, :commercial, :contact_mech_id, :country_geo_id, :county_geo_id, :directions, :geo_point_id, :postal_code, :postal_code_ext, :postal_code_geo_id, :state_province_geo_id, :to_name, :unit_number

  has_one :shipment, :dependent => :destroy
  has_one :shipment_route_segment
  belongs_to :country, :foreign_key =>  :county_geo_id
end
