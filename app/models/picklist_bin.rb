class PicklistBin < ActiveRecord::Base
  attr_accessible :bin_location_number, :picklist_id, :primary_order_id, :primary_order_part_seq_id
  # Association
  has_one :shipment

  belongs_to :picklist
  belongs_to :primary_order_part, :class_name => 'OrderPart', :foreign_key => [:primary_order_id, :primary_order_part_seq_id]
  has_many :picklist_bin_items
end
