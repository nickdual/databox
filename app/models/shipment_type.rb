class ShipmentType < ActiveRecord::Base
  attr_accessible :description, :name, :parent_id

  has_one :shipment
end
