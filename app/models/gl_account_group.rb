class GlAccountGroup < ActiveRecord::Base
  attr_accessible :gl_account_group_type_enum_id, :description

  belongs_to :gl_account_group_type
  has_one :gl_account_group_member
end