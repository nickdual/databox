class PartyRole < ActiveRecord::Base
  attr_accessible :party_id, :role_type_id
  # Composite primary keys
  # self.primary_keys = :party_id, :role_type_id
  belongs_to :party
  belongs_to :role_type
end
