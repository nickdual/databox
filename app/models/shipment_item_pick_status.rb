class ShipmentItemPickStatus < ActiveRecord::Base
  attr_accessible :description, :name, :sequence_num
  # Association
  has_many :shipment_item_sources
  has_many :status_valids, :class_name => 'ShipmentItemPickStatusValid', :foreign_key => :status_id
  has_many :to_status_valids, :class_name => 'ShipmentItemPickStatusValid', :foreign_key => :to_status_id
end
