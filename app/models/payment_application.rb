class PaymentApplication < ActiveRecord::Base
  attr_accessible :amount_applied, :billing_account_id, :invoice_id, :invoice_item_seq_id, :override_gl_account_id, :payment_application_id, :payment_id, :tax_auth_geo_id, :to_payment_id
  
  belongs_to :payment, :foreign_key => "payment_id"
  belongs_to :invoice, :class_name => "InvoiceItem", :foreign_key => "invoice_id"
  belongs_to :invoice_item_seq, :class_name => "InvoiceItem", :foreign_key => "invoice_item_seq"
  belongs_to :billing_account, :class_name => "BillingAccount", :foreign_key => "billing_account_id"
  belongs_to :to, :class_name => "Payment", :foreign_key => "to_payment_id"
  belongs_to :override_gl_account, :class_name => "GlAccount", :foreign_key => "override_gl_account_id"
end
