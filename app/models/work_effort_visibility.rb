class WorkEffortVisibility < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :work_efforts, :class_name => 'WorkEffort', :foreign_key => :visibility_id
end
