class AssetRegistration < ActiveRecord::Base
  attr_accessible :asset_id, :from_date, :gov_agency_party_id, :license_number, :registration_date, :registration_number, :thru_date
  # Composite primary keys
  self.primary_keys = :asset_id, :gov_agency_party_id
  # Association
  belongs_to :asset
  belongs_to :gov_agency, :class_name => 'Party', :foreign_key => :gov_agency_party_id
end
