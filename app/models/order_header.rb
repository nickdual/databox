class OrderHeader < ActiveRecord::Base
  attr_accessible :billing_account_id, :currency_uom_id, 
  :entry_date, :external_id, :grand_total, :last_ordered_date, :order_id, :order_name, :parent_order_id, 
  :product_store_id, :recurrence_info_id, :remaining_sub_total, :sales_channel_id, :status_id, :sync_status_id, 
  :terminal_id, :visit_id
  # Association
  has_one :shipment
  belongs_to :status, :class_name => 'OrderHeaderStatus', :foreign_key => :status_id
  #belongs_to :person
  belongs_to :product_store
  belongs_to :sales_channel
  belongs_to :sync_status
  belongs_to :parent, :class_name => 'OrderHeader', :foreign_key => :parent_order_id
  has_many :children, :class_name => 'OrderHeader', :foreign_key => :parent_order_id
  has_many :replacement_return_items, :class_name => 'ReturnItem', :foreign_key => :replacement_order_id
  has_many :order_adjustments, :class_name => 'OrderAdjustment', :foreign_key => :order_id
  has_many :order_communication_events, :class_name => 'OrderCommunicationEvent', :foreign_key => :order_id
  has_many :order_contact_meches, :class_name => 'OrderContactMech', :foreign_key => :order_id
  has_many :order_contents, :class_name => 'OrderContent', :foreign_key => :order_id
  has_many :order_items, :class_name => 'OrderItem', :foreign_key => :order_id
  has_many :order_notes, :class_name => 'OrderNote', :foreign_key => :order_id
  has_many :order_parts, :class_name => 'OrderPart', :foreign_key => :order_id
  has_many :order_shipments, :class_name => 'OrderShipment', :foreign_key => :order_id
  has_many :order_terms, :class_name => 'OrderTerm', :foreign_key => :order_id
  has_many :sales_opportunity_quotes, :class_name => 'SalesOpportunityQuote', :foreign_key => :order_id
  has_many :purchase_asset_maintenances, :class_name => 'AssetMaintenance', :foreign_key => :purchase_order_id
  has_many :asset_maintenance_order_items, :class_name => 'AssetMaintenanceOrderItem', :foreign_key => :order_id
  has_many :asset_reservations, :class_name =>  'AssetReservation', :foreign_key => :order_id
end
