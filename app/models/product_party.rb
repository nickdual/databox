class ProductParty < ActiveRecord::Base
  attr_accessible :comments, :from_date, :party_id, :product_id, :role_type_id, :sequence_num, :thru_date
  # Composite primary keys
  # self.primary_keys = :product_id, :party_id, :role_type_id, :from_date
  # Association
  belongs_to :product
  belongs_to :party
  belongs_to :role_type
end
