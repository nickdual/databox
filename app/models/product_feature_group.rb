class ProductFeatureGroup < ActiveRecord::Base
  attr_accessible :description
  # Association
  has_many :product_category_feat_grp_appls
  has_many :product_feature_group_appls
end
