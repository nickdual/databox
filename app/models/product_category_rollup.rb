class ProductCategoryRollup < ActiveRecord::Base
  attr_accessible :from_date, :parent_product_category_id, :product_category_id, :sequence_num, :thru_date
  # Composite primary keys
  self.primary_keys = :product_category_id, :parent_product_category_id, :from_date
  # Association
  belongs_to :product_category
  belongs_to :parent_product_category, :class_name => 'ProductCategory', :foreign_key => :parent_product_category_id 
end
