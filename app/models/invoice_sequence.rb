class InvoiceSequence < ActiveRecord::Base
  attr_accessible :description, :name, :sequence_num
  has_many :party_acctg_preferences
end
