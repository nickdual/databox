class BudgetType < ActiveRecord::Base
  attr_accessible :description, :name
  has_many :budgets
end
