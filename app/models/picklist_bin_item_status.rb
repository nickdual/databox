class PicklistBinItemStatus < ActiveRecord::Base
  attr_accessible :description, :name, :sequence_num
  # Association
  has_many :status_valids, :class_name => 'PicklistBinItemStatusValid', :foreign_key => :status_id
  has_many :to_status_valids, :class_name => 'PicklistBinItemStatusValid', :foreign_key => :to_status_id
end
