class AssetTypeGlAccount < ActiveRecord::Base
	attr_accessible :asset_type_enum_id, :asset_id, :organization_party_id, :asset_gl_account_id, :acc_dep_gl_account_id,
	:dep_gl_account_id, :profit_gl_account_id, :loss_gl_account_id

	belongs_to :asset_type
	belongs_to :asset
	belongs_to :party
	belongs_to :gl_account	

end
