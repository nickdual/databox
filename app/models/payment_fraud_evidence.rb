class PaymentFraudEvidence < ActiveRecord::Base
  attr_accessible :comments, :fraud_type_enum_id, :order_id, :party_id, :payment_fraud_evidence, :payment_id, :visit_id
  
  belongs_to :payment
  belongs_to :order_header
  belongs_to :party
end
