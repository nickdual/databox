class GlReconciliationStatus < ActiveRecord::Base
  attr_accessible :description, :sequence_num, :status_id
  has_many :gl_reconciliations
  has_many :status_valids, :class_name => 'GlReconciliationStatusValid', :foreign_key => :status_id
  has_many :to_status_valids, :class_name => 'GlReconciliationStatusValid', :foreign_key => :to_status_id
end
