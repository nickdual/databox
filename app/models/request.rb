class Request < ActiveRecord::Base
  attr_accessible :currency_uom_id, :description, :from_party_id, :fullfill_contact_mech_id, :maximun_amount_uom_id, :priority, :product_store_id, :request_category_id, :request_date, :request_id, :request_type_enum_id, :response_required_date, :sales_chaneel_enum_id, :status_id

  belongs_to :request_category
  belongs_to :status
  belongs_to :request_status
  belongs_to :from_party, :class_name => "Party", :foreign_key => "from_party_id"
  belongs_to :product_store
  belongs_to :sales_channel_enum, :class_name => "SalesChannel", :foreign_key => "sales_channel_enum_id"
  belongs_to :FullfillContactMech, :class_name => "ContactMech", :foreign_key => "fullfill_contact_mech_id"
end
