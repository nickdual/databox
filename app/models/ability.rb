class Ability
  include CanCan::Ability

  def initialize(user)
    if user
      #  Defining Admin role.
      if user.has_role? :admin
        can :manage, :all
      elsif user.has_role? :visitor
        can :read, :page
      elsif user.has_role? :account_lead
        can :read, :page
      elsif user.has_role? :automated_agent_role
        can :read, :page
      elsif user.has_role? :calendar_role
        can :read, :page
      elsif user.has_role? :client
        can :read, :page
      elsif user.has_role? :comm_participant
        can :read, :page
      elsif user.has_role? :consumer
        can :read, :page
      elsif user.has_role? :contractor
        can :read, :page
      elsif user.has_role? :customer
        can :read, :page
      elsif user.has_role? :distribution_channel
        can :read, :page
      elsif user.has_role? :ISP
        can :read, :page
      elsif user.has_role? :hosting_server
        can :read, :page
      elsif user.has_role? :manufacturer
        can :read, :page
      elsif user.has_role? :na
            can :read, :page
      elsif user.has_role? :organization_role
            can :read, :page
      elsif user.has_role? :owner
            can :read, :page
      elsif user.has_role? :prospect
            can :read, :page
      elsif user.has_role? :person_role
            can :read, :page
      elsif user.has_role? :referrer
            can :read, :page
      elsif user.has_role? :req_manager
            can :read, :page
      elsif user.has_role? :req_requester
            can :read, :page
      elsif user.has_role? :req_taker
            can :read, :page
      elsif user.has_role? :sfa_role
            can :read, :page
      elsif user.has_role? :shareholder
            can :read, :page
      elsif user.has_role? :subscriber
            can :read, :page
      elsif user.has_role? :vendor
            can :read, :page
      elsif user.has_role? :web_master
            can :read, :page
      elsif user.has_role? :workflow_role
            can :read, :page
      elsif user.has_role? :accountant
            can :read, :page
      end
    end
  end
end


# Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user permission to do.
    # If you pass :manage it will apply to every action. Other common actions here are
    # :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. If you pass
    # :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities
