class TimeEntry < ActiveRecord::Base
  attr_accessible :comments, :from_date_datetime, :hours, :invoice_id, :invoice_item_seq_id, :party_id, :rate_type_id, :thru_date, :timesheet_id, :work_effort_id
  # Association
  belongs_to :timesheet
  belongs_to :party
  belongs_to :work_effort
  belongs_to :invoice_item, :foreign_key => [:invoice_id, :invoice_item_seq_id]
end
