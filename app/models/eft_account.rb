class EftAccount < ActiveRecord::Base
  attr_accessible :account_number, :account_type, :bank_name, :company_name_on_account, :name_on_account, :payment_method_id, :routing_number, :years_at_bank
  
  belongs_to :payment_method
end
