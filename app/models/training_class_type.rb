class TrainingClassType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association 
  has_many :training_classes
end
