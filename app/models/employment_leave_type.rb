class EmploymentLeaveType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association 
  has_many :employment_leaves
end
