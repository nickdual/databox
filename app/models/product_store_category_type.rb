class ProductStoreCategoryType < ActiveRecord::Base
  attr_accessible :description, :name
  # Association
  has_many :product_store_categories, :class_name => 'ProductStoreCategory', :foreign_key => :store_category_type_id
end
