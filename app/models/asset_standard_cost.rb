class AssetStandardCost < ActiveRecord::Base
  attr_accessible :amount, :amount_uom_id, :asset_id, :asset_standard_cost_type_id, :from_date, :thru_date
  # Composite primary keys
  self.primary_keys = :asset_id, :asset_standard_cost_type_id, :from_date
  # Association
  belongs_to :asset
end
