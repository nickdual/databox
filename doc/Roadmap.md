DataBox Roadmap
===============

Phase 1 - v2013-01-04
-------
- Procurement
- Products
- Party
- Order
- Whitestone TEC Sync

Phase 2 - ~v2013-01-25
-------
- Logistics
- API

Phase 3
-------
- Mailchimp Sync
- Desk.com Sync
- Authorize.net Sync
- Google Analytics Sync
- Reports

Phase 4
-------
