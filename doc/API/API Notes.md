http://api.shopify.com/
http://api.spreecommerce.com/v1/products/


Receive a list of all Customers
Search for customers matching supplied query
Receive a single Customer
Create a new Customer
Modify an existing Customer
Receive a list of all Fulfillments
Receive a single Fulfillment
Receive a list of all Orders
Receive a single Order
Close an Order
Re-open a closed Order
Cancel an Order
Change an Order’s note, note-attributes, email, and buyer-accepts-marketing state (the only attributes modifiable through the API)
Receive a list of all Products
Receive a single Product
Receive a list of all Product Images
Receive a single Product Image
Receive a list of all Product Variants
Receive a single Product Variant