Given /^I have product promotions named (.+)$/ do |promotion_names|
  #i = 1
  promotion_names.split(', ').each do |promotion_name|
    ProductPromotion.create!(:promotion_name => promotion_name )
    #i = i + 1
  end
end

Then /^I should see "([^"]*)"$/ do |promotion_name|
  page.should have_content(promotion_name)
end
Then /^I should not see "([^"]*)"$/ do |name|
  page.should_not have_content(name)
end
Then /^I fill in "(.*?)" with "(.*?)"$/ do |field, value|
  fill_in(field, :with => value)
end

When /^I press "(.*?)"$/ do |button|
  click_button(button)
end
When /^I press the link "(.*?)"$/ do |link|
  find(link).click
end
When /^I have a product promotion/ do
  @user = User.find_by_email("abc@gmail.com")
  @product_promotion = FactoryGirl.create(:product_promotion)
  @product_promotion.update_attributes(:created_by_user_login => @user, :last_modified_by_user_login => @user)
end
Given /^I have a product store/ do
  @product_store = FactoryGirl.create(:product_store)
end
Then /^I select "(.*?)" from "(.*?)"$/ do |option, field|
  page.select option, :from => field
end
Then /^I check "(.*?)"/ do |class_name|
      check(class_name)
end
Then /^I should see "([^"]*)" in the "([^"]*)" input$/ do |content, id|
  find_field(id).value.should == content
end
Then /^I should see "([^"]*)" in the "([^"]*)" input within "([^"]*)"$/ do |content, id, scope|
 within(scope) do
    find_field(id).value.should == content
 end
end
When /^I wait for (\d+) seconds?$/ do |secs|
  sleep secs.to_i
end


