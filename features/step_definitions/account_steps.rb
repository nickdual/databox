Given /^I have an user$/ do
  @user = FactoryGirl.create(:user)
end
Then /^I give "(.*?)" with "(.*?)" within "(.*?)"$/ do |field, value, within|
  within(within) do
    fill_in(field, :with => value)
  end
end