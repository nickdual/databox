@javascript
Feature: manage product
  Scenario: List Product Promotion
    Given I have an user
    And  I have product promotions named Henry Zieme, Raleigh Donnelly III
    Then I go to login page
    And I give "user_email" with "abc@gmail.com" within "#ml-form"
    And I fill in "user_password" with "123456"
    When I press the link "#btn-login"
    When I go to the list of product promotions
    Then I should see "Henry Zieme"
    And I should see "Raleigh Donnelly III"
  Scenario: Create New Product Promotion
    Given I have an user
    Then I go to login page
    And I give "user_email" with "abc@gmail.com" within "#ml-form"
    And I fill in "user_password" with "123456"
    When I press the link "#btn-login"
    And I have a product promotion
    Then I go to the list of product promotions
    Then I go to the product promotion edit page
    Then I go to new product promotion page
    And I fill in "product_promotion_promotion_name" with "abc"
    And I fill in "product_promotion_promotion_text" with "abc"
    And I fill in "product_promotion_billback_factor" with "1"
    And I fill in "product_promotion_use_limit_per_customer" with "1"
    And I press "Save"
    Then I should see "Product promotion was successfully created."
  Scenario: Create New Promotion Code
    Given I have an user
    Then I go to login page
    And I give "user_email" with "abc@gmail.com" within "#ml-form"
    And I fill in "user_password" with "123456"
    When I press the link "SIGN IN"
    And I wait for 2 seconds
    And I have a product promotion
    Then I go to the list of product promotions
    Then I go to the product promotion edit page
    Then I go to new promotion code page
    And I have a product promotion
    And I fill in "promotion_code_use_limit_per_code" with "123456"
    And I fill in "promotion_code_use_limit_per_customer" with "123456"
    And I press "Save"
    Then I should see "Promotion code was successfully created."
    When I go to the list of promotion codes
    Then I should see "123456"
   Scenario: Create New Promotion Store
     Given I have an user
     And I have a product store
     Then I go to login page
     And I give "user_email" with "abc@gmail.com" within "#ml-form"
     And I fill in "user_password" with "123456"
     When I press the link "SIGN IN"
     And I wait for 2 seconds
     And I have a product promotion
     Then I go to the list of product promotions
     Then I go to the product promotion edit page
     Then I go to the list of promotion stores
     And I select "Store Name [1]" from "store_name_id"
     And I fill in "from_date" with "2012-12-26 00:08:07:486"
     And I fill in "thru_date" with "2012-12-26 00:08:07:486"
     And I press "Insert"
     When I go to the list of promotion stores
     Then I should see "2012-12-26 00:08:07"
     And I fill in "prst_sequence_id_1" with "12345"
     Then I press "Update"
     When I go to the list of promotion stores
     Then I should see "12345" in the "prst_sequence_id_1" input
     And I check "checkall"
     And I press "Delete"
     And I press "OK"
     Then I should not see "2012-12-26 00:08:07:486"
  Scenario: Create New Promotion Rule
    Given I have an user
    Then I go to login page
    And I give "user_email" with "abc@gmail.com" within "#ml-form"
    And I fill in "user_password" with "123456"
    When I press the link "SIGN IN"
    And I wait for 2 seconds
    And I have a product promotion
    Then I go to the list of product promotions
    Then I go to the product promotion edit page
    Then I go to the list of promotion rules
    And I fill in "promotion_rule_name" with "rule name 12345"
    Then I press "add_promotion_rule"
    And I wait for 2 seconds
    Then I go to the list of promotion rules
    Then I should see "rule name 12345" in the "promotion_rule_name" input within "#edit_promotion_rule_1"
