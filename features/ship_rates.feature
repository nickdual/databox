@javascript
Feature: shipment
#  Scenario: quote ship rate
#    When I have exits data
#    Then I am ship_rate page
#    And I press "quote-1"
#    Then I should see "Service Name"
#    And I should see "Total Price"

  Scenario: update shipment
    When I have exits data
    Then I am ship_rate page
    And I fill in "dimession_1" with "3.0,2.0,2.0"
    And I fill in "weight_1" with "3.0"
    And I fill in "starting_point_1" with "AS, Taulaga, 432"
    And I fill in "ending_point_1" with "BG, Beloslav, 76543"
    And I press "update-1"

  Scenario: delete one shipment
    When I have exits data
    Then I am ship_rate page
    And I check "check_delete_1"
    And I press "delete_shipment"
    And I should see "Confirmation Dialog"
    And I press "popup_ok"

  Scenario: delete all shipment
    When I have exits data
    Then I am ship_rate page
    And I check "checkall"
    And I press "delete_shipment"
    And I should see "Confirmation Dialog"
    And I press "popup_ok"