# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130119080328) do

  create_table "accommodation_classes", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "accommodation_map_types", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "accommodation_maps", :force => true do |t|
    t.integer  "accommodation_map_type_id"
    t.integer  "accommodation_class_id"
    t.integer  "asset_id"
    t.integer  "number_of_spaces"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "accommodation_spots", :force => true do |t|
    t.integer  "accommodation_class_id"
    t.integer  "asset_id"
    t.integer  "number_of_spaces"
    t.string   "description"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "acctg_trans", :force => true do |t|
    t.string   "acctg_trans_type_enum_id"
    t.string   "description"
    t.date     "transaction_date"
    t.boolean  "is_posted"
    t.date     "posted_date"
    t.date     "scheduled_posting_date"
    t.integer  "gl_journal_id"
    t.integer  "gl_fiscal_type_enum_id"
    t.string   "voucher_ref"
    t.date     "voucher_date"
    t.integer  "group_status_id"
    t.integer  "asset_id"
    t.integer  "physical_inventory_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.integer  "invoice_id"
    t.integer  "payment_id"
    t.integer  "fin_account_trans_id"
    t.integer  "shipment_id"
    t.integer  "receipt_id"
    t.integer  "work_effort_id"
    t.integer  "their_acctg_trans_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "acctg_trans_entries", :force => true do |t|
    t.integer  "acctg_trans_id"
    t.integer  "acctg_trans_entry_seq_id"
    t.string   "description"
    t.string   "voucher_ref"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.integer  "their_party_id"
    t.integer  "product_id"
    t.integer  "their_product_id"
    t.integer  "asset_id"
    t.integer  "gl_account_type_enum_id"
    t.integer  "gl_account_id"
    t.integer  "organization_party_id"
    t.float    "amount"
    t.integer  "amoun_uom_id"
    t.float    "orig_currency_amount"
    t.integer  "orig_currency_uom_id"
    t.boolean  "debit_credit_flag"
    t.date     "due_date"
    t.integer  "group_id"
    t.integer  "tax_id"
    t.string   "reconcile_status_id"
    t.integer  "settlement_term_id"
    t.boolean  "is_summary"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "acctg_trans_entry_reconcile_statuses", :force => true do |t|
    t.string   "description"
    t.integer  "sequence_num"
    t.string   "status_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "acctg_trans_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "parent_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "agreement_addendums", :force => true do |t|
    t.integer  "agreement_id"
    t.integer  "agreement_item_seq_id"
    t.datetime "addendum_creation_date"
    t.datetime "addendum_effective_date"
    t.text     "addendum_text"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "agreement_item_employments", :force => true do |t|
    t.integer  "agreement_id"
    t.integer  "agreement_item_seq_id"
    t.integer  "employment_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "agreement_item_geos", :force => true do |t|
    t.integer  "agreement_id"
    t.integer  "agreement_item_seq_id"
    t.integer  "geo_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "agreement_item_parties", :force => true do |t|
    t.integer  "agreement_id"
    t.integer  "agreement_item_seq_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "agreement_item_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "agreement_item_work_efforts", :force => true do |t|
    t.integer  "agreement_id"
    t.integer  "agreement_item_seq_id"
    t.integer  "work_effort_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "agreement_items", :force => true do |t|
    t.integer  "agreement_id"
    t.integer  "agreement_item_seq_id"
    t.integer  "agreement_item_type_id"
    t.integer  "currency_uom_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.text     "item_text"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "agreement_parties", :force => true do |t|
    t.integer  "agreement_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "agreement_terms", :force => true do |t|
    t.integer  "agreement_id"
    t.integer  "agreement_item_seq_id"
    t.integer  "settlement_term_id"
    t.integer  "term_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "agreement_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.integer  "parent_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "agreements", :force => true do |t|
    t.integer  "agreement_type_id"
    t.datetime "agreement_date"
    t.integer  "from_party_id"
    t.integer  "from_role_type_id"
    t.integer  "to_party_id"
    t.integer  "to_role_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.text     "description"
    t.text     "text_data"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "asset_classes", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "parent_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "asset_details", :force => true do |t|
    t.integer  "asset_id"
    t.integer  "asset_detail_seq_id"
    t.datetime "effective_date"
    t.decimal  "quantity_on_hand_diff"
    t.decimal  "available_to_promise_diff"
    t.decimal  "unit_cost"
    t.integer  "asset_reservation_id"
    t.integer  "shipment_id"
    t.integer  "shipment_item_seq_id"
    t.integer  "return_id"
    t.integer  "return_item_seq_id"
    t.integer  "work_effort_id"
    t.integer  "asset_maintenance_id"
    t.integer  "item_issuance_id"
    t.integer  "receipt_id"
    t.integer  "physical_inventory_id"
    t.integer  "variance_reason_id"
    t.text     "description"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "asset_geo_points", :force => true do |t|
    t.integer  "asset_id"
    t.integer  "geo_point_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "asset_identification_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "asset_identifications", :force => true do |t|
    t.integer  "asset_id"
    t.integer  "identification_type_id"
    t.string   "id_value"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "asset_maintenance_meters", :force => true do |t|
    t.integer  "asset_maintenance_id"
    t.integer  "product_meter_type_id"
    t.decimal  "meter_value"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "asset_maintenance_order_items", :force => true do |t|
    t.integer  "asset_maintenance_id"
    t.integer  "order_id"
    t.integer  "order_item_seq_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "asset_maintenance_status_valids", :force => true do |t|
    t.integer  "status_id"
    t.integer  "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "asset_maintenance_statuses", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "asset_maintenances", :force => true do |t|
    t.integer  "asset_id"
    t.integer  "status_id"
    t.integer  "maintenance_type_id"
    t.integer  "product_maintenance_id"
    t.integer  "task_work_effort_id"
    t.decimal  "interval_quantity"
    t.integer  "interval_uom_id"
    t.integer  "interval_meter_type_id"
    t.integer  "purchase_order_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "asset_meters", :force => true do |t|
    t.integer  "asset_id"
    t.integer  "product_meter_type_id"
    t.datetime "reading_date"
    t.decimal  "meter_value"
    t.integer  "reading_reason_id"
    t.integer  "asset_maintenance_id"
    t.integer  "work_effort_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "asset_party_assignments", :force => true do |t|
    t.integer  "asset_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "allocated_date"
    t.integer  "status_id"
    t.text     "comments"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "asset_product_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "asset_products", :force => true do |t|
    t.integer  "asset_id"
    t.integer  "product_id"
    t.integer  "asset_product_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.string   "comments"
    t.integer  "sequence_num"
    t.decimal  "quantity"
    t.integer  "quantity_uom_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "asset_registrations", :force => true do |t|
    t.integer  "asset_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "registration_date"
    t.integer  "gov_agency_party_id"
    t.string   "registration_number"
    t.string   "license_number"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "asset_reservation_orders", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "asset_reservations", :force => true do |t|
    t.integer  "order_id"
    t.integer  "order_item_seq_id"
    t.integer  "asset_id"
    t.integer  "reserve_order_id"
    t.decimal  "quantity"
    t.decimal  "quantity_not_available"
    t.datetime "reserved_datetime"
    t.datetime "promised_datetime"
    t.datetime "current_promised_date"
    t.integer  "priority"
    t.integer  "sequence_num"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "asset_standard_costs", :force => true do |t|
    t.integer  "asset_id"
    t.integer  "asset_standard_cost_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "amount_uom_id"
    t.decimal  "amount"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "asset_status_valids", :force => true do |t|
    t.integer  "status_id"
    t.integer  "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "asset_statuses", :force => true do |t|
    t.string   "status_id"
    t.string   "description"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "asset_type_gl_accounts", :force => true do |t|
    t.string   "asset_type_enum_id"
    t.string   "asset_id"
    t.integer  "organization_party_id"
    t.integer  "asset_gl_account_id"
    t.integer  "acc_dep_gl_account_id"
    t.integer  "dep_gl_account_id"
    t.integer  "profit_gl_account_id"
    t.integer  "loss_gl_account_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "asset_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "assets", :force => true do |t|
    t.integer  "asset_type_id"
    t.integer  "parent_asset_id"
    t.integer  "instance_of_product_id"
    t.integer  "class_id"
    t.integer  "status_id"
    t.boolean  "has_quantity"
    t.decimal  "quantity_on_hand_total"
    t.decimal  "available_to_promise_total"
    t.text     "asset_name"
    t.text     "comments"
    t.integer  "owner_party_id"
    t.integer  "acquire_order_id"
    t.integer  "acquire_order_item_seq_id"
    t.text     "serial_number"
    t.text     "soft_identifier"
    t.text     "activation_number"
    t.datetime "activation_valid_thru"
    t.datetime "receive_date"
    t.datetime "date_acquired"
    t.datetime "date_manufactured"
    t.datetime "date_last_serviced"
    t.datetime "date_next_service"
    t.date     "expected_end_of_life"
    t.date     "actual_end_of_life"
    t.decimal  "production_capacity"
    t.integer  "production_capacity_uom_id"
    t.integer  "calendar_id"
    t.integer  "facility_id"
    t.integer  "location_seq_id"
    t.integer  "container_id"
    t.integer  "lot_id"
    t.decimal  "purchase_cost"
    t.integer  "purchase_cost_uom_id"
    t.decimal  "salvage_value"
    t.decimal  "depreciation"
    t.integer  "depreciation_type_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "assets", ["activation_number"], :name => "INVITEM_ACTNM", :unique => true
  add_index "assets", ["serial_number"], :name => "INVITEM_SN"
  add_index "assets", ["soft_identifier"], :name => "INVITEM_SOFID", :unique => true

  create_table "benefit_types", :force => true do |t|
    t.integer  "parent_id"
    t.text     "description"
    t.string   "name"
    t.float    "employer_paid_percentage"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "billing_account_parties", :force => true do |t|
    t.integer  "billing_account_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "billing_account_teams", :force => true do |t|
    t.integer  "billing_account_term_id"
    t.integer  "billing_account_id"
    t.integer  "term_type_enum_id"
    t.integer  "term_value"
    t.integer  "term_uom_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "billing_account_term_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "billing_accounts", :force => true do |t|
    t.integer  "billing_account_id"
    t.integer  "bill_to_party_id"
    t.integer  "bill_from_id"
    t.integer  "account_limit"
    t.integer  "account_limit_uom_id"
    t.integer  "postal_contact_mech_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.text     "description"
    t.integer  "external_account_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "budget_item_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "budget_items", :force => true do |t|
    t.integer  "budget_id"
    t.integer  "budget_item_seq_id"
    t.string   "budget_item_type_enum_id"
    t.float    "amount"
    t.string   "purpose"
    t.string   "justification"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "budget_parties", :force => true do |t|
    t.integer  "budget_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "budget_review_results", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "budget_reviews", :force => true do |t|
    t.integer  "budgetId"
    t.integer  "party_id"
    t.string   "budget_review_result_enum_id"
    t.date     "review_date"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "budget_revision_impacts", :force => true do |t|
    t.integer  "budget_id"
    t.integer  "budget_item_seq_id"
    t.integer  "revision_seq_id"
    t.float    "revised_amount"
    t.boolean  "add_delete_flag"
    t.string   "revision_reason"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "budget_revisions", :force => true do |t|
    t.integer  "budget_id"
    t.integer  "revision_seq_id"
    t.date     "date_revision"
    t.string   "description"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "budget_scenario_applications", :force => true do |t|
    t.integer  "budget_scenario_id"
    t.integer  "budget_id"
    t.integer  "budget_item_seq_id"
    t.integer  "amount_change"
    t.float    "percentage_change"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "budget_scenario_rules", :force => true do |t|
    t.integer  "budget_scenario_id"
    t.string   "budget_item_type_enum_id"
    t.float    "amount_change"
    t.float    "percentage_change"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "budget_scenarios", :force => true do |t|
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "budget_statuses", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "budget_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "budgets", :force => true do |t|
    t.string   "budget_type_enum_id"
    t.integer  "time_period_id"
    t.string   "status_id"
    t.text     "comments"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "carrier_shipment_box_types", :force => true do |t|
    t.integer  "carrier_party_id"
    t.integer  "shipment_box_type_id"
    t.integer  "packaging_type_code"
    t.string   "oversize_code"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "carrier_shipment_methods", :force => true do |t|
    t.integer  "carrier_party_id"
    t.integer  "shipment_method_id"
    t.integer  "sequence_num"
    t.string   "carrier_service_code"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "cities", :force => true do |t|
    t.integer "country_id",                                                  :null => false
    t.integer "division_id"
    t.integer "geonames_id",                                                 :null => false
    t.string  "name",                                                        :null => false
    t.string  "ascii_name"
    t.text    "alternate_name"
    t.decimal "latitude",                     :precision => 14, :scale => 8, :null => false
    t.decimal "longitude",                    :precision => 14, :scale => 8, :null => false
    t.string  "country_iso_code_two_letters"
    t.string  "admin_1_code"
    t.string  "admin_2_code"
    t.string  "admin_3_code"
    t.string  "admin_4_code"
    t.integer "population"
    t.integer "geonames_timezone_id"
  end

  add_index "cities", ["ascii_name"], :name => "index_cities_on_ascii_name"
  add_index "cities", ["country_iso_code_two_letters"], :name => "index_cities_on_country_iso_code_two_letters"

  create_table "cogs_methods", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "communication_event_contents", :force => true do |t|
    t.integer  "communication_event_id"
    t.string   "content_location"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "assoc_type_id"
    t.integer  "sequence_num"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "communication_event_parties", :force => true do |t|
    t.integer  "communication_event_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.integer  "contact_mech_id"
    t.integer  "status_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "communication_event_party_status_valids", :force => true do |t|
    t.integer  "status_id"
    t.integer  "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "communication_event_party_statuses", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "communication_event_products", :force => true do |t|
    t.integer  "product_id"
    t.integer  "communication_event_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "communication_event_purposes", :force => true do |t|
    t.integer  "communication_event_id"
    t.integer  "purpose_id"
    t.text     "description"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "communication_event_status_valids", :force => true do |t|
    t.integer  "status_id"
    t.integer  "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "communication_event_statuses", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "communication_event_types", :force => true do |t|
    t.integer  "parent_type_id"
    t.text     "description"
    t.integer  "contact_mech_type_id"
    t.string   "name"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "communication_event_work_effs", :force => true do |t|
    t.integer  "work_effort_id"
    t.integer  "communication_event_id"
    t.text     "description"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "communication_events", :force => true do |t|
    t.integer  "communication_event_type_id"
    t.integer  "contact_mech_type_id"
    t.integer  "status_id"
    t.integer  "original_comm_event_id"
    t.integer  "parent_comm_event_id"
    t.integer  "from_contact_mech_id"
    t.integer  "to_contact_mech_id"
    t.integer  "from_party_id"
    t.integer  "from_role_type_id"
    t.integer  "to_party_id"
    t.integer  "to_role_type_id"
    t.datetime "entry_date"
    t.datetime "datetime_started"
    t.datetime "datetime_ended"
    t.text     "subject"
    t.string   "content_type"
    t.text     "content"
    t.text     "note"
    t.integer  "reason_id"
    t.integer  "contact_list_id"
    t.integer  "email_message_id"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "communication_purposes", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "contact_list_comm_statuses", :force => true do |t|
    t.integer  "contact_list_id"
    t.integer  "communication_event_id"
    t.integer  "contact_mech_id"
    t.integer  "party_id"
    t.integer  "message_id"
    t.integer  "status_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "contact_list_email_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "contact_list_emails", :force => true do |t|
    t.integer  "contact_list_id"
    t.integer  "email_type_enum_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "email_templated_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "contact_list_parties", :force => true do |t|
    t.integer  "contact_list_id"
    t.integer  "party_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "status_id"
    t.integer  "preferred_contact_mech_id"
    t.text     "opt_in_verify_code"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "contact_list_party_status_valids", :force => true do |t|
    t.string   "status_id"
    t.string   "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "contact_list_party_statuses", :force => true do |t|
    t.string   "status_id"
    t.text     "description"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "contact_list_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "contact_lists", :force => true do |t|
    t.integer  "contact_list_id"
    t.integer  "contact_list_type_enum_id"
    t.integer  "contact_mesh_type_enum_id"
    t.integer  "marketing_campaign_id"
    t.text     "contact_list_name"
    t.text     "description"
    t.text     "comments"
    t.boolean  "is_public"
    t.boolean  "single_use"
    t.integer  "owner_party_id"
    t.text     "verfity_email_from"
    t.text     "verify_email_screen"
    t.text     "verify_email_subject"
    t.text     "verify_email_website_id"
    t.text     "opt_out_screen"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "contact_mech_purposes", :force => true do |t|
    t.integer  "contact_mech_type_id"
    t.text     "description"
    t.string   "name"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "contact_mechanism_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "parent_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "contact_meches", :force => true do |t|
    t.integer  "contact_mech_type_id"
    t.text     "info_string"
    t.integer  "trust_level_id"
    t.integer  "payment_fraud_evidence_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "contact_meches", ["info_string"], :name => "index_contact_meches_on_info_string"

  create_table "contact_payment_trust_levels", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "containers", :force => true do |t|
    t.integer  "container_type_id"
    t.integer  "facility_id"
    t.integer  "geo_point_id"
    t.text     "description"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "cost_component_calcs", :force => true do |t|
    t.string   "description"
    t.integer  "cost_gl_account_type_id"
    t.integer  "offset_gl_account_type_id"
    t.decimal  "fixed_cost"
    t.decimal  "variable_cost"
    t.integer  "per_milli_second"
    t.integer  "cost_uom_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "cost_component_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "parent_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "cost_components", :force => true do |t|
    t.integer  "cost_component_type_id"
    t.integer  "product_id"
    t.integer  "party_id"
    t.integer  "geo_id"
    t.integer  "work_effort_id"
    t.integer  "asset_id"
    t.integer  "cost_component_calc_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.decimal  "cost"
    t.integer  "cost_uom_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "countries", :force => true do |t|
    t.string  "iso_code_two_letter",   :null => false
    t.string  "iso_code_three_letter", :null => false
    t.integer "iso_number",            :null => false
    t.string  "name",                  :null => false
    t.string  "capital"
    t.integer "population"
    t.string  "continent"
    t.string  "currency_code"
    t.string  "currency_name"
    t.string  "phone"
    t.integer "geonames_id"
    t.integer "country_code"
  end

  add_index "countries", ["geonames_id"], :name => "index_countries_on_geonames_id"
  add_index "countries", ["iso_code_two_letter"], :name => "index_countries_on_iso_code_two_letter", :unique => true
  add_index "countries", ["name"], :name => "index_countries_on_name"

  create_table "credit_card_type_gl_accounts", :force => true do |t|
    t.string   "credit_card_type_enum_id"
    t.integer  "organization_party_id"
    t.integer  "gl_account_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "credit_card_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "credit_cards", :force => true do |t|
    t.integer  "payment_method_id"
    t.integer  "credit_card_type_enum_id"
    t.text     "card_number"
    t.text     "card_number_lookup_hash"
    t.text     "valid_from_date"
    t.text     "expire_date"
    t.text     "issue_number"
    t.text     "company_name_on_card"
    t.text     "title_on_card"
    t.text     "first_name_on_card"
    t.text     "middle_name_on_card"
    t.text     "last_name_on_cad"
    t.text     "suffix_on_card"
    t.integer  "consecutive_failed_auths"
    t.datetime "last_failed_auth_date"
    t.integer  "consecutive_failed_nsf"
    t.datetime "last_failed_nsf_date"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "custom_method_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "custom_methods", :force => true do |t|
    t.string   "custom_method_id"
    t.integer  "custom_method_type_id"
    t.string   "custom_method_name"
    t.string   "description"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "deductions", :force => true do |t|
    t.integer  "deduction_id"
    t.integer  "deduction_type_enum_id"
    t.integer  "payment_id"
    t.decimal  "amount",                 :precision => 10, :scale => 2, :null => false
    t.datetime "created_at",                                            :null => false
    t.datetime "updated_at",                                            :null => false
  end

  create_table "deliverables", :force => true do |t|
    t.integer  "deliverable_type_id"
    t.text     "deliverable_name"
    t.text     "description"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "deliveries", :force => true do |t|
    t.integer  "origin_facility_id"
    t.integer  "dest_facility_id"
    t.datetime "actual_start_date"
    t.datetime "actual_arrival_date"
    t.datetime "estimated_start_date"
    t.datetime "estimated_arrival_date"
    t.integer  "asset_id"
    t.decimal  "start_mileage"
    t.decimal  "end_mileage"
    t.decimal  "fuel_used"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "divisions", :force => true do |t|
    t.integer "country_id"
    t.string  "code"
    t.string  "full_code"
    t.string  "name"
    t.string  "ascii_name"
    t.integer "geonames_id"
  end

  add_index "divisions", ["code"], :name => "index_divisions_on_code"
  add_index "divisions", ["full_code"], :name => "index_divisions_on_full_code", :unique => true
  add_index "divisions", ["geonames_id"], :name => "index_divisions_on_geonames_id", :unique => true

  create_table "eft_accounts", :force => true do |t|
    t.integer  "payment_method_id"
    t.text     "bank_name"
    t.text     "routing_number"
    t.text     "account_type"
    t.text     "account_number"
    t.text     "name_on_account"
    t.text     "company_name_on_account"
    t.integer  "years_at_bank"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "empl_position_classes", :force => true do |t|
    t.text     "title"
    t.text     "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "empl_position_parties", :force => true do |t|
    t.integer  "empl_position_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.text     "comments"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "empl_position_responsibilities", :force => true do |t|
    t.integer  "empl_position_id"
    t.integer  "responsibility_enum_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.text     "comments"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "empl_position_statuses", :force => true do |t|
    t.string   "description"
    t.string   "status_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "empl_positions", :force => true do |t|
    t.integer  "empl_position_class_id"
    t.integer  "status_id"
    t.integer  "filled_by_party_id"
    t.integer  "employer_organization_party_id"
    t.integer  "pay_grade_id"
    t.integer  "budget_id"
    t.integer  "budget_item_seq_id"
    t.datetime "estimated_from_date"
    t.datetime "estimated_thru_date"
    t.text     "salary_flag"
    t.text     "exempt_flag"
    t.text     "full_time_flag"
    t.text     "temporary_flag"
    t.datetime "actual_from_date"
    t.datetime "actual_thru_date"
    t.integer  "standard_hours_per_week"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  create_table "empl_valid_responsibilities", :force => true do |t|
    t.integer  "empl_position_type_enum_id"
    t.integer  "responsibility_enum_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.text     "comments"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "employment_application_referred_bies", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "employment_applications", :force => true do |t|
    t.integer  "empl_position_id"
    t.integer  "status_id"
    t.integer  "referred_by_enum_id"
    t.datetime "application_date"
    t.integer  "applying_party_id"
    t.integer  "referred_by_party_id"
    t.integer  "approver_party_id"
    t.integer  "job_requisition_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "employment_benefits", :force => true do |t|
    t.integer  "employment_id"
    t.integer  "benefit_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.decimal  "cost"
    t.float    "actual_employer_paid_percent"
    t.integer  "available_time"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "employment_leave_reasons", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "employment_leave_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "employment_leaves", :force => true do |t|
    t.integer  "party_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.integer  "leave_type_enum_id"
    t.integer  "leave_reason_enum_id"
    t.text     "leave_approved"
    t.integer  "approver_party_id"
    t.text     "description"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "employment_pay_grades", :force => true do |t|
    t.integer  "employment_id"
    t.integer  "pay_grade_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.text     "comments"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "employment_responsibilities", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "employment_statuses", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "employments", :force => true do |t|
    t.integer  "empl_position_id"
    t.integer  "employer_party_id"
    t.integer  "employee_party_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "termination_reason_enum_id"
    t.integer  "termination_type_enum_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "exam_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "facilities", :force => true do |t|
    t.integer  "facility_type_enum_id"
    t.integer  "parent_facility_id"
    t.integer  "owner_party_id"
    t.text     "facility_name"
    t.decimal  "facility_size"
    t.integer  "facility_size_uom_id"
    t.datetime "opened_date"
    t.datetime "closed_date"
    t.text     "description"
    t.integer  "geo_point_id"
    t.integer  "default_days_to_ship"
    t.integer  "weight_uom_id"
    t.integer  "inventory_item_type_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "facility_contact_meches", :force => true do |t|
    t.integer  "facility_id"
    t.integer  "contact_mech_id"
    t.integer  "contact_mech_purpose_enum_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.text     "extension"
    t.text     "comments"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "facility_contents", :force => true do |t|
    t.integer  "facility_id"
    t.text     "content_location"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "facility_group_members", :force => true do |t|
    t.integer  "facility_id"
    t.integer  "facility_group_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "sequence_num"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "facility_group_parties", :force => true do |t|
    t.integer  "facility_group_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "facility_group_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "facility_groups", :force => true do |t|
    t.integer  "parent_group_id"
    t.integer  "facility_group_type_enum_id"
    t.text     "description"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "facility_location_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "facility_locations", :force => true do |t|
    t.integer  "facility_id"
    t.integer  "location_seq_id"
    t.integer  "location_type_enum_id"
    t.integer  "area_id"
    t.integer  "aisle_id"
    t.integer  "section_id"
    t.integer  "level_id"
    t.integer  "position_id"
    t.integer  "geo_point_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "facility_parties", :force => true do |t|
    t.integer  "facility_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "facility_size_uoms", :force => true do |t|
    t.string   "name"
    t.string   "unit"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "facility_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "financial_account_auths", :force => true do |t|
    t.integer  "fin_account_auth_id"
    t.integer  "fin_account_id"
    t.decimal  "amount"
    t.integer  "amount_uom_id"
    t.datetime "authorization_date"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "financial_account_parties", :force => true do |t|
    t.integer  "fin_account_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "financial_account_replenish_methods", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "financial_account_replenish_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "financial_account_status_valids", :force => true do |t|
    t.string   "status_id"
    t.string   "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "financial_account_statuses", :force => true do |t|
    t.string   "description"
    t.integer  "sequence_num"
    t.integer  "status_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "financial_account_trans", :force => true do |t|
    t.integer  "fin_account_trans_id"
    t.integer  "fin_account_id"
    t.integer  "status_id"
    t.integer  "party_id"
    t.integer  "gl_reconciliation_id"
    t.datetime "transaction_date"
    t.datetime "entry_date"
    t.decimal  "amount"
    t.integer  "payment_id"
    t.integer  "order_id"
    t.integer  "order_item_seq_id"
    t.integer  "performed_by_party_id"
    t.integer  "reason_enum_id"
    t.text     "comments"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "financial_account_trans_reason_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "financial_account_trans_status_valids", :force => true do |t|
    t.string   "status_id"
    t.string   "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "financial_account_trans_statuses", :force => true do |t|
    t.string   "status_id"
    t.integer  "sequence_num"
    t.string   "description"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "financial_account_trans_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "financial_account_type_gl_accounts", :force => true do |t|
    t.integer  "fin_account_type_id"
    t.integer  "organization_party_id"
    t.integer  "gl_account_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "financial_account_types", :force => true do |t|
    t.integer  "fin_account_type_id"
    t.integer  "parent_type_id"
    t.text     "description"
    t.boolean  "is_refundable"
    t.integer  "account_code_length"
    t.boolean  "require_pin_code"
    t.integer  "pin_code_length"
    t.integer  "account_valid_days"
    t.integer  "auth_valid_days"
    t.boolean  "allow_auth_to_negative"
    t.decimal  "replenish_min_balance"
    t.decimal  "replenish_threshold"
    t.integer  "replenish_method_enum_id"
    t.integer  "replenish_type_enum_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "financial_accounts", :force => true do |t|
    t.integer  "fin_account_id"
    t.integer  "fin_account_type_id"
    t.integer  "status_id"
    t.text     "fin_account_name"
    t.text     "fin_account_code"
    t.text     "fin_account_pin"
    t.integer  "organization_party_id"
    t.integer  "owner_party_id"
    t.integer  "post_to_gl_account_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.boolean  "is_refundable"
    t.integer  "balance_uom_id"
    t.decimal  "actual_balance",        :precision => 10, :scale => 2, :null => false
    t.decimal  "availabe_balance",      :precision => 10, :scale => 2, :null => false
    t.integer  "replenish_payment_id"
    t.decimal  "replenish_level"
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
  end

  create_table "gift_card_fulfillments", :force => true do |t|
    t.integer  "fulfillment_id"
    t.integer  "type_enum_id"
    t.integer  "merchant_id"
    t.integer  "party_id"
    t.integer  "order_id"
    t.integer  "order_item_seq_id"
    t.integer  "survey_response_id"
    t.text     "card_number"
    t.text     "pin_number"
    t.decimal  "amount",             :precision => 10, :scale => 2, :null => false
    t.text     "response_code"
    t.text     "reference_num"
    t.text     "auth_code"
    t.datetime "fulfillment_date"
    t.datetime "created_at",                                        :null => false
    t.datetime "updated_at",                                        :null => false
  end

  create_table "gift_cards", :force => true do |t|
    t.integer  "payment_method_id"
    t.text     "card_number"
    t.text     "pin_number"
    t.text     "expire_date"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "gl_account_categories", :force => true do |t|
    t.integer  "category_type_enum_id"
    t.string   "description"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "gl_account_category_members", :force => true do |t|
    t.integer  "gl_account_id"
    t.integer  "gl_account_category_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.float    "amount_percentage"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "gl_account_category_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "gl_account_classes", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "parent_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "gl_account_group_members", :force => true do |t|
    t.integer  "gl_account_id"
    t.string   "gl_account_group_type_enum_id"
    t.integer  "gl_account_group_id"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "gl_account_groups", :force => true do |t|
    t.integer  "gl_account_group_type_enum_id"
    t.string   "description"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "gl_account_histories", :force => true do |t|
    t.integer  "gl_account_id"
    t.integer  "organization_party_id"
    t.integer  "time_period_id"
    t.float    "posted_debits"
    t.float    "posted_credits"
    t.float    "ending_balance"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "gl_account_organizations", :force => true do |t|
    t.integer  "gl_account_id"
    t.integer  "organization_party_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.float    "posted_balance"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "gl_account_parties", :force => true do |t|
    t.integer  "gl_account_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "gl_account_type_defaults", :force => true do |t|
    t.string   "gl_account_type_enum_id"
    t.integer  "organization_party_id"
    t.integer  "gl_accouny_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "gl_account_type_party_defaults", :force => true do |t|
    t.integer  "organization_party_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.string   "gl_account_type_enum_id"
    t.integer  "gl_account_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "gl_account_type_payment_types", :force => true do |t|
    t.string   "payment_type_enum_id"
    t.integer  "organization_party_id"
    t.string   "gl_account_type_enum_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "gl_account_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "parent_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "gl_accounts", :force => true do |t|
    t.integer  "parent_gl_account_id"
    t.string   "gl_account_type_enum_id"
    t.string   "gl_account_class_enum_id"
    t.string   "gl_resource_type_enum_id"
    t.string   "gl_xbrl_class_enum_id"
    t.string   "account_code"
    t.string   "account_name"
    t.string   "description"
    t.integer  "product_id"
    t.integer  "external_id"
    t.float    "posted_balance"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "gl_accounts", ["account_code"], :name => "index_gl_accounts_on_account_code"

  create_table "gl_budget_xrefs", :force => true do |t|
    t.integer  "gl_account_id"
    t.integer  "budget_item_type_enum_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.float    "allocation_percentage"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "gl_fiscal_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "gl_journals", :force => true do |t|
    t.string   "gl_journal_name"
    t.integer  "organization_party_id"
    t.boolean  "is_posted"
    t.date     "posted_date"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "gl_reconciliation_entries", :force => true do |t|
    t.string   "gl_reconciliation_id"
    t.integer  "acctg_trans_id"
    t.integer  "acctg_trans_entry_seq_id"
    t.float    "reconciled_amount"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "gl_reconciliation_status_valids", :force => true do |t|
    t.integer  "status_id"
    t.integer  "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "gl_reconciliation_statuses", :force => true do |t|
    t.string   "status_id"
    t.integer  "sequence_num"
    t.string   "description"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "gl_reconciliations", :force => true do |t|
    t.string   "gl_reconciliation_id"
    t.integer  "gl_reconciliation_name"
    t.string   "description"
    t.integer  "gl_account_id"
    t.integer  "status_id"
    t.integer  "organization_party_id"
    t.float    "reconciled_balance"
    t.float    "opening_balance"
    t.date     "reconciled_date"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "gl_resource_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "gl_xbrl_classes", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "interview_grades", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "inventory_item_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "inventory_variance_reasons", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "invoice_contact_meches", :force => true do |t|
    t.integer  "invoice_id"
    t.integer  "contact_mech_purpose_enum_id"
    t.integer  "contact_mech_id"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "invoice_item_assoc_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "invoice_item_assocs", :force => true do |t|
    t.integer  "invoice_item_assoc_id"
    t.integer  "invoice_id"
    t.integer  "invoice_item_seq_id"
    t.integer  "to_invoice_id"
    t.integer  "to_invoice_item_seq_id"
    t.integer  "invoice_item_assoc_type_enum_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "from_party_id"
    t.integer  "to_party_id"
    t.decimal  "quantity"
    t.decimal  "amount",                          :precision => 10, :scale => 2, :null => false
    t.datetime "created_at",                                                     :null => false
    t.datetime "updated_at",                                                     :null => false
  end

  create_table "invoice_items", :force => true do |t|
    t.integer  "invoice_id"
    t.integer  "invoice_item_seq_id"
    t.integer  "item_type_enum_id"
    t.integer  "override_gl_account_id"
    t.integer  "override_org_party_id"
    t.integer  "asset_id"
    t.integer  "product_id"
    t.integer  "parent_invoice_id"
    t.integer  "parent_invoice_item_seq_id"
    t.integer  "uom_id"
    t.boolean  "taxable_flag"
    t.decimal  "quantity"
    t.decimal  "amount",                     :precision => 10, :scale => 2, :null => false
    t.text     "description"
    t.integer  "tax_auth_party_id"
    t.integer  "tax_auth_geo_id"
    t.integer  "tax_authority_rate_id"
    t.integer  "sales_opportunity_id"
    t.datetime "created_at",                                                :null => false
    t.datetime "updated_at",                                                :null => false
  end

  create_table "invoice_parties", :force => true do |t|
    t.integer  "invoice_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.integer  "datetime_performed"
    t.decimal  "percentage"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "invoice_sequences", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "invoice_status_valids", :force => true do |t|
    t.string   "status_id"
    t.string   "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "invoice_statuses", :force => true do |t|
    t.string   "status_id"
    t.string   "description"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "invoice_terms", :force => true do |t|
    t.integer  "invoice_id"
    t.integer  "invoice_item_seq_id"
    t.integer  "settlement_term_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "invoice_types", :force => true do |t|
    t.string   "name"
    t.string   "parent_name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "invoices", :force => true do |t|
    t.integer  "invoice_id"
    t.integer  "invoice_type_enum_id"
    t.integer  "from_party_id"
    t.integer  "to_party_id"
    t.integer  "status_id"
    t.integer  "billing_account_id"
    t.datetime "invoice_date"
    t.datetime "due_date"
    t.datetime "paid_date"
    t.text     "invoice_message"
    t.text     "reference_number"
    t.text     "description"
    t.integer  "currency_uom_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "item_issuance_parties", :force => true do |t|
    t.integer  "item_issuance_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "item_issuances", :force => true do |t|
    t.integer  "asset_id"
    t.integer  "asset_reservation_id"
    t.integer  "order_id"
    t.integer  "order_item_seq_id"
    t.integer  "shipment_id"
    t.integer  "shipment_item_seq_id"
    t.integer  "asset_maintenance_id"
    t.datetime "issued_datetime"
    t.decimal  "quantity"
    t.decimal  "cancel_quantity"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "item_type_gl_accounts", :force => true do |t|
    t.string   "item_type_enum_id"
    t.integer  "organization_party_id"
    t.integer  "gl_accouny_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "job_interview_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "job_interviews", :force => true do |t|
    t.integer  "job_requisition_id"
    t.integer  "candidate_party_id"
    t.integer  "interviewer_party_id"
    t.integer  "job_interview_type_enum_id"
    t.integer  "interview_grade_enum_id"
    t.integer  "job_interview_result"
    t.date     "job_interview_date"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "job_posting_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "job_requisitions", :force => true do |t|
    t.integer  "duration_months"
    t.integer  "age"
    t.text     "gender"
    t.integer  "experience_months"
    t.integer  "experience_years"
    t.integer  "qualification"
    t.integer  "job_location_facility_id"
    t.integer  "skill_type_enum_id"
    t.integer  "no_of_resources"
    t.integer  "job_posting_type_enum_id"
    t.date     "job_requisition_date"
    t.integer  "exam_type_enum_id"
    t.date     "required_on_date"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "lots", :force => true do |t|
    t.datetime "creation_date"
    t.decimal  "quantity"
    t.datetime "expiration_date"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "mail_methods", :force => true do |t|
    t.string   "environment"
    t.boolean  "active",      :default => true
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "maintenance_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.integer  "parent_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "marital_statuses", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "market_interests", :force => true do |t|
    t.integer  "product_category_id"
    t.integer  "market_segment_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "market_segment_classifications", :force => true do |t|
    t.integer  "market_segment_id"
    t.integer  "party_classification_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "market_segment_geos", :force => true do |t|
    t.integer  "market_segment_id"
    t.integer  "geo_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "market_segment_parties", :force => true do |t|
    t.integer  "market_segment_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "market_segment_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "market_segments", :force => true do |t|
    t.integer  "market_segment_id"
    t.integer  "market_segment_type_enum_id"
    t.text     "description"
    t.integer  "product_store_id"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "marketing_campaign_notes", :force => true do |t|
    t.integer  "marketing_campaign_id"
    t.datetime "note_date"
    t.text     "note_text"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "marketing_campaign_parties", :force => true do |t|
    t.integer  "marketing_campaign_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "marketing_campaign_status_valids", :force => true do |t|
    t.string   "status_id"
    t.string   "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "marketing_campaign_statuses", :force => true do |t|
    t.string   "status_id"
    t.string   "description"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "marketing_campaigns", :force => true do |t|
    t.integer  "marketing_campaign_id"
    t.integer  "parent_campaign_id"
    t.integer  "status_id"
    t.text     "campaign_name"
    t.text     "campaign_summary"
    t.decimal  "budgeted_cost"
    t.decimal  "actual_cost"
    t.decimal  "estimated_cost"
    t.integer  "cost_uom_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.boolean  "is_active"
    t.integer  "converted_leads"
    t.float    "expected_response_percent"
    t.decimal  "expected_revenue"
    t.integer  "num_sent"
    t.datetime "start_date"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "operators", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "order_adjustment_billings", :force => true do |t|
    t.integer  "order_adjustment_id"
    t.integer  "invoice_id"
    t.integer  "invoice_item_seq_id"
    t.decimal  "amount"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "order_adjustments", :force => true do |t|
    t.integer  "item_type_id"
    t.integer  "order_id"
    t.integer  "order_item_seq_id"
    t.integer  "order_part_seq_id"
    t.text     "comments"
    t.text     "description"
    t.decimal  "amount"
    t.decimal  "recurring_amount"
    t.decimal  "amount_already_included"
    t.integer  "product_feature_id"
    t.integer  "corresponding_product_id"
    t.integer  "tax_authority_rate_id"
    t.integer  "source_reference_id"
    t.decimal  "source_percentage"
    t.integer  "customer_reference_id"
    t.integer  "primary_geo_id"
    t.integer  "secondary_geo_id"
    t.decimal  "exempt_amount"
    t.integer  "tax_auth_geo_id"
    t.integer  "tax_auth_party_id"
    t.integer  "override_gl_account_id"
    t.boolean  "include_in_tax"
    t.boolean  "include_in_shipping"
    t.integer  "original_adjustment_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "order_communication_events", :force => true do |t|
    t.integer  "order_id"
    t.integer  "communication_event_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "order_contact_meches", :force => true do |t|
    t.integer  "order_id"
    t.integer  "contact_mech_purpose_id"
    t.integer  "contact_mech_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "order_contents", :force => true do |t|
    t.integer  "order_content_type_id"
    t.integer  "order_id"
    t.integer  "order_item_seq_id"
    t.text     "content_location"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "order_header_status_valids", :force => true do |t|
    t.integer  "status_id"
    t.integer  "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "order_header_statuses", :force => true do |t|
    t.string   "description"
    t.integer  "sequence_num"
    t.string   "status_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "order_headers", :force => true do |t|
    t.integer  "order_id"
    t.text     "order_name"
    t.datetime "entry_date"
    t.integer  "status_id"
    t.integer  "currency_uom_id"
    t.integer  "billing_account_id"
    t.integer  "product_store_id"
    t.integer  "sales_channel_id"
    t.integer  "terminal_id"
    t.integer  "external_id"
    t.integer  "sync_status_id"
    t.integer  "visit_id"
    t.integer  "parent_order_id"
    t.integer  "recurrence_info_id"
    t.datetime "last_ordered_date"
    t.decimal  "remaining_sub_total"
    t.decimal  "grand_total"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "order_headers", ["external_id"], :name => "ORDEREXT_ID_IDX"

  create_table "order_item_billings", :force => true do |t|
    t.integer  "order_id"
    t.integer  "order_item_seq_id"
    t.integer  "invoice_id"
    t.integer  "invoice_item_seq_id"
    t.integer  "item_issuance_id"
    t.integer  "shipment_receipt_id"
    t.decimal  "quantity"
    t.decimal  "amount"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "order_item_status_valids", :force => true do |t|
    t.integer  "status_id"
    t.integer  "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "order_item_statuses", :force => true do |t|
    t.string   "description"
    t.string   "status_id"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "order_item_work_efforts", :force => true do |t|
    t.integer  "order_id"
    t.integer  "order_item_seq_id"
    t.integer  "work_effort_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "order_items", :force => true do |t|
    t.integer  "order_id"
    t.integer  "order_item_seq_id"
    t.integer  "order_part_seq_id"
    t.integer  "item_type_id"
    t.integer  "status_id"
    t.integer  "product_id"
    t.integer  "product_config_saved_id"
    t.text     "item_description"
    t.text     "comments"
    t.decimal  "quantity"
    t.decimal  "cancel_quantity"
    t.decimal  "selected_amount"
    t.decimal  "unit_price"
    t.decimal  "unitList_price"
    t.boolean  "is_modified_price"
    t.integer  "external_item_id"
    t.integer  "from_asset_id"
    t.integer  "budget_id"
    t.integer  "budget_item_seq_id"
    t.integer  "supplier_product_id"
    t.integer  "product_category_id"
    t.boolean  "is_promo"
    t.integer  "shopping_list_id"
    t.integer  "shopping_list_item_seq_id"
    t.integer  "subscription_id"
    t.integer  "override_gl_account_id"
    t.integer  "sales_opportunity_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "order_items", ["external_item_id"], :name => "ORDITMEXT_ID_IDX"
  add_index "order_items", ["order_id", "order_item_seq_id"], :name => "index_order_items_on_order_id_and_order_item_seq_id", :unique => true

  create_table "order_notes", :force => true do |t|
    t.integer  "order_id"
    t.datetime "note_date"
    t.text     "note_text"
    t.boolean  "internal_note"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "order_part_parties", :force => true do |t|
    t.integer  "order_id"
    t.integer  "order_part_seq_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "order_parts", :force => true do |t|
    t.integer  "order_id"
    t.integer  "order_part_seq_id"
    t.integer  "parent_part_seq_id"
    t.text     "part_name"
    t.integer  "vendor_party_id"
    t.integer  "customer_party_id"
    t.integer  "customer_po_id"
    t.integer  "vendor_agent_party_id"
    t.integer  "customer_agent_party_id"
    t.integer  "ship_to_party_id"
    t.integer  "facility_id"
    t.integer  "carrier_party_id"
    t.integer  "shipment_method_id"
    t.integer  "postal_contact_mech_id"
    t.integer  "telecom_contact_mech_id"
    t.text     "tracking_number"
    t.text     "shipping_instructions"
    t.boolean  "may_split"
    t.text     "gift_message"
    t.boolean  "is_gift"
    t.datetime "ship_after_date"
    t.datetime "ship_before_date"
    t.datetime "estimated_ship_date"
    t.datetime "estimated_delivery_date"
    t.datetime "valid_from_date"
    t.datetime "valid_thru_date"
    t.datetime "auto_cancel_date"
    t.datetime "dont_cancel_set_date"
    t.integer  "dont_cancel_set_user_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "order_requirement_commitments", :force => true do |t|
    t.integer  "order_id"
    t.integer  "order_item_seq_id"
    t.integer  "requirement_id"
    t.decimal  "quantity"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "order_sequences", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "order_shipments", :force => true do |t|
    t.integer  "order_id"
    t.integer  "order_item_seq_id"
    t.integer  "shipment_id"
    t.integer  "shipment_item_seq_id"
    t.decimal  "quantity"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "order_terms", :force => true do |t|
    t.integer  "order_id"
    t.integer  "order_item_seq_id"
    t.integer  "settlement_term_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "organizations", :force => true do |t|
    t.integer  "party_id"
    t.text     "organization_name"
    t.text     "office_site_name"
    t.decimal  "annual_revenue"
    t.decimal  "num_employees"
    t.text     "comments"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "organizations", ["organization_name"], :name => "PTY_ORG_NAME_IDX"

  create_table "parties", :force => true do |t|
    t.integer  "party_type_id"
    t.boolean  "disabled"
    t.integer  "external_id"
    t.integer  "data_source_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "user_id"
  end

  add_index "parties", ["external_id"], :name => "index_parties_on_external_id"

  create_table "party_acctg_preferences", :force => true do |t|
    t.integer  "party_id"
    t.integer  "fiscal_year_start_month"
    t.integer  "fiscal_year_start_day"
    t.string   "tax_form_enum_id"
    t.string   "cogs_method_enum_id"
    t.integer  "base_currency_uom_id"
    t.integer  "invoice_sequence_enumId"
    t.string   "invoice_id_prefix"
    t.integer  "last_invoice_number"
    t.integer  "last_invoice_restart_date"
    t.text     "use_invoice_id_for_returns"
    t.string   "quote_sequence_enum_id"
    t.text     "quote_id_prefix"
    t.integer  "last_quote_number"
    t.integer  "order_sequence_enum_id"
    t.text     "order_id_prefix"
    t.integer  "last_order_number"
    t.integer  "refund_payment_method_id"
    t.integer  "error_gl_journal_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "party_carrier_accounts", :force => true do |t|
    t.integer  "party_id"
    t.integer  "carrier_party_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "account_number"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "party_classification_appls", :force => true do |t|
    t.integer  "party_id"
    t.integer  "party_classification_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "party_classification_types", :force => true do |t|
    t.text     "description"
    t.integer  "parent_id"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "party_classifications", :force => true do |t|
    t.integer  "classification_type_id"
    t.integer  "parent_classification_id"
    t.text     "description"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "party_contact_meches", :force => true do |t|
    t.integer  "party_id"
    t.integer  "contact_mech_id"
    t.integer  "contact_mech_purpose_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.string   "extension"
    t.text     "comments"
    t.boolean  "allow_solicitation"
    t.date     "used_since"
    t.string   "verify_code"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "party_content_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "party_contents", :force => true do |t|
    t.integer  "party_id"
    t.text     "content_location"
    t.integer  "party_content_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "party_geo_points", :force => true do |t|
    t.integer  "party_id"
    t.integer  "geo_point_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "party_id_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "party_identifications", :force => true do |t|
    t.integer  "party_id"
    t.integer  "party_id_type_id"
    t.integer  "id_value"
    t.date     "expire_date"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "party_identifications", ["id_value"], :name => "PARTY_ID_VALUE"

  create_table "party_needs", :force => true do |t|
    t.integer  "need_type_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.integer  "communication_event_id"
    t.integer  "product_id"
    t.integer  "product_category_id"
    t.integer  "visit_id"
    t.datetime "datetime_recorded"
    t.text     "description"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "party_notes", :force => true do |t|
    t.integer  "party_id"
    t.datetime "note_date"
    t.text     "note_text"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "party_qualification_statuses", :force => true do |t|
    t.string   "description"
    t.integer  "sequence_num"
    t.string   "status_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "party_qualification_verifications", :force => true do |t|
    t.string   "description"
    t.integer  "sequence_num"
    t.string   "status_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "party_qualifications", :force => true do |t|
    t.integer  "party_id"
    t.integer  "qualification_type_enum_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.text     "description"
    t.text     "title"
    t.integer  "status_id"
    t.integer  "verification_status_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "party_rates", :force => true do |t|
    t.integer  "party_id"
    t.integer  "rate_type_enum_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.text     "default_rate"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "party_relationship_statuses", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "party_relationship_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "party_relationships", :force => true do |t|
    t.integer  "relationship_type_id"
    t.integer  "from_party_id"
    t.integer  "from_role_type_id"
    t.integer  "to_party_id"
    t.integer  "to_role_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "status_id"
    t.integer  "priority_type_id"
    t.integer  "permissions_id"
    t.text     "comments"
    t.string   "relationship_name"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "party_resumes", :force => true do |t|
    t.integer  "resume_id"
    t.integer  "party_id"
    t.text     "content_location"
    t.datetime "resume_date"
    t.text     "resume_text"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "party_roles", :force => true do |t|
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "party_skills", :force => true do |t|
    t.integer  "party_id"
    t.integer  "skill_type_enum_id"
    t.integer  "years_experience"
    t.integer  "rating"
    t.integer  "skill_level"
    t.datetime "started_using_date"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "party_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "pay_grade_salaries", :force => true do |t|
    t.integer  "pay_grade_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.decimal  "amount"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "pay_grades", :force => true do |t|
    t.integer  "pay_grade_id"
    t.text     "pay_grade_name"
    t.text     "comments"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "payment_applications", :force => true do |t|
    t.integer  "payment_application_id"
    t.integer  "payment_id"
    t.integer  "invoice_id"
    t.integer  "invoice_item_seq_id"
    t.integer  "billing_account_id"
    t.integer  "override_gl_account_id"
    t.integer  "to_payment_id"
    t.integer  "tax_auth_geo_id"
    t.decimal  "amount_applied",         :precision => 10, :scale => 2, :null => false
    t.datetime "created_at",                                            :null => false
    t.datetime "updated_at",                                            :null => false
  end

  create_table "payment_budget_allocations", :force => true do |t|
    t.integer  "payment_id"
    t.integer  "budget_id"
    t.integer  "budget_item_seq_id"
    t.decimal  "amount",             :precision => 10, :scale => 2, :null => false
    t.datetime "created_at",                                        :null => false
    t.datetime "updated_at",                                        :null => false
  end

  create_table "payment_fraud_evidences", :force => true do |t|
    t.integer  "payment_fraud_evidence"
    t.integer  "fraud_type_enum_id"
    t.text     "comments"
    t.integer  "payment_id"
    t.integer  "order_id"
    t.integer  "party_id"
    t.integer  "visit_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "payment_gateway_responses", :force => true do |t|
    t.integer  "payment_gateway_response_id"
    t.integer  "payment_service_type_enum_id"
    t.integer  "payment_method_id"
    t.integer  "trans_code_enum_id"
    t.decimal  "amount",                       :precision => 10, :scale => 2, :null => false
    t.integer  "amount_uom_id"
    t.text     "reference_num"
    t.text     "alt_reference"
    t.text     "sub_reference"
    t.text     "gateway_code"
    t.text     "gateway_flag"
    t.text     "gateway_avs_result"
    t.text     "gateway_cv_result"
    t.text     "gateway_score_result"
    t.text     "gateway_message"
    t.datetime "transaction_date"
    t.boolean  "result_declined"
    t.boolean  "result_nsf"
    t.boolean  "result_bad_expire"
    t.boolean  "result_bad_card_number"
    t.datetime "created_at",                                                  :null => false
    t.datetime "updated_at",                                                  :null => false
  end

  create_table "payment_method_type_gl_accounts", :force => true do |t|
    t.integer  "payment_method_type_enum_id"
    t.integer  "organization_party_id"
    t.integer  "gl_account_id"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "payment_method_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "payment_methods", :force => true do |t|
    t.integer  "payment_method_id"
    t.integer  "payment_method_type_enum_id"
    t.integer  "owner_party_id"
    t.text     "description"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "postal_contact_mech_id"
    t.integer  "telecom_contact_mech_id"
    t.integer  "trust_level_enum_id"
    t.integer  "payment_fraud_evidence_id"
    t.integer  "gl_account_id"
    t.integer  "fin_account_id"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "payment_status_valids", :force => true do |t|
    t.string   "status_id"
    t.string   "to_status_id"
    t.integer  "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "payment_statuses", :force => true do |t|
    t.string   "status_id"
    t.string   "description"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "payment_types", :force => true do |t|
    t.string   "name"
    t.string   "parent_name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "payments", :force => true do |t|
    t.integer  "payment_id"
    t.integer  "payment_type_enum_id"
    t.integer  "from_party_id"
    t.integer  "to_party_id"
    t.integer  "payment_method_type_enum_id"
    t.integer  "payment_method_id"
    t.integer  "order_id"
    t.integer  "status_id"
    t.datetime "effective_data"
    t.text     "payment_auth_code"
    t.text     "payment_ref_num"
    t.decimal  "amount",                      :precision => 10, :scale => 2, :null => false
    t.integer  "amount_uom_id"
    t.text     "comments"
    t.integer  "fin_account_auth_id"
    t.integer  "fin_account_trans_id"
    t.integer  "override_gl_account_id"
    t.decimal  "original_currency_amount",    :precision => 10, :scale => 2, :null => false
    t.integer  "original_currency_uom_id"
    t.boolean  "present_flag"
    t.boolean  "swiped_flag"
    t.integer  "process_attempt"
    t.boolean  "needs_nsf_retry"
    t.datetime "created_at",                                                 :null => false
    t.datetime "updated_at",                                                 :null => false
  end

  create_table "paypal_accounts", :force => true do |t|
    t.integer  "payment_method_id"
    t.integer  "payer_id"
    t.text     "expresss_checkout_token"
    t.text     "payer_status"
    t.boolean  "avs_addr"
    t.boolean  "avs_zip"
    t.integer  "correlation_id"
    t.text     "transaction_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "payroll_deduction_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "payroll_preferences", :force => true do |t|
    t.integer  "employment_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.float    "percentage"
    t.decimal  "flat_amount"
    t.integer  "deduction_type_enum_id"
    t.integer  "time_period_type_id"
    t.integer  "payment_method_type_enum_id"
    t.integer  "payment_method_id"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "people", :force => true do |t|
    t.integer  "party_id"
    t.text     "salutation"
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "personal_title"
    t.string   "suffix"
    t.string   "nickname"
    t.boolean  "gender"
    t.date     "birth_date"
    t.date     "deceased_date"
    t.float    "height"
    t.float    "weight"
    t.text     "mothers_maiden_name"
    t.text     "comments"
    t.integer  "marital_status_id"
    t.integer  "employment_status_id"
    t.integer  "residence_status_id"
    t.string   "occupation"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  add_index "people", ["first_name"], :name => "index_people_on_first_name"
  add_index "people", ["last_name"], :name => "index_people_on_last_name"

  create_table "performance_notes", :force => true do |t|
    t.integer  "employee_party_id"
    t.datetime "note_date"
    t.text     "note_text"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "performance_review_item_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "performance_review_items", :force => true do |t|
    t.integer  "performance_review_id"
    t.integer  "performance_review_item_seq_id"
    t.integer  "review_item_type_enum_id"
    t.integer  "review_rating_enum_id"
    t.text     "comments"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  create_table "performance_review_ratings", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "performance_reviews", :force => true do |t|
    t.integer  "employee_party_id"
    t.integer  "manager_party_id"
    t.integer  "payment_id"
    t.integer  "empl_position_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.text     "comments"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "person_trainings", :force => true do |t|
    t.integer  "party_id"
    t.integer  "training_class_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "work_effort_id"
    t.integer  "approver_party_id"
    t.text     "training_approved"
    t.text     "reason"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "physical_inventories", :force => true do |t|
    t.datetime "physical_inventory_date"
    t.integer  "party_id"
    t.text     "general_comments"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "picklist_bin_item_status_valids", :force => true do |t|
    t.integer  "status_id"
    t.integer  "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "picklist_bin_item_statuses", :force => true do |t|
    t.string   "description"
    t.integer  "sequence_num"
    t.string   "name"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "picklist_bin_items", :force => true do |t|
    t.integer  "picklist_bin_id"
    t.integer  "picklist_bin_item_seq_id"
    t.integer  "order_id"
    t.integer  "order_item_seq_id"
    t.integer  "asset_id"
    t.integer  "status_id"
    t.decimal  "quantity"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "picklist_bins", :force => true do |t|
    t.integer  "picklist_id"
    t.integer  "bin_location_number"
    t.integer  "primary_order_id"
    t.integer  "primary_order_part_seq_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "picklist_parties", :force => true do |t|
    t.integer  "picklist_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "picklist_status_valids", :force => true do |t|
    t.integer  "status_id"
    t.integer  "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "picklist_statuses", :force => true do |t|
    t.string   "description"
    t.integer  "sequence_num"
    t.string   "name"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "picklists", :force => true do |t|
    t.text     "description"
    t.integer  "facility_id"
    t.integer  "shipment_method_id"
    t.integer  "status_id"
    t.datetime "picklist_date"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "postal_addresses", :force => true do |t|
    t.integer  "contact_mech_id"
    t.string   "to_name"
    t.string   "attn_name"
    t.string   "address1"
    t.string   "address2"
    t.string   "unit_number"
    t.text     "directions"
    t.string   "city"
    t.integer  "county_geo_id"
    t.integer  "state_province_geo_id"
    t.integer  "country_geo_id"
    t.string   "postal_code"
    t.string   "postal_code_ext"
    t.integer  "postal_code_geo_id"
    t.integer  "geo_point_id"
    t.boolean  "commercial"
    t.string   "access_code"
    t.integer  "postal_code_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  add_index "postal_addresses", ["city"], :name => "CITY_IDX"
  add_index "postal_addresses", ["postal_code"], :name => "POSTAL_CODE_IDX"

  create_table "product_assoc_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "parent_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "product_assocs", :force => true do |t|
    t.integer  "product_id"
    t.integer  "to_product_id"
    t.integer  "product_assoc_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "sequence_num"
    t.text     "reason"
    t.decimal  "quantity"
    t.decimal  "scrap_factor"
    t.text     "instruction"
    t.integer  "routing_work_effort_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "product_average_costs", :force => true do |t|
    t.integer  "average_cost_type_id"
    t.integer  "organization_party_id"
    t.integer  "product_id"
    t.integer  "facility_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.decimal  "average_cost"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "product_calculated_infos", :force => true do |t|
    t.integer  "product_id"
    t.decimal  "total_quantity_ordered"
    t.integer  "total_times_viewed"
    t.decimal  "average_customer_rating"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "product_categories", :force => true do |t|
    t.integer  "product_category_type_id"
    t.text     "category_name"
    t.text     "description"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "product_category_content_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "product_category_contents", :force => true do |t|
    t.integer  "product_category_id"
    t.integer  "content_location"
    t.integer  "category_content_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "product_category_contents", ["product_category_id"], :name => "PRDCAT_CNT_CTTP"

  create_table "product_category_feat_grp_appls", :force => true do |t|
    t.integer  "product_category_id"
    t.integer  "product_feature_group_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "product_category_gl_accounts", :force => true do |t|
    t.integer  "product_category_id"
    t.integer  "organization_party_id"
    t.string   "gl_account_type_enum_id"
    t.integer  "gl_account_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "product_category_members", :force => true do |t|
    t.integer  "product_category_id"
    t.integer  "product_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.text     "comments"
    t.integer  "sequence_num"
    t.decimal  "quantity"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "product_category_members", ["product_category_id"], :name => "PRD_CMBR_PCT"

  create_table "product_category_parties", :force => true do |t|
    t.integer  "product_category_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.text     "comments"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "product_category_rollups", :force => true do |t|
    t.integer  "product_category_id"
    t.integer  "parent_product_category_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "sequence_num"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "product_category_rollups", ["parent_product_category_id"], :name => "PRDCR_PARPC"

  create_table "product_category_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "product_config_item_appls", :force => true do |t|
    t.integer  "product_id"
    t.integer  "config_item_id"
    t.integer  "sequence_num"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "config_type_id"
    t.text     "description"
    t.integer  "default_config_option_id"
    t.boolean  "is_mandatory"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "product_config_item_content_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "product_config_item_contents", :force => true do |t|
    t.integer  "config_item_id"
    t.text     "content_location"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "item_content_type_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "product_config_items", :force => true do |t|
    t.integer  "config_item_id"
    t.integer  "config_item_type_id"
    t.text     "config_item_name"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "product_config_option_iactns", :force => true do |t|
    t.integer  "config_item_id"
    t.integer  "config_option_seq_id"
    t.integer  "to_config_item_id"
    t.integer  "to_config_option_seq_id"
    t.integer  "iactn_type_id"
    t.text     "description"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "product_config_option_products", :force => true do |t|
    t.integer  "config_item_id"
    t.integer  "config_option_seq_id"
    t.integer  "product_id"
    t.decimal  "quantity"
    t.integer  "sequence_num"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "product_config_options", :force => true do |t|
    t.integer  "config_item_id"
    t.integer  "config_option_seq_id"
    t.text     "config_option_name"
    t.text     "description"
    t.integer  "sequence_num"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "product_config_saved_options", :force => true do |t|
    t.integer  "product_config_saved_id"
    t.integer  "config_item_id"
    t.integer  "config_option_seq_id"
    t.integer  "product_id"
    t.decimal  "quantity"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "product_config_saveds", :force => true do |t|
    t.integer  "configurable_product_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "product_content_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "product_contents", :force => true do |t|
    t.integer  "product_id"
    t.text     "content_location"
    t.integer  "product_content_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "sequence_num"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
  end

  create_table "product_cost_component_calcs", :force => true do |t|
    t.integer  "product_id"
    t.integer  "cost_component_type_id"
    t.datetime "from_date"
    t.integer  "cost_component_calc_id"
    t.integer  "sequence_num"
    t.datetime "thru_date"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "product_dimension_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "product_dimensions", :force => true do |t|
    t.integer  "product_id"
    t.integer  "dimension_type_id"
    t.integer  "value"
    t.integer  "value_uom_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "product_facilities", :force => true do |t|
    t.integer  "product_id"
    t.integer  "facility_id"
    t.decimal  "minimum_stock"
    t.decimal  "reorder_quantity"
    t.integer  "days_to_ship"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "product_facility_locations", :force => true do |t|
    t.integer  "product_id"
    t.integer  "facility_id"
    t.integer  "location_seq_id"
    t.decimal  "minimum_stock"
    t.decimal  "move_quantity"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "product_feature_appl_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "product_feature_appls", :force => true do |t|
    t.integer  "product_id"
    t.integer  "product_feature_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "appl_type_id"
    t.integer  "sequence_num"
    t.decimal  "amount"
    t.decimal  "recurring_amount"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "product_feature_group_appls", :force => true do |t|
    t.integer  "product_feature_group_id"
    t.integer  "product_feature_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "sequence_num"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "product_feature_groups", :force => true do |t|
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "product_feature_iactn_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "product_feature_iactns", :force => true do |t|
    t.integer  "to_product_feature_id"
    t.integer  "from_product_feature_id"
    t.integer  "iactn_type_id"
    t.integer  "product_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "product_feature_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.integer  "parent_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "product_features", :force => true do |t|
    t.integer  "product_feature_type_id"
    t.text     "description"
    t.decimal  "number_specified"
    t.integer  "number_uom_id"
    t.decimal  "default_amount"
    t.integer  "default_sequence_num"
    t.integer  "abbrev"
    t.integer  "id_code"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "product_geo_purposes", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "product_geos", :force => true do |t|
    t.integer  "product_id"
    t.integer  "geo_id"
    t.integer  "product_geo_purpose_id"
    t.string   "description"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "product_gl_accounts", :force => true do |t|
    t.integer  "product_id"
    t.integer  "organization_party_id"
    t.string   "gl_account_type_enum_id"
    t.integer  "gl_account_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "product_identification_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "product_identifications", :force => true do |t|
    t.integer  "product_id"
    t.integer  "product_id_type_id"
    t.integer  "id_value"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "product_identifications", ["id_value"], :name => "PRODUCT_ID_VALUE"

  create_table "product_maintenances", :force => true do |t|
    t.integer  "product_id"
    t.integer  "maintenance_type_id"
    t.text     "description"
    t.integer  "template_work_effort_id"
    t.decimal  "interval_quantity"
    t.integer  "interval_uom_id"
    t.integer  "interval_meter_type_id"
    t.integer  "repeat_count"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "product_meter_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.integer  "default_uom_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "product_meters", :force => true do |t|
    t.integer  "product_id"
    t.integer  "product_meter_type_id"
    t.integer  "meter_uom_id"
    t.string   "meter_name"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "product_parties", :force => true do |t|
    t.integer  "product_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "sequence_num"
    t.text     "comments"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "product_price_purposes", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "product_price_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "product_prices", :force => true do |t|
    t.integer  "product_id"
    t.integer  "product_store_id"
    t.integer  "vendor_party_id"
    t.integer  "customer_party_id"
    t.integer  "price_type_id"
    t.integer  "price_purpose_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.decimal  "min_quantity"
    t.decimal  "price"
    t.integer  "price_uom_id"
    t.integer  "term_uom_id"
    t.boolean  "tax_in_price"
    t.decimal  "tax_amount"
    t.decimal  "tax_percentage"
    t.integer  "tax_auth_party_id"
    t.integer  "tax_auth_geo_id"
    t.integer  "agreement_id"
    t.integer  "agreement_item_seq_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "product_promotions", :force => true do |t|
    t.integer  "billback_factor"
    t.integer  "override_org_party_id"
    t.string   "promotion_name",              :null => false
    t.boolean  "promotion_show_to_customer"
    t.text     "promotion_text"
    t.boolean  "require_code"
    t.integer  "use_limit_per_customer"
    t.integer  "use_limit_per_order"
    t.integer  "use_limit_per_promotion"
    t.boolean  "user_entered"
    t.integer  "created_by_user_login"
    t.integer  "last_modified_by_user_login"
    t.integer  "user_id"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "product_review_status_valids", :force => true do |t|
    t.integer  "status_id"
    t.integer  "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "product_review_statuses", :force => true do |t|
    t.string   "description"
    t.integer  "sequence_num"
    t.string   "status_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "product_reviews", :force => true do |t|
    t.integer  "product_store_id"
    t.integer  "product_id"
    t.integer  "user_id"
    t.integer  "status_id"
    t.boolean  "posted_anonymous"
    t.datetime "posted_datetime"
    t.decimal  "product_rating"
    t.text     "product_review"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "product_store_categories", :force => true do |t|
    t.integer  "product_store_id"
    t.integer  "product_category_id"
    t.integer  "store_category_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "sequence_num"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "product_store_category_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "product_store_email_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "product_store_emails", :force => true do |t|
    t.integer  "product_store_id"
    t.integer  "email_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "email_template_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "product_store_facilities", :force => true do |t|
    t.integer  "product_store_id"
    t.integer  "facility_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "sequence_num"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "product_store_group_members", :force => true do |t|
    t.integer  "product_store_group_id"
    t.integer  "product_store_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "sequence_num"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "product_store_group_parties", :force => true do |t|
    t.integer  "product_store_group_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "product_store_group_rollups", :force => true do |t|
    t.integer  "product_store_group_id"
    t.integer  "parent_group_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "sequence_num"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "product_store_groups", :force => true do |t|
    t.integer  "product_store_group_type_id"
    t.text     "description"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "product_store_parties", :force => true do |t|
    t.integer  "product_store_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "sequence_num"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "product_stores", :force => true do |t|
    t.string   "store_name"
    t.integer  "organization_party_id"
    t.integer  "inventory_facility_id"
    t.integer  "reservation_order_id"
    t.integer  "requirement_method_id"
    t.text     "default_locale"
    t.integer  "default_currency_uom_id"
    t.integer  "default_sales_channel_id"
    t.boolean  "require_customer_role"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "product_subscription_resources", :force => true do |t|
    t.integer  "product_id"
    t.integer  "subscription_resource_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "purchase_from_date"
    t.datetime "purchase_thru_date"
    t.integer  "available_time"
    t.integer  "available_time_uom_id"
    t.integer  "use_count_limit"
    t.integer  "use_time"
    t.integer  "use_time_uom_id"
    t.integer  "use_role_type_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "product_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "parent_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "products", :force => true do |t|
    t.integer  "product_type_id"
    t.text     "product_name"
    t.text     "description"
    t.text     "comments"
    t.datetime "sales_introduction_date"
    t.datetime "sales_discontinuation_date"
    t.boolean  "sales_disc_when_not_avail"
    t.datetime "support_discontinuation_date"
    t.boolean  "require_inventory"
    t.boolean  "requirement_method_id"
    t.boolean  "charge_shipping"
    t.boolean  "in_shipping_box"
    t.integer  "default_shipment_box_type_id"
    t.boolean  "returnable"
    t.boolean  "require_amount"
    t.integer  "amount_uom_type_id"
    t.decimal  "fixed_amount"
    t.integer  "origin_geo_id"
    t.integer  "bill_of_material_level"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.text     "sku"
  end

  add_index "products", ["product_name"], :name => "index_products_on_product_name"

  create_table "promotion_categories", :force => true do |t|
    t.string   "product_category_id"
    t.string   "sub_category_status"
    t.string   "group_id"
    t.string   "product_promotion_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "promotion_code_emails", :force => true do |t|
    t.string   "email"
    t.string   "product_promotion_code_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "promotion_code_parties", :force => true do |t|
    t.integer  "party_id"
    t.string   "product_promotion_code_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "promotion_codes", :force => true do |t|
    t.string   "product_promotion_code_id"
    t.string   "product_promotion_id"
    t.boolean  "user_entered"
    t.boolean  "require_email_or_party"
    t.integer  "use_limit_per_code"
    t.integer  "use_limit_per_customer"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "created_by_user_login"
    t.integer  "last_modified_by_user_login"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "promotion_products", :force => true do |t|
    t.integer  "product_id"
    t.string   "product_promotion_id"
    t.string   "sub_category_status"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "promotion_rule_action_categories", :force => true do |t|
    t.string   "product_category_id"
    t.string   "sub_category_status"
    t.integer  "group_id"
    t.integer  "promotion_rule_action_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "promotion_rule_action_products", :force => true do |t|
    t.integer  "product_id"
    t.string   "sub_category_status"
    t.integer  "promotion_rule_action_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "promotion_rule_actions", :force => true do |t|
    t.integer  "promotion_rule_id"
    t.string   "promotion_rule_action_name"
    t.integer  "quantity"
    t.integer  "amount"
    t.integer  "item_id"
    t.string   "party_id"
    t.string   "service_name"
    t.boolean  "use_cart_quantity"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "promotion_rule_condition_categories", :force => true do |t|
    t.string   "product_category_id"
    t.string   "sub_category_status"
    t.integer  "group_id"
    t.integer  "promotion_rule_condition_id"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "promotion_rule_condition_products", :force => true do |t|
    t.integer  "product_id"
    t.string   "sub_category_status"
    t.integer  "promotion_rule_condition_id"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "promotion_rule_condition_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "promotion_rule_conditions", :force => true do |t|
    t.integer  "promotion_rule_id"
    t.string   "promotion_rule_condition_name"
    t.integer  "operator_id"
    t.integer  "value"
    t.integer  "other"
    t.integer  "shipment_method_id"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "promotion_rules", :force => true do |t|
    t.string   "product_promotion_id"
    t.string   "name"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "promotion_stores", :force => true do |t|
    t.integer  "product_store_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "sequence_num"
    t.string   "product_promotion_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "qualification_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "parent_id"
  end

  create_table "quote_sequences", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "rate_amounts", :force => true do |t|
    t.integer  "rate_amount_id"
    t.integer  "rate_type_enum_id"
    t.integer  "time_period_type_id"
    t.integer  "work_effort_id"
    t.integer  "party_id"
    t.integer  "empl_position_type_enum_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.decimal  "rate_amount"
    t.integer  "rate_currency_uom_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "rate_time_period_types", :force => true do |t|
    t.string   "description"
    t.string   "time_period_type_id"
    t.integer  "period_length"
    t.string   "length_uom_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "rate_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "rejection_reasons", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "request_categories", :force => true do |t|
    t.integer  "request_category_id"
    t.integer  "parent_category_id"
    t.integer  "responsible_party_id"
    t.text     "description"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "request_comm_events", :force => true do |t|
    t.integer  "request_id"
    t.integer  "communication_event_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "request_contents", :force => true do |t|
    t.integer  "request_id"
    t.text     "content_location"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "request_item_work_efforts", :force => true do |t|
    t.integer  "request_id"
    t.integer  "request_item_seq_id"
    t.integer  "work_effort_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "request_items", :force => true do |t|
    t.integer  "request_id"
    t.integer  "request_item_seq_id"
    t.integer  "status_id"
    t.integer  "request_resolution_enum_id"
    t.integer  "priority"
    t.integer  "sequence_num"
    t.datetime "required_by_date"
    t.integer  "product_id"
    t.integer  "quantity"
    t.integer  "selected_amount"
    t.decimal  "maximum_amount"
    t.text     "description"
    t.text     "story"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "request_notes", :force => true do |t|
    t.integer  "request_id"
    t.datetime "note_date"
    t.integer  "request_item_seq_id"
    t.text     "note_text"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "request_parties", :force => true do |t|
    t.integer  "request_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "request_work_efforts", :force => true do |t|
    t.integer  "request_id"
    t.integer  "work_effort_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "requests", :force => true do |t|
    t.integer  "request_id"
    t.integer  "request_type_enum_id"
    t.integer  "request_category_id"
    t.integer  "status_id"
    t.text     "description"
    t.integer  "from_party_id"
    t.integer  "priority"
    t.datetime "request_date"
    t.datetime "response_required_date"
    t.integer  "product_store_id"
    t.integer  "sales_chaneel_enum_id"
    t.integer  "fullfill_contact_mech_id"
    t.integer  "maximun_amount_uom_id"
    t.integer  "currency_uom_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "requirement_budget_allocations", :force => true do |t|
    t.integer  "requirment_id"
    t.integer  "budget_id"
    t.integer  "budget_item_seq_id"
    t.decimal  "amount",             :precision => 10, :scale => 2, :null => false
    t.datetime "created_at",                                        :null => false
    t.datetime "updated_at",                                        :null => false
  end

  create_table "requirement_methods", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "requirement_parties", :force => true do |t|
    t.integer  "requirement_id"
    t.integer  "part_id"
    t.integer  "role_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "requirement_request_items", :force => true do |t|
    t.integer  "requirement_id"
    t.integer  "request_id"
    t.integer  "request_item_seq_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "requirements", :force => true do |t|
    t.integer  "requirement_type_enum_id"
    t.integer  "status_id"
    t.integer  "facility_id"
    t.integer  "deliverable_id"
    t.integer  "asset_id"
    t.integer  "product_id"
    t.text     "description"
    t.datetime "requirement_start_date"
    t.decimal  "estimated_budget",         :precision => 10, :scale => 2, :null => false
    t.decimal  "quantity",                 :precision => 10, :scale => 2, :null => false
    t.text     "use_case"
    t.text     "reason"
    t.datetime "created_at",                                              :null => false
    t.datetime "updated_at",                                              :null => false
  end

  create_table "residence_statuses", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "return_contact_meches", :force => true do |t|
    t.integer  "return_id"
    t.integer  "contact_mech_purpose_id"
    t.integer  "contact_mech_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "return_header_status_valids", :force => true do |t|
    t.integer  "status_id"
    t.integer  "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "return_header_statuses", :force => true do |t|
    t.string   "description"
    t.integer  "sequence_num"
    t.string   "status_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "return_header_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "return_headers", :force => true do |t|
    t.integer  "return_id"
    t.integer  "status_id"
    t.integer  "from_party_id"
    t.integer  "to_party_id"
    t.integer  "payment_method_id"
    t.integer  "fin_account_id"
    t.integer  "billing_account_id"
    t.datetime "entry_date"
    t.integer  "origin_contact_mech_id"
    t.integer  "destination_facility_id"
    t.boolean  "needs_inventory_receive"
    t.integer  "currency_uom_id"
    t.integer  "supplier_rma_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.integer  "return_header_type_id"
  end

  create_table "return_item_billings", :force => true do |t|
    t.integer  "return_id"
    t.integer  "return_item_seq_id"
    t.integer  "invoice_id"
    t.integer  "invoice_item_seq_id"
    t.integer  "shipment_receipt_id"
    t.decimal  "quantity"
    t.decimal  "amount"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "return_item_shipments", :force => true do |t|
    t.integer  "return_id"
    t.integer  "return_item_seq_id"
    t.integer  "shipment_id"
    t.integer  "shipment_item_seq_id"
    t.decimal  "quantity"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "return_items", :force => true do |t|
    t.integer  "return_id"
    t.integer  "return_item_seq_id"
    t.integer  "return_reason_id"
    t.integer  "return_response_id"
    t.integer  "item_type_id"
    t.integer  "product_id"
    t.text     "description"
    t.integer  "order_id"
    t.integer  "order_item_seq_id"
    t.integer  "order_adjustment_id"
    t.integer  "status_id"
    t.integer  "expected_inventory_status_id"
    t.decimal  "return_quantity"
    t.decimal  "received_quantity"
    t.decimal  "return_price"
    t.integer  "replacement_order_id"
    t.integer  "original_payment_id"
    t.integer  "refund_payment_id"
    t.integer  "billing_account_id"
    t.integer  "fin_account_trans_id"
    t.decimal  "response_amount"
    t.datetime "response_date"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  add_index "return_items", ["order_item_seq_id"], :name => "RTN_ITM_BYORDITM"

  create_table "return_reasons", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "return_responses", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "role_types", :force => true do |t|
    t.integer  "parent_type_id"
    t.text     "description"
    t.string   "name"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "roles_users", :id => false, :force => true do |t|
    t.integer  "role_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "roles_users", ["role_id"], :name => "index_roles_users_on_role_id"
  add_index "roles_users", ["user_id"], :name => "index_roles_users_on_user_id"

  create_table "sales_channels", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "sales_forecast_details", :force => true do |t|
    t.integer  "sales_forecast_id"
    t.integer  "sales_forecast_detail_seq_id"
    t.decimal  "amount"
    t.decimal  "quantity"
    t.integer  "quantity_uom_id"
    t.integer  "product_id"
    t.integer  "product_category_id"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "sales_forecasts", :force => true do |t|
    t.integer  "parent_sales_forecast_id"
    t.integer  "organization_party_id"
    t.integer  "internal_party_id"
    t.integer  "time_period_id"
    t.integer  "currency_uom_id"
    t.decimal  "quota_amount"
    t.decimal  "forecast_amount"
    t.decimal  "best_case_amount"
    t.decimal  "closed_amount"
    t.decimal  "percent_of_quota_forecast"
    t.decimal  "percent_of_quota_closed"
    t.decimal  "pipeline_amount"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "sales_opportunities", :force => true do |t|
    t.integer  "type_id"
    t.string   "opportunity_name"
    t.text     "description"
    t.text     "next_step"
    t.decimal  "estimated_amount"
    t.decimal  "estimated_probability"
    t.integer  "currency_uom_id"
    t.integer  "marketing_campaign_id"
    t.integer  "data_source_id"
    t.datetime "estimated_close_date"
    t.integer  "opportunity_stage_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "sales_opportunity_competitors", :force => true do |t|
    t.integer  "sales_opportunity_id"
    t.integer  "competitor_party_id"
    t.integer  "position_id"
    t.text     "strengths"
    t.text     "weaknesses"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "sales_opportunity_parties", :force => true do |t|
    t.integer  "sales_opportunity_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "sales_opportunity_quotes", :force => true do |t|
    t.integer  "sales_opportunity_id"
    t.integer  "order_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "sales_opportunity_stages", :force => true do |t|
    t.integer  "opportunity_stage_id"
    t.text     "description"
    t.decimal  "default_probability"
    t.integer  "sequence_num"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "sales_opportunity_trackings", :force => true do |t|
    t.integer  "sales_opportunity_id"
    t.integer  "tracking_code_id"
    t.datetime "received_date"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "sales_opportunity_work_efforts", :force => true do |t|
    t.integer  "sales_opportunity_id"
    t.integer  "work_effort_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "settlement_term_values", :force => true do |t|
    t.string   "term_id"
    t.string   "description"
    t.string   "term_value"
    t.string   "term_value_uom_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "settlement_terms", :force => true do |t|
    t.integer  "settlement_term_id"
    t.integer  "term_type_enum_id"
    t.text     "descriptin"
    t.integer  "term_value"
    t.integer  "term_value_uom_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "shipment_box_types", :force => true do |t|
    t.text     "description"
    t.integer  "dimension_uom_id"
    t.decimal  "box_length"
    t.decimal  "box_width"
    t.decimal  "box_height"
    t.integer  "weight_uom_id"
    t.decimal  "box_weight"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "shipment_contact_meches", :force => true do |t|
    t.integer  "shipment_id"
    t.integer  "contact_mech_purpose_id"
    t.integer  "contact_mech_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "shipment_item_billings", :force => true do |t|
    t.integer  "shipment_id"
    t.integer  "shipment_item_seq_id"
    t.integer  "invoice_id"
    t.integer  "invoice_item_seq_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "shipment_item_pick_status_valids", :force => true do |t|
    t.integer  "status_id"
    t.integer  "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "shipment_item_pick_statuses", :force => true do |t|
    t.string   "description"
    t.integer  "sequence_num"
    t.string   "name"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "shipment_item_sources", :force => true do |t|
    t.integer  "shipment_id"
    t.integer  "product_id"
    t.integer  "binLocation_number"
    t.integer  "order_id"
    t.integer  "order_item_seq_id"
    t.integer  "return_id"
    t.integer  "return_item_seq_id"
    t.integer  "status_id"
    t.decimal  "quantity"
    t.decimal  "quantity_not_packed"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "shipment_items", :force => true do |t|
    t.integer  "shipment_id"
    t.integer  "shipment_item_seq_id"
    t.integer  "product_id"
    t.decimal  "quantity"
    t.text     "description"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "shipment_methods", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "shipment_package_contents", :force => true do |t|
    t.integer  "shipment_id"
    t.integer  "shipment_package_seq_id"
    t.integer  "shipment_item_seq_id"
    t.decimal  "quantity"
    t.integer  "sub_product_id"
    t.decimal  "sub_product_quantity"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "shipment_package_route_segs", :force => true do |t|
    t.integer  "shipment_id"
    t.integer  "shipment_package_seq_id"
    t.integer  "shipment_route_segment_id"
    t.string   "tracking_code"
    t.string   "box_number"
    t.text     "label_image"
    t.text     "label_intl_sign_image"
    t.text     "label_html"
    t.boolean  "label_printed"
    t.text     "international_invoice"
    t.decimal  "package_transport_cost"
    t.decimal  "package_service_cost"
    t.decimal  "package_other_cost"
    t.decimal  "cod_amount"
    t.decimal  "insured_amount"
    t.integer  "amount_uom_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "shipment_packages", :force => true do |t|
    t.integer  "shipment_id"
    t.integer  "shipment_package_seq_id"
    t.integer  "shipment_box_type_id"
    t.datetime "date_created"
    t.decimal  "box_length"
    t.decimal  "box_height"
    t.decimal  "box_width"
    t.integer  "dimension_uom_id"
    t.decimal  "weight"
    t.integer  "weight_uom_id"
    t.decimal  "insured_value"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "shipment_receipt_parties", :force => true do |t|
    t.integer  "shipment_receipt_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "shipment_receipts", :force => true do |t|
    t.integer  "asset_id"
    t.integer  "product_id"
    t.integer  "shipment_id"
    t.integer  "shipment_item_seq_id"
    t.integer  "shipment_package_seq_id"
    t.integer  "order_id"
    t.integer  "order_item_seq_id"
    t.integer  "return_id"
    t.integer  "return_item_seq_id"
    t.integer  "rejection_reason_id"
    t.integer  "received_by_user_id"
    t.datetime "datetime_received"
    t.text     "item_description"
    t.decimal  "quantity_accepted"
    t.decimal  "quantity_rejected"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "shipment_route_segment_status_valids", :force => true do |t|
    t.integer  "status_id"
    t.integer  "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "shipment_route_segment_statuses", :force => true do |t|
    t.string   "description"
    t.integer  "sequence_num"
    t.string   "name"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "shipment_route_segments", :force => true do |t|
    t.integer  "shipment_id"
    t.integer  "delivery_id"
    t.integer  "origin_facility_id"
    t.integer  "origin_contact_mech_id"
    t.integer  "origin_telecom_number_id"
    t.integer  "dest_facility_id"
    t.integer  "dest_contact_mech_id"
    t.integer  "dest_telecom_number_id"
    t.integer  "carrier_party_id"
    t.integer  "shipment_method_id"
    t.integer  "status_id"
    t.string   "carrier_delivery_zone"
    t.string   "carrier_restriction_codes"
    t.text     "carrier_restriction_desc"
    t.decimal  "billing_weight"
    t.integer  "billing_weight_uom_id"
    t.decimal  "actual_transport_cost"
    t.decimal  "actual_service_cost"
    t.decimal  "actual_other_cost"
    t.decimal  "actual_cost"
    t.integer  "cost_uom_id"
    t.datetime "actual_start_date"
    t.datetime "actual_arrival_date"
    t.datetime "estimated_start_date"
    t.datetime "estimated_arrival_date"
    t.string   "tracking_id_number"
    t.text     "tracking_digest"
    t.integer  "home_delivery_type"
    t.datetime "home_delivery_date"
    t.integer  "third_party_account_number"
    t.integer  "third_party_postal_code"
    t.integer  "third_party_country_geo_code"
    t.text     "ups_high_value_report"
    t.integer  "billing_weight_id"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "shipment_status_valids", :force => true do |t|
    t.integer  "status_id"
    t.integer  "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "shipment_statuses", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "shipment_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.integer  "parent_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "shipments", :force => true do |t|
    t.integer  "shipment_type_id"
    t.integer  "status_id"
    t.integer  "picklist_bin_id"
    t.integer  "primary_order_id"
    t.integer  "primary_order_part_seq_id"
    t.integer  "primary_return_id"
    t.datetime "estimated_ready_date"
    t.datetime "estimated_ship_date"
    t.integer  "estimated_ship_work_eff_id"
    t.datetime "estimated_arrival_date"
    t.integer  "estimated_arrival_work_eff_id"
    t.datetime "latest_cancel_date"
    t.decimal  "estimated_ship_cost"
    t.integer  "cost_uom_id"
    t.text     "handling_instructions"
    t.integer  "origin_facility_id"
    t.integer  "origin_contact_mech_id"
    t.integer  "origin_telecom_number_id"
    t.integer  "destination_facility_id"
    t.integer  "destination_contact_mech_id"
    t.integer  "destination_telecom_number_id"
    t.integer  "to_party_id"
    t.integer  "from_party_id"
    t.decimal  "additional_shipping_charge"
    t.text     "addtl_shipping_charge_desc"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "status_items", :force => true do |t|
    t.string   "status_id"
    t.string   "description"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "subscription_deliveries", :force => true do |t|
    t.integer  "subscription_id"
    t.datetime "date_sent"
    t.integer  "communication_event_id"
    t.text     "comments"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "subscription_resources", :force => true do |t|
    t.integer  "parent_resource_id"
    t.text     "description"
    t.text     "content_location"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "subscription_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "subscriptions", :force => true do |t|
    t.integer  "subscription_type_id"
    t.integer  "subscription_resource_id"
    t.integer  "subscriber_party_id"
    t.integer  "deliver_to_contact_mech_id"
    t.integer  "order_id"
    t.integer  "order_item_seq_id"
    t.integer  "product_id"
    t.integer  "external_subscription_id"
    t.text     "description"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "purchase_from_date"
    t.datetime "purchase_thru_date"
    t.integer  "available_time"
    t.integer  "available_time_uom_id"
    t.integer  "use_time"
    t.integer  "use_time_uom_id"
    t.integer  "use_count_limit"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "supplier_preferred_orders", :force => true do |t|
    t.integer  "sequence_num"
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "supplier_products", :force => true do |t|
    t.integer  "supplier_party_id"
    t.integer  "product_id"
    t.text     "supplier_product_name"
    t.text     "comments"
    t.decimal  "quantity_increment"
    t.decimal  "quantity_included"
    t.decimal  "quantity_uom_id"
    t.integer  "preferred_order_id"
    t.integer  "supplier_rating_type_id"
    t.decimal  "standard_lead_time_days"
    t.boolean  "can_drop_ship"
    t.boolean  "supplier_commission_percent"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "sync_statuses", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "tax_authorities", :force => true do |t|
    t.integer  "tax_auth_geo_id"
    t.integer  "tax_auth_party_id"
    t.boolean  "require_tax_id_for_exemption"
    t.string   "tax_id_format_pattern"
    t.boolean  "include_tax_in_price"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "tax_authority_assoc_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "tax_authority_assocs", :force => true do |t|
    t.integer  "tax_auth_geo_id"
    t.integer  "tax_auth_party_id"
    t.integer  "to_tax_auth_geo_id"
    t.integer  "to_tax_auth_party_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.string   "assoc_type_enum_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "tax_authority_categories", :force => true do |t|
    t.integer  "tax_auth_geo_id"
    t.integer  "tax_auth_party_id"
    t.integer  "product_category_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "tax_authority_gl_accounts", :force => true do |t|
    t.integer  "tax_auth_geo_id"
    t.integer  "tax_auth_party_id"
    t.integer  "organization_party_id"
    t.integer  "gl_account_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "tax_authority_rate_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "tax_authority_rates", :force => true do |t|
    t.integer  "tax_auth_geo_id"
    t.integer  "tax_auth_party_id"
    t.integer  "rate_type_enum_id"
    t.integer  "product_store_id"
    t.integer  "product_category_id"
    t.string   "title_transfer_enum_id"
    t.float    "min_item_price"
    t.float    "min_purchase"
    t.boolean  "tax_shipping"
    t.float    "tax_percentage"
    t.boolean  "tax_promotions"
    t.date     "from_date"
    t.date     "thru_date"
    t.string   "description"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "tax_forms", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "telecom_numbers", :force => true do |t|
    t.integer  "contact_mech_id"
    t.string   "country_code"
    t.string   "area_code"
    t.string   "contact_number"
    t.string   "ask_for_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "telecom_numbers", ["area_code", "contact_number"], :name => "AREA_CONTACT_IDX"

  create_table "term_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.integer  "parent_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "termination_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "time_entries", :force => true do |t|
    t.integer  "timesheet_id"
    t.integer  "party_id"
    t.integer  "rate_type_id"
    t.string   "from_date_datetime"
    t.datetime "thru_date"
    t.decimal  "hours"
    t.string   "comments"
    t.integer  "work_effort_id"
    t.integer  "invoice_id"
    t.integer  "invoice_item_seq_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "time_period_types", :force => true do |t|
    t.string   "time_period_type_id"
    t.text     "description"
    t.integer  "period_length"
    t.string   "length_uom_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "time_period_types", ["length_uom_id"], :name => "index_time_period_types_on_length_uom_id"
  add_index "time_period_types", ["time_period_type_id"], :name => "index_time_period_types_on_time_period_type_id"

  create_table "time_periods", :force => true do |t|
    t.integer  "parent_period_id"
    t.string   "time_period_type_id"
    t.integer  "party_id"
    t.integer  "period_num"
    t.text     "period_name"
    t.date     "from_date"
    t.date     "thru_date"
    t.boolean  "is_closed"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "time_periods", ["parent_period_id"], :name => "index_time_periods_on_parent_period_id"
  add_index "time_periods", ["party_id"], :name => "index_time_periods_on_party_id"
  add_index "time_periods", ["time_period_type_id"], :name => "index_time_periods_on_time_period_type_id"

  create_table "timesheet_parties", :force => true do |t|
    t.integer  "timesheet_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "timesheet_status_valids", :force => true do |t|
    t.integer  "status_id"
    t.integer  "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "timesheet_statuses", :force => true do |t|
    t.string   "description"
    t.integer  "sequence_num"
    t.string   "status_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "timesheets", :force => true do |t|
    t.integer  "party_id"
    t.integer  "client_party_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "status_id"
    t.string   "comments"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "track_codes", :force => true do |t|
    t.integer  "tracking_code_id"
    t.integer  "tracking_code_type_enum_id"
    t.integer  "marketing_campaign_id"
    t.text     "redirect_url"
    t.text     "comments"
    t.text     "description"
    t.integer  "trackable_lifetime"
    t.integer  "billable_lifetime"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "group_id"
    t.integer  "subgroup_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "tracking_code_order_returns", :force => true do |t|
    t.integer  "tracking_code_type_enum_id"
    t.integer  "return_id"
    t.integer  "order_id"
    t.integer  "order_item_seq_id"
    t.integer  "tracking_code_id"
    t.boolean  "is_billable"
    t.text     "side_id"
    t.boolean  "has_exported"
    t.boolean  "affiliate_referred_time_stamp"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "tracking_code_orders", :force => true do |t|
    t.integer  "order_id"
    t.integer  "tracking_code_type_enum_id"
    t.integer  "tracking_code_id"
    t.boolean  "is_billable"
    t.text     "site_id"
    t.boolean  "has_exported"
    t.datetime "affiliate_referred_time_stamp"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "tracking_code_sources", :force => true do |t|
    t.string   "status_id"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "tracking_code_types", :force => true do |t|
    t.string   "status_id"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "tracking_code_visits", :force => true do |t|
    t.integer  "tracking_code_id"
    t.integer  "visit_id"
    t.datetime "from_date"
    t.integer  "source_enum_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "training_class_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "training_classes", :force => true do |t|
    t.integer  "training_class_type_enum_id"
    t.text     "description"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "unemployment_claim_statuses", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "unemployment_claims", :force => true do |t|
    t.datetime "unemployment_claim_date"
    t.integer  "employment_id"
    t.integer  "status_id"
    t.text     "description"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "user_accounts", :force => true do |t|
    t.integer  "party_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                :default => "", :null => false
    t.string   "encrypted_password",                   :default => ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                        :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
    t.string   "invitation_token",       :limit => 60
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.boolean  "approval"
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["invitation_token"], :name => "index_users_on_invitation_token"
  add_index "users", ["invited_by_id"], :name => "index_users_on_invited_by_id"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

  create_table "variance_reason_gl_accounts", :force => true do |t|
    t.string   "variance_reason_enum_id"
    t.integer  "organization_party_id"
    t.integer  "gl_account_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "versions", :force => true do |t|
    t.string   "item_type",  :null => false
    t.integer  "item_id",    :null => false
    t.string   "event",      :null => false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], :name => "index_versions_on_item_type_and_item_id"

  create_table "weight_uoms", :force => true do |t|
    t.string   "name"
    t.string   "unit"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "work_effort_asset_assign_statuses", :force => true do |t|
    t.string   "description"
    t.integer  "sequence_num"
    t.string   "name"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "work_effort_asset_assigns", :force => true do |t|
    t.integer  "work_effort_id"
    t.integer  "asset_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "status_id"
    t.decimal  "allocated_cost"
    t.text     "comments"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "work_effort_asset_neededs", :force => true do |t|
    t.integer  "work_effort_id"
    t.integer  "asset_product_id"
    t.float    "estimated_quantity"
    t.float    "estimated_duration"
    t.decimal  "estimated_cost"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "work_effort_asset_produceds", :force => true do |t|
    t.integer  "work_effort_id"
    t.integer  "asset_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "work_effort_asset_useds", :force => true do |t|
    t.integer  "work_effort_id"
    t.integer  "asset_id"
    t.integer  "status_id"
    t.float    "quantity"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "work_effort_assoc_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "work_effort_assocs", :force => true do |t|
    t.integer  "work_effort_id"
    t.integer  "to_work_effort_id"
    t.integer  "work_effort_assoc_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "sequence_num"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "work_effort_billings", :force => true do |t|
    t.integer  "work_effort_id"
    t.integer  "invoice_id"
    t.integer  "invoice_item_seq_id"
    t.float    "percentage"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "work_effort_contact_meches", :force => true do |t|
    t.integer  "work_effort_id"
    t.integer  "contact_mech_id"
    t.integer  "contact_mech_purpose_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.string   "extension"
    t.text     "comments"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "work_effort_content_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "work_effort_contents", :force => true do |t|
    t.integer  "work_effort_id"
    t.string   "content_location"
    t.integer  "content_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "work_effort_deliverable_prods", :force => true do |t|
    t.integer  "work_effort_id"
    t.integer  "deliverable_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "work_effort_notes", :force => true do |t|
    t.integer  "work_effort_id"
    t.datetime "note_date"
    t.text     "note_text"
    t.boolean  "internal_note"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "work_effort_parties", :force => true do |t|
    t.integer  "work_effort_id"
    t.integer  "party_id"
    t.integer  "role_type_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "status_id"
    t.integer  "availability_id"
    t.integer  "delegate_reason_id"
    t.integer  "expectation_id"
    t.text     "comments"
    t.boolean  "must_rsvp"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "work_effort_party_availabilities", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "work_effort_party_delegate_reasons", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "work_effort_party_expectations", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "work_effort_party_statuses", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "work_effort_product_statuses", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.integer  "sequence_num"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "work_effort_product_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "work_effort_products", :force => true do |t|
    t.integer  "work_effort_id"
    t.integer  "product_id"
    t.datetime "from_date"
    t.datetime "thru_date"
    t.integer  "type_id"
    t.integer  "status_id"
    t.float    "estimated_quantity"
    t.decimal  "estimated_cost"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "work_effort_purposes", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "work_effort_skill_standards", :force => true do |t|
    t.integer  "work_effort_id"
    t.integer  "skill_type_id"
    t.float    "estimated_num_people"
    t.float    "estimated_duration"
    t.decimal  "estimated_cost"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "work_effort_status_valids", :force => true do |t|
    t.integer  "status_id"
    t.integer  "to_status_id"
    t.string   "transition_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "work_effort_statuses", :force => true do |t|
    t.string   "description"
    t.integer  "sequence_num"
    t.string   "name"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "work_effort_types", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "work_effort_visibilities", :force => true do |t|
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "work_efforts", :force => true do |t|
    t.string   "universal_id"
    t.integer  "source_reference_id"
    t.integer  "parent_work_effort_id"
    t.integer  "work_effort_type_id"
    t.integer  "purpose_id"
    t.integer  "visibility_id"
    t.integer  "status_id"
    t.integer  "priority"
    t.boolean  "send_notification_email"
    t.integer  "percent_complete"
    t.integer  "revision_number"
    t.string   "work_effort_name"
    t.text     "description"
    t.text     "location"
    t.integer  "facility_id"
    t.string   "info_url"
    t.datetime "estimated_start_date"
    t.datetime "estimated_completion_date"
    t.datetime "actual_start_date"
    t.datetime "actual_completion_date"
    t.float    "estimated_work_time"
    t.float    "estimated_setup_time"
    t.float    "actual_work_time"
    t.float    "actual_setup_time"
    t.float    "total_time_allowed"
    t.integer  "time_uom_id"
    t.integer  "recurrence_info_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "work_requirement_fulfillments", :force => true do |t|
    t.integer  "requirement_id"
    t.integer  "work_effort_id"
    t.integer  "fulfillment_type_enum_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

end
