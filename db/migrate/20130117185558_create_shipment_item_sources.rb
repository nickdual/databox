class CreateShipmentItemSources < ActiveRecord::Migration
  def change
    create_table :shipment_item_sources do |t|
      t.integer :shipment_id
      t.integer :product_id
      t.integer :binLocation_number
      t.integer :order_id
      t.integer :order_item_seq_id
      t.integer :return_id
      t.integer :return_item_seq_id
      t.integer :status_id
      t.decimal :quantity
      t.decimal :quantity_not_packed

      t.timestamps
    end
  end
end
