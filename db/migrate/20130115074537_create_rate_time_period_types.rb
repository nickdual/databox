class CreateRateTimePeriodTypes < ActiveRecord::Migration
  def change
    create_table :rate_time_period_types do |t|
      t.string :description
      t.string :time_period_type_id
      t.integer :period_length
      t.string :length_uom_id

      t.timestamps
    end
  end
end
