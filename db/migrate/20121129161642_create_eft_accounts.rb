class CreateEftAccounts < ActiveRecord::Migration
  def change
    create_table :eft_accounts do |t|
      t.integer :payment_method_id
      t.text :bank_name
      t.text :routing_number
      t.text :account_type
      t.text :account_number
      t.text :name_on_account
      t.text :company_name_on_account
      t.integer :years_at_bank

      t.timestamps
    end
  end
end
