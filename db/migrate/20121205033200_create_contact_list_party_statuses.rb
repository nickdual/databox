class CreateContactListPartyStatuses < ActiveRecord::Migration
  def change
    create_table :contact_list_party_statuses do |t|
      t.string :status_id
      t.text :description
      t.integer :sequence_num

      t.timestamps
    end
  end
end
