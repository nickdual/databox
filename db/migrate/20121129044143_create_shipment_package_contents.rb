class CreateShipmentPackageContents < ActiveRecord::Migration
  def change
    create_table :shipment_package_contents do |t|
      t.integer :shipment_id
      t.integer :shipment_package_seq_id
      t.integer :shipment_item_seq_id
      t.decimal :quantity
      t.integer :sub_product_id
      t.decimal :sub_product_quantity

      t.timestamps
    end
  end
end
