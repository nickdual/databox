class CreatePartyRelationshipTypes < ActiveRecord::Migration
  def change
    create_table :party_relationship_types do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
