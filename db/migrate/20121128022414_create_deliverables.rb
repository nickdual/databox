class CreateDeliverables < ActiveRecord::Migration
  def change
    create_table :deliverables do |t|
      t.integer :deliverable_type_id
      t.text :deliverable_name
      t.text :description

      t.timestamps
    end
  end
end
