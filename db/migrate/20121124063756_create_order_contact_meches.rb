class CreateOrderContactMeches < ActiveRecord::Migration
  def change
    create_table :order_contact_meches do |t|
      t.integer :order_id
      t.integer :contact_mech_purpose_id
      t.integer :contact_mech_id

      t.timestamps
    end
  end
end
