class CreateProductConfigOptionIactns < ActiveRecord::Migration
  def change
    create_table :product_config_option_iactns do |t|
      t.integer :config_item_id
      t.integer :config_option_seq_id
      t.integer :to_config_item_id
      t.integer :to_config_option_seq_id
      t.integer :iactn_type_id
      t.text :description

      t.timestamps
    end
  end
end
