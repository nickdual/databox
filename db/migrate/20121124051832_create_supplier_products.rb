class CreateSupplierProducts < ActiveRecord::Migration
  def change
    create_table :supplier_products do |t|
      t.integer :supplier_party_id
      t.integer :product_id
      t.text :supplier_product_name
      t.text :comments
      t.decimal :quantity_increment
      t.decimal :quantity_included
      t.decimal :quantity_uom_id
      t.integer :preferred_order_id
      t.integer :supplier_rating_type_id
      t.decimal :standard_lead_time_days
      t.boolean :can_drop_ship
      t.boolean :supplier_commission_percent

      t.timestamps
    end
  end
end
