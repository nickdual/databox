class CreateCommunicationEventContents < ActiveRecord::Migration
  def change
    create_table :communication_event_contents do |t|
      t.integer :communication_event_id
      t.string :content_location
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :assoc_type_id
      t.integer :sequence_num

      t.timestamps
    end
  end
end
