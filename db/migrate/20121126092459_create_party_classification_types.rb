class CreatePartyClassificationTypes < ActiveRecord::Migration
  def change
    create_table :party_classification_types do |t|
      t.text :description
      t.integer :parent_id
      t.string :name

      t.timestamps
    end
  end
end
