class CreateEmplPositionStatuses < ActiveRecord::Migration
  def change
    create_table :empl_position_statuses do |t|
      t.string :description
      t.string :status_id

      t.timestamps
    end
  end
end
