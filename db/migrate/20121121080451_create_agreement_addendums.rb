class CreateAgreementAddendums < ActiveRecord::Migration
  def change
    create_table :agreement_addendums do |t|
      t.integer :agreement_id
      t.integer :agreement_item_seq_id
      t.datetime :addendum_creation_date
      t.datetime :addendum_effective_date
      t.text :addendum_text

      t.timestamps
    end
  end
end
