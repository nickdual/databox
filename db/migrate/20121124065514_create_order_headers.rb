class CreateOrderHeaders < ActiveRecord::Migration
  def change
    create_table :order_headers do |t|
      t.integer :order_id
      t.text :order_name
      t.datetime :entry_date
      t.integer :status_id
      t.integer :currency_uom_id
      t.integer :billing_account_id
      t.integer :product_store_id
      t.integer :sales_channel_id
      t.integer :terminal_id
      t.integer :external_id
      t.integer :sync_status_id
      t.integer :visit_id
      t.integer :parent_order_id
      t.integer :recurrence_info_id
      t.datetime :last_ordered_date
      t.decimal :remaining_sub_total
      t.decimal :grand_total

      t.timestamps
    end
    add_index :order_headers, :external_id, :name => 'ORDEREXT_ID_IDX'
  end
end
