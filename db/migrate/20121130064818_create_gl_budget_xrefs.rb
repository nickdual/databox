class CreateGlBudgetXrefs < ActiveRecord::Migration
  def change
    create_table :gl_budget_xrefs do |t|
			t.integer :gl_account_id
    	t.integer :budget_item_type_enum_id
    	t.date :from_date
    	t.date :thru_date
    	t.float :allocation_percentage
    	
      t.timestamps
    end
  end
end
