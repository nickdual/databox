class CreateBudgetReviewResults < ActiveRecord::Migration
  def change
    create_table :budget_review_results do |t|

      t.timestamps
    end
  end
end
