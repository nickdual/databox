class CreateAssetMaintenanceStatuses < ActiveRecord::Migration
  def change
    create_table :asset_maintenance_statuses do |t|
      t.string :description
      t.string :name
      t.integer :sequence_num

      t.timestamps
    end
  end
end
