class CreateGlAccountCategoryTypes < ActiveRecord::Migration
  def change
    create_table :gl_account_category_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
