class CreatePaymentGatewayResponses < ActiveRecord::Migration
  def change
    create_table :payment_gateway_responses do |t|
      t.integer :payment_gateway_response_id
      t.integer :payment_service_type_enum_id
      t.integer :payment_method_id
      t.integer :trans_code_enum_id
      t.decimal :amount, :precision => 10, :scale => 2,:null => false
      t.integer :amount_uom_id
      t.text :reference_num
      t.text :alt_reference
      t.text :sub_reference
      t.text :gateway_code
      t.text :gateway_flag
      t.text :gateway_avs_result
      t.text :gateway_cv_result
      t.text :gateway_score_result
      t.text :gateway_message
      t.datetime :transaction_date
      t.boolean :result_declined
      t.boolean :result_nsf
      t.boolean :result_bad_expire
      t.boolean :result_bad_card_number

      t.timestamps
    end
  end
end
