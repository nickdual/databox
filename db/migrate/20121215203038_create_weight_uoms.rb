class CreateWeightUoms < ActiveRecord::Migration
  def change
    create_table :weight_uoms do |t|
      t.string :name
      t.string :unit

      t.timestamps
    end
  end
end
