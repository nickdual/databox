class CreateJobInterviewTypes < ActiveRecord::Migration
  def change
    create_table :job_interview_types do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
