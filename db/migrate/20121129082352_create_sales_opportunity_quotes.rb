class CreateSalesOpportunityQuotes < ActiveRecord::Migration
  def change
    create_table :sales_opportunity_quotes do |t|
      t.integer :sales_opportunity_id
      t.integer :order_id

      t.timestamps
    end
  end
end
