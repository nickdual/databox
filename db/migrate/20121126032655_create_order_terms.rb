class CreateOrderTerms < ActiveRecord::Migration
  def change
    create_table :order_terms do |t|
      t.integer :order_id
      t.integer :order_item_seq_id
      t.integer :settlement_term_id

      t.timestamps
    end
  end
end
