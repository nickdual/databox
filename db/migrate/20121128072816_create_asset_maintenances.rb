class CreateAssetMaintenances < ActiveRecord::Migration
  def change
    create_table :asset_maintenances do |t|
      t.integer :asset_id
      t.integer :status_id
      t.integer :maintenance_type_id
      t.integer :product_maintenance_id
      t.integer :task_work_effort_id
      t.decimal :interval_quantity
      t.integer :interval_uom_id
      t.integer :interval_meter_type_id
      t.integer :purchase_order_id

      t.timestamps
    end
  end
end
