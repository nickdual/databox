class CreatePartyIdTypes < ActiveRecord::Migration
  def change
    create_table :party_id_types do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
