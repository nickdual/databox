class CreateAgreementTerms < ActiveRecord::Migration
  def change
    create_table :agreement_terms do |t|
      t.integer :agreement_id
      t.integer :agreement_item_seq_id
      t.integer :settlement_term_id
      t.integer :term_type_id
      t.datetime :from_date
      t.datetime :thru_date

      t.timestamps
    end
  end
end
