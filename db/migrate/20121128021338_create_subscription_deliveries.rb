class CreateSubscriptionDeliveries < ActiveRecord::Migration
  def change
    create_table :subscription_deliveries do |t|
      t.integer :subscription_id
      t.datetime :date_sent
      t.integer :communication_event_id
      t.text :comments

      t.timestamps
    end
  end
end
