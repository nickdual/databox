class CreateSalesChannels < ActiveRecord::Migration
  def change
    create_table :sales_channels do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
