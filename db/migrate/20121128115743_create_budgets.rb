class CreateBudgets < ActiveRecord::Migration
	def change
		create_table :budgets do |t|
			t.enum :budget_type_enum_id	
			t.integer :time_period_id
			t.string :status_id
			t.text :comments
			
			t.timestamps
		end
	end
end
