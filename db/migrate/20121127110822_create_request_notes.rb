class CreateRequestNotes < ActiveRecord::Migration
  def change
    create_table :request_notes do |t|
      t.integer :request_id
      t.datetime :note_date
      t.integer :request_item_seq_id
      t.text :note_text

      t.timestamps
    end
  end
end
