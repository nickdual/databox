class CreateProductCategoryFeatGrpAppls < ActiveRecord::Migration
  def change
    create_table :product_category_feat_grp_appls do |t|
      t.integer :product_category_id
      t.integer :product_feature_group_id
      t.datetime :from_date
      t.datetime :thru_date

      t.timestamps
    end
  end
end
