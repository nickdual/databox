class CreateTimePeriods < ActiveRecord::Migration
  def change
    create_table :time_periods do |t|
      t.integer :parent_period_id
      t.string :time_period_type_id
      t.references :party
      t.integer :period_num
      t.text :period_name
      t.date :from_date
      t.date :thru_date
      t.boolean :is_closed

      t.timestamps
    end

    add_index :time_periods, :parent_period_id
    add_index :time_periods, :time_period_type_id
    add_index :time_periods, :party_id
  end
end
