class CreateProductContentTypes < ActiveRecord::Migration
  def change
    create_table :product_content_types do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
