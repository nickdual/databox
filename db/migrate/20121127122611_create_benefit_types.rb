class CreateBenefitTypes < ActiveRecord::Migration
  def change
    create_table :benefit_types do |t|
      t.integer :parent_id
      t.text :description
      t.string :name
      t.float :employer_paid_percentage

      t.timestamps
    end
  end
end
