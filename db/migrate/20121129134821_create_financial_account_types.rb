class CreateFinancialAccountTypes < ActiveRecord::Migration
  def change
    create_table :financial_account_types do |t|
      t.integer :fin_account_type_id
      t.integer :parent_type_id
      t.text :description
      t.boolean :is_refundable
      t.integer :account_code_length
      t.boolean :require_pin_code
      t.integer :pin_code_length
      t.integer :account_valid_days
      t.integer :auth_valid_days
      t.boolean :allow_auth_to_negative
      t.decimal :replenish_min_balance
      t.decimal :replenish_threshold
      t.integer :replenish_method_enum_id
      t.integer :replenish_type_enum_id

      t.timestamps
    end
  end
end
