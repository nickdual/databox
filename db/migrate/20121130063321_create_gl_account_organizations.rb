class CreateGlAccountOrganizations < ActiveRecord::Migration
  def change
    create_table :gl_account_organizations do |t|
    	t.integer :gl_account_id
    	t.integer :organization_party_id
    	t.date :from_date
    	t.date :thru_date
    	t.float :posted_balance
    	
      t.timestamps
    end
  end
end
