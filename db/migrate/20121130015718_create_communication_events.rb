class CreateCommunicationEvents < ActiveRecord::Migration
  def change
    create_table :communication_events do |t|
      t.integer :communication_event_type_id
      t.integer :contact_mech_type_id
      t.integer :status_id
      t.integer :original_comm_event_id
      t.integer :parent_comm_event_id
      t.integer :from_contact_mech_id
      t.integer :to_contact_mech_id
      t.integer :from_party_id
      t.integer :from_role_type_id
      t.integer :to_party_id
      t.integer :to_role_type_id
      t.datetime :entry_date
      t.datetime :datetime_started
      t.datetime :datetime_ended
      t.text :subject
      t.string :content_type
      t.text :content
      t.text :note
      t.integer :reason_id
      t.integer :contact_list_id
      t.integer :email_message_id

      t.timestamps
    end
  end
end
