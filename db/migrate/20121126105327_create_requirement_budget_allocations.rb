class CreateRequirementBudgetAllocations < ActiveRecord::Migration
  def change
    create_table :requirement_budget_allocations do |t|
      t.integer :requirment_id
      t.integer :budget_id
      t.integer :budget_item_seq_id
      t.decimal :amount, :precision => 10, :scale => 2, :null => false

      t.timestamps
    end
  end
end
