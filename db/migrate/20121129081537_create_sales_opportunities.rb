class CreateSalesOpportunities < ActiveRecord::Migration
  def change
    create_table :sales_opportunities do |t|
      t.integer :type_id
      t.string :opportunity_name
      t.text :description
      t.text :next_step
      t.decimal :estimated_amount
      t.decimal :estimated_probability
      t.integer :currency_uom_id
      t.integer :marketing_campaign_id
      t.integer :data_source_id
      t.datetime :estimated_close_date
      t.integer :opportunity_stage_id

      t.timestamps
    end
  end
end
