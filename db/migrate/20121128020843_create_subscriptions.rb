class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.integer :subscription_type_id
      t.integer :subscription_resource_id
      t.integer :subscriber_party_id
      t.integer :subscriber_party_id
      t.integer :deliver_to_contact_mech_id
      t.integer :order_id
      t.integer :order_item_seq_id
      t.integer :product_id
      t.integer :external_subscription_id
      t.text :description
      t.datetime :from_date
      t.datetime :thru_date
      t.datetime :purchase_from_date
      t.datetime :purchase_thru_date
      t.integer :available_time
      t.integer :available_time_uom_id
      t.integer :use_time
      t.integer :use_time_uom_id
      t.integer :use_count_limit

      t.timestamps
    end
  end
end
