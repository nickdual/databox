class CreateCommunicationEventWorkEffs < ActiveRecord::Migration
  def change
    create_table :communication_event_work_effs do |t|
      t.integer :work_effort_id
      t.integer :communication_event_id
      t.text :description

      t.timestamps
    end
  end
end
