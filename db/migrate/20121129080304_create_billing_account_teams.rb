class CreateBillingAccountTeams < ActiveRecord::Migration
  def change
    create_table :billing_account_teams do |t|
      t.integer :billing_account_term_id
      t.integer :billing_account_id
      t.integer :term_type_enum_id
      t.integer :term_value
      t.integer :term_uom_id

      t.timestamps
    end
  end
end
