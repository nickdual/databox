class CreatePromotionRules < ActiveRecord::Migration
  def change
    create_table :promotion_rules do |t|
      t.string :product_promotion_id
      t.string :name

      t.timestamps
    end
  end
end
