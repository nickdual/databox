class CreateAcctgTransEntryReconcileStatuses < ActiveRecord::Migration
  def change
    create_table :acctg_trans_entry_reconcile_statuses do |t|
      t.string :description
      t.integer :sequence_num
      t.string :status_id

      t.timestamps
    end
  end
end
