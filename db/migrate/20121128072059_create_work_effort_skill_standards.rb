class CreateWorkEffortSkillStandards < ActiveRecord::Migration
  def change
    create_table :work_effort_skill_standards do |t|
      t.integer :work_effort_id
      t.integer :skill_type_id
      t.float :estimated_num_people
      t.float :estimated_duration
      t.decimal :estimated_cost

      t.timestamps
    end
  end
end
