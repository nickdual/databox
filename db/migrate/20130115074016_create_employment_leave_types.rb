class CreateEmploymentLeaveTypes < ActiveRecord::Migration
  def change
    create_table :employment_leave_types do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
