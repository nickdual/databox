class CreateEmploymentLeaveReasons < ActiveRecord::Migration
  def change
    create_table :employment_leave_reasons do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
