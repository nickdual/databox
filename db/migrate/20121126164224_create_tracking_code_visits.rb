class CreateTrackingCodeVisits < ActiveRecord::Migration
  def change
    create_table :tracking_code_visits do |t|
      t.integer :tracking_code_id
      t.integer :visit_id
      t.datetime :from_date
      t.integer :source_enum_id

      t.timestamps
    end
  end
end
