class CreatePartyQualificationVerifications < ActiveRecord::Migration
  def change
    create_table :party_qualification_verifications do |t|
      t.string :description
      t.integer :sequence_num
      t.string :status_id

      t.timestamps
    end
  end
end
