class CreateProductPrices < ActiveRecord::Migration
  def change
    create_table :product_prices do |t|
      t.integer :product_id
      t.integer :product_store_id
      t.integer :vendor_party_id
      t.integer :customer_party_id
      t.integer :price_type_id
      t.integer :price_purpose_id
      t.datetime :from_date
      t.datetime :thru_date
      t.decimal :min_quantity
      t.decimal :price
      t.integer :price_uom_id
      t.integer :term_uom_id
      t.boolean :tax_in_price
      t.decimal :tax_amount
      t.decimal :tax_percentage
      t.integer :tax_auth_party_id
      t.integer :tax_auth_geo_id
      t.integer :agreement_id
      t.integer :agreement_item_seq_id

      t.timestamps
    end
  end
end
