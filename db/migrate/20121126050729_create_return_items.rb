class CreateReturnItems < ActiveRecord::Migration
  def change
    create_table :return_items do |t|
      t.integer :return_id
      t.integer :return_item_seq_id
      t.integer :return_reason_id
      t.integer :return_response_id
      t.integer :item_type_id
      t.integer :product_id
      t.text :description
      t.integer :order_id
      t.integer :order_item_seq_id
      t.integer :order_adjustment_id
      t.integer :status_id
      t.integer :expected_inventory_status_id
      t.decimal :return_quantity
      t.decimal :received_quantity
      t.decimal :return_price
      t.integer :replacement_order_id
      t.integer :original_payment_id
      t.integer :refund_payment_id
      t.integer :billing_account_id
      t.integer :fin_account_trans_id
      t.decimal :response_amount
      t.datetime :response_date

      t.timestamps
    end
    add_index :return_items, :order_item_seq_id, :name => 'RTN_ITM_BYORDITM'
  end  
end
