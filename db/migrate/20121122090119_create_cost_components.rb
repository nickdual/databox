class CreateCostComponents < ActiveRecord::Migration
  def change
    create_table :cost_components do |t|
      t.integer :cost_component_type_id
      t.integer :product_id
      t.integer :party_id
      t.integer :geo_id
      t.integer :work_effort_id
      t.integer :asset_id
      t.integer :cost_component_calc_id
      t.datetime :from_date
      t.datetime :thru_date
      t.decimal :cost
      t.integer :cost_uom_id

      t.timestamps
    end
  end
end
