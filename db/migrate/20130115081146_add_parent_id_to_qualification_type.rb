class AddParentIdToQualificationType < ActiveRecord::Migration
  def change
    add_column :qualification_types, :parent_id, :integer
  end
end
