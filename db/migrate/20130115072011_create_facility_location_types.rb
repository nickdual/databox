class CreateFacilityLocationTypes < ActiveRecord::Migration
  def change
    create_table :facility_location_types do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
