class CreateWorkEffortDeliverableProds < ActiveRecord::Migration
  def change
    create_table :work_effort_deliverable_prods do |t|
      t.integer :work_effort_id
      t.integer :deliverable_id

      t.timestamps
    end
  end
end
