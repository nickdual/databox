class CreatePerformanceReviewRatings < ActiveRecord::Migration
  def change
    create_table :performance_review_ratings do |t|
      t.string :name
      t.string :description
      t.integer :sequence_num

      t.timestamps
    end
  end
end
