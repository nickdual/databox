class CreateWorkEffortPartyAvailabilities < ActiveRecord::Migration
  def change
    create_table :work_effort_party_availabilities do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
