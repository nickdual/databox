class CreateOrderParts < ActiveRecord::Migration
  def change
    create_table :order_parts do |t|
      t.integer :order_id
      t.integer :order_part_seq_id
      t.integer :parent_part_seq_id
      t.text :part_name
      t.integer :vendor_party_id
      t.integer :customer_party_id
      t.integer :customer_po_id
      t.integer :vendor_agent_party_id
      t.integer :customer_agent_party_id
      t.integer :ship_to_party_id
      t.integer :facility_id
      t.integer :carrier_party_id
      t.integer :shipment_method_id
      t.integer :postal_contact_mech_id
      t.integer :telecom_contact_mech_id
      t.text :tracking_number
      t.text :shipping_instructions
      t.boolean :may_split
      t.text :gift_message
      t.boolean :is_gift
      t.datetime :ship_after_date
      t.datetime :ship_before_date
      t.datetime :estimated_ship_date
      t.datetime :estimated_delivery_date
      t.datetime :valid_from_date
      t.datetime :valid_thru_date
      t.datetime :auto_cancel_date
      t.datetime :dont_cancel_set_date
      t.integer :dont_cancel_set_user_id

      t.timestamps
    end
  end
end
