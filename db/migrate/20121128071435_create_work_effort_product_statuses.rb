class CreateWorkEffortProductStatuses < ActiveRecord::Migration
  def change
    create_table :work_effort_product_statuses do |t|
      t.string :description
      t.string :name
      t.integer :sequence_num

      t.timestamps
    end
  end
end
