class CreateProductFeatureGroups < ActiveRecord::Migration
  def change
    create_table :product_feature_groups do |t|
      t.text :description

      t.timestamps
    end
  end
end
