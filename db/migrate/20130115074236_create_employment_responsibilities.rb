class CreateEmploymentResponsibilities < ActiveRecord::Migration
  def change
    create_table :employment_responsibilities do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
