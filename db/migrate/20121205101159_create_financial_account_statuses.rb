class CreateFinancialAccountStatuses < ActiveRecord::Migration
  def change
    create_table :financial_account_statuses do |t|
      t.string :description
      t.integer :sequence_num
      t.integer :status_id

      t.timestamps
    end
  end
end
