class CreateShipmentItemPickStatuses < ActiveRecord::Migration
  def change
    create_table :shipment_item_pick_statuses do |t|
      t.string :description
      t.integer :sequence_num
      t.string :name

      t.timestamps
    end
  end
end
