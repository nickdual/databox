class CreateContactPaymentTrustLevels < ActiveRecord::Migration
  def change
    create_table :contact_payment_trust_levels do |t|
      t.string :name
      t.string :description
      t.integer :sequence_num

      t.timestamps
    end
  end
end
