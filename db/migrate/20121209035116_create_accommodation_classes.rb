class CreateAccommodationClasses < ActiveRecord::Migration
  def change
    create_table :accommodation_classes do |t|

      t.timestamps
    end
  end
end
