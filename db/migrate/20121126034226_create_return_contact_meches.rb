class CreateReturnContactMeches < ActiveRecord::Migration
  def change
    create_table :return_contact_meches do |t|
      t.integer :return_id
      t.integer :contact_mech_purpose_id
      t.integer :contact_mech_id

      t.timestamps
    end
  end
end
