class AddReturnHeaderTypeIdToReturnHeader < ActiveRecord::Migration
  def change
    add_column :return_headers, :return_header_type_id, :integer
  end
end
