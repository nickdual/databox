class CreateInvoiceContactMeches < ActiveRecord::Migration
  def change
    create_table :invoice_contact_meches do |t|
      t.integer :invoice_id
      t.integer :contact_mech_purpose_enum_id
      t.integer :contact_mech_id

      t.timestamps
    end
  end
end
