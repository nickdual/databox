class CreateContactMeches < ActiveRecord::Migration
  def change
    create_table :contact_meches do |t|
      t.integer :contact_mech_type_id
      t.text :info_string
      t.integer :trust_level_id
      t.integer :payment_fraud_evidence_id

      t.timestamps
    end
    add_index :contact_meches, :info_string
  end
end
