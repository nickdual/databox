class CreatePhysicalInventories < ActiveRecord::Migration
  def change
    create_table :physical_inventories do |t|
      t.datetime :physical_inventory_date
      t.integer :party_id
      t.text :general_comments

      t.timestamps
    end
  end
end
