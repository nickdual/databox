class CreatePaymentFraudEvidences < ActiveRecord::Migration
  def change
    create_table :payment_fraud_evidences do |t|
      t.integer :payment_fraud_evidence
      t.integer :fraud_type_enum_id
      t.text :comments
      t.integer :payment_id
      t.integer :order_id
      t.integer :party_id
      t.integer :visit_id

      t.timestamps
    end
  end
end
