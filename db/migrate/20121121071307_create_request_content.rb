class CreateRequestContent < ActiveRecord::Migration
  def create
	  create_table "request_content", :force => true do |t|
	    t.integer   "request_id"
	    t.text   "content_location"
	    t.datetime   "from_date"
	    t.datetime   "thru_date"
	    t.datetime "created_at",                    :null => false
	    t.datetime "updated_at",                    :null => false
	  end
  end
end
