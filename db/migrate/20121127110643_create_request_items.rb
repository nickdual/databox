class CreateRequestItems < ActiveRecord::Migration
  def change
    create_table :request_items do |t|
      t.integer :request_id
      t.integer :request_item_seq_id
      t.integer :status_id
      t.integer :request_resolution_enum_id
      t.integer :priority
      t.integer :sequence_num
      t.datetime :required_by_date
      t.integer :product_id
      t.integer :quantity
      t.integer :selected_amount
      t.decimal :maximum_amount
      t.text :description
      t.text :story

      t.timestamps
    end
  end
end
