class CreatePaymentMethods < ActiveRecord::Migration
  def change
    create_table :payment_methods do |t|
      t.integer :payment_method_id
      t.integer :payment_method_type_enum_id
      t.integer :owner_party_id
      t.text :description
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :postal_contact_mech_id
      t.integer :telecom_contact_mech_id
      t.integer :trust_level_enum_id
      t.integer :payment_fraud_evidence_id
      t.integer :gl_account_id
      t.integer :fin_account_id

      t.timestamps
    end
  end
end
