class CreateEmploymentApplicationReferredBies < ActiveRecord::Migration
  def change
    create_table :employment_application_referred_bies do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
