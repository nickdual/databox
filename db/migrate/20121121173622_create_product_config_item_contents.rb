class CreateProductConfigItemContents < ActiveRecord::Migration
  def change
    create_table :product_config_item_contents do |t|
      t.integer :config_item_id
      t.text :content_location
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :item_content_type_id

      t.timestamps
    end
  end
end
