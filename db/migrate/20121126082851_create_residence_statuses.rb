class CreateResidenceStatuses < ActiveRecord::Migration
  def change
    create_table :residence_statuses do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
