class CreateShipments < ActiveRecord::Migration
  def change
    create_table :shipments do |t|
      t.integer :shipment_type_id
      t.integer :status_id
      t.integer :picklist_bin_id
      t.integer :primary_order_id
      t.integer :primary_order_part_seq_id
      t.integer :primary_return_id
      t.datetime :estimated_ready_date
      t.datetime :estimated_ship_date
      t.integer :estimated_ship_work_eff_id
      t.datetime :estimated_arrival_date
      t.integer :estimated_arrival_work_eff_id
      t.datetime :latest_cancel_date
      t.decimal :estimated_ship_cost
      t.integer :cost_uom_id
      t.text :handling_instructions
      t.integer :origin_facility_id
      t.integer :origin_contact_mech_id
      t.integer :origin_telecom_number_id
      t.integer :destination_facility_id
      t.integer :destination_contact_mech_id
      t.integer :destination_telecom_number_id
      t.integer :to_party_id
      t.integer :from_party_id
      t.decimal :additional_shipping_charge
      t.text :addtl_shipping_charge_desc

      t.timestamps
    end
  end
end
