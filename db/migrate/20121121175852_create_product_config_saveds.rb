class CreateProductConfigSaveds < ActiveRecord::Migration
  def change
    create_table :product_config_saveds do |t|
      t.integer :configurable_product_id

      t.timestamps
    end
  end
end
