class CreateDeliveries < ActiveRecord::Migration
  def change
    create_table :deliveries do |t|
      t.integer :origin_facility_id
      t.integer :dest_facility_id
      t.datetime :actual_start_date
      t.datetime :actual_arrival_date
      t.datetime :estimated_start_date
      t.datetime :estimated_arrival_date
      t.integer :asset_id
      t.decimal :start_mileage
      t.decimal :end_mileage
      t.decimal :fuel_used

      t.timestamps
    end
  end
end
