class CreateTimeEntries < ActiveRecord::Migration
  def change
    create_table :time_entries do |t|
      t.integer :timesheet_id
      t.integer :party_id
      t.integer :rate_type_id
      t.string :from_date_datetime
      t.datetime :thru_date
      t.decimal :hours
      t.string :comments
      t.integer :work_effort_id
      t.integer :invoice_id
      t.integer :invoice_item_seq_id

      t.timestamps
    end
  end
end
