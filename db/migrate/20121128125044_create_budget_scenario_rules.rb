class CreateBudgetScenarioRules < ActiveRecord::Migration
  def change
    create_table :budget_scenario_rules do |t|
    	t.integer :budget_scenario_id
    	t.enum :budget_item_type_enum_id
    	t.float :amount_change
    	t.float :percentage_change

      t.timestamps
    end
  end
end
