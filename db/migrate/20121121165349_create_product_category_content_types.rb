class CreateProductCategoryContentTypes < ActiveRecord::Migration
  def change
    create_table :product_category_content_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
