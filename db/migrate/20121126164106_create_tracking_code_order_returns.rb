class CreateTrackingCodeOrderReturns < ActiveRecord::Migration
  def change
    create_table :tracking_code_order_returns do |t|
      t.integer :tracking_code_type_enum_id
      t.integer :return_id
      t.integer :order_id
      t.integer :order_item_seq_id
      t.integer :tracking_code_id
      t.boolean :is_billable
      t.text :side_id
      t.boolean :has_exported
      t.boolean :affiliate_referred_time_stamp

      t.timestamps
    end
  end
end
