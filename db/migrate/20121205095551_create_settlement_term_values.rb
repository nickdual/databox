class CreateSettlementTermValues < ActiveRecord::Migration
  def change
    create_table :settlement_term_values do |t|
      t.string :term_id
      t.string :description
      t.string :term_value
      t.string :term_value_uom_id

      t.timestamps
    end
  end
end
