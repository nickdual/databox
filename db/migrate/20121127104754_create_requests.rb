class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.integer :request_id
      t.integer :request_type_enum_id
      t.integer :request_category_id
      t.integer :status_id
      t.text :description
      t.integer :from_party_id
      t.integer :priority
      t.datetime :request_date
      t.datetime :response_required_date
      t.integer :product_store_id
      t.integer :sales_chaneel_enum_id
      t.integer :fullfill_contact_mech_id
      t.integer :maximun_amount_uom_id
      t.integer :currency_uom_id

      t.timestamps
    end
  end
end
