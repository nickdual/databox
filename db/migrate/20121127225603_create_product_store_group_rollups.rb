class CreateProductStoreGroupRollups < ActiveRecord::Migration
  def change
    create_table :product_store_group_rollups do |t|
      t.integer :product_store_group_id
      t.integer :parent_group_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :sequence_num

      t.timestamps
    end
  end
end
