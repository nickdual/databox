class CreateAssetIdentifications < ActiveRecord::Migration
  def change
    create_table :asset_identifications do |t|
      t.integer :asset_id
      t.integer :identification_type_id
      t.string :id_value

      t.timestamps
    end
  end
end
