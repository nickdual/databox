class AddIndexForOrderItems < ActiveRecord::Migration
  def up
    add_index :order_items, [:order_id,:order_item_seq_id], :unique => true
  end

  def down
  end
end
