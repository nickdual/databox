class CreateProductGeoPurposes < ActiveRecord::Migration
  def change
    create_table :product_geo_purposes do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
