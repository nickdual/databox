class CreateTrackCodes < ActiveRecord::Migration
  def change
    create_table :track_codes do |t|
      t.integer :tracking_code_id
      t.integer :tracking_code_type_enum_id
      t.integer :marketing_campaign_id
      t.text :redirect_url
      t.text :comments
      t.text :description
      t.integer :trackable_lifetime
      t.integer :billable_lifetime
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :group_id
      t.integer :subgroup_id

      t.timestamps
    end
  end
end
