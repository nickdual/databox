class CreateCostComponentTypes < ActiveRecord::Migration
  def change
    create_table :cost_component_types do |t|
      t.string :name
      t.string :description
      t.integer :parent_id

      t.timestamps
    end
  end
end
