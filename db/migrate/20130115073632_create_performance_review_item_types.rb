class CreatePerformanceReviewItemTypes < ActiveRecord::Migration
  def change
    create_table :performance_review_item_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
