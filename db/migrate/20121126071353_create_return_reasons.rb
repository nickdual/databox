class CreateReturnReasons < ActiveRecord::Migration
  def change
    create_table :return_reasons do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
