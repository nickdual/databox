class CreateContactMechanismTypes < ActiveRecord::Migration
  def change
    create_table :contact_mechanism_types do |t|
      t.string :name
      t.string :description
      t.integer :parent_id
      t.timestamps
    end
  end
end
