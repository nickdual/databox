class CreateSupplierPreferredOrders < ActiveRecord::Migration
  def change
    create_table :supplier_preferred_orders do |t|
      t.integer :sequence_num
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
