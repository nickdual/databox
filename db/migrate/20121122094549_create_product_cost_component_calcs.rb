class CreateProductCostComponentCalcs < ActiveRecord::Migration
  def change
    create_table :product_cost_component_calcs do |t|
      t.integer :product_id
      t.integer :cost_component_type_id
      t.datetime :from_date
      t.integer :cost_component_calc_id
      t.integer :sequence_num
      t.datetime :thru_date

      t.timestamps
    end
  end
end
