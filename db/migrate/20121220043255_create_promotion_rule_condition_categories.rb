class CreatePromotionRuleConditionCategories < ActiveRecord::Migration
  def change
    create_table :promotion_rule_condition_categories do |t|
      t.string :product_category_id
      t.enum :sub_category_status
      t.integer :group_id
      t.integer :promotion_rule_condition_id

      t.timestamps
    end
  end
end
