class CreateProductFacility < ActiveRecord::Migration
  def create
	  create_table "product_facility", :force => true do |t|
	    t.integer   "product_id"
	    t.integer   "facility_id"
	    t.integer   "minimum_stock"
	    t.integer   "reorder_quantity"
	    t.integer   "days_to_ship"
	    t.datetime "created_at",                    :null => false
	    t.datetime "updated_at",                    :null => false
	  end
  end
end
