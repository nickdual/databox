class CreateAssetStandardCosts < ActiveRecord::Migration
  def change
    create_table :asset_standard_costs do |t|
      t.integer :asset_id
      t.integer :asset_standard_cost_type_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :amount_uom_id
      t.decimal :amount

      t.timestamps
    end
  end
end
