class CreateTimePeriodTypes < ActiveRecord::Migration
  def change
    create_table :time_period_types do |t|
      t.string :time_period_type_id
      t.text :description
      t.integer :period_length
      t.string :length_uom_id

      t.timestamps
    end

    add_index :time_period_types, :time_period_type_id
    add_index :time_period_types, :length_uom_id
  end
end
