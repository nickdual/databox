class CreateProductMeterTypes < ActiveRecord::Migration
  def change
    create_table :product_meter_types do |t|
      t.string :description
      t.string :name
      t.integer :default_uom_id
      t.timestamps
    end
  end
end
