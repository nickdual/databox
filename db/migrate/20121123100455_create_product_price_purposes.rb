class CreateProductPricePurposes < ActiveRecord::Migration
  def change
    create_table :product_price_purposes do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
