class CreateProductDimensions < ActiveRecord::Migration
  def change
    create_table :product_dimensions do |t|
      t.integer :product_id
      t.integer :dimension_type_id
      t.integer :value
      t.integer :value_uom_id

      t.timestamps
    end
  end
end
