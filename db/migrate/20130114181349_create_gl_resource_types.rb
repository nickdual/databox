class CreateGlResourceTypes < ActiveRecord::Migration
  def change
    create_table :gl_resource_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
