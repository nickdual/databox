class CreateProductCategoryParties < ActiveRecord::Migration
  def change
    create_table :product_category_parties do |t|
      t.integer :product_category_id
      t.integer :party_id
      t.integer :role_type_id
      t.datetime :from_date
      t.datetime :thru_date
      t.text :comments

      t.timestamps
    end
  end
end
