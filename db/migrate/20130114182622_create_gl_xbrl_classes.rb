class CreateGlXbrlClasses < ActiveRecord::Migration
  def change
    create_table :gl_xbrl_classes do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
