class CreateProductGeo < ActiveRecord::Migration
  def create
	  create_table "product_geo", :force => true do |t|
	    t.integer   "product_id"
	    t.integer   "geo_id"
	    t.integer   "product_geo_purpose_id"
	    t.text   "description"
	    t.datetime "created_at",                    :null => false
	    t.datetime "updated_at",                    :null => false
	  end
  end
end
