class CreateRequestCategories < ActiveRecord::Migration
  def change
    create_table :request_categories do |t|
      t.integer :request_category_id
      t.integer :parent_category_id
      t.integer :responsible_party_id
      t.text :description

      t.timestamps
    end
  end
end
