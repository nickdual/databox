class CreateOrderRequirementCommitments < ActiveRecord::Migration
  def change
    create_table :order_requirement_commitments do |t|
      t.integer :order_id
      t.integer :order_item_seq_id
      t.integer :requirement_id
      t.decimal :quantity

      t.timestamps
    end
  end
end
