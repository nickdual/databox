class CreateMarketInterests < ActiveRecord::Migration
  def change
    create_table :market_interests do |t|
      t.integer :product_category_id
      t.integer :market_segment_id
      t.datetime :from_date
      t.datetime :thru_date

      t.timestamps
    end
  end
end
