class CreateProductStoreCategories < ActiveRecord::Migration
  def change
    create_table :product_store_categories do |t|
      t.integer :product_store_id
      t.integer :product_category_id
      t.integer :store_category_type_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :sequence_num

      t.timestamps
    end
  end
end
