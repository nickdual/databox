class CreateMarketSegmentTypes < ActiveRecord::Migration
  def change
    create_table :market_segment_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
