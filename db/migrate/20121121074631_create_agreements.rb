class CreateAgreements < ActiveRecord::Migration
  def change
    create_table :agreements do |t|
      t.integer :agreement_type_id
      t.datetime :agreement_date
      t.integer :from_party_id
      t.integer :from_role_type_id
      t.integer :to_party_id
      t.integer :to_role_type_id
      t.datetime :from_date
      t.datetime :thru_date
      t.text :description
      t.text :text_data

      t.timestamps
    end
  end
end
