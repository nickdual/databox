class CreateInvoiceTypes < ActiveRecord::Migration
  def change
    create_table :invoice_types do |t|
      t.string :name
      t.string :parent_name
      t.string :description

      t.timestamps
    end
  end
end
