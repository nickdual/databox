class CreateProductFeatureIactns < ActiveRecord::Migration
  def change
    create_table :product_feature_iactns do |t|
      t.integer :to_product_feature_id
      t.integer :from_product_feature_id
      t.integer :iactn_type_id
      t.integer :product_id

      t.timestamps
    end
  end
end
