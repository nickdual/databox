class CreateOrderItemBillings < ActiveRecord::Migration
  def change
    create_table :order_item_billings do |t|
      t.integer :order_id
      t.integer :order_item_seq_id
      t.integer :invoice_id
      t.integer :invoice_item_seq_id
      t.integer :item_issuance_id
      t.integer :shipment_receipt_id
      t.decimal :quantity
      t.decimal :amount

      t.timestamps
    end
  end
end
