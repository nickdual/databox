class CreateAssetReservationOrders < ActiveRecord::Migration
  def change
    create_table :asset_reservation_orders do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
