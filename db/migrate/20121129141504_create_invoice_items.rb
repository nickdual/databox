class CreateInvoiceItems < ActiveRecord::Migration
  def change
    create_table :invoice_items do |t|
      t.integer :invoice_id
      t.integer :invoice_item_seq_id
      t.integer :item_type_enum_id
      t.integer :override_gl_account_id
      t.integer :override_org_party_id
      t.integer :asset_id
      t.integer :product_id
      t.integer :parent_invoice_id
      t.integer :parent_invoice_item_seq_id
      t.integer :uom_id
      t.boolean :taxable_flag
      t.decimal :quantity
      t.decimal :amount, :precision => 10, :scale => 2,:null => false
      t.text :description
      t.integer :tax_auth_party_id
      t.integer :tax_auth_geo_id
      t.integer :tax_authority_rate_id
      t.integer :sales_opportunity_id

      t.timestamps
    end
  end
end
