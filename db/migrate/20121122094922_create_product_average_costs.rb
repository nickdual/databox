class CreateProductAverageCosts < ActiveRecord::Migration
  def change
    create_table :product_average_costs do |t|
      t.integer :average_cost_type_id
      t.integer :organization_party_id
      t.integer :product_id
      t.integer :facility_id
      t.datetime :from_date
      t.datetime :thru_date
      t.decimal :average_cost

      t.timestamps
    end
  end
end
