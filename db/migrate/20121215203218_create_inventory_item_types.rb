class CreateInventoryItemTypes < ActiveRecord::Migration
  def change
    create_table :inventory_item_types do |t|
      t.string :name

      t.timestamps
    end
  end
end
