class CreateProductConfigItemContentTypes < ActiveRecord::Migration
  def change
    create_table :product_config_item_content_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
