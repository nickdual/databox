class CreateWorkEffortPartyExpectations < ActiveRecord::Migration
  def change
    create_table :work_effort_party_expectations do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
