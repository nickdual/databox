class CreatePartyCarrierAccounts < ActiveRecord::Migration
  def change
    create_table :party_carrier_accounts do |t|
      t.integer :party_id
      t.integer :carrier_party_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :account_number

      t.timestamps
    end
  end
end
