class CreateFacilitySizeUoms < ActiveRecord::Migration
  def change
    create_table :facility_size_uoms do |t|
      t.string :name
      t.string :unit

      t.timestamps
    end
  end
end
