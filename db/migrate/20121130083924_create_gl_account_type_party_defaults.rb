class CreateGlAccountTypePartyDefaults < ActiveRecord::Migration
  def change
    create_table :gl_account_type_party_defaults do |t|
    	t.integer :organization_party_id
    	t.integer :party_id
    	t.integer :role_type_id    	
    	t.enum :gl_account_type_enum_id
    	t.integer :gl_account_id

      t.timestamps
    end
  end
end
