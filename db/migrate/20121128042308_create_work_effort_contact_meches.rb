class CreateWorkEffortContactMeches < ActiveRecord::Migration
  def change
    create_table :work_effort_contact_meches do |t|
      t.integer :work_effort_id
      t.integer :contact_mech_id
      t.integer :contact_mech_purpose_id
      t.datetime :from_date
      t.datetime :thru_date
      t.string :extension
      t.text :comments

      t.timestamps
    end
  end
end
