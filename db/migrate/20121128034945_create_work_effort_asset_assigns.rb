class CreateWorkEffortAssetAssigns < ActiveRecord::Migration
  def change
    create_table :work_effort_asset_assigns do |t|
      t.integer :work_effort_id
      t.integer :asset_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :status_id
      t.decimal :allocated_cost
      t.text :comments

      t.timestamps
    end
  end
end
