class CreatePaymentStatusValids < ActiveRecord::Migration
  def change
    create_table :payment_status_valids do |t|
      t.string :status_id
      t.string :to_status_id
      t.integer :transition_name

      t.timestamps
    end
  end
end
