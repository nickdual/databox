class CreateProductGlAccounts < ActiveRecord::Migration
  def change
    create_table :product_gl_accounts do |t|
    	t.integer :product_id
    	t.integer :organization_party_id
    	t.enum :gl_account_type_enum_id
    	t.integer :gl_account_id

      t.timestamps
    end
  end
end
