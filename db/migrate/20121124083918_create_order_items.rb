class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.integer :order_id
      t.integer :order_item_seq_id
      t.integer :order_part_seq_id
      t.integer :item_type_id
      t.integer :status_id
      t.integer :product_id
      t.integer :product_config_saved_id
      t.text :item_description
      t.text :comments
      t.decimal :quantity
      t.decimal :cancel_quantity
      t.decimal :selected_amount
      t.decimal :unit_price
      t.decimal :unitList_price
      t.boolean :is_modified_price
      t.integer :external_item_id
      t.integer :from_asset_id
      t.integer :budget_id
      t.integer :budget_item_seq_id
      t.integer :supplier_product_id
      t.integer :product_category_id
      t.boolean :is_promo
      t.integer :shopping_list_id
      t.integer :shopping_list_item_seq_id
      t.integer :subscription_id
      t.integer :override_gl_account_id
      t.integer :sales_opportunity_id

      t.timestamps
    end
    add_index :order_items, :external_item_id, :name => 'ORDITMEXT_ID_IDX'
  end
end
