class CreateTrackingCodeTypes < ActiveRecord::Migration
  def change
    create_table :tracking_code_types do |t|
      t.integer :id
      t.string :status_id
      t.string :description

      t.timestamps
    end
  end
end
