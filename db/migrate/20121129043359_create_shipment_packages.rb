class CreateShipmentPackages < ActiveRecord::Migration
  def change
    create_table :shipment_packages do |t|
      t.integer :shipment_id
      t.integer :shipment_package_seq_id
      t.integer :shipment_box_type_id
      t.datetime :date_created
      t.decimal :box_length
      t.decimal :box_height
      t.decimal :box_width
      t.integer :dimension_uom_id
      t.decimal :weight
      t.integer :weight_uom_id
      t.decimal :insured_value

      t.timestamps
    end
  end
end
