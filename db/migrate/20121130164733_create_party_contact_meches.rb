class CreatePartyContactMeches < ActiveRecord::Migration
  def change
    create_table :party_contact_meches do |t|
      t.integer :party_id
      t.integer :contact_mech_id
      t.integer :contact_mech_purpose_id
      t.datetime :from_date
      t.datetime :thru_date
      t.string :extension
      t.text :comments
      t.boolean :allow_solicitation
      t.date :used_since
      t.string :verify_code

      t.timestamps
    end
  end
end
