class CreateRequestItemWorkEfforts < ActiveRecord::Migration
  def change
    create_table :request_item_work_efforts do |t|
      t.integer :request_id
      t.integer :request_item_seq_id
      t.integer :work_effort_id

      t.timestamps
    end
  end
end
