class CreateGlJournals < ActiveRecord::Migration
  def change
    create_table :gl_journals do |t|
    	t.string :gl_journal_name
    	t.integer :organization_party_id
    	t.boolean :is_posted
    	t.date :posted_date

      t.timestamps
    end
  end
end
