class CreateAssetMaintenanceStatusValids < ActiveRecord::Migration
  def change
    create_table :asset_maintenance_status_valids do |t|
      t.integer :status_id
      t.integer :to_status_id
      t.string :transition_name

      t.timestamps
    end
  end
end
