class CreateFinancialAccountReplenishTypes < ActiveRecord::Migration
  def change
    create_table :financial_account_replenish_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
