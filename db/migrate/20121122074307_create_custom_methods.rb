class CreateCustomMethods < ActiveRecord::Migration
  def change
    create_table :custom_methods do |t|
      t.string :custom_method_id
      t.integer :custom_method_type_id
      t.string :custom_method_name
      t.string :description

      t.timestamps
    end
  end
end
