class CreateWorkEffortAssetUseds < ActiveRecord::Migration
  def change
    create_table :work_effort_asset_useds do |t|
      t.integer :work_effort_id
      t.integer :asset_id
      t.integer :status_id
      t.float :quantity

      t.timestamps
    end
  end
end
