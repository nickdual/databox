class CreateProductCategories < ActiveRecord::Migration
  def change
    create_table :product_categories do |t|
      t.integer :product_category_type_id
      t.text :category_name
      t.text :description

      t.timestamps
    end
  end
end
