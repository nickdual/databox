class CreatePaymentStatuses < ActiveRecord::Migration
  def change
    create_table :payment_statuses do |t|
      t.string :status_id
      t.string :description
      t.integer :sequence_num

      t.timestamps
    end
  end
end
