class CreatePromotionProducts < ActiveRecord::Migration
  def change
    create_table :promotion_products do |t|
      t.integer :product_id
      t.string :product_promotion_id
      t.enum :sub_category_status

      t.timestamps
    end
  end
end
