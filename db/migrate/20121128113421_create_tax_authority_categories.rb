class CreateTaxAuthorityCategories < ActiveRecord::Migration
  def change
    create_table :tax_authority_categories do |t|
    	t.integer :tax_auth_geo_id
    	t.integer :tax_auth_party_id
    	t.integer :product_category_id

      t.timestamps
    end
  end
end
