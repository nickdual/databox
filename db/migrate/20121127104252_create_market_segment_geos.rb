class CreateMarketSegmentGeos < ActiveRecord::Migration
  def change
    create_table :market_segment_geos do |t|
      t.integer :market_segment_id
      t.integer :geo_id

      t.timestamps
    end
  end
end
