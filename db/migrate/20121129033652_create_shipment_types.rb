class CreateShipmentTypes < ActiveRecord::Migration
  def change
    create_table :shipment_types do |t|
      t.string :description
      t.string :name
      t.integer :parent_id

      t.timestamps
    end
  end
end
