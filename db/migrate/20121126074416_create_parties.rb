class CreateParties < ActiveRecord::Migration
  def change
    create_table :parties do |t|
      t.integer :party_type_id
      t.boolean :disabled
      t.integer :external_id
      t.integer :data_source_id

      t.timestamps
    end
    add_index :parties, :external_id
  end
end
