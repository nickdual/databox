class CreateProductCategoryRollups < ActiveRecord::Migration
  def change
    create_table :product_category_rollups do |t|
      t.integer :product_category_id
      t.integer :parent_product_category_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :sequence_num

      t.timestamps
    end
    add_index :product_category_rollups, :parent_product_category_id, :name => 'PRDCR_PARPC'
  end
end
