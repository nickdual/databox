class CreateGlAccounts < ActiveRecord::Migration
	def change
		create_table :gl_accounts do |t|
			t.integer :parent_gl_account_id
			t.enum :gl_account_type_enum_id
			t.enum :gl_account_class_enum_id
			t.enum :gl_resource_type_enum_id
			t.enum :gl_xbrl_class_enum_id
			t.string :account_code
			t.string :account_name
			t.string :description
			t.integer :product_id
			t.integer :external_id
			t.float :posted_balance

			t.timestamps
		end

		add_index :gl_accounts, :account_code
	end
end
