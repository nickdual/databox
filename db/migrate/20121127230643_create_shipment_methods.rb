class CreateShipmentMethods < ActiveRecord::Migration
  def change
    create_table :shipment_methods do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
