class CreatePromotionRuleConditions < ActiveRecord::Migration
  def change
    create_table :promotion_rule_conditions do |t|
      t.integer :promotion_rule_id
      t.enum :promotion_rule_condition_name
      t.integer :operator_id
      t.integer :value
      t.integer :other
      t.integer :shipment_method_id
      t.timestamps
    end
  end
end
