class CreateRequestCommEvent < ActiveRecord::Migration
  def create
	  create_table "request_comm_event", :force => true do |t|
	    t.integer   "request_id"
	    t.integer   "communication_event_id"
	    t.datetime "created_at",                    :null => false
	    t.datetime "updated_at",                    :null => false
	  end
	end
end
