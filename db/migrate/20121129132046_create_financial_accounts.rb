class CreateFinancialAccounts < ActiveRecord::Migration
  def change
    create_table :financial_accounts do |t|
      t.integer :fin_account_id
      t.integer :fin_account_type_id
      t.integer :status_id
      t.text :fin_account_name
      t.text :fin_account_code
      t.text :fin_account_pin
      t.integer :organization_party_id
      t.integer :owner_party_id
      t.integer :post_to_gl_account_id
      t.datetime :from_date
      t.datetime :thru_date
      t.boolean :is_refundable
      t.integer :balance_uom_id
      t.decimal :actual_balance, :precision => 10, :scale => 2, :null => false
      t.decimal :availabe_balance, :precision => 10, :scale => 2, :null => false
      t.integer :replenish_payment_id
      t.decimal :replenish_level

      t.timestamps
    end
  end
end
