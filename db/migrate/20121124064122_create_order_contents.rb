class CreateOrderContents < ActiveRecord::Migration
  def change
    create_table :order_contents do |t|
      t.integer :order_content_type_id
      t.integer :order_id
      t.integer :order_item_seq_id
      t.text :content_location
      t.datetime :from_date
      t.datetime :thru_date

      t.timestamps
    end
  end
end
