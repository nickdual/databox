class CreateAgreementItemGeos < ActiveRecord::Migration
  def change
    create_table :agreement_item_geos do |t|
      t.integer :agreement_id
      t.integer :agreement_item_seq_id
      t.integer :geo_id

      t.timestamps
    end
  end
end
