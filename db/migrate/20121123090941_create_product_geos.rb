class CreateProductGeos < ActiveRecord::Migration
  def change
    create_table :product_geos do |t|
      t.integer :product_id
      t.integer :geo_id
      t.integer :product_geo_purpose_id
      t.string :description

      t.timestamps
    end
  end
end
