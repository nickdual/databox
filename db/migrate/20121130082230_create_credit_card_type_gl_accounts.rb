class CreateCreditCardTypeGlAccounts < ActiveRecord::Migration
  def change
    create_table :credit_card_type_gl_accounts do |t|
    	t.enum :credit_card_type_enum_id
    	t.integer :organization_party_id
    	t.integer :gl_account_id

      t.timestamps
    end
  end
end
