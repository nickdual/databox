class CreateFinancialAccountTransReasonTypes < ActiveRecord::Migration
  def change
    create_table :financial_account_trans_reason_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
