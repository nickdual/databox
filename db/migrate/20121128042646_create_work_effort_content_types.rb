class CreateWorkEffortContentTypes < ActiveRecord::Migration
  def change
    create_table :work_effort_content_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
