class CreateSalesOpportunityStages < ActiveRecord::Migration
  def change
    create_table :sales_opportunity_stages do |t|
      t.integer :opportunity_stage_id
      t.text :description
      t.decimal :default_probability
      t.integer :sequence_num

      t.timestamps
    end
  end
end
