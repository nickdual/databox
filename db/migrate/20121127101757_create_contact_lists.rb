class CreateContactLists < ActiveRecord::Migration
  def change
    create_table :contact_lists do |t|
      t.integer :contact_list_id
      t.integer :contact_list_type_enum_id
      t.integer :contact_mesh_type_enum_id
      t.integer :marketing_campaign_id
      t.text :contact_list_name
      t.text :description
      t.text :comments
      t.boolean :is_public
      t.boolean :single_use
      t.integer :owner_party_id
      t.text :verfity_email_from
      t.text :verify_email_screen
      t.text :verify_email_subject
      t.text :verify_email_website_id
      t.text :opt_out_screen

      t.timestamps
    end
  end
end
