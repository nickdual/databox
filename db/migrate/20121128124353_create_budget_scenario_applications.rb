class CreateBudgetScenarioApplications < ActiveRecord::Migration
  def change
    create_table :budget_scenario_applications do |t|
    	t.integer :budget_scenario_id
    	t.integer :budget_id
    	t.integer :budget_item_seq_id
    	t.integer :amount_change
    	t.float :percentage_change
    	
      t.timestamps
    end
  end
end
