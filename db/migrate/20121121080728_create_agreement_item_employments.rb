class CreateAgreementItemEmployments < ActiveRecord::Migration
  def change
    create_table :agreement_item_employments do |t|
      t.integer :agreement_id
      t.integer :agreement_item_seq_id
      t.integer :employment_id

      t.timestamps
    end
  end
end
