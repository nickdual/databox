class CreateInvoiceStatuses < ActiveRecord::Migration
  def change
    create_table :invoice_statuses do |t|
      t.string :status_id
      t.string :description
      t.integer :sequence_num

      t.timestamps
    end
  end
end
