class CreateCarrierShipmentMethods < ActiveRecord::Migration
  def change
    create_table :carrier_shipment_methods do |t|
      t.integer :carrier_party_id
      t.integer :shipment_method_id
      t.integer :sequence_num
      t.string :carrier_service_code

      t.timestamps
    end
  end
end
