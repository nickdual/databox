class CreateProductConfigItems < ActiveRecord::Migration
  def change
    create_table :product_config_items do |t|
      t.integer :config_item_id
      t.integer :config_item_type_id
      t.text :config_item_name

      t.timestamps
    end
  end
end
