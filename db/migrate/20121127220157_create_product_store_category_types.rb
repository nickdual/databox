class CreateProductStoreCategoryTypes < ActiveRecord::Migration
  def change
    create_table :product_store_category_types do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
