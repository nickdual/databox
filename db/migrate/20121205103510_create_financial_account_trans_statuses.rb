class CreateFinancialAccountTransStatuses < ActiveRecord::Migration
  def change
    create_table :financial_account_trans_statuses do |t|
      t.string :status_id
      t.integer :sequence_num
      t.string :description

      t.timestamps
    end
  end
end
