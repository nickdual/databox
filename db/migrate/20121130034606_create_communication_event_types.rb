class CreateCommunicationEventTypes < ActiveRecord::Migration
  def change
    create_table :communication_event_types do |t|
      t.integer :parent_type_id
      t.text :description
      t.integer :contact_mech_type_id
      t.string :name
      t.timestamps
    end
  end
end
