class CreateJobPostingTypes < ActiveRecord::Migration
  def change
    create_table :job_posting_types do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
