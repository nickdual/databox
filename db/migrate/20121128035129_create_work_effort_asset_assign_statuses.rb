class CreateWorkEffortAssetAssignStatuses < ActiveRecord::Migration
  def change
    create_table :work_effort_asset_assign_statuses do |t|
      t.string :description
      t.integer :sequence_num
      t.string :name

      t.timestamps
    end
  end
end
