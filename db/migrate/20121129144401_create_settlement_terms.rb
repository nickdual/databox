class CreateSettlementTerms < ActiveRecord::Migration
  def change
    create_table :settlement_terms do |t|
      t.integer :settlement_term_id
      t.integer :term_type_enum_id
      t.text :descriptin
      t.integer :term_value
      t.integer :term_value_uom_id

      t.timestamps
    end
  end
end
