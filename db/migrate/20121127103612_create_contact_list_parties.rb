class CreateContactListParties < ActiveRecord::Migration
  def change
    create_table :contact_list_parties do |t|
      t.integer :contact_list_id
      t.integer :party_id
      t.datetime :from_date
      t.datetime :thru_date
      t.integer :status_id
      t.integer :preferred_contact_mech_id
      t.text :opt_in_verify_code

      t.timestamps
    end
  end
end
