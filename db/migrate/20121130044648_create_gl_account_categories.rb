class CreateGlAccountCategories < ActiveRecord::Migration
  def change
    create_table :gl_account_categories do |t|
    	t.integer :category_type_enum_id
    	t.string :description

      t.timestamps
    end
  end
end
