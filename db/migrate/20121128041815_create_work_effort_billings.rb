class CreateWorkEffortBillings < ActiveRecord::Migration
  def change
    create_table :work_effort_billings do |t|
      t.integer :work_effort_id
      t.integer :invoice_id
      t.integer :invoice_item_seq_id
      t.float :percentage

      t.timestamps
    end
  end
end
