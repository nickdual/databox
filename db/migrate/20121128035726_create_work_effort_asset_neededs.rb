class CreateWorkEffortAssetNeededs < ActiveRecord::Migration
  def change
    create_table :work_effort_asset_neededs do |t|
      t.integer :work_effort_id
      t.integer :asset_product_id
      t.float :estimated_quantity
      t.float :estimated_duration
      t.decimal :estimated_cost

      t.timestamps
    end
  end
end
