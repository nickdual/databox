class CreateAgreementItemTypes < ActiveRecord::Migration
  def change
    create_table :agreement_item_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
