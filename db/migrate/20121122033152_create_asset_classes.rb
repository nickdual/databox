class CreateAssetClasses < ActiveRecord::Migration
  def change
    create_table :asset_classes do |t|
      t.string :name
      t.string :description
      t.integer :parent_id

      t.timestamps
    end
  end
end
