class CreateMarketingCampaigns < ActiveRecord::Migration
  def change
    create_table :marketing_campaigns do |t|
      t.integer :marketing_campaign_id
      t.integer :parent_campaign_id
      t.integer :status_id
      t.text :campaign_name
      t.text :campaign_summary
      t.decimal :budgeted_cost
      t.decimal :actual_cost
      t.decimal :estimated_cost
      t.integer :cost_uom_id
      t.datetime :from_date
      t.datetime :thru_date
      t.boolean :is_active
      t.integer :converted_leads
      t.float :expected_response_percent
      t.decimal :expected_revenue
      t.integer :num_sent
      t.datetime :start_date

      t.timestamps
    end
  end
end
