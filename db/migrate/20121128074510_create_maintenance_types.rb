class CreateMaintenanceTypes < ActiveRecord::Migration
  def change
    create_table :maintenance_types do |t|
      t.string :description
      t.string :name
      t.integer :parent_id

      t.timestamps
    end
  end
end
