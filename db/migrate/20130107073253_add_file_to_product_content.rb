class AddFileToProductContent < ActiveRecord::Migration
  def change
      add_attachment :product_contents, :file
  end
end