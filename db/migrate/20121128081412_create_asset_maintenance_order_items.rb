class CreateAssetMaintenanceOrderItems < ActiveRecord::Migration
  def change
    create_table :asset_maintenance_order_items do |t|
      t.integer :asset_maintenance_id
      t.integer :order_id
      t.integer :order_item_seq_id

      t.timestamps
    end
  end
end
