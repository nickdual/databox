class CreateInvoiceParties < ActiveRecord::Migration
  def change
    create_table :invoice_parties do |t|
      t.integer :invoice_id
      t.integer :party_id
      t.integer :role_type_id
      t.integer :datetime_performed
      t.decimal :percentage

      t.timestamps
    end
  end
end
