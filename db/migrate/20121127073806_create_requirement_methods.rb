class CreateRequirementMethods < ActiveRecord::Migration
  def change
    create_table :requirement_methods do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
