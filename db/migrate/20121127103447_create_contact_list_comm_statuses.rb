class CreateContactListCommStatuses < ActiveRecord::Migration
  def change
    create_table :contact_list_comm_statuses do |t|
      t.integer :contact_list_id
      t.integer :communication_event_id
      t.integer :contact_mech_id
      t.integer :party_id
      t.integer :message_id
      t.integer :status_id

      t.timestamps
    end
  end
end
