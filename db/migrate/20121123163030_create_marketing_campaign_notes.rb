class CreateMarketingCampaignNotes < ActiveRecord::Migration
  def change
    create_table :marketing_campaign_notes do |t|
      t.integer :marketing_campaign_id
      t.datetime :note_date
      t.text :note_text

      t.timestamps
    end
  end
end
