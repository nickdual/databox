class CreateTaxAuthorityRates < ActiveRecord::Migration
	def change
		create_table :tax_authority_rates do |t|
			t.integer :tax_auth_geo_id
			t.integer :tax_auth_party_id
			t.integer :rate_type_enum_id
			t.integer :product_store_id
			t.integer :product_category_id
			t.enum :title_transfer_enum_id
			t.float :min_item_price
			t.float :min_purchase
			t.boolean :tax_shipping
			t.float :tax_percentage
			t.boolean :tax_promotions
			t.date :from_date
			t.date :thru_date
			t.string :description

			t.timestamps
		end
	end
end
