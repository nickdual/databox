class CreateSalesOpportunityCompetitors < ActiveRecord::Migration
  def change
    create_table :sales_opportunity_competitors do |t|
      t.integer :sales_opportunity_id
      t.integer :competitor_party_id
      t.integer :position_id
      t.text :strengths
      t.text :weaknesses

      t.timestamps
    end
  end
end
