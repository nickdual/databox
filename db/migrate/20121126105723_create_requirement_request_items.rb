class CreateRequirementRequestItems < ActiveRecord::Migration
  def change
    create_table :requirement_request_items do |t|
      t.integer :requirement_id
      t.integer :request_id
      t.integer :request_item_seq_id

      t.timestamps
    end
  end
end
