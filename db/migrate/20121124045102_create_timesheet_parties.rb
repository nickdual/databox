class CreateTimesheetParties < ActiveRecord::Migration
  def change
    create_table :timesheet_parties do |t|
      t.integer :timesheet_id
      t.integer :party_id
      t.integer :role_type_id

      t.timestamps
    end
  end
end
