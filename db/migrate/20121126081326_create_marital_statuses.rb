class CreateMaritalStatuses < ActiveRecord::Migration
  def change
    create_table :marital_statuses do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
