class CreateBudgetItemTypes < ActiveRecord::Migration
  def change
    create_table :budget_item_types do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
