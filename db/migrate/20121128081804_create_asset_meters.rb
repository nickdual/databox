class CreateAssetMeters < ActiveRecord::Migration
  def change
    create_table :asset_meters do |t|
      t.integer :asset_id
      t.integer :product_meter_type_id
      t.datetime :reading_date
      t.decimal :meter_value
      t.integer :reading_reason_id
      t.integer :asset_maintenance_id
      t.integer :work_effort_id

      t.timestamps
    end
  end
end
