class CreateAgreementItemParties < ActiveRecord::Migration
  def change
    create_table :agreement_item_parties do |t|
      t.integer :agreement_id
      t.integer :agreement_item_seq_id
      t.integer :party_id
      t.integer :role_type_id

      t.timestamps
    end
  end
end
