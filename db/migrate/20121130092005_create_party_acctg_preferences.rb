class CreatePartyAcctgPreferences < ActiveRecord::Migration
  def change
    create_table :party_acctg_preferences do |t|
    	t.integer :party_id
    	t.integer :fiscal_year_start_month
    	t.integer :fiscal_year_start_day
    	t.enum :tax_form_enum_id
    	t.enum :cogs_method_enum_id
    	t.integer :base_currency_uom_id
    	t.integer :invoice_sequence_enumId
    	t.string :invoice_id_prefix
    	t.integer :last_invoice_number
    	t.integer :last_invoice_restart_date
    	t.text :use_invoice_id_for_returns
    	t.enum :quote_sequence_enum_id
    	t.text :quote_id_prefix
    	t.integer :last_quote_number
    	t.integer :order_sequence_enum_id
    	t.text :order_id_prefix
    	t.integer :last_order_number
    	t.integer :refund_payment_method_id
    	t.integer :error_gl_journal_id

      t.timestamps
    end
  end
end
