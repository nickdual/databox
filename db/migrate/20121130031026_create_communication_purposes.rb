class CreateCommunicationPurposes < ActiveRecord::Migration
  def change
    create_table :communication_purposes do |t|
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
