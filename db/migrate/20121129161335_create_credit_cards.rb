class CreateCreditCards < ActiveRecord::Migration
  def change
    create_table :credit_cards do |t|
      t.integer :payment_method_id
      t.integer :credit_card_type_enum_id
      t.text :card_number
      t.text :card_number_lookup_hash
      t.text :valid_from_date
      t.text :expire_date
      t.text :issue_number
      t.text :company_name_on_card
      t.text :title_on_card
      t.text :first_name_on_card
      t.text :middle_name_on_card
      t.text :last_name_on_cad
      t.text :suffix_on_card
      t.integer :consecutive_failed_auths
      t.datetime :last_failed_auth_date
      t.integer :consecutive_failed_nsf
      t.datetime :last_failed_nsf_date

      t.timestamps
    end
  end
end
