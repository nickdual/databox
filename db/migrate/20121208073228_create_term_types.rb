class CreateTermTypes < ActiveRecord::Migration
  def change
    create_table :term_types do |t|
      t.string :description
      t.string :name
      t.integer :parent_id

      t.timestamps
    end
  end
end
