class CreateMarketSegmentParties < ActiveRecord::Migration
  def change
    create_table :market_segment_parties do |t|
      t.integer :market_segment_id
      t.integer :party_id
      t.integer :role_type_id

      t.timestamps
    end
  end
end
