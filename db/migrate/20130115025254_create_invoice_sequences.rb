class CreateInvoiceSequences < ActiveRecord::Migration
  def change
    create_table :invoice_sequences do |t|
      t.string :name
      t.string :description
      t.integer :sequence_num

      t.timestamps
    end
  end
end
