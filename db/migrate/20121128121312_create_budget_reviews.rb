class CreateBudgetReviews < ActiveRecord::Migration
  def change
    create_table :budget_reviews do |t|
    	t.integer :budgetId
    	t.integer :party_id
    	t.enum :budget_review_result_enum_id
    	t.date :review_date

      t.timestamps
    end
  end
end
