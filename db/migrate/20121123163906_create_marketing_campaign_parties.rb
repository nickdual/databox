class CreateMarketingCampaignParties < ActiveRecord::Migration
  def change
    create_table :marketing_campaign_parties do |t|
      t.integer :marketing_campaign_id
      t.integer :party_id
      t.integer :role_type_id
      t.datetime :from_date
      t.datetime :thru_date

      t.timestamps
    end
  end
end
