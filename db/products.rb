# Adding sample products to test Search functionality.
Product.create([
    {product_type_id: '1', product_name:'Eclairs', description:'Coolest candies ever and forever...'},
    {product_type_id: '11', product_name:'BMW i8 Concept', description:'Coolest car ever.'},
    {product_type_id: '1', product_name:'Orango', description:'Coolest candies ever and forever...'},
    {product_type_id: '1', product_name:'Chocbar', description:'Coolest candies ever and forever...'},
    {product_type_id: '1', product_name:'Fanty', description:'Coolest candies ever and forever...'},
    {product_type_id: '1', product_name:'Toyota Corrolla', description:'Coolest candies ever and forever...'},
    {product_type_id: '1', product_name:'KitKat', description:'Coolest candies ever and forever...'},
    {product_type_id: '1', product_name:'Dolphins', description:'Coolest candies ever and forever...'},
    {product_type_id: '1', product_name:'Dairy Milk', description:'Coolest candies ever and forever...'},
    {product_type_id: '1', product_name:'Chocys', description:'Coolest candies ever and forever...'},
    {product_type_id: '11', product_name:'Hunda Accord', description:'Coolest candies ever and forever...'},
    {product_type_id: '1', product_name:'Eclairs1', description:'Coolest candies ever and forever...'},
    {product_type_id: '1', product_name:'Eclairs2', description:'Coolest candies ever and forever...'},
    {product_type_id: '1', product_name:'Eclairs3', description:'Coolest candies ever and forever...'},
    {product_type_id: '1', product_name:'Eclairs4', description:'Coolest candies ever and forever...'},
    {product_type_id: '1', product_name:'Toyota Camery', description:'Coolest candies ever and forever...'},
    {product_type_id: '1', product_name:'Eclairs1', description:'Coolest candies ever and forever...'},
    {product_type_id: '1', product_name:'Eclairs2', description:'Coolest candies ever and forever...'},
    {product_type_id: '11', product_name:'Hunda Civic', description:'Coolest candies ever and forever...'},
    {product_type_id: '12', product_name:'Dairy Milk', description:'Coolest candies ever and forever...'},
    {product_type_id: '12', product_name:'Kit Kat', description:'Coolest candies ever and forever...'}
               ])
