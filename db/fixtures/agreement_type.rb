AgreementType.seed do |s|
  s.id = 1
  s.name = 'PRODUCT_AGREEMENT'
  s.description = 'Product'
  s.parent_id = nil
end
AgreementType.seed do |s|
  s.id = 2
  s.name = 'PURCHASE_AGREEMENT'
  s.description = 'Purchase'
  s.parent_id = 1
end
AgreementType.seed do |s|
  s.id = 3
  s.name = 'SALES_AGREEMENT'
  s.description = 'Sales'
  s.parent_id = 1
end
AgreementType.seed do |s|
  s.id = 4
  s.name = 'EMPLOYMENT_AGREEMENT'
  s.description = 'Employment'
  s.parent_id = nil
end
AgreementType.seed do |s|
  s.id = 5
  s.name = 'OTHER_AGREEMENT'
  s.description = 'Other'
  s.parent_id = nil
end
AgreementType.seed do |s|
  s.id = 6
  s.name = 'COMMISSION_AGREEMENT'
  s.description = 'Commission'
  s.parent_id = nil
end
AgreementType.seed do |s|
  s.id = 7
  s.name = 'EULA'
  s.description = 'End User License Agreement'
  s.parent_id = nil
end