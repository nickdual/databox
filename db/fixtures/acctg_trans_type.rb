AcctgTransType.seed do |s|
  s.id = 1
  s.name = 'INTERNAL_ACCTG_TRANS'
  s.description = 'Internal'
  s.parent_id = nil
end
AcctgTransType.seed do |s|
  s.id = 2
  s.name = 'AMORTIZATION'
  s.description = 'Amortization'
  s.parent_id = 1
end
AcctgTransType.seed do |s|
  s.id = 3
  s.name = 'DEPRECIATION'
  s.description = 'Depreciation'
  s.parent_id = 1
end
AcctgTransType.seed do |s|
  s.id = 4
  s.name = 'CAPITALIZATION'
  s.description = 'Capitalization'
  s.parent_id = 1
end
AcctgTransType.seed do |s|
  s.id = 5
  s.name = 'INVENTORY'
  s.description = 'Inventory'
  s.parent_id = 1
end
AcctgTransType.seed do |s|
  s.id = 6
  s.name = 'ITEM_VARIANCE'
  s.description = 'Inventory Item Variance'
  s.parent_id = 1
end
AcctgTransType.seed do |s|
  s.id = 7
  s.name = 'OTHER_INTERNAL'
  s.description = 'Other Internal'
  s.parent_id = 1
end
AcctgTransType.seed do |s|
  s.id = 8
  s.name = 'PERIOD_CLOSING'
  s.description = 'Period Closing'
  s.parent_id = 1
end
AcctgTransType.seed do |s|
  s.id = 9
  s.name = 'EXTERNAL_ACCTG_TRANS'
  s.description = 'External'
  s.parent_id = nil
end
AssetClass.seed do |s|
  s.id = 10
  s.name = 'OBLIGATION_ACCTG_TRA'
  s.description = 'Obligation'
  s.parent_id = 9
end
AcctgTransType.seed do |s|
  s.id = 11
  s.name = 'CREDIT_LINE'
  s.description = 'Credit Line'
  s.parent_id = 10
end
AcctgTransType.seed do |s|
  s.id = 12
  s.name = 'CREDIT_MEMO'
  s.description = 'Credit Memo'
  s.parent_id = 10
end
AcctgTransType.seed do |s|
  s.id = 13
  s.name = 'NOTE'
  s.description = 'Note'
  s.parent_id = 10
end

AcctgTransType.seed do |s|
  s.id = 14
  s.name = 'OTHER_OBLIGATION'
  s.description = 'Other Obligation'
  s.parent_id = 10
end
AcctgTransType.seed do |s|
  s.id = 15
  s.name = 'SALES'
  s.description = 'Sales'
  s.parent_id = 10
end
AcctgTransType.seed do |s|
  s.id = 16
  s.name = 'TAX_DUE'
  s.description = 'Tax Due'
  s.parent_id = 10
end
AcctgTransType.seed do |s|
  s.id = 17
  s.name = 'PAYMENT_ACCTG_TRANS'
  s.description = 'Payment'
  s.parent_id = 9
end

AcctgTransType.seed do |s|
  s.id = 18
  s.name = 'DISBURSEMENT'
  s.description = 'Disbursement'
  s.parent_id = 17
end
AcctgTransType.seed do |s|
  s.id = 19
  s.name = 'RECEIPT'
  s.description = 'Receipt'
  s.parent_id = 17
end
AcctgTransType.seed do |s|
  s.id = 20
  s.name = 'INVENTORY_RETURN'
  s.description = 'Inventory from Return'
  s.parent_id = 9
end
AcctgTransType.seed do |s|
  s.id = 21
  s.name = 'SALES_INVOICE'
  s.description = 'Sales Invoice'
  s.parent_id = 10
end
AcctgTransType.seed do |s|
  s.id = 22
  s.name = 'PURCHASE_INVOICE'
  s.description = 'Purchase Invoice'
  s.parent_id = 10
end
AssetClass.seed do |s|
  s.id = 23
  s.name = 'CUST_RTN_INVOICE'
  s.description = 'Customer Return'
  s.parent_id = 10
  end
AssetClass.seed do |s|
  s.id = 24
  s.name = 'SALES_SHIPMENT'
  s.description = 'Sales Shipment'
  s.parent_id = 9
end
AssetClass.seed do |s|
  s.id = 25
  s.name = 'SHIPMENT_RECEIPT'
  s.description = 'Shipment Receipt'
  s.parent_id = 9
end
AssetClass.seed do |s|
  s.id = 26
  s.name = 'MANUFACTURING'
  s.description = 'Manufacturing Return'
  s.parent_id = nil
end
AssetClass.seed do |s|
  s.id = 27
  s.name = 'INCOMING_PAYMENT'
  s.description = 'Incoming Payment (Receipt)'
  s.parent_id = nil
end
AssetClass.seed do |s|
  s.id = 28
  s.name = 'OUTGOING_PAYMENT'
  s.description = 'Outgoing Payment (Disbursement)'
  s.parent_id = nil
end
AssetClass.seed do |s|
  s.id = 29
  s.name = 'PAYMENT_APPL'
  s.description = 'Payment Applied'
  s.parent_id = nil
end

