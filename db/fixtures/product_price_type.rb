ProductPriceType.seed do |s|
  s.id = 1
  s.name = 'LIST_PRICE'
  s.description = 'List Price'
end
ProductPriceType.seed do |s|
  s.id = 2
  s.name = 'CURRENT_PRICE'
  s.description = 'Current Price'
end
ProductPriceType.seed do |s|
  s.id = 3
  s.name = 'AVERAGE_COST'
  s.description = 'Average Cost'
end
ProductPriceType.seed do |s|
  s.id = 4
  s.name = 'MINIMUM_PRICE'
  s.description = 'Minimum Price'
end
ProductPriceType.seed do |s|
  s.id = 5
  s.name = 'MAXIMUM_PRICE'
  s.description = 'Maximum Price'
end
ProductPriceType.seed do |s|
  s.id = 6
  s.name = 'PROMO_PRICE'
  s.description = 'Promotional Price'
end
ProductPriceType.seed do |s|
  s.id = 7
  s.name = 'COMPETITIVE_PRICE'
  s.description = 'Competitive Price'
end
ProductPriceType.seed do |s|
  s.id = 8
  s.name = 'WHOLESALE_PRICE'
  s.description = 'Wholesale Price'
end
ProductPriceType.seed do |s|
  s.id = 9
  s.name = 'SPECIAL_PROMO_PRICE'
  s.description = 'Special Promo Price'
end
ProductPriceType.seed do |s|
  s.id = 10
  s.name = 'BOX_PRICE'
  s.description = 'Box Price'
end
ProductPriceType.seed do |s|
  s.id = 11
  s.name = 'MINIMUM_ORDER_PRICE'
  s.description = 'Minimum Order Price'
end


