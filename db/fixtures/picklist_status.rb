PicklistStatus.seed do |s|
  s.id = 1
  s.name = 'PICKLIST_INPUT'
  s.description = 'Input'
  s.sequence_num = 1
end
PicklistStatus.seed do |s|
  s.id = 2
  s.name = 'PICKLIST_ASSIGNED'
  s.description = 'Assigned'
  s.sequence_num = 2
end
PicklistStatus.seed do |s|
  s.id = 3
  s.name = 'PICKLIST_PRINTED'
  s.description = 'Printed'
  s.sequence_num = 3
end
PicklistStatus.seed do |s|
  s.id = 4
  s.name = 'PICKLIST_PICKED'
  s.description = 'Picked'
  s.sequence_num = 10
end
PicklistStatus.seed do |s|
  s.id = 5
  s.name = 'PICKLIST_CANCELLED'
  s.description = 'Cancelled'
  s.sequence_num = 99
end
