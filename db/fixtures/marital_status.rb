MaritalStatus.seed do |s|
  s.id = 1
  s.name = 'MARS_SINGLE'
  s.description = 'Single'
end
MaritalStatus.seed do |s|
  s.id = 2
  s.name = 'MARS_MARRIED'
  s.description = 'Married'
end
MaritalStatus.seed do |s|
  s.id = 3
  s.name = 'MARS_DIVORCED'
  s.description = 'Divorced'
end
MaritalStatus.seed do |s|
  s.id = 4
  s.name = 'MARS_WIDOW'
  s.description = 'Widowed/Widower'
end

