FinancialAccountReplenishType.seed do |s|
   s.name = 'FARP_AUTOMATIC'
   s.description = 'Automatic Replenish'
end
FinancialAccountReplenishType.seed do |s|
   s.name = 'FARP_MANUAL'
   s.description = 'Manual Replenish' 
end
FinancialAccountReplenishType.seed do |s|
   s.name = 'FARP_NONE'
   s.description = 'No Replenish'
end