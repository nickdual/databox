PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_PROPOSED'
  s.to_status_id = 'PMNT_PROMISED'
  s.transition_name = 'Promised'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_PROPOSED'
  s.to_status_id = 'PMNT_AUTHORIZED'
  s.transition_name = 'Sent'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_PROPOSED'
  s.to_status_id = 'PMNT_SENT'
  s.transition_name = 'Sent'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_PROPOSED'
  s.to_status_id = 'PMNT_RECEIVED'
  s.transition_name = 'Received'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_PROPOSED'
  s.to_status_id = 'PMNT_CANCELLED'
  s.transition_name = 'Cancelled'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_PROPOSED'
  s.to_status_id = 'PMNT_DECLINED'
  s.transition_name = 'Sent'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_AUTHORIZED'
  s.to_status_id = 'PMNT_SENT'
  s.transition_name = 'Sent'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_AUTHORIZED'
  s.to_status_id = 'PMNT_VOID'
  s.transition_name = 'Voided'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_AUTHORIZED'
  s.to_status_id = 'PMNT_DECLINED'
  s.transition_name = 'Declined'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_SENT'
  s.to_status_id = 'PMNT_REFUNDED'
  s.transition_name = 'Refunded'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_RECEIVED'
  s.to_status_id = 'PMNT_REFUNDED'
  s.transition_name = 'Refunded'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_CONFIRMED'
  s.to_status_id = 'PMNT_REFUNDED'
  s.transition_name = 'Refunded'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_SENT'
  s.to_status_id = 'PPMNT_CONFIRMED'
  s.transition_name = 'Confirmed'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_CONFIRMED'
  s.to_status_id = 'PMNT_CONFIRMED'
  s.transition_name = 'Confirmed'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_CONFIRMED'
  s.to_status_id = 'PMNT_VOID'
  s.transition_name = 'Voided'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_CONFIRMED'
  s.to_status_id = 'PMNT_VOID'
  s.transition_name = 'Voided'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_CONFIRMED'
  s.to_status_id = 'PMNT_VOID'
  s.transition_name = 'Voided'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_CONFIRMED'
  s.to_status_id = 'PMNT_PROMISED'
  s.transition_name = 'Not Paid'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_CONFIRMED'
  s.to_status_id = 'PMNT_PROMISED'
  s.transition_name = 'Not Paid'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_CONFIRMED'
  s.to_status_id = 'PMNT_SENT'
  s.transition_name = 'Confirmed to Sent'
end
PaymentStatusValid.seed do |s|
  s.status_id = 'PMNT_CONFIRMED'
  s.to_status_id = 'PMNT_RECEIVED'
  s.transition_name = 'Confirmed to Received'
end