ShipmentRouteSegmentStatusValid.seed do |s|
  s.id = 1
  s.status_id = 1
  s.to_status_id = 2
  s.transition_name = 'Confirm'
end
ShipmentRouteSegmentStatusValid.seed do |s|
  s.id = 2
  s.status_id = 2
  s.to_status_id = 3
  s.transition_name = 'Accept'
end
ShipmentRouteSegmentStatusValid.seed do |s|
  s.id = 3
  s.status_id = 2
  s.to_status_id = 4
  s.transition_name = 'Void'
end
ShipmentRouteSegmentStatusValid.seed do |s|
  s.id = 4
  s.status_id = 3
  s.to_status_id = 4
  s.transition_name = 'Void'
end
