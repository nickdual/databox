ProductCategoryContentType.seed do |s|
  s.id = 1
  s.name = 'CATEGORY_NAME'
  s.description = 'Category Name'
end
ProductCategoryContentType.seed do |s|
  s.id = 2
  s.name = 'DESCRIPTION'
  s.description = 'Description'
end
ProductCategoryContentType.seed do |s|
  s.id = 3
  s.name = 'LONG_DESCRIPTION'
  s.description = 'Description - Long'
end
ProductCategoryContentType.seed do |s|
  s.id = 4
  s.name = 'CATEGORY_IMAGE_URL'
  s.description = 'Category Image URL'
end
ProductCategoryContentType.seed do |s|
  s.id = 5
  s.name = 'CATEGORY_IMAGE_ALT'
  s.description = 'Category Image Alt Text'
end
ProductCategoryContentType.seed do |s|
  s.id = 6
  s.name = 'FOOTER'
  s.description = 'Footer'
end
ProductCategoryContentType.seed do |s|
  s.id = 7
  s.name = 'CAT_META_KEYWORDS'
  s.description = 'Meta-Keywords'
end
ProductCategoryContentType.seed do |s|
  s.id = 8
  s.name = 'CAT_META_DESCRIPTION'
  s.description = 'Meta-Description'
end