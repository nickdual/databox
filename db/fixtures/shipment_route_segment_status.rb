ShipmentRouteSegmentStatus.seed do |s|
  s.id = 1
  s.description = 'Not Started'
  s.name = 'SHRSCS_NOT_STARTED'
  s.sequence_num = 1
end
ShipmentRouteSegmentStatus.seed do |s|
  s.id = 2
  s.description = 'Confirmed'
  s.name = 'SHRSCS_CONFIRMED'
  s.sequence_num = 2
end
ShipmentRouteSegmentStatus.seed do |s|
  s.id = 3
  s.description = 'Accepted'
  s.name = 'SHRSCS_ACCEPTED'
  s.sequence_num = 3
end
ShipmentRouteSegmentStatus.seed do |s|
  s.id = 4
  s.description = 'Voided'
  s.name = 'SHRSCS_VOIDED'
  s.sequence_num = 8
end