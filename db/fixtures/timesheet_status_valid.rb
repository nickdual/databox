TimesheetStatusValid.seed do |s|
  s.id = 1
  s.status_id = 1
  s.to_status_id = 2
  s.transition_name = 'Confirm complete'
end
TimesheetStatusValid.seed do |s|
  s.id = 2
  s.status_id = 2
  s.to_status_id = 3
  s.transition_name = 'Approve'
end
