EmploymentApplicationReferredBy.seed do |s|
  s.id = 1
  s.name = 'EARB_NEWSPAPER'
  s.description = 'Newspaper'
end
EmploymentApplicationReferredBy.seed do |s|
  s.id = 2
  s.name = 'EARB_PERSONAL'
  s.description = 'Personal Referral'

end
EmploymentApplicationReferredBy.seed do |s|
  s.id = 3
  s.name = 'EARB_INTERNET'
  s.description = 'Internet'

end
EmploymentApplicationReferredBy.seed do |s|
  s.id = 4
  s.name = 'EARB_ADV'
  s.description = 'Advertisement'

end
