ProductType.seed do |s|
  s.id = 1
  s.name = 'SERVICE'
  s.description = 'Service'
  s.parent_id = nil
end
ProductType.seed do |s|
  s.id = 2
  s.name = 'DIGITAL'
  s.description = 'Digital Product'
  s.parent_id = nil
end
ProductType.seed do |s|
  s.id = 3
  s.name = 'VIRTUAL'
  s.description = 'Virtual Product'
  s.parent_id = nil
end
ProductType.seed do |s|
  s.id = 4
  s.name = 'ASSET_USAGE'
  s.description = 'Fixed Asset Usage'
  s.parent_id = nil
end
ProductType.seed do |s|
  s.id = 5
  s.name = 'DIGITAL_AND_GOOD'
  s.description = 'Digital and Physical Good'
  s.parent_id = 6
end
ProductType.seed do |s|
  s.id = 6
  s.name = 'PHYSICAL_GOOD'
  s.description = 'Physical Good'
  s.parent_id = nil
end

ProductType.seed do |s|
  s.id = 7
  s.name = 'RAW_MATERIAL'
  s.description = 'Raw Material'
  s.parent_id = 6
end
ProductType.seed do |s|
  s.id = 8
  s.name = 'SUB_ASSEMBLY'
  s.description = 'Sub-assembly'
  s.parent_id = 6
end
ProductType.seed do |s|
  s.id = 9
  s.name = 'FINISHED_GOOD'
  s.description = 'Finished Good'
  s.parent_id = 6
end
ProductType.seed do |s|
  s.id = 10
  s.name = 'MARKETING_PKG_AUTO'
  s.description = 'Marketing Package: Manufactured'
  s.parent_id = 6
end
ProductType.seed do |s|
  s.id = 11
  s.name = 'MARKETING_PKG_PICK'
  s.description = 'Marketing Package: Pick Assembly'
  s.parent_id = 6
end
ProductType.seed do |s|
  s.id = 12
  s.name = 'CONFIGURABLE'
  s.description = 'Configurable Good'
  s.parent_id = 6
end