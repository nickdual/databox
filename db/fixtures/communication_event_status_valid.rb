CommunicationEventStatusValid.seed do |s|
  s.id = 1
  s.status_id = 2
  s.to_status_id = 1
  s.transition_name = 'Set Pending, only visible to originator'
end
CommunicationEventStatusValid.seed do |s|
  s.id = 2
  s.status_id = 2
  s.to_status_id = 4
  s.transition_name = 'Set In Progress, waiting to be send'
end
CommunicationEventStatusValid.seed do |s|
  s.id = 3
  s.status_id = 2
  s.to_status_id = 6
  s.transition_name = 'Complete'
end
CommunicationEventStatusValid.seed do |s|
  s.id = 4
  s.status_id = 1
  s.to_status_id = 2
  s.transition_name = 'Entered,visible to all participants'
end
CommunicationEventStatusValid.seed do |s|
  s.id = 5
  s.status_id = 1
  s.to_status_id = 4
  s.transition_name = 'Set In Progress, waiting to be send'
end
CommunicationEventStatusValid.seed do |s|
  s.id = 6
  s.status_id = 4
  s.to_status_id = 6
  s.transition_name = 'Complete'
end
CommunicationEventStatusValid.seed do |s|
  s.id = 7
  s.status_id = 4
  s.to_status_id = 9
  s.transition_name = 'Bounced'
end
CommunicationEventStatusValid.seed do |s|
  s.id = 8
  s.status_id = 6
  s.to_status_id = 7
  s.transition_name = 'Resolve'
end
CommunicationEventStatusValid.seed do |s|
  s.id = 9
  s.status_id = 6
  s.to_status_id = 8
  s.transition_name = 'Refer'
end
CommunicationEventStatusValid.seed do |s|
  s.id = 10
  s.status_id = 6
  s.to_status_id = 9
  s.transition_name = 'Bounced'
end
CommunicationEventStatusValid.seed do |s|
  s.id = 11
  s.status_id = 5
  s.to_status_id = 6
  s.transition_name = 'Complete'
end
CommunicationEventStatusValid.seed do |s|
  s.id = 12
  s.status_id = 5
  s.to_status_id = 2
  s.transition_name = 'Corrected'
end
CommunicationEventStatusValid.seed do |s|
  s.id = 13
  s.status_id = 1
  s.to_status_id = 6
  s.transition_name = 'Complete'
end
CommunicationEventStatusValid.seed do |s|
  s.id = 14
  s.status_id = 2
  s.to_status_id = 10
  s.transition_name = 'Cancel'
end
CommunicationEventStatusValid.seed do |s|
  s.id = 15
  s.status_id = 1
  s.to_status_id = 10
  s.transition_name = 'Cancel'
end
CommunicationEventStatusValid.seed do |s|
  s.id = 16
  s.status_id = 4
  s.to_status_id = 10
  s.transition_name = 'Cancel'
end
CommunicationEventStatusValid.seed do |s|
  s.id = 17
  s.status_id = 6
  s.to_status_id = 10
  s.transition_name = 'Cancel'
end
CommunicationEventStatusValid.seed do |s|
  s.id = 18
  s.status_id = 7
  s.to_status_id = 10
  s.transition_name = 'Cancel'
end
CommunicationEventStatusValid.seed do |s|
  s.id = 19
  s.status_id = 8
  s.to_status_id = 10
  s.transition_name = 'Cancel'
end
CommunicationEventStatusValid.seed do |s|
  s.id = 20
  s.status_id = 5
  s.to_status_id = 10
  s.transition_name = 'Cancel'
end