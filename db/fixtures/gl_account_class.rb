GlAccountClass.seed do |s|
  s.id = 1
  s.name = 'DEBIT'
  s.description = 'Debit'
  s.parent_id = nil
end
GlAccountClass.seed do |s|
  s.id = 2
  s.name = 'CREDIT'
  s.description = 'Credit'
  s.parent_id = nil
end
GlAccountClass.seed do |s|
  s.id = 3
  s.name = 'RESOURCE'
  s.description = 'Resource'
  s.parent_id = nil
end
GlAccountClass.seed do |s|
  s.id = 4
  s.name = 'ASSET'
  s.description = 'Asset'
  s.parent_id = 1
end
GlAccountClass.seed do |s|
  s.id = 5
  s.name = 'CURRENT_ASSET'
  s.description = 'Current Asset'
  s.parent_id = 4
end
GlAccountClass.seed do |s|
  s.id = 6
  s.name = 'CASH_EQUIVALENT'
  s.description = 'Cash and Equivalent'
  s.parent_id = 5
end
GlAccountClass.seed do |s|
  s.id = 7
  s.name = 'INVENTORY_ASSET'
  s.description = 'Inventory Asset'
  s.parent_id = 5
end
GlAccountClass.seed do |s|
  s.id = 8
  s.name = 'LIABILITY'
  s.description = 'Liability'
  s.parent_id = 2
end
GlAccountClass.seed do |s|
  s.id = 9
  s.name = 'CURRENT_LIABILITY'
  s.description = 'Current Liability'
  s.parent_id = 8
end
GlAccountClass.seed do |s|
  s.id = 10
  s.name = 'LONGTERM_LIABILITY'
  s.description = 'Long Term Liability'
  s.parent_id = 8
end
GlAccountClass.seed do |s|
  s.id = 11
  s.name = 'EQUITY'
  s.description = 'Equity'
  s.parent_id = 2
end
GlAccountClass.seed do |s|
  s.id = 12
  s.name = 'OWNERS_EQUITY'
  s.description = 'Owners Equity'
  s.parent_id = 11
end
GlAccountClass.seed do |s|
  s.id = 13
  s.name = 'RETAINED_EARNINGS'
  s.description = 'Retained Earnings'
  s.parent_id = 11
end
GlAccountClass.seed do |s|
  s.id = 14
  s.name = 'DISTRIBUTION'
  s.description = 'Equity Distribution'
  s.parent_id = 1
end
GlAccountClass.seed do |s|
  s.id = 15
  s.name = 'RETURN_OF_CAPITAL'
  s.description = 'Return of Capital'
  s.parent_id = 14
end
GlAccountClass.seed do |s|
  s.id = 16
  s.name = 'DIVIDEND'
  s.description = 'Dividends'
  s.parent_id = 14
end
GlAccountClass.seed do |s|
  s.id = 17
  s.name = 'REVENUE'
  s.description = 'Revenue'
  s.parent_id = 2
end
GlAccountClass.seed do |s|
  s.id = 18
  s.name = 'CONTRA_REVENUE'
  s.description = 'Contra Revenue'
  s.parent_id = 1
end
GlAccountClass.seed do |s|
  s.id = 19
  s.name = 'INCOME'
  s.description = 'Income'
  s.parent_id = 2
end
GlAccountClass.seed do |s|
  s.id = 20
  s.name = 'CASH_INCOME'
  s.description = 'Cash Income'
  s.parent_id = 19
end
GlAccountClass.seed do |s|
  s.id = 21
  s.name = 'NON_CASH_INCOME'
  s.description = 'Non-Cash Income'
  s.parent_id = 19
end
GlAccountClass.seed do |s|
  s.id = 22
  s.name = 'EXPENSE'
  s.description = 'Expense'
  s.parent_id = 1
end
GlAccountClass.seed do |s|
  s.id = 23
  s.name = 'CASH_EXPENSE'
  s.description = 'Cash Expense'
  s.parent_id = 22
end
GlAccountClass.seed do |s|
  s.id = 24
  s.name = 'INTEREST_EXPENSE'
  s.description = 'Interest Expense'
  s.parent_id = 23
end
GlAccountClass.seed do |s|
  s.id = 25
  s.name = 'COGS_EXPENSE'
  s.description = 'Cost of Goods Sold Expense'
  s.parent_id = 23
end
GlAccountClass.seed do |s|
  s.id = 26
  s.name = 'SGA_EXPENSE'
  s.description = 'Selling, General, and Administrative Expense'
  s.parent_id = 23
end
GlAccountClass.seed do |s|
  s.id = 27
  s.name = 'NON_CASH_EXPENSE'
  s.description = 'Non-Cash Expense'
  s.parent_id = 22
end
GlAccountClass.seed do |s|
  s.id = 28
  s.name = 'DEPRECIATION'
  s.description = 'Depreciation'
  s.parent_id = 27
end
GlAccountClass.seed do |s|
  s.id = 29
  s.name = 'AMORTIZATION'
  s.description = 'Amortization'
  s.parent_id = 27
end
GlAccountClass.seed do |s|
  s.id = 30
  s.name = 'INVENTORY_ADJUST'
  s.description = 'Inventory Adjustment'
  s.parent_id = 27
end
GlAccountClass.seed do |s|
  s.id = 31
  s.name = 'CONTRA_ASSET'
  s.description = 'Contra Asset'
  s.parent_id = 2
end
GlAccountClass.seed do |s|
  s.id = 32
  s.name = 'CONTRA_ASSET'
  s.description = 'Contra Asset'
  s.parent_id = 2
end
GlAccountClass.seed do |s|
  s.id = 33
  s.name = 'ACCUM_DEPRECIATION'
  s.description = 'Accumulated Depreciation'
  s.parent_id = 32
end
GlAccountClass.seed do |s|
  s.id = 34
  s.name = 'ACCUM_AMORTIZATION'
  s.description = 'Accumulated Amortization'
  s.parent_id = 32
end
GlAccountClass.seed do |s|
  s.id = 35
  s.name = 'NON_POSTING'
  s.description = 'Non-Posting'
  s.parent_id = 1
end