EmploymentStatus.seed do |s|
  s.id = 1
  s.name = 'EMPS_FULLTIME'
  s.description = 'Full-time Employed'
end
EmploymentStatus.seed do |s|
  s.id = 2
  s.name = 'EMPS_PARTTIME'
  s.description = 'Part-time Employed'
end
EmploymentStatus.seed do |s|
  s.id = 3
  s.name = 'EMPS_SELF'
  s.description = 'Self Employed'
end
EmploymentStatus.seed do |s|
  s.id = 4
  s.name = 'EMPS_HOUSE'
  s.description = 'House-Person'
end
EmploymentStatus.seed do |s|
  s.id = 5
  s.name = 'EMPS_RETIRED'
  s.description = 'Retired'
end
EmploymentStatus.seed do |s|
  s.id = 6
  s.name = 'EMPS_STUDENT'
  s.description = 'Student'
end
EmploymentStatus.seed do |s|
  s.id = 7
  s.name = 'EMPS_UNEMP'
  s.description = 'Unemployed'
end