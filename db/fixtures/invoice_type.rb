InvoiceType.seed do |s|
   s.name = 'INVOICE_ORDER'
   s.description = 'Order'
end
InvoiceType.seed do |s|
   s.name = 'INVOICE_RETURN'
   s.description = 'Return'
end
InvoiceType.seed do |s|
   s.name = 'INVOICE_PAYROLL'
   s.description = 'Payroll'
end
InvoiceType.seed do |s|
   s.name = 'INVOICE_COMMISSION'
   s.description = 'Commission'
end
InvoiceType.seed do |s|
   s.name = 'INVOICE_TEMPLATE'
   s.description = 'Template'
end
InvoiceType.seed do |s|
   s.name = 'IT_PAYABLE'
   s.description = 'Payable Invoice'
end
InvoiceType.seed do |s|
   s.name = 'PURCHASE_INVOICE'
   s.parent_name = 'IT_PAYABLE'
   s.description = 'Purchase Invoice'
end
InvoiceType.seed do |s|
   s.name = 'CUST_RTN_INVOICE'
   s.parent_name = 'IT_PAYABLE'
   s.description = 'Customer Return'
end
InvoiceType.seed do |s|
   s.name = 'COMMISSION_INVOICE'
   s.parent_name = 'IT_PAYABLE'
   s.description = 'Commission'
end
InvoiceType.seed do |s|
   s.name = 'PAYROLL_INVOICE'
   s.parent_name = 'IT_PAYABLE'
   s.description = 'Payroll'
end
InvoiceType.seed do |s|
   s.name = 'IT_RECEIVABLE'
   s.description = 'Receivable Invoice'
end
InvoiceType.seed do |s|
   s.name = 'SALES_INVOICE'
   s.parent_name = 'IT_RECEIVABLE'
   s.description = 'Sales Invoice'
end
InvoiceType.seed do |s|
   s.name = 'VEND_RTN_INVOICE'
   s.parent_name = 'IT_RECEIVABLE'
   s.description = 'Vendor Return'
end
InvoiceType.seed do |s|
   s.name = 'TEMPLATE'
   s.description = 'Invoice Template'
end
InvoiceType.seed do |s|
   s.name = 'SALES_INV_TEMPLATE'
   s.parent_name = 'TEMPLATE'
   s.description = 'Sales Invoice Template'
end
InvoiceType.seed do |s|
   s.name = 'PUR_INV_TEMPLATE'
   s.parent_name = 'TEMPLATE'
   s.description = 'Purchase Invoice Template'
end