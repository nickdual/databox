CommunicationEventType.seed do |s|
  s.id = 1
  s.name = 'EMAIL_COMMUNICATION'
  s.description = 'Email'
  s.contact_mech_type_id = 4
  s.parent_type_id = nil
end
CommunicationEventType.seed do |s|
  s.id = 2
  s.name = 'FACE_TO_FACE_COMMUNI'
  s.description = 'Face-To-Face'
  s.contact_mech_type_id = nil
  s.parent_type_id = nil
end
CommunicationEventType.seed do |s|
  s.id = 3
  s.name = 'FAX_COMMUNICATION'
  s.description = 'Fax'
  s.contact_mech_type_id = 2
  s.parent_type_id = nil
end
CommunicationEventType.seed do |s|
  s.id = 4
  s.name = 'LETTER_CORRESPONDENC'
  s.description = 'Letter'
  s.contact_mech_type_id = 1
  s.parent_type_id = nil
end
CommunicationEventType.seed do |s|
  s.id = 5
  s.name = 'PHONE_COMMUNICATION'
  s.description = 'Phone'
  s.contact_mech_type_id = 2
  s.parent_type_id = nil
end
CommunicationEventType.seed do |s|
  s.id = 6
  s.name = 'WEB_SITE_COMMUNICATI'
  s.description = 'Web Site'
  s.contact_mech_type_id = 7
  s.parent_type_id = nil
end
CommunicationEventType.seed do |s|
  s.id = 7
  s.name = 'COMMENT_NOTE'
  s.description = 'Comment/Note'
  s.contact_mech_type_id = nil
  s.parent_type_id = nil
end
CommunicationEventType.seed do |s|
  s.id = 8
  s.name = 'AUTO_EMAIL_COMM'
  s.description = 'Auto Email'
  s.contact_mech_type_id = 4
  s.parent_type_id = nil
end