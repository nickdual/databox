FinancialAccountTransStatus.seed do |s|
   s.status_id = 'FINACT_TRNS_APPROVED'
   s.sequence_num = 1
   s.description = 'Approved'
end
FinancialAccountTransStatus.seed do |s|
   s.status_id = 'FINACT_TRNS_CREATED'
   s.sequence_num = 2
   s.description = 'Created'
end
FinancialAccountTransStatus.seed do |s|
   s.status_id = 'FINACT_TRNS_CANCELLD'
   s.sequence_num = 9
   s.description = 'Cancelled'
end


