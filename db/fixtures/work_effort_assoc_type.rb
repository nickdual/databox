WorkEffortAssocType.seed do |s|
  s.id = 1
  s.description = 'Dependent'
  s.name = 'WEAT_DEPENDENT'
end
WorkEffortAssocType.seed do |s|
  s.id = 2
  s.description = 'Concurrent'
  s.name = 'WEAT_CONCURRENCY'
end
WorkEffortAssocType.seed do |s|
  s.id = 3
  s.description = 'Precedent'
  s.name = 'WEAT_PRECEDENCY'
end
WorkEffortAssocType.seed do |s|
  s.id = 4
  s.description = 'Clone'
  s.name = 'WEAT_CLONE'
end
WorkEffortAssocType.seed do |s|
  s.id = 5
  s.description = 'Routing Component'
  s.name = 'WEAT_ROUTING_COMP'
end