WorkEffortType.seed do |s|
  s.id = 1
  s.description = 'Task'
  s.name = 'WET_TASK'
end
WorkEffortType.seed do |s|
  s.id = 2
  s.description = 'Event'
  s.name = 'WET_EVENT'
end
WorkEffortType.seed do |s|
  s.id = 3
  s.description = 'Available'
  s.name = 'WET_AVAILABLE'
end
WorkEffortType.seed do |s|
  s.id = 4
  s.description = 'Time Off'
  s.name = 'WET_TIME_OFF'
end