WorkEffortStatus.seed do |s|
  s.id = 1
  s.description = 'In Planning'
  s.name = 'WE_IN_PLANNING'
  s.sequence_num = 1
end
WorkEffortStatus.seed do |s|
  s.id = 2
  s.description = 'Scheduled'
  s.name = 'WE_SCHEDULED'
  s.sequence_num = 2
end
WorkEffortStatus.seed do |s|
  s.id = 3
  s.description = 'In Progress'
  s.name = 'WE_IN_PROGRESS'
  s.sequence_num = 3
end
WorkEffortStatus.seed do |s|
  s.id = 4
  s.description = 'Complete'
  s.name = 'WE_COMPLETE'
  s.sequence_num = 9
end
WorkEffortStatus.seed do |s|
  s.id = 5
  s.description = 'Closed'
  s.name = 'WE_CLOSED'
  s.sequence_num = 10
end
WorkEffortStatus.seed do |s|
  s.id = 6
  s.description = 'On Hold'
  s.name = 'WE_ON_HOLD'
  s.sequence_num = 98
end
WorkEffortStatus.seed do |s|
  s.id = 7
  s.description = 'Cancelled'
  s.name = 'WE_CANCELLED'
  s.sequence_num = 99
end