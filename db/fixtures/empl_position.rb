EmplPosition.seed do |s|
  s.empl_position_class_id = 1
  s.status_id = 1
  s.filled_by_party_id = 1
  s.employer_organization_party_id = 1
  s.pay_grade_id = 1
  s.budget_id = 1
  s.budget_item_seq_id = 1
  s.estimated_from_date = Time.now
  s.estimated_thru_date = Time.now
  s.salary_flag = 'text'
  s.exempt_flag = 'text'
  s.full_time_flag = 'text'
  s.temporary_flag = 'text'
  s.actual_from_date = Time.now
  s.actual_thru_date = Time.now
  s.standard_hours_per_week = 1
end

