CommunicationPurpose.seed do |s|
  s.id = 1
  s.description = 'Activity Request'
  s.name = 'ACTIVITY_REQUEST'
end
CommunicationPurpose.seed do |s|
  s.id = 2
  s.description = 'Conference'
  s.name = 'CONFERENCE'
end
CommunicationPurpose.seed do |s|
  s.id = 3
  s.description = 'Customer Service'
  s.name = 'CUSTOMER_SERVICE'
end
CommunicationPurpose.seed do |s|
  s.id = 4
  s.description = 'Customer Support'
  s.name = 'CUSTOMER_SUPPORT'
end
CommunicationPurpose.seed do |s|
  s.id = 5
  s.description = 'Meeting'
  s.name = 'MEETING'
end
CommunicationPurpose.seed do |s|
  s.id = 6
  s.description = 'Sales Inquiry'
  s.name = 'SALES_INQUIRY'
end
CommunicationPurpose.seed do |s|
  s.id = 7
  s.description = 'Sales Follow Up'
  s.name = 'SALES_FOLLOW_UP'
end
CommunicationPurpose.seed do |s|
  s.id = 8
  s.description = 'Seminar'
  s.name = 'SEMINAR'
end