OrderItemStatusValid.seed do |s|
  s.id = 1
  s.status_id = 1
  s.to_status_id = 2
  s.transition_name = 'Approve Item'
end
OrderItemStatusValid.seed do |s|
  s.id = 2
  s.status_id = 1
  s.to_status_id = 4
  s.transition_name = 'Reject Item'
end
OrderItemStatusValid.seed do |s|
  s.id = 3
  s.status_id = 1
  s.to_status_id = 5
  s.transition_name = 'Cancel Item'
end
OrderItemStatusValid.seed do |s|
  s.id = 4
  s.status_id = 2
  s.to_status_id = 3
  s.transition_name = 'Complete Item'
end
OrderItemStatusValid.seed do |s|
  s.id = 5
  s.status_id = 2
  s.to_status_id = 5
  s.transition_name = 'Cancel Item'
end
OrderItemStatusValid.seed do |s|
  s.id = 6
  s.status_id = 3
  s.to_status_id = 2
  s.transition_name = 'Approve Item'
end
