SettlementTermValue.seed do |s|
   s.term_id = 'IMMED'
   s.description = 'Immediate (Due on Delivery)'
   s.term_value = '0'
   s.term_value_uom_id = 'TF_DAY'
end
SettlementTermValue.seed do |s|
   s.term_id = 'NET_15'
   s.description = 'Net Due in 15 Days'
   s.term_value = '15'
   s.term_value_uom_id = 'TF_DAY'
end
SettlementTermValue.seed do |s|
   s.term_id = 'NET_30'
   s.description = 'Net Due in 30 Days'
   s.term_value = '30'
   s.term_value_uom_id = 'TF_DAY'
end
SettlementTermValue.seed do |s|
   s.term_id = 'NET_60'
   s.description = 'Net Due in 60 Days'
   s.term_value = '60'
   s.term_value_uom_id = 'TF_DAY'
end
SettlementTermValue.seed do |s|
   s.term_id = 'NET_90'
   s.description = 'Net Due in 90 Days'
   s.term_value = '90'
   s.term_value_uom_id = 'TF_DAY'
end