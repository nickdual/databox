RequestContent.seed do |s|
  s.request_id = 1
  s.content_location = 1
  s.from_date = Time.zone.now
  s.thru_date = Time.zone.now.advance(:months => 1)
end