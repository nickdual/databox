EmploymentResponsibility.seed do |s|
  s.id = 1
  s.name = 'FIN_MGMT'
  s.description = 'Finance Management'
end

EmploymentResponsibility.seed do |s|
  s.id = 2
  s.name = 'INV_MGMT'
  s.description = 'Inventory Management'
end
EmploymentResponsibility.seed do |s|
  s.id = 3
  s.name = 'PUR_MGMT'
  s.description = 'Purchase Management'
end
EmploymentResponsibility.seed do |s|
  s.id = 4
  s.name = 'RES_MGMT'
  s.description = 'Resource Management'
end
EmploymentResponsibility.seed do |s|
  s.id = 5
  s.name = 'PROD_MGMT'
  s.description = 'Production Management'
end

EmploymentResponsibility.seed do |s|
  s.id = 6
  s.name = 'SALES_MGMT'
  s.description = 'Sales Management'
end