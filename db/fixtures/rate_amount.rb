RateAmount.seed do |s|
  s.rate_amount_id = 1
  s.rate_type_enum_id = 1
  s.time_period_type_id = 1
  s.work_effort_id = 1
  s.party_id = 1
  s.empl_position_type_enum_id = 1
  s.from_date = Time.now
  s.thru_date = Time.now
  s.rate_amount = 1.0
  s.rate_currency_uom_id = 1
end
