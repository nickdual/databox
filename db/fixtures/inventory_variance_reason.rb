InventoryVarianceReason.seed do |s|
  s.id = 1
  s.name = 'VAR_LOST'
  s.description = 'Lost'
end
InventoryVarianceReason.seed do |s|
  s.id = 2
  s.name = 'VAR_STOLEN'
  s.description = 'Stolen'
end
InventoryVarianceReason.seed do |s|
  s.id = 3
  s.name = 'VAR_FOUND'
  s.description = 'Found'
end
InventoryVarianceReason.seed do |s|
  s.id = 4
  s.name = 'VAR_DAMAGED'
  s.description = 'Damaged'
end
InventoryVarianceReason.seed do |s|
  s.id = 5
  s.name = 'VAR_INTEGR'
  s.description = 'Integration'
end
InventoryVarianceReason.seed do |s|
  s.id = 6
  s.name = 'VAR_SAMPLE'
  s.description = 'Sample (Giveaway)'
end
InventoryVarianceReason.seed do |s|
  s.id = 7
  s.name = 'VAR_MISSHIP_ORDERED'
  s.description = 'Mis-shipped Item Ordered (+)'
end
InventoryVarianceReason.seed do |s|
  s.id = 7
  s.name = 'VAR_MISSHIP_SHIPPED'
  s.description = 'Mis-shipped Item Shipped (-)'
end