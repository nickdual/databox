PartyContentType.seed do |s|
  s.id = 1
  s.name = 'INTERNAL'
  s.description = 'Internal Content'
end
PartyContentType.seed do |s|
  s.id = 2
  s.name = 'USERDEF'
  s.description = 'User Defined Content'
end
PartyContentType.seed do |s|
  s.id = 3
  s.name = 'LGOIMGURL'
  s.description = 'Logo Image URL'
end
PartyContentType.seed do |s|
  s.id = 4
  s.name = 'VNDSHPINF'
  s.description = 'Vendor Shipping Info'
end
