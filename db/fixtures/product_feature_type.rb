ProductFeatureType.seed do |s|
  s.id = 1
  s.name = 'ACCESSORY'
  s.description = 'Accessory'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 2
  s.name = 'AMOUNT'
  s.description = 'Amount'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 3
  s.name = 'NET_WEIGHT'
  s.description = 'Net Weight'
  s.parent_id = 2
end
ProductFeatureType.seed do |s|
  s.id = 4
  s.name = 'ARTIST'
  s.description = 'Artist'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 5
  s.name = 'BILLING_FEATURE'
  s.description = 'Billing Feature'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 6
  s.name = 'BRAND'
  s.description = 'Brand'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 7
  s.name = 'CARE'
  s.description = 'Care'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 8
  s.name = 'COLOR'
  s.description = 'Color'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 9
  s.name = 'DIMENSION'
  s.description = 'Dimension'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 10
  s.name = 'EQUIP_CLASS'
  s.description = 'Equipment'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 11
  s.name = 'FABRIC'
  s.description = 'Fabric'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 12
  s.name = 'GENRE'
  s.description = 'Genre'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 13
  s.name = 'HARDWARE_FEATURE'
  s.description = 'Hardware Feature'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 14
  s.name = 'LICENSE'
  s.description = 'License'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 15
  s.name = 'ORIGIN'
  s.description = 'Origin'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 16
  s.name = 'OTHER_FEATURE'
  s.description = 'Other Feature'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 17
  s.name = 'PRODUCT_QUALITY'
  s.description = 'Product Quality'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 18
  s.name = 'SIZE'
  s.description = 'Size'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 19
  s.name = 'SOFTWARE_FEATURE'
  s.description = 'Software Feature'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 20
  s.name = 'STYLE'
  s.description = 'Style'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 21
  s.name = 'SYMPTOM'
  s.description = 'Symptom'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 22
  s.name = 'TOPIC'
  s.description = 'Topic'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 23
  s.name = 'TYPE'
  s.description = 'Type'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 24
  s.name = 'WARRANTY'
  s.description = 'Warranty'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 25
  s.name = 'MODEL_YEAR'
  s.description = 'Model Year'
  s.parent_id = nil
end
ProductFeatureType.seed do |s|
  s.id = 25
  s.name = 'YEAR_MADE'
  s.description = 'Year Made'
  s.parent_id = nil
end