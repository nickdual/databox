InvoiceStatus.seed do |s|
   s.status_id = 'INVOICE_IN_PROCESS'
   s.description = 'In-Process'
   s.sequence_num = 1
end
InvoiceStatus.seed do |s|
   s.status_id = 'INVOICE_APPROVED'
   s.description = 'Approved'
   s.sequence_num = 2
end
InvoiceStatus.seed do |s|
   s.status_id = 'INVOICE_SENT'
   s.description = 'Sent'
   s.sequence_num = 10
end
InvoiceStatus.seed do |s|
   s.status_id = 'INVOICE_RECEIVED'
   s.description = 'Received'
   s.sequence_num = 11
end
InvoiceStatus.seed do |s|
   s.status_id = 'INVOICE_READY'
   s.description = 'Ready for Posting'
   s.sequence_num = 20
end
InvoiceStatus.seed do |s|
   s.status_id = 'INVOICE_PAID'
   s.description = 'Paid'
   s.sequence_num = 30
end
InvoiceStatus.seed do |s|
   s.status_id = 'INVOICE_WRITEOFF'
   s.description = 'Write Off'
   s.sequence_num = 31
end
InvoiceStatus.seed do |s|
   s.status_id = 'INVOICE_CANCELLED'
   s.description = 'Cancelled'
   s.sequence_num = 99
end