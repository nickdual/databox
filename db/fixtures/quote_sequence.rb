QuoteSequence.seed do |s|
  s.id = 1
  s.name = 'QTESQ_STANDARD'
  s.description = 'Standard (faster, may have gaps, per system)'
  s.sequence_num = 1
end
QuoteSequence.seed do |s|
  s.id = 2
  s.name = 'QTESQ_ENF_SEQ'
  s.description = 'Enforced Sequence (no gaps, per organization)'
  s.sequence_num =  2
end
