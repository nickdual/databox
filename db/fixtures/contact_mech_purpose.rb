ContactMechPurpose.seed do |s|
  s.id = 1
  s.name = 'SHIPPING_DESTINATION'
  s.description = 'Shipping Destination Address'
  s.contact_mech_type_id = 1
end
ContactMechPurpose.seed do |s|
  s.id = 2
  s.name = 'SHIPPING_ORIGIN'
  s.description = 'Shipping Origin Address'
  s.contact_mech_type_id = 1
end
ContactMechPurpose.seed do |s|
  s.id = 3
  s.name = 'BILLING_LOCATION'
  s.description = 'Billing (AP) Address'
  s.contact_mech_type_id = 1
end
ContactMechPurpose.seed do |s|
  s.id = 4
  s.name = 'PAYMENT_LOCATION'
  s.description = 'Payment (AR) Address'
  s.contact_mech_type_id = 1
end
ContactMechPurpose.seed do |s|
  s.id = 5
  s.name = 'GENERAL_LOCATION'
  s.description = 'General Correspondence Address'
  s.contact_mech_type_id = 1
end
ContactMechPurpose.seed do |s|
  s.id = 6
  s.name = 'PUR_RET_LOCATION'
  s.description = 'Purchase Return Address'
  s.contact_mech_type_id = 1
end
ContactMechPurpose.seed do |s|
  s.id = 7
  s.name = 'PRIMARY_LOCATION'
  s.description = 'Primary Address'
  s.contact_mech_type_id = 1
end
ContactMechPurpose.seed do |s|
  s.id = 8
  s.name = 'PREVIOUS_LOCATION'
  s.description = 'Previous Address'
  s.contact_mech_type_id = 1
end
ContactMechPurpose.seed do |s|
  s.id = 9
  s.name = 'PHONE_SHIP_DEST'
  s.description = 'Shipping Destination Phone Number'
  s.contact_mech_type_id = 2
end
ContactMechPurpose.seed do |s|
  s.id = 10
  s.name = 'PHONE_SHIP_ORIG'
  s.description = 'Shipping Origin Phone Number'
  s.contact_mech_type_id = 2
end
ContactMechPurpose.seed do |s|
  s.id = 11
  s.name = 'PHONE_BILLING'
  s.description = 'Billing (AP) Phone Number'
  s.contact_mech_type_id = 2
end
ContactMechPurpose.seed do |s|
  s.id = 12
  s.name = 'PHONE_DID'
  s.description = 'Direct Inward Dialing Phone Number'
  s.contact_mech_type_id = 2
end
ContactMechPurpose.seed do |s|
  s.id = 13
  s.name = 'PHONE_PAYMENT'
  s.description = 'Payment (AR) Phone Number'
  s.contact_mech_type_id = 2
end
ContactMechPurpose.seed do |s|
  s.id = 14
  s.name = 'PHONE_HOME'
  s.description = 'Main Home Phone Number'
  s.contact_mech_type_id = 2
end
ContactMechPurpose.seed do |s|
  s.id = 15
  s.name = 'PHONE_WORK'
  s.description = 'Main Work Phone Number'
  s.contact_mech_type_id = 2
end
ContactMechPurpose.seed do |s|
  s.id = 16
  s.name = 'PHONE_WORK_SEC'
  s.description = 'Secondary Work Phone Number'
  s.contact_mech_type_id = 2
end
ContactMechPurpose.seed do |s|
  s.id = 17
  s.name = 'FAX_NUMBER'
  s.description = 'Main Fax Number'
  s.contact_mech_type_id = 2
end
ContactMechPurpose.seed do |s|
  s.id = 18
  s.name = 'FAX_NUMBER_SEC'
  s.description = 'Secondary Fax Number'
  s.contact_mech_type_id = 2
end
ContactMechPurpose.seed do |s|
  s.id = 19
  s.name = 'FAX_SHIPPING'
  s.description = 'Shipping Destination Fax Number'
  s.contact_mech_type_id = 2
end
ContactMechPurpose.seed do |s|
  s.id = 20
  s.name = 'FAX_BILLING'
  s.description = 'Billing Destination Fax Number'
  s.contact_mech_type_id = 2
end
ContactMechPurpose.seed do |s|
  s.id = 21
  s.name = 'PHONE_MOBILE'
  s.description = 'Main Mobile Phone Number'
  s.contact_mech_type_id = 2
end
ContactMechPurpose.seed do |s|
  s.id = 22
  s.name = 'PHONE_ASSISTANT'
  s.description = "Assistant's Phone Number"
  s.contact_mech_type_id = 2
end
ContactMechPurpose.seed do |s|
  s.id = 23
  s.name = 'PRIMARY_PHONE'
  s.description = 'Primary Phone Number'
  s.contact_mech_type_id = 2
end
ContactMechPurpose.seed do |s|
  s.id = 24
  s.name = 'PHONE_QUICK'
  s.description = 'Quick Calls Phone Number'
  s.contact_mech_type_id = 2
end
ContactMechPurpose.seed do |s|
  s.id = 25
  s.name = 'MARKETING_EMAIL'
  s.description = 'Primary Marketing Email Address'
  s.contact_mech_type_id = 4
end
ContactMechPurpose.seed do |s|
  s.id = 26
  s.name = 'PRIMARY_EMAIL'
  s.description = 'Primary Email Address'
  s.contact_mech_type_id = 4
end
ContactMechPurpose.seed do |s|
  s.id = 27
  s.name = 'BILLING_EMAIL'
  s.description = 'Billing (AP) Email'
  s.contact_mech_type_id = 4
end
ContactMechPurpose.seed do |s|
  s.id = 28
  s.name = 'PAYMENT_EMAIL'
  s.description = 'Payment (AR) Email'
  s.contact_mech_type_id = 4
end
ContactMechPurpose.seed do |s|
  s.id = 29
  s.name = 'OTHER_EMAIL'
  s.description = 'Other Email Address'
  s.contact_mech_type_id = 4
end
ContactMechPurpose.seed do |s|
  s.id = 30
  s.name = 'SUPPORT_EMAIL'
  s.description = 'Support Email'
  s.contact_mech_type_id = 4
end
ContactMechPurpose.seed do |s|
  s.id = 31
  s.name = 'ORDER_EMAIL'
  s.description = 'Order Notification Email Address'
  s.contact_mech_type_id = 4
end
ContactMechPurpose.seed do |s|
  s.id = 32
  s.name = 'PRIMARY_WEB_URL'
  s.description = 'Primary Website URL'
  s.contact_mech_type_id = 7

end