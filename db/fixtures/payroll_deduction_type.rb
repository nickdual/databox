PayrollDeductionType.seed do |s|
  s.id = 1
  s.name = 'PDT_FED_TAX'
  s.description = 'Federal Tax'

end
PayrollDeductionType.seed do |s|
  s.id = 2
  s.name = 'PDT_STATE_TAX'
  s.description = 'State Tax'

end
PayrollDeductionType.seed do |s|
  s.id = 3
  s.name = 'PDT_INSURANCE'
  s.description = 'Insurance'

end
PayrollDeductionType.seed do |s|
  s.id = 4
  s.name = 'PDT_CAFE_PLAN'
  s.description = 'Cafeteria Plan'

end
