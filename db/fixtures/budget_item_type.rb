BudgetItemType.seed do |s|
  s.id = 1
  s.name = 'BIT_REQUIRED'
  s.description = 'Required'
end
BudgetItemType.seed do |s|
  s.id = 2
  s.name = 'BIT_DISCRETIONARY'
  s.description = 'Discretionary'
end