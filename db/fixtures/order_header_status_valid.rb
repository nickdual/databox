OrderHeaderStatusValid.seed do |s|
  s.id = 1
  s.status_id = 1
  s.to_status_id = 2
  s.transition_name = 'Make Open/Tentative'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 2
  s.status_id = 2
  s.to_status_id = 1
  s.transition_name = 'Begin Changes'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 3
  s.status_id = 2
  s.to_status_id = 3
  s.transition_name = 'Propose'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 4
  s.status_id = 2
  s.to_status_id = 4
  s.transition_name = 'Place'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 5
  s.status_id = 1
  s.to_status_id = 3
  s.transition_name = 'Propose'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 6
  s.status_id = 3
  s.to_status_id = 1
  s.transition_name = 'Begin Changes'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 7
  s.status_id = 3
  s.to_status_id = 4
  s.transition_name = 'Place'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 8
  s.status_id = 3
  s.to_status_id = 10
  s.transition_name = 'Reject'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 9
  s.status_id = 3
  s.to_status_id = 11
  s.transition_name = 'Cancel'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 10
  s.status_id = 1
  s.to_status_id = 4
  s.transition_name = 'Place'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 11
  s.status_id = 4
  s.to_status_id = 5
  s.transition_name = 'Process'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 12
  s.status_id = 4
  s.to_status_id = 6
  s.transition_name = 'Approve'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 13
  s.status_id = 4
  s.to_status_id = 9
  s.transition_name = 'Hold'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 14
  s.status_id = 4
  s.to_status_id = 10
  s.transition_name = 'Reject'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 15
  s.status_id = 4
  s.to_status_id = 11
  s.transition_name = 'Cancel'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 16
  s.status_id = 5
  s.to_status_id = 9
  s.transition_name = 'Hold'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 17
  s.status_id = 5
  s.to_status_id = 6
  s.transition_name = 'Approve'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 18
  s.status_id = 5
  s.to_status_id = 10
  s.transition_name = 'Reject'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 19
  s.status_id = 5
  s.to_status_id = 11
  s.transition_name = 'Cancel'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 20
  s.status_id = 6
  s.to_status_id = 7
  s.transition_name = 'Send'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 21
  s.status_id = 6
  s.to_status_id = 5
  s.transition_name = 'Process'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 22
  s.status_id = 6
  s.to_status_id = 8
  s.transition_name = 'Complete'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 23
  s.status_id = 6
  s.to_status_id = 11
  s.transition_name = 'Cancel'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 24
  s.status_id = 6
  s.to_status_id = 9
  s.transition_name = 'Hold'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 25
  s.status_id = 9
  s.to_status_id = 5
  s.transition_name = 'Process'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 26
  s.status_id = 9
  s.to_status_id = 6
  s.transition_name = 'Approve'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 27
  s.status_id = 9
  s.to_status_id = 11
  s.transition_name = 'Cancel'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 28
  s.status_id = 7
  s.to_status_id = 8
  s.transition_name = 'Order Completed'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 29
  s.status_id = 7
  s.to_status_id = 11
  s.transition_name = 'Order Cancelled'
end
OrderHeaderStatusValid.seed do |s|
  s.id = 30
  s.status_id = 8
  s.to_status_id = 6
  s.transition_name = 'Approve (un-complete)'
end