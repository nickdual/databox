ExamType.seed do |s|
  s.id = 1
  s.name = 'EXAM_WRITTEN_APT'
  s.description = 'Written Aptitude Exam'
end
ExamType.seed do |s|
  s.id = 2
  s.name = 'EXAM_TECHNICAL'
  s.description = 'Technical Exam'
end
ExamType.seed do |s|
  s.id = 3
  s.name = 'EXAM_GROUP_DISCN'
  s.description = 'Group Discussion'
end
