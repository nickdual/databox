WorkEffortVisibility.seed do |s|
  s.id = 1
  s.description = 'General (public access)'
  s.name = 'WES_PUBLIC'
end
WorkEffortVisibility.seed do |s|
  s.id = 2
  s.description = 'Restricted (private access)'
  s.name = 'WES_PRIVATE'
end
WorkEffortVisibility.seed do |s|
  s.id = 3
  s.description = 'Top Secret (confidential access)'
  s.name = 'WES_CONFIDENTIAL'
end