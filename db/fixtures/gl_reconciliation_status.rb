GlReconciliationStatus.seed do |s|
  s.id = 1
  s.status_id = 'GLREC_CREATED'
  s.description = 'Created'
  s.sequence_num = 1
end
GlReconciliationStatus.seed do |s|
  s.id = 2
  s.status_id = 'GLREC_RECONCILED'
  s.description = 'Reconciled'
  s.sequence_num = 2
end