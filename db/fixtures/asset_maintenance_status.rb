AssetMaintenanceStatus.seed do |s|
  s.id = 1
  s.description = 'Created'
  s.name = 'FAM_CREATED'
  s.sequence_num = 1
end
AssetMaintenanceStatus.seed do |s|
  s.id = 2
  s.description = 'Scheduled'
  s.name = 'FAM_SCHEDULED'
  s.sequence_num = 3
end
AssetMaintenanceStatus.seed do |s|
  s.id = 3
  s.description = 'In Process'
  s.name = 'FAM_IN_PROCESS'
  s.sequence_num = 5
end
AssetMaintenanceStatus.seed do |s|
  s.id = 4
  s.description = 'Completed'
  s.name = 'FAM_COMPLETED'
  s.sequence_num = 10
end
AssetMaintenanceStatus.seed do |s|
  s.id = 5
  s.description = 'Cancelled'
  s.name = 'FAM_CANCELLED'
  s.sequence_num = 99
end