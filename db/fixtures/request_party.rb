RequestParty.seed do |s|
  s.request_id = 1
  s.party_id = 1
  s.role_type_id = 1
  s.from_date = Time.zone.now
  s.thru_date = Time.zone.now.advance(:months => 1)
end
