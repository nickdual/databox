ProductReviewStatus.seed do |s|
  s.id = 1
  s.status_id = 'PRR_PENDING'
  s.description = 'Pending'
  s.sequence_num = 1
end
ProductReviewStatus.seed do |s|
  s.id = 2
  s.status_id = 'PRR_APPROVED'
  s.description = 'Approved'
  s.sequence_num = 2
end
ProductReviewStatus.seed do |s|
  s.id = 3
  s.status_id = 'PRR_DELETED'
  s.description = 'Deleted'
  s.sequence_num = 99
end

