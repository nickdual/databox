OrderSequence.seed do |s|
  s.id = 1
  s.name = 'ODRSQ_STANDARD'
  s.description = 'Standard (faster, may have gaps, per system)'
  s.sequence_num = 1
end
OrderSequence.seed do |s|
  s.id = 2
  s.name = 'Enforced Sequence (no gaps, per organization)'
  s.description = 'ODRSQ_ENF_SEQ'
  s.sequence_num =  2
end
