AssetStatusValid.seed do |s|
  s.id = 1
  s.status_id = 1
  s.to_status_id = 2
  s.transition_name = 'Order Arrived'
end
AssetStatusValid.seed do |s|
  s.id = 2
  s.status_id = 2
  s.to_status_id = 3
  s.transition_name = 'Promise'
end
AssetStatusValid.seed do |s|
  s.id = 3
  s.status_id = 2
  s.to_status_id = 7
  s.transition_name = 'Hold'
end
AssetStatusValid.seed do |s|
  s.id = 4
  s.status_id = 2
  s.to_status_id = 11
  s.transition_name = 'Mark As Defective'
end
AssetStatusValid.seed do |s|
  s.id = 5
  s.status_id = 2
  s.to_status_id = 8
  s.transition_name = 'Being Transfered'
end
AssetStatusValid.seed do |s|
  s.id = 6
  s.status_id = 7
  s.to_status_id = 2
  s.transition_name = 'Release Hold'
end
AssetStatusValid.seed do |s|
  s.id = 7
  s.status_id = 7
  s.to_status_id = 11
  s.transition_name = 'Mark Held Defective'
end
AssetStatusValid.seed do |s|
  s.id = 8
  s.status_id = 8
  s.to_status_id = 2
  s.transition_name = 'Transfer Complete'
end
AssetStatusValid.seed do |s|
  s.id = 9
  s.status_id = 3
  s.to_status_id = 2
  s.transition_name = 'Cancel Promise'
end
AssetStatusValid.seed do |s|
  s.id = 10
  s.status_id = 3
  s.to_status_id = 4
  s.transition_name = 'Deliver'
end
AssetStatusValid.seed do |s|
  s.id = 11
  s.status_id = 3
  s.to_status_id = 11
  s.transition_name = 'Mark As Defective'
end
AssetStatusValid.seed do |s|
  s.id = 12
  s.status_id = 3
  s.to_status_id = 9
  s.transition_name = 'Being Transfered (Promised)'
end
AssetStatusValid.seed do |s|
  s.id = 13
  s.status_id = 9
  s.to_status_id = 3
  s.transition_name = 'Transfer Complete (Promised)'
end
AssetStatusValid.seed do |s|
  s.id = 14
  s.status_id = 4
  s.to_status_id = 10
  s.transition_name = 'Return Status Pending'
end
AssetStatusValid.seed do |s|
  s.id = 15
  s.status_id = 10
  s.to_status_id = 2
  s.transition_name = 'Make Return Available'
end
AssetStatusValid.seed do |s|
  s.id = 16
  s.status_id = 10
  s.to_status_id = 7
  s.transition_name = 'Make Return Held'
end
AssetStatusValid.seed do |s|
  s.id = 17
  s.status_id = 10
  s.to_status_id = 11
  s.transition_name = 'Mark Return Defective'
end
AssetStatusValid.seed do |s|
  s.id = 18
  s.status_id = 4
  s.to_status_id = 5
  s.transition_name = 'Activate'
end
AssetStatusValid.seed do |s|
  s.id = 19
  s.status_id = 5
  s.to_status_id = 6
  s.transition_name = 'Deactivate'
end
AssetStatusValid.seed do |s|
  s.id = 20
  s.status_id = 5
  s.to_status_id = 10
  s.transition_name = 'Return Status Pending'
end
AssetStatusValid.seed do |s|
  s.id = 21
  s.status_id = 6
  s.to_status_id = 7
  s.transition_name = 'Hold Inactive'
end
AssetStatusValid.seed do |s|
  s.id = 22
  s.status_id = 6
  s.to_status_id = 10
  s.transition_name = 'Return Inactive'
end
