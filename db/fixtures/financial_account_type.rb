FinancialAccountType.seed do |s|
   s.description = 'Gift Certificate'
   s.fin_account_type_id = 'GIFTCERT_ACCOUNT'
   replenish_type_enum_id = 'FARP_MANUAL'
   is_refundable = 'N'
end
FinancialAccountType.seed do |s|
   s.description = 'Deposit Account'
   s.fin_account_type_id = 'DEPOSIT_ACCOUNT'
   replenish_type_enum_id = 'FARP_MANUAL'
   is_refundable = 'Y'
end
FinancialAccountType.seed do |s|
   s.description = 'Bank Account'
   s.fin_account_type_id = 'BANK_ACCOUNT'
   replenish_type_enum_id = 'FARP_MANUAL'
   is_refundable = 'N'
end
FinancialAccountType.seed do |s|
   s.description = 'Gift Certificate'
   s.fin_account_type_id = '"GIFTCERT_ACCOUNT'
   replenish_type_enum_id = 'FARP_MANUAL'
   is_refundable = 'Y'
   parent_type_id = 'DEPOSIT_ACCOUNT'
end
FinancialAccountType.seed do |s|
   s.description = 'Investment Account'
   s.fin_account_type_id = '"INVESTMENT_ACCOUNT'
   replenish_type_enum_id = 'FARP_MANUAL'
   parent_type_id = 'DEPOSIT_ACCOUNT'
   is_refundable = 'Y'
end
FinancialAccountType.seed do |s|
   s.description = 'Replenish Account'
   s.fin_account_type_id = 'REPLENISH_ACCOUNT'
   replenish_type_enum_id = 'FARP_AUTOMATIC'
   is_refundable = 'Y'
   parent_type_id = 'DEPOSIT_ACCOUNT'
end
FinancialAccountType.seed do |s|
   s.description = 'Service Credit Account'
   s.fin_account_type_id = 'SVCCRED_ACCOUNT'
   replenish_type_enum_id = 'FARP_MANUAL'
   is_refundable = 'N'
end
FinancialAccountType.seed do |s|
   s.description = 'Loan Account'
   s.fin_account_type_id = 'LOAN_ACCOUNT'
   replenish_type_enum_id = 'FARP_MANUAL'
   is_refundable = 'Y'
end
FinancialAccountType.seed do |s|
   s.description = 'Credit Card Account'
   s.fin_account_type_id = 'CREDIT_CARD_ACCOUNT'
   replenish_type_enum_id = 'FARP_MANUAL'
   is_refundable = 'Y'
   parent_type_id = 'LOAN_ACCOUNT'
end
FinancialAccountType.seed do |s|
   s.description = 'Equity Line Account'
   s.fin_account_type_id = 'EQUITY_LINE_ACCOUNT'
   replenish_type_enum_id = 'FARP_MANUAL'
   is_refundable = 'Y'
   parent_type_id = 'LOAN_ACCOUNT'
end