SupplierPreferredOrder.seed do |s|
  s.id = 1
  s.name = 'MAIN_SUPPL'
  s.description = 'Main Supplier'
  s.sequence_num = 1
end
SupplierPreferredOrder.seed do |s|
  s.id = 2
  s.name = 'ALT_SUPPL'
  s.description = 'Alternate Supplier'
  s.sequence_num = 2
end
