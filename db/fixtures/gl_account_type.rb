GlAccountType.seed do |s|
  s.id = 1
  s.name = 'ACCOUNTS_RECEIVABLE'
  s.description = 'Accounts Receivable'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 2
  s.name = 'ACCOUNTS_PAYABLE'
  s.description = 'Accounts Payable'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 3
  s.name = 'BALANCE_ACCOUNT'
  s.description = 'Balance'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 4
  s.name = 'BANK_STLMNT_ACCOUNT'
  s.description = 'Bank Settlement'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 5
  s.name = 'UNDEPOSITED_RECEIPTS'
  s.description = 'Undeposited Receipts'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 6
  s.name = 'MRCH_STLMNT_ACCOUNT'
  s.description = 'Merchant Account Settlement'
  s.parent_id = 1
end
GlAccountType.seed do |s|
  s.id = 7
  s.name = 'CURRENT_ASSET'
  s.description = 'Current Asset'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 8
  s.name = 'FIXED_ASSET'
  s.description = 'Fixed Asset'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 9
  s.name = 'FIXED_ASSET_MAINT'
  s.description = 'Fixed Asset Maintenance'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 10
  s.name = 'OTHER_ASSET'
  s.description = 'Other Asset'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 11
  s.name = 'CREDIT_CARD'
  s.description = 'Credit Card'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 12
  s.name = 'CURRENT_LIABILITY'
  s.description = 'Current Liability'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 13
  s.name = 'LONG_TERM_LIABILITY'
  s.description = 'Long Term Liability'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 14
  s.name = 'CUSTOMER_ACCOUNT'
  s.description = 'Customer'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 15
  s.name = 'SUPPLIER_ACCOUNT'
  s.description = 'Supplier'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 16
  s.name = 'PRODUCT_ACCOUNT'
  s.description = 'Product'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 17
  s.name = 'INVENTORY_ACCOUNT'
  s.description = 'Inventory'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 18
  s.name = 'INV_ADJ_AVG_COST'
  s.description = 'Inventory Adjustment from Average Cost'
  s.parent_id = 17
end
GlAccountType.seed do |s|
  s.id = 19
  s.name = 'INV_ADJ_VAL'
  s.description = 'Inventory Item Value Adjustment'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 20
  s.name = 'RAWMAT_INVENTORY'
  s.description = 'Raw Materials Inventory'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 21
  s.name = 'WIP_INVENTORY'
  s.description = 'Work In Progress Inventory'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 22
  s.name = 'TAX_ACCOUNT'
  s.description = 'Tax'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 23
  s.name = 'PROFIT_LOSS_ACCOUNT'
  s.description = 'Profit Loss'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 24
  s.name = 'SALES_ACCOUNT'
  s.description = 'Sales'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 25
  s.name = 'SALES_RETURNS'
  s.description = 'Customer Returns'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 26
  s.name = 'DISCOUNTS_ACCOUNT'
  s.description = 'Discounts'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 27
  s.name = 'COGS_ACCOUNT'
  s.description = 'Cost of Goods Sold'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 28
  s.name = 'COGS_ADJ_AVG_COST'
  s.description = 'COGS - Average Cost Adjustment'
  s.parent_id = 27
end
GlAccountType.seed do |s|
  s.id = 29
  s.name = 'PURCHASE_ACCOUNT'
  s.description = 'Purchase'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 30
  s.name = 'INV_CHANGE_ACCOUNT'
  s.description = 'Inventory Change'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 31
  s.name = 'EXPENSE'
  s.description = 'Expense'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 32
  s.name = 'OPERATING_EXPENSE'
  s.description = 'Operating Expense'
  s.parent_id = 31
end
GlAccountType.seed do |s|
  s.id = 33
  s.name = 'OTHER_EXPENSE'
  s.description = 'Other Expense'
  s.parent_id = 31
end
GlAccountType.seed do |s|
  s.id = 34
  s.name = 'OWNERS_EQUITY'
  s.description = "Owner's Equity"
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 35
  s.name = 'RETAINED_EARNINGS'
  s.description = 'Retained Earnings'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 36
  s.name = 'CUSTOMER_DEPOSIT'
  s.description = 'Customer Deposits'
  s.parent_id = 12
end
GlAccountType.seed do |s|
  s.id = 37
  s.name = 'CUSTOMER_CREDIT'
  s.description = 'Customer Credits'
  s.parent_id = 12
end
GlAccountType.seed do |s|
  s.id = 38
  s.name = 'CUSTOMER_GC_DEPOSIT'
  s.description = 'Customer Gift Certificate Balances'
  s.parent_id = 12
end
GlAccountType.seed do |s|
  s.id = 39
  s.name = 'UNINVOICED_SHIP_RCPT'
  s.description = 'Uninvoiced Shipment Receipts'
  s.parent_id = 12
end
GlAccountType.seed do |s|
  s.id = 40
  s.name = 'PURCHASE_PRICE_VAR'
  s.description = 'Purchase Price Variance'
  s.parent_id = 33
end
GlAccountType.seed do |s|
  s.id = 41
  s.name = 'INCOME'
  s.description = 'Income'
  s.parent_id = nil
end
GlAccountType.seed do |s|
  s.id = 42
  s.name = 'OTHER_INCOME'
  s.description = 'Other Income'
  s.parent_id = 41
end
GlAccountType.seed do |s|
  s.id = 43
  s.name = 'INTEREST_INCOME'
  s.description = 'Interest Income'
  s.parent_id = 41
end
GlAccountType.seed do |s|
  s.id = 44
  s.name = 'INTRSTINC_RECEIVABLE'
  s.description = 'Interest Income Receivables'
  s.parent_id = 1
end
GlAccountType.seed do |s|
  s.id = 45
  s.name = 'INTRSTINC_RECEIVABLE'
  s.description = 'Interest Income Receivables'
  s.parent_id = 1
end
GlAccountType.seed do |s|
  s.id = 46
  s.name = 'PREPAID_EXPENSES'
  s.description = 'Prepaid Expenses'
  s.parent_id = 7
end
GlAccountType.seed do |s|
  s.id = 47
  s.name = 'INVENTORY_XFER_OUT'
  s.description = 'Receivable from Inventory Transferred Out'
  s.parent_id = 7
end
GlAccountType.seed do |s|
  s.id = 48
  s.name = 'INVENTORY_XFER_IN'
  s.description = 'Payable from Inventory Transferred In'
  s.parent_id = 12
end
GlAccountType.seed do |s|
  s.id = 49
  s.name = 'ACCPAYABLE_UNAPPLIED'
  s.description = 'Accounts Payable - Unapplied Payments'
  s.parent_id = 2
end
GlAccountType.seed do |s|
  s.id = 50
  s.name = 'ACCREC_UNAPPLIED'
  s.description = 'Accounts Receivable - Unapplied Payments'
  s.parent_id = 1
end
GlAccountType.seed do |s|
  s.id = 51
  s.name = 'COMMISSION_EXPENSE'
  s.description = 'Commission Expense'
  s.parent_id = 32
end
GlAccountType.seed do |s|
  s.id = 52
  s.name = 'COMMISSIONS_PAYABLE'
  s.description = 'Commissions Payables'
  s.parent_id = 12
end
GlAccountType.seed do |s|
  s.id = 53
  s.name = 'WRITEOFF'
  s.description = 'Write Off'
  s.parent_id = 33
end
GlAccountType.seed do |s|
  s.id = 54
  s.name = 'ACCTRECV_WRITEOFF'
  s.description = 'Accounts Receivables Write Off'
  s.parent_id = 53
end
GlAccountType.seed do |s|
  s.id = 55
  s.name = 'ACCTPAY_WRITEOFF'
  s.description = 'Accounts Payables Write Off'
  s.parent_id = 53
end
GlAccountType.seed do |s|
  s.id = 56
  s.name = 'COMMISSIONS_WRITEOFF'
  s.description = 'Commissions Payables Write Off'
  s.parent_id = 53
end
GlAccountType.seed do |s|
  s.id = 57
  s.name = 'INTRSTINC_WRITEOFF'
  s.description = 'Interest Income Write Off'
  s.parent_id = 53
end
GlAccountType.seed do |s|
  s.id = 58
  s.name = 'FX_GAIN_LOSS_ACCT'
  s.description = 'Foreign Exchange Gain/Loss'
  s.parent_id = 33
end
GlAccountType.seed do |s|
  s.id = 59
  s.name = 'FX_GAIN_ACCOUNT'
  s.description = 'Foreign Exchange Gain'
  s.parent_id = 58
end
GlAccountType.seed do |s|
  s.id = 60
  s.name = 'FX_LOSS_ACCOUNT'
  s.description = 'Foreign Exchange Loss'
  s.parent_id = 58
end
