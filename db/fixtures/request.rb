Request.seed do |s|
  s.request_id = 1
  s.request_type_enum_id = 1
  s.request_category_id = 1
  s.status_id = 1
  s.request_date = Time.zone.now
  s.response_required_date = Time.zone.now.advance(:months => 1)
end