AssetStatus.seed do |s|
  s.id = 1
  s.status_id = 'AST_ON_ORDER'
  s.description = 'On Order'
  s.sequence_num = 1
end
AssetStatus.seed do |s|
  s.id = 2
  s.status_id = 'AST_AVAILABLE'
  s.description = 'Available'
  s.sequence_num = 2
end
AssetStatus.seed do |s|
  s.id = 3
  s.status_id = 'AST_PROMISED'
  s.description = 'Promised'
  s.sequence_num = 3
end
AssetStatus.seed do |s|
  s.id = 4
  s.status_id = 'AST_DELIVERED'
  s.description = 'Delivered'
  s.sequence_num = 4
end
AssetStatus.seed do |s|
  s.id = 5
  s.status_id = 'AST_ACTIVATED'
  s.description = 'Activated'
  s.sequence_num = 5
end
AssetStatus.seed do |s|
  s.id = 6
  s.status_id = 'AST_DEACTIVATED'
  s.description = 'Deactivated'
  s.sequence_num = 6
end
AssetStatus.seed do |s|
  s.id = 7
  s.status_id = 'AST_ON_HOLD'
  s.description = 'On Hold'
  s.sequence_num = 7
end
AssetStatus.seed do |s|
  s.id = 8
  s.status_id = 'AST_BEING_TRANS'
  s.description = 'Being Transferred'
  s.sequence_num = 10
end
AssetStatus.seed do |s|
  s.id = 9
  s.status_id = 'AST_BEING_TRANS_PRM'
  s.description = 'Being Transferred (Promised)'
  s.sequence_num = 11
end
AssetStatus.seed do |s|
  s.id = 10
  s.status_id = 'AST_RETURNED'
  s.description = 'Returned'
  s.sequence_num = 20
end
AssetStatus.seed do |s|
  s.id = 11
  s.status_id = 'AST_DEFECTIVE'
  s.description = 'Defective'
  s.sequence_num = 21
end