ContactListPartyStatusValid.seed do |s|
  s.status_id = 'CLPT_PENDING'
  s.to_status_id = 'CLPT_ACCEPTED'
  s.transition_name = 'Accept'
end
ContactListPartyStatusValid.seed do |s|
  s.status_id = 'CLPT_PENDING'
  s.to_status_id = 'CLPT_REJECTED'
  s.transition_name = 'Reject'
end
ContactListPartyStatusValid.seed do |s|
  s.status_id = 'CLPT_ACCEPTED'
  s.to_status_id = 'CLPT_REJECTED'
  s.transition_name = 'Reject'
end
ContactListPartyStatusValid.seed do |s|
  s.status_id = 'CLPT_REJECTED'
  s.to_status_id = 'CLPT_ACCEPTED'
  s.transition_name = 'Accept'
end
ContactListPartyStatusValid.seed do |s|
  s.status_id = 'CLPT_REJECTED'
  s.to_status_id = 'CLPT_PENDING'
  s.transition_name = 'Pending Accept'
end
ContactListPartyStatusValid.seed do |s|
  s.status_id = 'CLPT_ACCEPTED'
  s.to_status_id = 'CLPT_IN_USE'
  s.transition_name = 'Locked - In Use'
end
ContactListPartyStatusValid.seed do |s|
  s.status_id = 'CLPT_ACCEPTED'
  s.to_status_id = 'CLPT_INVALID'
  s.transition_name = 'Invalid'
end
ContactListPartyStatusValid.seed do |s|
  s.status_id = 'CLPT_ACCEPTED'
  s.to_status_id = 'CLPT_UNSUBS_PENDING'
  s.transition_name = 'Subscription Accepted - Unsubscription Pending'
end
ContactListPartyStatusValid.seed do |s|
  s.status_id = 'CLPT_PENDING'
  s.to_status_id = 'CLPT_UNSUBS_PENDING'
  s.transition_name = 'Subscription Pending - Unsubscription Pending'
end
ContactListPartyStatusValid.seed do |s|
  s.status_id = 'CLPT_IN_USE'
  s.to_status_id = 'CLPT_UNSUBS_PENDING'
  s.transition_name = 'Subscription In Use - Unsubscription Pending'
end
ContactListPartyStatusValid.seed do |s|
  s.status_id = 'CLPT_INVALID'
  s.to_status_id = 'CLPT_UNSUBS_PENDING'
  s.transition_name = 'Subscription Invalid - Unsubscription Pending'
end
ContactListPartyStatusValid.seed do |s|
  s.status_id = 'CLPT_ACCEPTED'
  s.to_status_id = 'CLPT_UNSUBSCRIBED'
  s.transition_name = 'Subscription Accepted - Unsubscribed'
end
ContactListPartyStatusValid.seed do |s|
  s.status_id = 'CLPT_PENDING'
  s.to_status_id = 'CLPT_UNSUBSCRIBED'
  s.transition_name = 'Subscription Accepted - Unsubscribed'
end
ContactListPartyStatusValid.seed do |s|
  s.status_id = 'CLPT_IN_USE'
  s.to_status_id = 'CLPT_UNSUBSCRIBED'
  s.transition_name = 'Subscription In Use - Unsubscribed'
end
ContactListPartyStatusValid.seed do |s|
  s.status_id = 'CLPT_INVALID'
  s.to_status_id = 'CLPT_UNSUBSCRIBED'
  s.transition_name = 'Subscription Invalid - Unsubscribed'
end
