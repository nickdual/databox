TaxForm.seed do |s|
  s.id = 1
  s.name = 'US_IRS_1120'
  s.description = 'Form 1120 (US IRS)'
  s.sequence_num = 1
end
TaxForm.seed do |s|
  s.id = 2
  s.name = 'US_IRS_1120S'
  s.description = 'Form 1120S (US IRS)'
  s.sequence_num =  2
end
TaxForm.seed do |s|
  s.id = 3
  s.name = 'US_IRS_1065'
  s.description = 'Form 1065 (US IRS)'
  s.sequence_num =  3
end
TaxForm.seed do |s|
  s.id = 4
  s.name = 'US_IRS_990'
  s.description = 'Form 990 (US IRS)'
  s.sequence_num =   4
end
TaxForm.seed do |s|
  s.id = 5
  s.name = 'US_IRS_990PF'
  s.description = 'Form 990-PF (US IRS)'
  s.sequence_num =  5
  end
TaxForm.seed do |s|
  s.id = 6
  s.name = 'US_IRS_990T'
  s.description = 'Form 990-T (US IRS)'
  s.sequence_num =  6
  end
TaxForm.seed do |s|
  s.id = 7
  s.name = 'US_IRS_1040'
  s.description = 'Form 1040 (US IRS)'
  s.sequence_num =  7
end