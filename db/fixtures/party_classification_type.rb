PartyClassificationType.seed do |s|
  s.id = 1
  s.name = 'EEOC_CLASSIFICATION'
  s.description = 'EEOC (Equal Opportunity)'
  s.parent_id = nil
end
PartyClassificationType.seed do |s|
  s.id = 2
  s.name = 'MINORITY_CLASSIFICAT'
  s.description = 'Minority'
  s.parent_id = nil
end
PartyClassificationType.seed do |s|
  s.id = 3
  s.name = 'INCOME_CLASSIFICATIO'
  s.description = 'Income'
  s.parent_id = nil
end
PartyClassificationType.seed do |s|
  s.id = 4
  s.name = 'INDUSTRY_CLASSIFICAT'
  s.description = 'Industry'
  s.parent_id = nil
end
PartyClassificationType.seed do |s|
  s.id = 5
  s.name = 'ORGANIZATION_CLASSIF'
  s.description = 'Organization'
  s.parent_id = nil
end
PartyClassificationType.seed do |s|
  s.id = 6
  s.name = 'PERSON_CLASSIFICATIO'
  s.description = 'Person'
  s.parent_id = nil
end
PartyClassificationType.seed do |s|
  s.id = 7
  s.name = 'SIZE_CLASSIFICATION'
  s.description = 'Size'
  s.parent_id = nil
end
PartyClassificationType.seed do |s|
  s.id = 8
  s.name = 'TRADE_CLASSIFICATION'
  s.description = 'Trade'
  s.parent_id = nil 
end
PartyClassificationType.seed do |s|
  s.id = 9
  s.name = 'TRADE_WHOLE_CLASSIFI'
  s.description = 'Wholesale'
  s.parent_id = 8
end
PartyClassificationType.seed do |s|
  s.id = 10
  s.name = 'TRADE_RETAIL_CLASSIF'
  s.description = 'Retail'
  s.parent_id = 8
end
PartyClassificationType.seed do |s|
  s.id = 11
  s.name = 'ANNUAL_REVENUE'
  s.description = 'Annual Revenue'
  s.parent_id = nil
end
PartyClassificationType.seed do |s|
  s.id = 12
  s.name = 'NUMBER_OF_EMPLOYEES'
  s.description = 'Number of Employees'
  s.parent_id = nil
end
PartyClassificationType.seed do |s|
  s.id = 13
  s.name = 'VALUE_RATING'
  s.description = 'Value Rating'
  s.parent_id = nil
end
PartyClassificationType.seed do |s|
  s.id = 14
  s.name = 'SIC_CODE'
  s.description = 'SIC Code'
  s.parent_id = nil
end
PartyClassificationType.seed do |s|
  s.id = 15
  s.name = 'OWNERSHIP'
  s.description = 'Ownership'
  s.parent_id = nil
end