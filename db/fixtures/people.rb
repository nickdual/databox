Person.seed do |s|
  s.id = 1
  s.birth_date = '1982-12-31 00:00:00'
  s.first_name = 'Moms'
  s.last_name = 'Maass'
  s.gender = 'Male'
  s.party_id = 1
end
Person.seed do |s|
  s.id = 2
  s.birth_date = '1962-01-31 01:20:30'
  s.first_name = 'Lorin'
  s.last_name = 'Maathai'
  s.gender = 'Male'
  s.party_id = 2
end
Person.seed do |s|
  s.id = 3
  s.birth_date = '1971-02-13 10:30:11'
  s.first_name = 'Luke'
  s.last_name = 'Mably'
  s.gender = 'Male'
  s.party_id = 3
end
Person.seed do |s|
  s.id = 4
  s.birth_date = '1952-05-12 11:30:01'
  s.first_name = 'Vicki'
  s.last_name = 'Mabrey'
  s.gender = 'Male'
  s.party_id = 4
end
Person.seed do |s|
  s.id = 5
  s.birth_date = '1967-07-17 05:35:25'
  s.first_name = 'Matthew'
  s.last_name = 'Macfadyen'
  s.gender = 'Male'
  s.party_id = 5
end
Person.seed do |s|
  s.id = 6
  s.birth_date = '1978-08-29 11:10:03'
  s.first_name = 'Jackson'
  s.last_name = 'Farlane'
  s.gender = 'Male'
  s.party_id = 6
end
Person.seed do |s|
  s.id = 7
  s.birth_date = '1989-03-23 12:27:57'
  s.first_name = 'Steven'
  s.last_name = 'Gibbon'
  s.gender = 'Male'
  s.party_id = 7
end
Person.seed do |s|
  s.id = 8
  s.birth_date = '1986-05-19 04:46:49'
  s.first_name = 'Edward'
  s.last_name = 'Ginnis'
  s.gender = 'Male'
  s.party_id = 8
end
Person.seed do |s|
  s.id = 9
  s.birth_date = '1973-03-02 14:57:46'
  s.first_name = 'Kenneth'
  s.last_name = 'Gowan'
  s.gender = 'Male'
  s.party_id = 9
end
Person.seed do |s|
  s.id = 10
  s.birth_date = '1973-10-11 21:18:56'
  s.first_name = 'Anthony'
  s.last_name = 'Graw'
  s.gender = 'Male'
  s.party_id = 10
end
Person.seed do |s|
  s.id = 11
  s.birth_date = '1973-01-30 18:19:47'
  s.first_name = 'Matthew'
  s.last_name = 'Gregor'
  s.gender = 'Male'
  s.party_id = 11
end
Person.seed do |s|
  s.id = 12
  s.birth_date = '1975-10-19 22:19:51'
  s.first_name = 'Larry'
  s.last_name = 'Mach'
  s.gender = 'Male'
  s.party_id = 12
end
Person.seed do |s|
  s.id = 13
  s.birth_date = '1975-05-18 22:26:30'
  s.first_name = 'Raymond'
  s.last_name = 'Assis'
  s.gender = 'Male'
  s.party_id = 13
end
Person.seed do |s|
  s.id = 14
  s.birth_date = '1991-04-10 20:37:32'
  s.first_name = 'Joshua'
  s.last_name = 'Machado'
  s.gender = 'Male'
  s.party_id = 14
end
Person.seed do |s|
  s.id = 15
  s.birth_date = '1970-04-19 08:29:23'
  s.first_name = 'Jerry'
  s.last_name = 'Machel'
  s.gender = 'Male'
  s.party_id = 15
end
Person.seed do |s|
  s.id = 16
  s.birth_date = '1984-01-20 05:28:25'
  s.first_name = 'Dennis'
  s.last_name = 'Machen'
  s.gender = 'Male'
  s.party_id = 16
end
Person.seed do |s|
  s.id = 17
  s.birth_date = '1986-08-05 05:28:33'
  s.first_name = 'Harold'
  s.last_name = 'Machiavelli'
  s.gender = 'Male'
  s.party_id = 17
end
Person.seed do |s|
  s.id = 18
  s.birth_date = '1981-04-01 23:15:24'
  s.first_name = 'Douglas'
  s.last_name = 'Macht'
  s.gender = 'Male'
  s.party_id = 18
end
Person.seed do |s|
  s.id = 19
  s.birth_date = '1974-06-24 20:43:17'
  s.first_name = 'Arthur'
  s.last_name = 'Machtley'
  s.gender = 'Male'
  s.party_id = 19
end
Person.seed do |s|
  s.id = 20
  s.birth_date = '1978-03-11 22:37:05'
  s.first_name = 'Ryan'
  s.last_name = 'Innis'
  s.gender = 'Male'
  s.party_id = 20
end
Person.seed do |s|
  s.id = 21
  s.birth_date = '1988-06-17 07:51:33'
  s.first_name = 'Roger'
  s.last_name = 'Morrison'
  s.gender = 'Male'
  s.party_id = 21
end
Person.seed do |s|
  s.id = 22
  s.birth_date = '1983-09-28 01:56:24'
  s.first_name = 'Jonathan'
  s.last_name = 'Mack'
  s.gender = 'Male'
  s.party_id = 22
end
Person.seed do |s|
  s.id = 23
  s.birth_date = '1988-03-30 13:31:24'
  s.first_name = 'Terry'
  s.last_name = 'Mackail'
  s.gender = 'Male'
  s.party_id = 23
end
Person.seed do |s|
  s.id = 24
  s.birth_date = '1993-02-18 08:23:57'
  s.first_name = 'Samuel'
  s.last_name = 'Mackay'
  s.gender = 'Male'
  s.party_id = 24
end
Person.seed do |s|
  s.id = 25
  s.birth_date = '1986-11-11 15:36:11'
  s.first_name = 'Ralph'
  s.last_name = 'MacKaye'
  s.gender = 'Male'
  s.party_id = 25
end
Person.seed do |s|
  s.id = 26
  s.birth_date = '1987-09-27 15:05:30'
  s.first_name = 'Benjamin'
  s.last_name = 'Kenzie'
  s.gender = 'Male'
  s.party_id = 26
end
Person.seed do |s|
  s.id = 27
  s.birth_date = '1975-11-14 07:04:01'
  s.first_name = 'Brandon'
  s.last_name = 'Mackey'
  s.gender = 'Male'
  s.party_id = 27
end
Person.seed do |s|
  s.id = 28
  s.birth_date = '1982-12-3100:00:00'
  s.first_name = 'Wayne'
  s.last_name = 'Lachlan'
  s.gender = 'Male'
  s.party_id = 28
end
Person.seed do |s|
  s.id = 29
  s.birth_date = '1975-04-23 14:05:23'
  s.first_name = 'Louis'
  s.last_name = 'Lane'
  s.gender = 'Male'
  s.party_id = 29
end
Person.seed do |s|
  s.id = 30
  s.birth_date = '1972-09-19 07:05:52'
  s.first_name = 'Aaron'
  s.last_name = 'Macleish'
  s.gender = 'Male'
  s.party_id = 30
end
Person.seed do |s|
  s.id = 31
  s.birth_date = '1988-10-01 15:44:06'
  s.first_name = 'Eugene'
  s.last_name = 'Leod'
  s.gender = 'Male'
  s.party_id = 31
end
Person.seed do |s|
  s.id = 32
  s.birth_date = '1980-11-11 07:54:09'
  s.first_name = 'Todd'
  s.last_name = 'Mahon'
  s.gender = 'Male'
  s.party_id = 32
end
Person.seed do |s|
  s.id = 33
  s.birth_date = '1984-12-15 02:47:23'
  s.first_name = 'Sean'
  s.last_name = 'Millan'
  s.gender = 'Male'
  s.party_id = 33
end