FacilityLocationType.seed do |s|
  s.id = 1
  s.name = 'FLT_PICKLOC'
  s.description = 'Pick/Primary'
end
FacilityLocationType.seed do |s|
  s.id = 2
  s.name = 'FLT_BULK'
  s.description = 'Bulk'
end