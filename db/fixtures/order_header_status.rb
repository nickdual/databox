OrderHeaderStatus.seed do |s|
  s.id = 1
  s.status_id = 'ORDER_BEING_CHANGED'
  s.description = 'Being Changed'
  s.sequence_num = 0
end
OrderHeaderStatus.seed do |s|
  s.id = 2
  s.status_id = 'ORDER_OPEN'
  s.description = 'Open (Tentative)'
  s.sequence_num = 1
end
OrderHeaderStatus.seed do |s|
  s.id = 3
  s.status_id = 'ORDER_PROPOSED'
  s.description = 'Proposed by Vendor'
  s.sequence_num = 2
end
OrderHeaderStatus.seed do |s|
  s.id = 4
  s.status_id = 'ORDER_PLACED'
  s.description = 'Placed (Accepted by Customer)'
  s.sequence_num = 3
end
OrderHeaderStatus.seed do |s|
  s.id = 5
  s.status_id = 'ORDER_PROCESSING'
  s.description = 'Processing'
  s.sequence_num = 11
end
OrderHeaderStatus.seed do |s|
  s.id = 6
  s.status_id = 'ORDER_APPROVED'
  s.description = 'Approved by Vendor'
  s.sequence_num = 12
end
OrderHeaderStatus.seed do |s|
  s.id = 7
  s.status_id = 'ORDER_SENT'
  s.description = 'Sent'
  s.sequence_num = 13
end
OrderHeaderStatus.seed do |s|
  s.id = 8
  s.status_id = 'ORDER_COMPLETED'
  s.description = 'Completed'
  s.sequence_num = 21
end
OrderHeaderStatus.seed do |s|
  s.id = 9
  s.status_id = 'ORDER_HOLD'
  s.description = 'Held'
  s.sequence_num = 22
end
OrderHeaderStatus.seed do |s|
  s.id = 10
  s.status_id = 'ORDER_REJECTED'
  s.description = 'Rejected'
  s.sequence_num = 98
end
OrderHeaderStatus.seed do |s|
  s.id = 11
  s.status_id = 'ORDER_CANCELLED'
  s.description = 'Cancelled'
  s.sequence_num = 99
end
OrderHeaderStatus.seed do |s|
  s.id = 12
  s.status_id = 'ORDER_WISH_LIST'
  s.description = 'Wish List'
  s.sequence_num = 101
end
OrderHeaderStatus.seed do |s|
  s.id = 13
  s.status_id = 'ORDER_GIFT_REGISTRY'
  s.description = 'Gift Registry'
  s.sequence_num = 102
end
OrderHeaderStatus.seed do |s|
  s.id = 14
  s.status_id = 'ORDER_AUTO_REORDER'
  s.description = 'Auto Reorder'
  s.sequence_num = 103
end
OrderHeaderStatus.seed do |s|
  s.id = 15
  s.status_id = 'ORDER_AUTO_REORDER'
  s.description = 'Auto Reorder'
  s.sequence_num = 103
end