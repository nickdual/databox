ShipmentItemPickStatus.seed do |s|
  s.id = 1
  s.description = 'Pending'
  s.name = 'PICKITEM_PENDING'
  s.sequence_num = 1
end
ShipmentItemPickStatus.seed do |s|
  s.id = 2
  s.description = 'Picked'
  s.name = 'PICKITEM_PICKED'
  s.sequence_num = 5
end
ShipmentItemPickStatus.seed do |s|
  s.id = 3
  s.description = 'Packed'
  s.name = 'PICKITEM_PACKED'
  s.sequence_num = 9
end
ShipmentItemPickStatus.seed do |s|
  s.id = 4
  s.description = 'Cancelled'
  s.name = 'PICKITEM_CANCELLED'
  s.sequence_num = 99
end