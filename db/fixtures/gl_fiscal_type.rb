
GlFiscalType.seed do |s|
  s.id = 1
  s.name = 'GLFT_ACTUAL'
  s.description = 'Actual'
end
GlFiscalType.seed do |s|
  s.id = 2
  s.name = 'GLFT_BUDGET'
  s.description = 'Budget'
end
GlFiscalType.seed do |s|
  s.id = 3
  s.name = 'GLFT_FORECAST'
  s.description = 'Forecast'
end
GlFiscalType.seed do |s|
  s.id = 4
  s.name = 'GLFT_PLAN'
  s.description = 'Plan'
end
GlFiscalType.seed do |s|
  s.id = 5
  s.name = 'GLFT_SCENARIO'
  s.description = 'Scenario'
end
