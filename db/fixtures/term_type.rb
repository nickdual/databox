TermType.seed do |s|
  s.id = 1
  s.description = 'Financial'
  s.name = 'FINANCIAL_TERM'
  s.parent_id = nil
end
TermType.seed do |s|
  s.id = 2
  s.description = 'Payment (net days)'
  s.name = 'FIN_PAYMENT_TERM'
  s.parent_id = 1
end
TermType.seed do |s|
  s.id = 3
  s.description = 'Payment net days, part 1'
  s.name = 'FIN_PAY_NETDAYS_1'
  s.parent_id = 2
end
TermType.seed do |s|
  s.id = 4
  s.description = 'Payment net days, part 2'
  s.name = 'FIN_PAY_NETDAYS_2'
  s.parent_id = 2
end
TermType.seed do |s|
  s.id = 5
  s.description = 'Payment net days, part 3'
  s.name = 'FIN_PAY_NETDAYS_3'
  s.parent_id = 2
end
TermType.seed do |s|
  s.id = 6
  s.description = 'Payment (discounted if paid within specified days)'
  s.name = 'FIN_PAYMENT_DISC'
  s.parent_id = 1
end
TermType.seed do |s|
  s.id = 7
  s.description = 'Payment (due on specified day of month)'
  s.name = 'FIN_PAYMENT_FIXDAY'
  s.parent_id = 1
end
TermType.seed do |s|
  s.id = 8
  s.description = 'Late Fee (percent)'
  s.name = 'FIN_LATE_FEE_TERM'
  s.parent_id = 1
end
TermType.seed do |s|
  s.id = 9
  s.description = 'Penalty For Collection Agency'
  s.name = 'FIN_COLLECT_TERM'
  s.parent_id = 1
end
TermType.seed do |s|
  s.id = 10
  s.description = 'Non-Returnable Sales Item'
  s.name = 'FIN_NORTN_ITEM_TERM'
  s.parent_id = 1
end
TermType.seed do |s|
  s.id = 11
  s.description = 'Incentive'
  s.name = 'INCENTIVE'
  s.parent_id = nil
end
TermType.seed do |s|
  s.id = 12
  s.description = 'Legal'
  s.name = 'LEGAL_TERM'
  s.parent_id = nil
end
TermType.seed do |s|
  s.id = 13
  s.description = 'Threshold'
  s.name = 'THRESHOLD'
  s.parent_id = nil
end
TermType.seed do |s|
  s.id = 14
  s.description = 'Clause For Renewal'
  s.name = 'CLAUSE_FOR_RENEWAL'
  s.parent_id = nil
end
TermType.seed do |s|
  s.id = 15
  s.description = 'Agreement Termination'
  s.name = 'AGREEMENT_TERMINATIO'
  s.parent_id = nil
end
TermType.seed do |s|
  s.id = 16
  s.description = 'Indemnification'
  s.name = 'INDEMNIFICATION'
  s.parent_id = nil
end
TermType.seed do |s|
  s.id = 17
  s.description = 'Non-Compete'
  s.name = 'NON_COMPETE'
  s.parent_id = nil
end
TermType.seed do |s|
  s.id = 18
  s.description = 'Exclusive Relationship'
  s.name = 'EXCLUSIVE_RELATIONSH'
  s.parent_id = nil
end
TermType.seed do |s|
  s.id = 19
  s.description = 'Commission'
  s.name = 'COMMISSION_TERM'
  s.parent_id = nil
end
TermType.seed do |s|
  s.id = 20
  s.description = 'Commission Term Fixed Per Unit'
  s.name = 'FIN_COMM_FIXED'
  s.parent_id = 19
end
TermType.seed do |s|
  s.id = 21
  s.description = 'Commission Term Variable'
  s.name = 'FIN_COMM_VARIABLE'
  s.parent_id = 19
end
TermType.seed do |s|
  s.id = 22
  s.description = 'Commission Term Minimum Per Unit'
  s.name = 'FIN_COMM_MIN'
  s.parent_id = 19
end
TermType.seed do |s|
  s.id = 23
  s.description = 'Commission Term Maximum Per Unit'
  s.name = 'FIN_COMM_MAX'
  s.parent_id = 19
end
TermType.seed do |s|
  s.id = 24
  s.description = 'Incoterm'
  s.name = 'INCO_TERM'
  s.parent_id = nil
end
TermType.seed do |s|
  s.id = 25
  s.description = 'Incoterm Ex Works'
  s.name = 'EXW'
  s.parent_id = 24
end
TermType.seed do |s|
  s.id = 26
  s.description = 'Incoterm Free Carrier'
  s.name = 'FCA'
  s.parent_id = 24
end
TermType.seed do |s|
  s.id = 27
  s.description = 'Incoterm Free Alongside Ship'
  s.name = 'FAS'
  s.parent_id = 24
end
TermType.seed do |s|
  s.id = 28
  s.description = 'Incoterm Free On Board'
  s.name = 'FOB'
  s.parent_id = 24
end
TermType.seed do |s|
  s.id = 29
  s.description = 'Incoterm Cost and Freight'
  s.name = 'CFR'
  s.parent_id = 24
end
TermType.seed do |s|
  s.id = 30
  s.description = 'Incoterm Cost, Insurance and Freight'
  s.name = 'CIF'
  s.parent_id = 24
end
TermType.seed do |s|
  s.id = 31
  s.description = 'Incoterm Carriage Paid To'
  s.name = 'CPT'
  s.parent_id = 24
end
TermType.seed do |s|
  s.id = 32
  s.description = 'Incoterm Carriage and Insurance Paid to'
  s.name = 'CIP'
  s.parent_id = 24
end
TermType.seed do |s|
  s.id = 33
  s.description = 'Incoterm Delivered At Frontier'
  s.name = 'DAF'
  s.parent_id = 24
end
TermType.seed do |s|
  s.id = 34
  s.description = 'Incoterm Delivered Ex Ship'
  s.name = 'DES'
  s.parent_id = 24
end
TermType.seed do |s|
  s.id = 35
  s.description = 'Incoterm Delivered Ex Quay'
  s.name = 'DEQ'
  s.parent_id = 24
end
TermType.seed do |s|
  s.id = 36
  s.description = 'Incoterm Delivered Duty Unpaid'
  s.name = 'DDU'
  s.parent_id = 24
end
TermType.seed do |s|
  s.id = 37
  s.description = 'Incoterm Delivered Duty Paid'
  s.name = 'DDP'
  s.parent_id = 24
end
TermType.seed do |s|
  s.id = 38
  s.description = 'Purchasing'
  s.name = 'PURCHASING'
  s.parent_id = nil
end
TermType.seed do |s|
  s.id = 39
  s.description = 'Vendor Customer ID'
  s.name = 'PURCH_VENDOR_ID'
  s.parent_id = 38
end
TermType.seed do |s|
  s.id = 40
  s.description = 'Preferred Freight'
  s.name = 'PURCH_FREIGHT'
  s.parent_id = 38
end
TermType.seed do |s|
  s.id = 41
  s.description = 'Other'
  s.name = 'OTHER_TERM'
  s.parent_id = 38
end