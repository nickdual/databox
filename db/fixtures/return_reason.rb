ReturnReason.seed do |s|
  s.id = 1
  s.name = 'RTN_NOT_WANT'
  s.description = 'Did Not Want'
end
ReturnReason.seed do |s|
  s.id = 2
  s.name = 'RTN_DEFECTIVE_ITEM'
  s.description = 'Defective'
end
ReturnReason.seed do |s|
  s.id = 3
  s.name = 'RTN_MISSHIP_ITEM'
  s.description = 'Mis-Shipped'
end
ReturnReason.seed do |s|
  s.id = 4
  s.name = 'RTN_DIG_FILL_FAIL'
  s.description = 'Digital Fulfillment Failed'
end
ReturnReason.seed do |s|
  s.id = 5
  s.name = 'RTN_COD_REJECT'
  s.description = 'COD Payment Rejected'
end
ReturnReason.seed do |s|
  s.id = 6
  s.name = 'RTN_SIZE_EXCHANGE'
  s.description = 'Size Exchange'
end