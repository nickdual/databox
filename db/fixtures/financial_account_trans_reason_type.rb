FinancialAccountTransReasonType.seed do |s|
   s.name = 'FATR_PURCHASE'
   s.description = 'Purchase'
end
FinancialAccountTransReasonType.seed do |s|
   s.name = 'FATR_IDEPOSIT'
   s.description = 'Initial Deposit'
end
FinancialAccountTransReasonType.seed do |s|
   s.name = 'FATR_REPLENISH'
   s.description = 'Replenishment'
end
FinancialAccountTransReasonType.seed do |s|
   s.name = 'FATR_REFUND'
   s.description = 'Refund'
end