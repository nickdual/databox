PaymentStatus.seed do |s|
  s.status_id = 'PMNT_PROPOSED'
  s.description = 'Proposed'
  s.sequence_num = 1
end
PaymentStatus.seed do |s|
  s.status_id = 'PMNT_PROMISED'
  s.description = 'Promised'
  s.sequence_num = 2
end
PaymentStatus.seed do |s|
  s.status_id = 'PMNT_AUTHORIZED'
  s.description = 'Authorized'
  s.sequence_num = 3
end
PaymentStatus.seed do |s|
  s.status_id = 'PMNT_SENT'
  s.description = 'Sent/Settled'
  s.sequence_num = 4
end
PaymentStatus.seed do |s|
  s.status_id = 'PMNT_RECEIVED'
  s.description = 'Received'
  s.sequence_num = 5
end
PaymentStatus.seed do |s|
  s.status_id = 'PMNT_CONFIRMED'
  s.description = 'Confirmed Paid'
  s.sequence_num = 6
end
PaymentStatus.seed do |s|
  s.status_id = 'PMNT_CANCELLED'
  s.description = 'Cancelled'
  s.sequence_num = 91
end
PaymentStatus.seed do |s|
  s.status_id = 'PMNT_VOID'
  s.description = 'Void'
  s.sequence_num = 92
end
PaymentStatus.seed do |s|
  s.status_id = 'PMNT_DECLINED'
  s.description = 'Declined'
  s.sequence_num = 93
end
PaymentStatus.seed do |s|
  s.status_id = 'PMNT_REFUNDED'
  s.description = 'Refunded'
  s.sequence_num = 94
end