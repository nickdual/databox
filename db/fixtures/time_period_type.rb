TimePeriodType.seed do |s|
  s.id = 1
  s.description = 'Fiscal Year'
  s.time_period_type_id = 'FISCAL_YEAR'
  s.period_length = '1'
  s.length_uom_id = 'TF_yr'
end

TimePeriodType.seed do |s|
  s.id = 2
  s.description = 'Fiscal Quarter'
  s.time_period_type_id = 'FISCAL_QUARTER'
  s.period_length = '3'
  s.length_uom_id = 'TF_mon'
end

TimePeriodType.seed do |s|
  s.id = 3
  s.description = 'Fiscal Month'
  s.time_period_type_id = 'FISCAL_MONTH'
  s.period_length = '1'
  s.length_uom_id = 'TF_mon'
end

TimePeriodType.seed do |s|
  s.id = 4
  s.description = 'Fiscal Week'
  s.time_period_type_id = 'FISCAL_WEEK'
  s.period_length = '1'
  s.length_uom_id = 'TF_wk'
end

TimePeriodType.seed do |s|
  s.id = 5
  s.description = 'Fiscal Bi-Week'
  s.time_period_type_id = 'FISCAL_BIWEEK'
  s.period_length = '2'
  s.length_uom_id = 'TF_wk'
end

TimePeriodType.seed do |s|
  s.id = 6
  s.description = 'Sales Quarter'
  s.time_period_type_id = 'SALES_QUARTER'
  s.period_length = '3'
  s.length_uom_id = 'TF_mon'
end

TimePeriodType.seed do |s|
  s.id = 7
  s.description = 'Sales Month'
  s.time_period_type_id = 'SALES_MONTH'
  s.period_length = '1'
  s.length_uom_id = 'TF_mon'
end
