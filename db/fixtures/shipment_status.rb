ShipmentStatus.seed do |s|
  s.id = 1
  s.description = 'Input'
  s.name = 'SHIPMENT_INPUT'
  s.sequence_num = 1
end
ShipmentStatus.seed do |s|
  s.id = 2
  s.description = 'Scheduled'
  s.name = 'SHIPMENT_SCHEDULED'
  s.sequence_num = 2
end
ShipmentStatus.seed do |s|
  s.id = 3
  s.description = 'Picked'
  s.name = 'SHIPMENT_PICKED'
  s.sequence_num = 3
end
ShipmentStatus.seed do |s|
  s.id = 4
  s.description = 'Packed'
  s.name = 'SHIPMENT_PACKED'
  s.sequence_num = 4
end
ShipmentStatus.seed do |s|
  s.id = 5
  s.description = 'Shipped'
  s.name = 'SHIPMENT_SHIPPED'
  s.sequence_num = 5
end
ShipmentStatus.seed do |s|
  s.id = 6
  s.description = 'Delivered'
  s.name = 'SHIPMENT_DELIVERED'
  s.sequence_num = 6
end
ShipmentStatus.seed do |s|
  s.id = 7
  s.description = 'Cancelled'
  s.name = 'SHIPMENT_CANCELLED'
  s.sequence_num = 99
end