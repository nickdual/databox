PicklistBinItemStatus.seed do |s|
  s.id = 1
  s.name = 'PICKITEM_PENDING'
  s.description = 'Pending'
  s.sequence_num = 1
end
PicklistBinItemStatus.seed do |s|
  s.id = 2
  s.name = 'PICKITEM_COMPLETED'
  s.description = 'Completed'
  s.sequence_num = 50
end
PicklistBinItemStatus.seed do |s|
  s.id = 3
  s.name = 'PICKITEM_CANCELLED'
  s.description = 'Cancelled'
  s.sequence_num = 99
end
