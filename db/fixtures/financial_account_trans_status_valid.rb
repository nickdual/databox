FinancialAccountTransStatusValid.seed do |s|
   s.status_id = 'FINACT_TRNS_CREATED'
   s.to_status_id = 'FINACT_TRNS_APPROVED'
   s.transition_name = 'Approve'
end
FinancialAccountTransStatusValid.seed do |s|
   s.status_id = 'FINACT_TRNS_CREATED'
   s.to_status_id = 'FINACT_TRNS_CANCELLD'
   s.transition_name = 'Cancel'
end
FinancialAccountTransStatusValid.seed do |s|
   s.status_id = 'FINACT_TRNS_APPROVED'
   s.to_status_id = 'FINACT_TRNS_CANCELLD'
   s.transition_name = 'Cancel'
end
