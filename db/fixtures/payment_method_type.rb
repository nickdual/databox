PaymentMethodType.seed do |s|
   s.name = 'PMT_CREDIT_CARD'
   s.description = 'Credit Card'
end
PaymentMethodType.seed do |s|
   s.name = 'PMT_GIFT_CARD'
   s.description = 'Gift Card'
end
PaymentMethodType.seed do |s|
   s.name = 'PMT_GIFT_CERTIFICATE'
   s.description = 'Gift Certificate'
end
PaymentMethodType.seed do |s|
   s.name = 'PMT_CASH'
   s.description = 'Cash'
end
PaymentMethodType.seed do |s|
   s.name = 'PMT_EFT_ACCOUNT'
   s.description = 'Electronic Funds Transfer'
end
PaymentMethodType.seed do |s|
   s.name = 'PMT_FIN_ACCOUNT'
   s.description = 'Financial Account'
end
PaymentMethodType.seed do |s|
   s.name = 'PMT_PERSONAL_CHECK'
   s.description = 'Personal Check'
end
PaymentMethodType.seed do |s|
   s.name = 'PMT_COMPANY_CHECK'
   s.description = 'Company Check'
end
PaymentMethodType.seed do |s|
   s.name = 'PMT_CERTIFIED_CHECK'
   s.description = 'Certified Check'
end
PaymentMethodType.seed do |s|
   s.name = 'PMT_MONEY_ORDER'
   s.description = 'Money Order'
end
PaymentMethodType.seed do |s|
   s.name = 'PMT_COMPANY_ACCOUNT'
   s.description = 'Company Account'
end
PaymentMethodType.seed do |s|
   s.name = 'PMT_EXT_BILLACT'
   s.description = 'Billing Account'
end
PaymentMethodType.seed do |s|
   s.name = 'PMT_EXT_COD'
   s.description = 'Cash On Delivery'
end
PaymentMethodType.seed do |s|
   s.name = 'PMT_EXT_OFFLINE'
   s.description = 'Offline Payment'
end
