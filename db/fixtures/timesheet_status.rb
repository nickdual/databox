TimesheetStatus.seed do |s|
  s.id = 1
  s.status_id = 'TIMESHEET_IN_PROCESS'
  s.description = 'In-Process'
  s.sequence_num = 1
end
TimesheetStatus.seed do |s|
  s.id = 2
  s.status_id = 'TIMESHEET_COMPLETE'
  s.description = 'Completed'
  s.sequence_num = 2
end
TimesheetStatus.seed do |s|
  s.id = 3
  s.status_id = 'TIMESHEET_APPROVED'
  s.description = 'Approved'
  s.sequence_num = 3
end

