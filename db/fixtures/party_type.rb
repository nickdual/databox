PartyType.seed do |s|
  s.id = 1
  s.name = 'AUTOMATED_AGENT'
  s.description = 'Automated Agent'
end
PartyType.seed do |s|
  s.id = 2
  s.name = 'PERSON'
  s.description = 'Person'
end
PartyType.seed do |s|
  s.id = 3
  s.name = 'ORGANIZATION'
  s.description = 'Organization'
end
