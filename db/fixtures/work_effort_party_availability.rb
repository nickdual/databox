WorkEffortPartyAvailability.seed do |s|
  s.id = 1
  s.description = 'Available'
  s.name = 'WEPA_AV_AVAILABLE'
end
WorkEffortPartyAvailability.seed do |s|
  s.id = 2
  s.description = 'Busy'
  s.name = 'WEPA_AV_BUSY'
end
WorkEffortPartyAvailability.seed do |s|
  s.id = 3
  s.description = 'Away'
  s.name = 'WEPA_AV_AWAY'
end