RateType.seed do |s|
  s.id = 1
  s.name = 'STANDARD'
  s.description = 'Standard Rate'
end

RateType.seed do |s|
  s.id = 2
  s.name = 'DISCOUNTED'
  s.description = 'Discounted Rate'
end
RateType.seed do |s|
  s.id = 3
  s.name = 'OVERTIME'
  s.description = 'Overtime Rate'
end
RateType.seed do |s|
  s.id = 4
  s.name = 'AVERAGE_PAY_RATE'
  s.description = 'Average Pay Rate'
end
RateType.seed do |s|
  s.id = 5
  s.name = 'HIGH_PAY_RATE'
  s.description = 'Highest Pay Rate'
end

RateType.seed do |s|
  s.id = 6
  s.name = 'LOW_PAY_RATE'
  s.description = 'Lowest Pay Rate'
end