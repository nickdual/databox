MarketingCampaignStatusValid.seed do |s|
  s.status_id = 'MKTG_CAMP_PLANNED'
  s.to_status_id = 'MKTG_CAMP_APPROVED'
  s.transition_name = 'Approve Marketing Campaign'
end
MarketingCampaignStatusValid.seed do |s|
  s.status_id = 'MKTG_CAMP_APPROVED'
  s.to_status_id = 'MKTG_CAMP_INPROGRESS'
  s.transition_name = 'Marketing Campaign In Progress'
end
MarketingCampaignStatusValid.seed do |s|
  s.status_id = 'MKTG_CAMP_INPROGRESS'
  s.to_status_id = 'MKTG_CAMP_COMPLETED'
  s.transition_name = 'Complete Marketing Campaign'
end
MarketingCampaignStatusValid.seed do |s|
  s.status_id = 'MKTG_CAMP_PLANNED'
  s.to_status_id = 'MKTG_CAMP_CANCELLED'
  s.transition_name = 'Cancel Marketing Campaign'
end
MarketingCampaignStatusValid.seed do |s|
  s.status_id = 'MKTG_CAMP_PLANNED'
  s.to_status_id = 'MKTG_CAMP_CANCELLED'
  s.transition_name = 'Cancel Marketing Campaign'
end
MarketingCampaignStatusValid.seed do |s|
  s.status_id = 'MKTG_CAMP_INPROGRESS'
  s.to_status_id = 'MKTG_CAMP_CANCELLED'
  s.transition_name = 'Cancel Marketing Campaign'
end