PaymentType.seed do |s|
   s.name = 'PAYMENT_DISBURSE'
   s.description = 'Disbursement'
end
PaymentType.seed do |s|
   s.name = 'PAYMENT_REFUND'
   s.description = 'Refund'
end
PaymentType.seed do |s|
   s.name = 'PAYMENT_INVOICE'
   s.description = 'Invoice'
end
PaymentType.seed do |s|
   s.name = 'DISBURSEMENT'
   s.description = 'Disbursement'
end
PaymentType.seed do |s|
   s.name = 'CUSTOMER_REFUND'
   s.parent_name = 'DISBURSEMENT'
   s.description = 'Customer Refund'
end
PaymentType.seed do |s|
   s.name = 'VENDOR_PAYMENT'
   s.parent_name = 'DISBURSEMENT'
   s.description = 'Vendor Payment'
end
PaymentType.seed do |s|
   s.name = 'VENDOR_PREPAY'
   s.parent_name = 'DISBURSEMENT'
   s.description = 'Vendor Prepayment'
end
PaymentType.seed do |s|
   s.name = 'COMMISSION_PAYMENT'
   s.parent_name = 'DISBURSEMENT'
   s.description = 'Commission Payment'
end
PaymentType.seed do |s|
   s.name = 'PAYROLL_PAYMENT'
   s.parent_name = 'DISBURSEMENT'
   s.description = 'Payroll Payment'
end
PaymentType.seed do |s|
   s.name = 'PAY_CHECK'
   s.parent_name = 'DISBURSEMENT'
   s.description = 'Pay Check'
end
PaymentType.seed do |s|
   s.name = 'Tax Payment'
   s.parent_name = 'DISBURSEMENT'
   s.description = 'TAX_PAYMENT'
end
PaymentType.seed do |s|
   s.name = 'SALES_TAX_PAYMENT'
   s.parent_name = 'TAX_PAYMENT'
   s.description = 'Sales Tax Payment'
end
PaymentType.seed do |s|
   s.name = 'PAYROLL_TAX_PAYMENT'
   s.parent_name = 'TAX_PAYMENT'
   s.description = 'Payroll Tax Payment'
end
PaymentType.seed do |s|
   s.name = 'INCOME_TAX_PAYMENT'
   s.parent_name = 'TAX_PAYMENT'
   s.description = 'Income Tax Payment'
end
PaymentType.seed do |s|
   s.name = 'GC_WITHDRAWAL'
   s.parent_name = 'DISBURSEMENT'
   s.description = 'Gift Certificate Withdrawal'
end
PaymentType.seed do |s|
   s.name = 'RECEIPT'
   s.description = 'Receipt'
end
PaymentType.seed do |s|
   s.name = 'CUSTOMER_PAYMENT'
   s.parent_name = 'RECEIPT'
   s.description = 'Customer Payment'
end
PaymentType.seed do |s|
   s.name = 'CUSTOMER_DEPOSIT'
   s.parent_name = 'RECEIPT'
   s.description = 'Customer Deposit'
end
PaymentType.seed do |s|
   s.name = 'INTEREST_RECEIPT'
   s.parent_name = 'RECEIPT'
   s.description = 'Interest Receipt'
end
PaymentType.seed do |s|
   s.name = 'GC_DEPOSIT'
   s.parent_name = 'RECEIPT'
   s.description = 'Gift Certificate Deposit'
end