OrderHeader.seed do |s|
  s.id = 1
  s.order_id = 1
  s.status_id = 1
  s.grand_total = 19.0
end
OrderHeader.seed do |s|
  s.id = 2
  s.order_id = 2
  s.status_id = 1
  s.grand_total = 17.0
end
OrderHeader.seed do |s|
  s.id = 3
  s.order_id = 3
  s.status_id = 1
  s.grand_total = 21.0
end
OrderHeader.seed do |s|
  s.id = 4
  s.order_id = 4
  s.status_id = 2
  s.grand_total = 34.0
end
OrderHeader.seed do |s|
  s.id = 5
  s.order_id = 5
  s.status_id = 10
  s.grand_total = 89.3
end
OrderHeader.seed do |s|
  s.id = 6
  s.order_id = 6
  s.status_id = 11
  s.grand_total = 15.0
end
OrderHeader.seed do |s|
  s.id = 7
  s.order_id = 7
  s.status_id = 10
  s.grand_total = 21.0
end