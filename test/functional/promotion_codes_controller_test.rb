#require 'test_helper'
#
#class PromotionCodesControllerTest < ActionController::TestCase
#  setup do
#    @promotion_code = promotion_codes(:one)
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:promotion_codes)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create promotion_code" do
#    assert_difference('PromotionCode.count') do
#      post :create, promotion_code: { created_by_user_login: @promotion_code.created_by_user_login, from_date: @promotion_code.from_date, last_modified_by_user_login: @promotion_code.last_modified_by_user_login, product_promotion_code_id: @promotion_code.product_promotion_code_id, product_promotion_id: @promotion_code.product_promotion_id, require_email_or_party: @promotion_code.require_email_or_party, thru_date: @promotion_code.thru_date, use_limit_per_code: @promotion_code.use_limit_per_code, use_limit_per_customer: @promotion_code.use_limit_per_customer, user_entered: @promotion_code.user_entered }
#    end
#
#    assert_redirected_to promotion_code_path(assigns(:promotion_code))
#  end
#
#  test "should show promotion_code" do
#    get :show, id: @promotion_code
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @promotion_code
#    assert_response :success
#  end
#
#  test "should update promotion_code" do
#    put :update, id: @promotion_code, promotion_code: { created_by_user_login: @promotion_code.created_by_user_login, from_date: @promotion_code.from_date, last_modified_by_user_login: @promotion_code.last_modified_by_user_login, product_promotion_code_id: @promotion_code.product_promotion_code_id, product_promotion_id: @promotion_code.product_promotion_id, require_email_or_party: @promotion_code.require_email_or_party, thru_date: @promotion_code.thru_date, use_limit_per_code: @promotion_code.use_limit_per_code, use_limit_per_customer: @promotion_code.use_limit_per_customer, user_entered: @promotion_code.user_entered }
#    assert_redirected_to promotion_code_path(assigns(:promotion_code))
#  end
#
#  test "should destroy promotion_code" do
#    assert_difference('PromotionCode.count', -1) do
#      delete :destroy, id: @promotion_code
#    end
#
#    assert_redirected_to promotion_codes_path
#  end
#end
