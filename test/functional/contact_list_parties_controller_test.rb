#require 'test_helper'
#
#class ContactListPartiesControllerTest < ActionController::TestCase
#  setup do
#    @contact_list_party = contact_list_parties(:one)
#    @controller = ContactListPartiesController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:contact_list_parties)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create contact_list_party" do
#    assert_difference('ContactListParty.count') do
#      post :create, contact_list_party: { contact_list_id: @contact_list_party.contact_list_id, from_date: @contact_list_party.from_date, opt_in_verify_code: @contact_list_party.opt_in_verify_code, party_id: @contact_list_party.party_id, preferred_contact_mech_id: @contact_list_party.preferred_contact_mech_id, status_id: @contact_list_party.status_id, thru_date: @contact_list_party.thru_date }
#    end
#
#    assert_redirected_to contact_list_party_path(assigns(:contact_list_party))
#  end
#
#  test "should show contact_list_party" do
#    get :show, id: @contact_list_party
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @contact_list_party
#    assert_response :success
#  end
#
#  test "should update contact_list_party" do
#    put :update, id: @contact_list_party, contact_list_party: { contact_list_id: @contact_list_party.contact_list_id, from_date: @contact_list_party.from_date, opt_in_verify_code: @contact_list_party.opt_in_verify_code, party_id: @contact_list_party.party_id, preferred_contact_mech_id: @contact_list_party.preferred_contact_mech_id, status_id: @contact_list_party.status_id, thru_date: @contact_list_party.thru_date }
#    assert_redirected_to contact_list_party_path(assigns(:contact_list_party))
#  end
#
#  test "should destroy contact_list_party" do
#    assert_difference('ContactListParty.count', -1) do
#      delete :destroy, id: @contact_list_party
#    end
#
#    assert_redirected_to contact_list_parties_path
#  end
#end
