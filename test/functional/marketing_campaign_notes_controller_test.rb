#require 'test_helper'
#
#class MarketingCampaignNotesControllerTest < ActionController::TestCase
#  setup do
#    @marketing_campaign_note = marketing_campaign_notes(:one)
#    @controller = MarketingCampaignNotesController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:marketing_campaign_notes)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create marketing_campaign_note" do
#    assert_difference('MarketingCampaignNote.count') do
#      post :create, marketing_campaign_note: { marketingCampaignId: @marketing_campaign_note.marketingCampaignId, noteDate: @marketing_campaign_note.noteDate, noteText: @marketing_campaign_note.noteText }
#    end
#
#    assert_redirected_to marketing_campaign_note_path(assigns(:marketing_campaign_note))
#  end
#
#  test "should show marketing_campaign_note" do
#    get :show, id: @marketing_campaign_note
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @marketing_campaign_note
#    assert_response :success
#  end
#
#  test "should update marketing_campaign_note" do
#    put :update, id: @marketing_campaign_note, marketing_campaign_note: { marketingCampaignId: @marketing_campaign_note.marketingCampaignId, noteDate: @marketing_campaign_note.noteDate, noteText: @marketing_campaign_note.noteText }
#    assert_redirected_to marketing_campaign_note_path(assigns(:marketing_campaign_note))
#  end
#
#  test "should destroy marketing_campaign_note" do
#    assert_difference('MarketingCampaignNote.count', -1) do
#      delete :destroy, id: @marketing_campaign_note
#    end
#
#    assert_redirected_to marketing_campaign_notes_path
#  end
#end
