#require 'test_helper'
#
#class RequestPartiesControllerTest < ActionController::TestCase
#  setup do
#    @request_party = request_parties(:one)
#    @controller = RequestPartiesController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:request_parties)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create request_party" do
#    assert_difference('RequestParty.count') do
#      post :create, request_party: { from_date: @request_party.from_date, party_id: @request_party.party_id, request_id: @request_party.request_id, role_type_id: @request_party.role_type_id, thru_date: @request_party.thru_date }
#    end
#
#    assert_redirected_to request_party_path(assigns(:request_party))
#  end
#
#  test "should show request_party" do
#    get :show, id: @request_party
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @request_party
#    assert_response :success
#  end
#
#  test "should update request_party" do
#    put :update, id: @request_party, request_party: { from_date: @request_party.from_date, party_id: @request_party.party_id, request_id: @request_party.request_id, role_type_id: @request_party.role_type_id, thru_date: @request_party.thru_date }
#    assert_redirected_to request_party_path(assigns(:request_party))
#  end
#
#  test "should destroy request_party" do
#    assert_difference('RequestParty.count', -1) do
#      delete :destroy, id: @request_party
#    end
#
#    assert_redirected_to request_parties_path
#  end
#end
