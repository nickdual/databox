#require 'test_helper'
#
#class MarketingCampaignsControllerTest < ActionController::TestCase
#  setup do
#    @marketing_campaign = marketing_campaigns(:one)
#    @controller = MarketingCampaignsController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:marketing_campaigns)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create marketing_campaign" do
#    assert_difference('MarketingCampaign.count') do
#      post :create, marketing_campaign: { actual_cost: @marketing_campaign.actual_cost, budgeted_cost: @marketing_campaign.budgeted_cost, campaign_name: @marketing_campaign.campaign_name, campaign_summary: @marketing_campaign.campaign_summary, converted_leads: @marketing_campaign.converted_leads, cost_uom_id: @marketing_campaign.cost_uom_id, created_at: @marketing_campaign.created_at, estimated_cost: @marketing_campaign.estimated_cost, expected_response_percent: @marketing_campaign.expected_response_percent, expected_revenue: @marketing_campaign.expected_revenue, from_date: @marketing_campaign.from_date, is_active: @marketing_campaign.is_active, marketing_campaign_id: @marketing_campaign.marketing_campaign_id, num_sent: @marketing_campaign.num_sent, parent_campaign_id: @marketing_campaign.parent_campaign_id, start_date: @marketing_campaign.start_date, status_id: @marketing_campaign.status_id, thru_date: @marketing_campaign.thru_date, updated_at: @marketing_campaign.updated_at }
#    end
#
#    assert_redirected_to marketing_campaign_path(assigns(:marketing_campaign))
#  end
#
#  test "should show marketing_campaign" do
#    get :show, id: @marketing_campaign
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @marketing_campaign
#    assert_response :success
#  end
#
#  test "should update marketing_campaign" do
#    put :update, id: @marketing_campaign, marketing_campaign: { actual_cost: @marketing_campaign.actual_cost, budgeted_cost: @marketing_campaign.budgeted_cost, campaign_name: @marketing_campaign.campaign_name, campaign_summary: @marketing_campaign.campaign_summary, converted_leads: @marketing_campaign.converted_leads, cost_uom_id: @marketing_campaign.cost_uom_id, created_at: @marketing_campaign.created_at, estimated_cost: @marketing_campaign.estimated_cost, expected_response_percent: @marketing_campaign.expected_response_percent, expected_revenue: @marketing_campaign.expected_revenue, from_date: @marketing_campaign.from_date, is_active: @marketing_campaign.is_active, marketing_campaign_id: @marketing_campaign.marketing_campaign_id, num_sent: @marketing_campaign.num_sent, parent_campaign_id: @marketing_campaign.parent_campaign_id, start_date: @marketing_campaign.start_date, status_id: @marketing_campaign.status_id, thru_date: @marketing_campaign.thru_date, updated_at: @marketing_campaign.updated_at }
#    assert_redirected_to marketing_campaign_path(assigns(:marketing_campaign))
#  end
#
#  test "should destroy marketing_campaign" do
#    assert_difference('MarketingCampaign.count', -1) do
#      delete :destroy, id: @marketing_campaign
#    end
#
#    assert_redirected_to marketing_campaigns_path
#  end
#end
