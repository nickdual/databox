#require 'test_helper'
#
#class ContactListsControllerTest < ActionController::TestCase
#  setup do
#    @contact_list = contact_lists(:one)
#    @controller = ContactListsController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:contact_lists)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create contact_list" do
#    assert_difference('ContactList.count') do
#      post :create, contact_list: { comments: @contact_list.comments, contact_list_id: @contact_list.contact_list_id, contact_list_name: @contact_list.contact_list_name, contact_list_type_enum_id: @contact_list.contact_list_type_enum_id, contact_mesh_type_enum_id: @contact_list.contact_mesh_type_enum_id, description: @contact_list.description, is_public: @contact_list.is_public, marketing_campaign_id: @contact_list.marketing_campaign_id, opt_out_screen: @contact_list.opt_out_screen, owner_party_id: @contact_list.owner_party_id, single_use: @contact_list.single_use, verfity_email_from: @contact_list.verfity_email_from, verify_email_screen: @contact_list.verify_email_screen, verify_email_subject: @contact_list.verify_email_subject, verify_email_website_id: @contact_list.verify_email_website_id }
#    end
#
#    assert_redirected_to contact_list_path(assigns(:contact_list))
#  end
#
#  test "should show contact_list" do
#    get :show, id: @contact_list
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @contact_list
#    assert_response :success
#  end
#
#  test "should update contact_list" do
#    put :update, id: @contact_list, contact_list: { comments: @contact_list.comments, contact_list_id: @contact_list.contact_list_id, contact_list_name: @contact_list.contact_list_name, contact_list_type_enum_id: @contact_list.contact_list_type_enum_id, contact_mesh_type_enum_id: @contact_list.contact_mesh_type_enum_id, description: @contact_list.description, is_public: @contact_list.is_public, marketing_campaign_id: @contact_list.marketing_campaign_id, opt_out_screen: @contact_list.opt_out_screen, owner_party_id: @contact_list.owner_party_id, single_use: @contact_list.single_use, verfity_email_from: @contact_list.verfity_email_from, verify_email_screen: @contact_list.verify_email_screen, verify_email_subject: @contact_list.verify_email_subject, verify_email_website_id: @contact_list.verify_email_website_id }
#    assert_redirected_to contact_list_path(assigns(:contact_list))
#  end
#
#  test "should destroy contact_list" do
#    assert_difference('ContactList.count', -1) do
#      delete :destroy, id: @contact_list
#    end
#
#    assert_redirected_to contact_lists_path
#  end
#end
