#require 'test_helper'
#
#class ContactListEmailsControllerTest < ActionController::TestCase
#  setup do
#    @contact_list_email = contact_list_emails(:one)
#    @controller = ContactListEmailsController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:contact_list_emails)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create contact_list_email" do
#    assert_difference('ContactListEmail.count') do
#      post :create, contact_list_email: { contact_list_id: @contact_list_email.contact_list_id, email_templated_id: @contact_list_email.email_templated_id, email_type_enum_id: @contact_list_email.email_type_enum_id, from_date: @contact_list_email.from_date, thru_date: @contact_list_email.thru_date }
#    end
#
#    assert_redirected_to contact_list_email_path(assigns(:contact_list_email))
#  end
#
#  test "should show contact_list_email" do
#    get :show, id: @contact_list_email
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @contact_list_email
#    assert_response :success
#  end
#
#  test "should update contact_list_email" do
#    put :update, id: @contact_list_email, contact_list_email: { contact_list_id: @contact_list_email.contact_list_id, email_templated_id: @contact_list_email.email_templated_id, email_type_enum_id: @contact_list_email.email_type_enum_id, from_date: @contact_list_email.from_date, thru_date: @contact_list_email.thru_date }
#    assert_redirected_to contact_list_email_path(assigns(:contact_list_email))
#  end
#
#  test "should destroy contact_list_email" do
#    assert_difference('ContactListEmail.count', -1) do
#      delete :destroy, id: @contact_list_email
#    end
#
#    assert_redirected_to contact_list_emails_path
#  end
#end
