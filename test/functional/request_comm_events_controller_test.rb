#require 'test_helper'
#
#class RequestCommEventsControllerTest < ActionController::TestCase
#  setup do
#    @request_comm_event = request_comm_events(:one)
#    @controller = RequestCommEventsController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:request_comm_events)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create request_comm_event" do
#    assert_difference('RequestCommEvent.count') do
#      post :create, request_comm_event: { communication: @request_comm_event.communication, request_id: @request_comm_event.request_id }
#    end
#
#    assert_redirected_to request_comm_event_path(assigns(:request_comm_event))
#  end
#
#  test "should show request_comm_event" do
#    get :show, id: @request_comm_event
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @request_comm_event
#    assert_response :success
#  end
#
#  test "should update request_comm_event" do
#    put :update, id: @request_comm_event, request_comm_event: { communication: @request_comm_event.communication, request_id: @request_comm_event.request_id }
#    assert_redirected_to request_comm_event_path(assigns(:request_comm_event))
#  end
#
#  test "should destroy request_comm_event" do
#    assert_difference('RequestCommEvent.count', -1) do
#      delete :destroy, id: @request_comm_event
#    end
#
#    assert_redirected_to request_comm_events_path
#  end
#end
