#require 'test_helper'
#
#class TrackCodesControllerTest < ActionController::TestCase
#  setup do
#    @track_code = track_codes(:one)
#    @controller = TrackCodesController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:track_codes)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create track_code" do
#    assert_difference('TrackCode.count') do
#      post :create, track_code: { billable_lifetime: @track_code.billable_lifetime, comments: @track_code.comments, description: @track_code.description, from_date: @track_code.from_date, group_id: @track_code.group_id, marketing_campaign_id: @track_code.marketing_campaign_id, redirect_url: @track_code.redirect_url, subgroup_id: @track_code.subgroup_id, thru_date: @track_code.thru_date, trackable_lifetime: @track_code.trackable_lifetime, tracking_code_id: @track_code.tracking_code_id, tracking_code_type_enum_id: @track_code.tracking_code_type_enum_id }
#    end
#
#    assert_redirected_to track_code_path(assigns(:track_code))
#  end
#
#  test "should show track_code" do
#    get :show, id: @track_code
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @track_code
#    assert_response :success
#  end
#
#  test "should update track_code" do
#    put :update, id: @track_code, track_code: { billable_lifetime: @track_code.billable_lifetime, comments: @track_code.comments, description: @track_code.description, from_date: @track_code.from_date, group_id: @track_code.group_id, marketing_campaign_id: @track_code.marketing_campaign_id, redirect_url: @track_code.redirect_url, subgroup_id: @track_code.subgroup_id, thru_date: @track_code.thru_date, trackable_lifetime: @track_code.trackable_lifetime, tracking_code_id: @track_code.tracking_code_id, tracking_code_type_enum_id: @track_code.tracking_code_type_enum_id }
#    assert_redirected_to track_code_path(assigns(:track_code))
#  end
#
#  test "should destroy track_code" do
#    assert_difference('TrackCode.count', -1) do
#      delete :destroy, id: @track_code
#    end
#
#    assert_redirected_to track_codes_path
#  end
#end
