#require 'test_helper'
#
#class RequestNotesControllerTest < ActionController::TestCase
#  setup do
#    @request_note = request_notes(:one)
#    @controller = RequestNotesController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:request_notes)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create request_note" do
#    assert_difference('RequestNote.count') do
#      post :create, request_note: { note_date: @request_note.note_date, note_text: @request_note.note_text, request_id: @request_note.request_id, request_item_seq_id: @request_note.request_item_seq_id }
#    end
#
#    assert_redirected_to request_note_path(assigns(:request_note))
#  end
#
#  test "should show request_note" do
#    get :show, id: @request_note
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @request_note
#    assert_response :success
#  end
#
#  test "should update request_note" do
#    put :update, id: @request_note, request_note: { note_date: @request_note.note_date, note_text: @request_note.note_text, request_id: @request_note.request_id, request_item_seq_id: @request_note.request_item_seq_id }
#    assert_redirected_to request_note_path(assigns(:request_note))
#  end
#
#  test "should destroy request_note" do
#    assert_difference('RequestNote.count', -1) do
#      delete :destroy, id: @request_note
#    end
#
#    assert_redirected_to request_notes_path
#  end
#end
