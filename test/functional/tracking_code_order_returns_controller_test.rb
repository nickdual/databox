#require 'test_helper'
#
#class TrackingCodeOrderReturnsControllerTest < ActionController::TestCase
#  setup do
#    @tracking_code_order_return = tracking_code_order_returns(:one)
#    @controller = TrackingCodeOrderReturnsController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:tracking_code_order_returns)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create tracking_code_order_return" do
#    assert_difference('TrackingCodeOrderReturn.count') do
#      post :create, tracking_code_order_return: { affiliate_referred_time_stamp: @tracking_code_order_return.affiliate_referred_time_stamp, has_exported: @tracking_code_order_return.has_exported, is_billable: @tracking_code_order_return.is_billable, order_id: @tracking_code_order_return.order_id, order_item_seq_id: @tracking_code_order_return.order_item_seq_id, return_id: @tracking_code_order_return.return_id, side_id: @tracking_code_order_return.side_id, tracking_code_id: @tracking_code_order_return.tracking_code_id, tracking_code_type_enum_id: @tracking_code_order_return.tracking_code_type_enum_id }
#    end
#
#    assert_redirected_to tracking_code_order_return_path(assigns(:tracking_code_order_return))
#  end
#
#  test "should show tracking_code_order_return" do
#    get :show, id: @tracking_code_order_return
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @tracking_code_order_return
#    assert_response :success
#  end
#
#  test "should update tracking_code_order_return" do
#    put :update, id: @tracking_code_order_return, tracking_code_order_return: { affiliate_referred_time_stamp: @tracking_code_order_return.affiliate_referred_time_stamp, has_exported: @tracking_code_order_return.has_exported, is_billable: @tracking_code_order_return.is_billable, order_id: @tracking_code_order_return.order_id, order_item_seq_id: @tracking_code_order_return.order_item_seq_id, return_id: @tracking_code_order_return.return_id, side_id: @tracking_code_order_return.side_id, tracking_code_id: @tracking_code_order_return.tracking_code_id, tracking_code_type_enum_id: @tracking_code_order_return.tracking_code_type_enum_id }
#    assert_redirected_to tracking_code_order_return_path(assigns(:tracking_code_order_return))
#  end
#
#  test "should destroy tracking_code_order_return" do
#    assert_difference('TrackingCodeOrderReturn.count', -1) do
#      delete :destroy, id: @tracking_code_order_return
#    end
#
#    assert_redirected_to tracking_code_order_returns_path
#  end
#end
