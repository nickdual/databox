#require 'test_helper'
#
#class RequestItemWorkEffortsControllerTest < ActionController::TestCase
#  setup do
#    @request_item_work_effort = request_item_work_efforts(:one)
#    @controller = RequestItemWorkEffortsController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:request_item_work_efforts)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create request_item_work_effort" do
#    assert_difference('RequestItemWorkEffort.count') do
#      post :create, request_item_work_effort: { request_id: @request_item_work_effort.request_id, request_item_seq_id: @request_item_work_effort.request_item_seq_id, work_effort_id: @request_item_work_effort.work_effort_id }
#    end
#
#    assert_redirected_to request_item_work_effort_path(assigns(:request_item_work_effort))
#  end
#
#  test "should show request_item_work_effort" do
#    get :show, id: @request_item_work_effort
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @request_item_work_effort
#    assert_response :success
#  end
#
#  test "should update request_item_work_effort" do
#    put :update, id: @request_item_work_effort, request_item_work_effort: { request_id: @request_item_work_effort.request_id, request_item_seq_id: @request_item_work_effort.request_item_seq_id, work_effort_id: @request_item_work_effort.work_effort_id }
#    assert_redirected_to request_item_work_effort_path(assigns(:request_item_work_effort))
#  end
#
#  test "should destroy request_item_work_effort" do
#    assert_difference('RequestItemWorkEffort.count', -1) do
#      delete :destroy, id: @request_item_work_effort
#    end
#
#    assert_redirected_to request_item_work_efforts_path
#  end
#end
