#require 'test_helper'
#
#class TrackingCodeOrdersControllerTest < ActionController::TestCase
#  setup do
#    @tracking_code_order = tracking_code_orders(:one)
#    @controller = TrackingCodeOrdersController.new
#  end
#
#  test "should get index" do
#    get :index
#    assert_response :success
#    assert_not_nil assigns(:tracking_code_orders)
#  end
#
#  test "should get new" do
#    get :new
#    assert_response :success
#  end
#
#  test "should create tracking_code_order" do
#    assert_difference('TrackingCodeOrder.count') do
#      post :create, tracking_code_order: { affiliate_referred_time_stamp: @tracking_code_order.affiliate_referred_time_stamp, has_exported: @tracking_code_order.has_exported, is_billable: @tracking_code_order.is_billable, order_id: @tracking_code_order.order_id, site_id: @tracking_code_order.site_id, tracking_code_id: @tracking_code_order.tracking_code_id, tracking_code_type_enum_id: @tracking_code_order.tracking_code_type_enum_id }
#    end
#
#    assert_redirected_to tracking_code_order_path(assigns(:tracking_code_order))
#  end
#
#  test "should show tracking_code_order" do
#    get :show, id: @tracking_code_order
#    assert_response :success
#  end
#
#  test "should get edit" do
#    get :edit, id: @tracking_code_order
#    assert_response :success
#  end
#
#  test "should update tracking_code_order" do
#    put :update, id: @tracking_code_order, tracking_code_order: { affiliate_referred_time_stamp: @tracking_code_order.affiliate_referred_time_stamp, has_exported: @tracking_code_order.has_exported, is_billable: @tracking_code_order.is_billable, order_id: @tracking_code_order.order_id, site_id: @tracking_code_order.site_id, tracking_code_id: @tracking_code_order.tracking_code_id, tracking_code_type_enum_id: @tracking_code_order.tracking_code_type_enum_id }
#    assert_redirected_to tracking_code_order_path(assigns(:tracking_code_order))
#  end
#
#  test "should destroy tracking_code_order" do
#    assert_difference('TrackingCodeOrder.count', -1) do
#      delete :destroy, id: @tracking_code_order
#    end
#
#    assert_redirected_to tracking_code_orders_path
#  end
#end
