task :seed_products => :environment do
  Product.create([
     {product_type_id: '1', product_name:'Eclairs', description:'Coolest candies ever and forever...', sku:'234dG5'},
     {product_type_id: '11', product_name:'BMW i8 Concept', description:'Coolest car ever.', sku:'233dG5'},
     {product_type_id: '1', product_name:'Orango', description:'Coolest candies ever and forever...', sku:'234dH5'},
     {product_type_id: '1', product_name:'Chocbar', description:'Coolest candies ever and forever...', sku:'2344dG5'},
     {product_type_id: '1', product_name:'Fanty', description:'Coolest candies ever and forever...', sku:'1234dG5'},
     {product_type_id: '1', product_name:'Toyota Corrolla', description:'Coolest candies ever and forever...', sku:'234HG5'},
     {product_type_id: '1', product_name:'KitKat', description:'Coolest candies ever and forever...', sku:'234D5'},
     {product_type_id: '1', product_name:'Dolphins', description:'Coolest candies ever and forever...', sku:'2332G5'},
     {product_type_id: '1', product_name:'Dairy Milk', description:'Coolest candies ever and forever...', sku:'234Y5'},
     {product_type_id: '1', product_name:'Chocys', description:'Coolest candies ever and forever...', sku:'234d65'},
     {product_type_id: '11', product_name:'Hunda Accord', description:'Coolest candies ever and forever...', sku:'234TU'},
     {product_type_id: '1', product_name:'Eclairs1', description:'Coolest candies ever and forever...', sku:'234dG5'},
     {product_type_id: '1', product_name:'Eclairs2', description:'Coolest candies ever and forever...', sku:'234dG5'},
     {product_type_id: '1', product_name:'Eclairs3', description:'Coolest candies ever and forever...', sku:'234d55'},
     {product_type_id: '1', product_name:'Eclairs4', description:'Coolest candies ever and forever...', sku:'234TU'},
     {product_type_id: '1', product_name:'Toyota Camery', description:'Coolest candies ever and forever...', sku:'234TU'},
     {product_type_id: '1', product_name:'Eclairs1', description:'Coolest candies ever and forever...', sku:'234TU'},
     {product_type_id: '1', product_name:'Eclairs2', description:'Coolest candies ever and forever...', sku:'234TU'},
     {product_type_id: '11', product_name:'Hunda Civic', description:'Coolest candies ever and forever...', sku:'234RE'},
     {product_type_id: '12', product_name:'Dairy Milk', description:'Coolest candies ever and forever...', sku:'23XTU'},
     {product_type_id: '12', product_name:'Kit Kat', description:'Coolest candies ever and forever...', sku:'234EU'},
     {product_type_id: '123', product_name:'MacBook Pro', description:'Awesome! Awesome is the only word to describe this incredible machine.', sku:'MAC204'},
     {product_type_id: '111', product_name:'XPS ConceptBook', description:'The word awesome has got new definition. And its XPS', sku:'XPS501'}
  ])

  CostComponent.create([
      {product_id:'1', cost:'503'},
      {product_id:'2', cost:'504'},
      {product_id:'3', cost:'502'},
      {product_id:'4', cost:'530'},
      {product_id:'5', cost:'540'},
      {product_id:'6', cost:'503'},
      {product_id:'7', cost:'540'},
      {product_id:'8', cost:'540'},
      {product_id:'9', cost:'540'},
      {product_id:'10', cost:'350'},
      {product_id:'11', cost:'550'},
      {product_id:'12', cost:'350'},
      {product_id:'13', cost:'520'},
      {product_id:'14', cost:'520'},
      {product_id:'15', cost:'530'},
      {product_id:'16', cost:'505'},
      {product_id:'17', cost:'520'},
      {product_id:'18', cost:'520'},
      {product_id:'19', cost:'510'},
      {product_id:'20', cost:'501'},
      {product_id:'21', cost:'501'},
      {product_id:'22', cost:'5000'},
      {product_id:'23', cost:'5001'}
                       ])
  ProductPrice.create([
        {product_id:'1', price:'520'},
        {product_id:'2', price:'505'},
        {product_id:'3', price:'505'},
        {product_id:'4', price:'535'},
        {product_id:'5', price:'550'},
        {product_id:'6', price:'553'},
        {product_id:'7', price:'540'},
        {product_id:'8', price:'550'},
        {product_id:'9', price:'560'},
        {product_id:'10', price:'360'},
        {product_id:'11', price:'550'},
        {product_id:'12', price:'370'},
        {product_id:'13', price:'570'},
        {product_id:'14', price:'540'},
        {product_id:'15', price:'540'},
        {product_id:'16', price:'565'},
        {product_id:'17', price:'530'},
        {product_id:'18', price:'510'},
        {product_id:'19', price:'540'},
        {product_id:'20', price:'591'},
        {product_id:'21', price:'541'},
        {product_id:'22', price:'5500'},
        {product_id:'23', price:'5501'}
                         ])
  SupplierProduct.create([
          {product_id:'1', standard_lead_time_days:'5'},
          {product_id:'2', standard_lead_time_days:'15'},
          {product_id:'3', standard_lead_time_days:'51'},
          {product_id:'4', standard_lead_time_days:'53'},
          {product_id:'5', standard_lead_time_days:'25'},
          {product_id:'6', standard_lead_time_days:'15'},
          {product_id:'7', standard_lead_time_days:'5'},
          {product_id:'8', standard_lead_time_days:'25'},
          {product_id:'9', standard_lead_time_days:'15'},
          {product_id:'10', standard_lead_time_days:'15'},
          {product_id:'11', standard_lead_time_days:'15'},
          {product_id:'12', standard_lead_time_days:'15'},
          {product_id:'13', standard_lead_time_days:'25'},
          {product_id:'14', standard_lead_time_days:'56'},
          {product_id:'15', standard_lead_time_days:'6'},
          {product_id:'16', standard_lead_time_days:'57'},
          {product_id:'17', standard_lead_time_days:'57'},
          {product_id:'18', standard_lead_time_days:'65'},
          {product_id:'19', standard_lead_time_days:'75'},
          {product_id:'20', standard_lead_time_days:'45'},
          {product_id:'21', standard_lead_time_days:'25'},
          {product_id:'22', standard_lead_time_days:'15'},
          {product_id:'23', standard_lead_time_days:'3'}
                        ])
  AssetProduct.create([
          {product_id:'1', asset_id:'1'},
          {product_id:'2', asset_id:'1'},
          {product_id:'3', asset_id:'1'},
          {product_id:'4', asset_id:'1'},
          {product_id:'5', asset_id:'1'},
          {product_id:'6', asset_id:'1'},
          {product_id:'7', asset_id:'2'},
          {product_id:'8', asset_id:'2'},
          {product_id:'9', asset_id:'2'},
          {product_id:'10', asset_id:'3'},
          {product_id:'11', asset_id:'3'},
          {product_id:'12', asset_id:'4'},
          {product_id:'13', asset_id:'5'},
          {product_id:'14', asset_id:'6'},
          {product_id:'15', asset_id:'7'},
          {product_id:'16', asset_id:'8'},
          {product_id:'17', asset_id:'8'},
          {product_id:'18', asset_id:'8'},
          {product_id:'19', asset_id:'8'},
          {product_id:'20', asset_id:'9'},
          {product_id:'21', asset_id:'9'},
          {product_id:'22', asset_id:'3'},
          {product_id:'23', asset_id:'3'}
                        ])
  Asset.create([
            {quantity_on_hand_total:'32641'},
            {quantity_on_hand_total:'3522'},
            {quantity_on_hand_total:'2343'},
            {quantity_on_hand_total:'2344'},
            {quantity_on_hand_total:'5324'},
            {quantity_on_hand_total:'6234'},
            {quantity_on_hand_total:'3247'},
            {quantity_on_hand_total:'23428'},
            {quantity_on_hand_total:'9342'}
                          ])

end

#Sample Store Names for Orders testing
task :seed_stores => :environment do
  ProductStore.create([
      {store_name:'orderwithme.com'},
      {store_name:'orderwithme1.com'},
      {store_name:'orderwithme2.com'},
      {store_name:'orderwithme3.com'},
      {store_name:'orderwithme4.com'},
      {store_name:'orderwithme5.com'},
      {store_name:'orderwithme6.com'}
               ])
end

#Sample Store Channels for Orders testing
task :seed_sales_channels => :environment do
  SalesChannel.create([
      {name: 'Google Checkout Channel', description: 'GC_SALES_CHANNEL'},
      {name: 'Web Channel', description: 'WEB_SALES_CHANNEL'},
      {name: 'POS Channel', description: 'POS_SALES_CHANNEL'},
      {name: 'Fax Channel', description: 'FAX_SALES_CHANNEL'},
      {name: 'Phone Channel', description: 'PHONE_SALES_CHANNEL'},
      {name: 'E-Mail Channel', description: 'EMAIL_SALES_CHANNEL'},
      {name: 'Snail Mail Channel', description: 'SNAIL_SALES_CHANNEL'},
      {name: 'Affiliate Channel', description: 'AFFIL_SALES_CHANNEL'},
      {name: 'eBay Channel', description: 'EBAY_SALES_CHANNEL'},
      {name: 'Rakuten Channel', description: 'RAKUTEN_SALES_CHANNEL'},
      {name: 'Unknown Channel', description: 'UNKNWN_SALES_CHANNEL'}
                      ])
end

#For New Roles.
task :seed_new_roles  => :environment do
  Role.create([
      {name: "buyer"},
      {name: "sourcer"}
              ])
end

#Sample suppliers for Find Party testing
task :seed_supplier => :environment do

end