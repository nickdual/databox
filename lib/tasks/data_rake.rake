require 'faker'
namespace :db do
  desc "Fill database with sample data"
  task :populate => :environment do
    #Rake::Task['db:reset'].invoke

    #Add an admin user with credentials u: admin p: adminowm

    User.create(email:'admin@owm.com',
                password:'adminowm',
                sign_in_count:"0",
                created_at:"2012-11-21 10:24:28.586343",
                confirmed_at:'2012-11-22 10:01:19.653129',
                approval:true)
    #Make first user Admin
    user = User.first
    user.add_role('admin')
    #Creating Roles

    Role.create([
                    {name:'account_lead'},
                    {name:'admin'},
                    {name:'agent'},
                    {name:'automated_agent_role'},
                    {name:'calendar_role'},
                    {name:'client'},
                    {name:'comm_participant'},
                    {name:'consumer'},
                    {name:'contractor'},
                    {name:'customer'},
                    {name:'distribution_channel'},
                    {name:'ISP'},
                    {name:'hosting_server'},
                    {name:'manufacturer'},
                    {name:'na'},
                    {name:'organization_role'},
                    {name:'owner'},
                    {name:'prospect'},
                    {name:'person_role'},
                    {name:'referrer'},
                    {name:'req_manager'},
                    {name:'req_requester'},
                    {name:'req_taker'},
                    {name:'sfa_role'},
                    {name:'shareholder'},
                    {name:'subscriber'},
                    {name:'vendor'},
                    {name:'visitor'},
                    {name:'web_master'},
                    {name:'workflow_role'},
                    {name:'accountant'}
                ])

    99.times do |n|
      billback_factor = rand(1..10)
      override_org_party_id = rand(1..10)
      promotion_name = Faker::Name.name
      promotion_show_to_customer = true
      promotion_text = Faker::Name.name
      require_code = true
      use_limit_per_customer = rand(1..10)
      use_limit_per_order = rand(1..10)
      use_limit_per_promotion = rand(1..10)
      user_entered = true
      ProductPromotion.create!(
          billback_factor: billback_factor,
          override_org_party_id: override_org_party_id,
          promotion_name: promotion_name,
          promotion_show_to_customer: promotion_show_to_customer,
          promotion_text: promotion_text,
          require_code: require_code,
          use_limit_per_customer: use_limit_per_customer,
          use_limit_per_order: use_limit_per_order,
          use_limit_per_promotion: use_limit_per_promotion,
          user_id: user.id,
          user_entered: user_entered,
          created_by_user_login: user,
          last_modified_by_user_login: user
      )
    end

    99.times do |n|
      product_store_id = rand(0..1)
      sequence_num = rand(1..10)
      thru_date = Time.now
      product_promotion_id = 1
      PromotionStore.create!(
          product_store_id: product_store_id,
          sequence_num: sequence_num,
          thru_date: thru_date,
          product_promotion_id: product_promotion_id
      )
    end

    PromotionRule.create(id: 1, name: '54444')

    Operator.create!(name: 'Is')
    Operator.create!(name: 'Is Not')
    Operator.create!(name: 'Is Less Than')
    Operator.create!(name: 'Is Less Than or Equal To')
    Operator.create!(name: 'Is Greater Than')
    Operator.create!(name: 'Is Greater Than or Equal To')

    ShipmentMethod.create!(name: 'UPS Air')
    ShipmentMethod.create!(name: 'FEDEX Express')
    ShipmentMethod.create!(name: 'DHL Express')
    ShipmentMethod.create!(name: 'USPS Express')
    ShipmentMethod.create!(name: 'FEDEX First Overnight')
    ShipmentMethod.create!(name: 'DHL Ground')
    ShipmentMethod.create!(name: 'UPS Ground')
    ShipmentMethod.create!(name: 'FEDEX Ground')
    ShipmentMethod.create!(name: 'FEDEX Ground Home Delivery')
    ShipmentMethod.create!(name: 'FEDEX International Economy')
    ShipmentMethod.create!(name: 'FEDEX International Priority')
    ShipmentMethod.create!(name: 'FEDEX Next Morning')
    ShipmentMethod.create!(name: 'UPS Guaranteed Next Day')
    ShipmentMethod.create!(name: 'FEDEX Guaranteed Next Day')
    ShipmentMethod.create!(name: 'FEDEX Next Afternoon')
    ShipmentMethod.create!(name: 'DHL Next Afternoon')
    ShipmentMethod.create!(name: 'NA_ No Shipping')
    ShipmentMethod.create!(name: 'FEDEX Priority Overnight')
    ShipmentMethod.create!(name: 'FEDEX Same Day')
    ShipmentMethod.create!(name: '_NA_ Sea')
    ShipmentMethod.create!(name: 'UPS Second Day')
    ShipmentMethod.create!(name: 'DHL Second Day')
    ShipmentMethod.create!(name: 'FEDEX Second Day')
    ShipmentMethod.create!(name: '_NA_ Standard')
    ShipmentMethod.create!(name: 'USPS Standard')
    ShipmentMethod.create!(name: 'FEDEX Standard Overnight')

    ProductCategory.create(:id => 1,:category_name => "A1")
    ProductCategory.create(:id => 2,:category_name => "A2")
    ProductCategory.create(:id => 3,:category_name => "A3")
    ProductCategory.create(:id => 4,:category_name => "A4")
    ProductCategory.create(:id => 5,:category_name => "A5")
    ProductCategory.create(:id => 6,:category_name => "A6")
    PromotionRuleCondition.create(promotion_rule_id: 1, promotion_rule_condition_name: 'PPIP_ORDER_TOTAL', operator_id: 1, value: 22, other: 222, shipment_method_id: 1)
    PromotionRuleCondition.create(promotion_rule_id: 1, promotion_rule_condition_name: 'PPIP_PRODUCT_TOTAL', operator_id: 2, value: 22, other: 222, shipment_method_id: 2)
    PromotionRuleConditionCategory.create( product_category_id: "1", sub_category_status: "include", group_id: 1, promotion_rule_condition_id: 1  )

    10.times do |n|
      PromotionRuleAction.create(
          amount: 10,
          item_id: 1,
          party_id: 1,
          promotion_rule_action_name: "PPRA_GIFT_WITH_PURCHASE",
          promotion_rule_id: 1,
          quantity: 10,
          service_name: Faker::Name.name,
          use_cart_quantity: 10
      )
    end

    Facility.create(:facility_name => "Luffy")
    Facility.create(:facility_name => "Sanj")

    PaymentMethod.create(:description => "VISA")
    PaymentMethod.create(:description => "CREDIT CARD")
    PaymentMethod.create(:description => "MASTER CARD")
    PaymentMethod.create(:description => "PAYPAL")


    BillingAccount.create(:billing_account_id => 234234)
    BillingAccount.create(:billing_account_id => 32432)
    BillingAccount.create(:billing_account_id => 6856)
    BillingAccount.create(:billing_account_id => 456456)


    20.times do |n|
      ReturnHeader.create({
                              :return_header_type_id => 1,
                              :status_id => 1,
                              :from_party_id => 1,
                              :to_party_id => 1,
                              :payment_method_id => 1,
                              :fin_account_id => 1,
                              :billing_account_id => 1,
                              :entry_date => Time.now,
                              :origin_contact_mech_id => 1,
                              :destination_facility_id => 1,
                              :needs_inventory_receive => 1,
                              :currency_uom_id => 1,
                              :supplier_rma_id => 1
                          })
    end

    Rake::Task['geonames_rails:load_data'].invoke
    99.times do |n|
      user = User.create(
                          :email => Faker::Internet.email ,
                          :password => "password",
                          :password_confirmation => "password",
                          :sign_in_count=> "0",
                          :created_at => "2012-11-21 10:24:28.586343",
                          :confirmed_at=> '2012-11-22 10:01:19.653129',
                          :approval => true)
      user.add_role('consumer')

    end
    99.times do |n|
      product = Product.create(:product_type_id => 1,
                          :product_name =>  Faker::Name.name,
                          :description => Populator.sentences(2..10),
                          :comments =>  Populator.sentences(2..10),
                          :sales_introduction_date => DateTime.now - 1.months,
                          :sales_disc_when_not_avail => true,
                          :sales_discontinuation_date => DateTime.now,
                          :default_shipment_box_type_id => 1,
                          :support_discontinuation_date =>  DateTime.now,
                          :require_inventory => true,
                          :requirement_method_id => true,
                          :charge_shipping => true,
                          :in_shipping_box => true ,
                          :returnable => true,
                          :require_amount => true ,
                          :amount_uom_type_id => rand(1..3),
                          :sku => n + 1
      )

    end

    99.times do
      cost_component = CostComponent.create(
          :cost_component_type_id => rand(1..10),
          :product_id => rand(1..99),
          :party_id => rand(1..10),
          :geo_id => rand(1..10),
          :work_effort_id => rand(1..10),
          :asset_id => rand(1..10) ,
          :cost_component_calc_id => rand(1..10),
          :from_date => DateTime.now - 1.months,
          :thru_date => DateTime.now ,
          :cost => 1.0 * rand(),
          :cost_uom_id => rand(1..10)
      )
    end

    # Party Data
    10.times do
      party = Party.create(
          :party_type_id => rand(1..4)
      )
    end

    # Party Product Fake Data
    99.times do |n|
      product_party = ProductParty.create(
          :product_id => n+1,
          :party_id => rand(1..10)
      )
    end

    # Supplier Product Fake Data
    99.times do |n|
      supplier_product =  SupplierProduct.create(
          :product_id => n+1,
          :comments => Populator.sentences(2..10),
          :standard_lead_time_days => rand(10..30)
      )
    end

    #Fake Asset Data for Products
    5.times do
      asset = Asset.create(
          :asset_type_id => rand(1..4),
          :quantity_on_hand_total => rand(243..9765)
      )
    end

    99.times do
      product_price = ProductPrice.create(
          :product_id=> rand(1..99),
          :product_store_id => rand(1..10) ,
          :vendor_party_id => rand(1..10),
          :customer_party_id =>rand(1..10),
          :price_type_id => rand(1..10),
          :price_purpose_id => rand(1..10),
          :from_date => DateTime.now - 2.months,
          :thru_date => DateTime.now,
          :min_quantity => 1.0 *rand(),
          :price => 2.0 *rand(),
          :price_uom_id => rand(1..10),
          :term_uom_id => rand(),
          :tax_in_price => true,
          :tax_amount => 1.0 * rand(),
          :tax_percentage => 2.0 * rand(),
          :tax_auth_party_id => rand(1..10),
          :tax_auth_geo_id => rand(),
          :agreement_id => rand(),
          :agreement_item_seq_id => rand()
      )
    end
    99.times do |n|
      order_header = OrderHeader.create(
          :order_id=> n + 1,
          :order_name => Populator.sentences(2..10) ,
          :entry_date =>  DateTime.now - 1.months,
          :status_id => 1,
          :currency_uom_id => rand(1..10),
          :billing_account_id => rand(1..10),
          :product_store_id => n + 1,
          :sales_channel_id => n + 1,
          :terminal_id => n + 1,
          :external_id => n + 1,
          :sync_status_id => n + 1,
          :visit_id => n + 1,
          :parent_order_id => n + 1,
          :recurrence_info_id => n + 1,
          :last_ordered_date => DateTime.now,
          :remaining_sub_total => 2.0 * rand(),
          :grand_total => 2.0 * rand(),

      )
    end
    99.times do |n|
      organization = Organization.create(
          party_id: n + 1,
          organization_name: Populator.sentences(2..10),
          office_site_name: Populator.sentences(2..10),
          annual_revenue: 2.0 * rand(),
          num_employees: 2.0 * rand(),
          comments: Populator.sentences(2..10)
      )
      end
    99.times do |n|
      order_item = OrderItem.create(
          order_id: n + 1,
          order_item_seq_id: n + 1,
          order_part_seq_id: n + 1,
          item_type_id: n + 1,
          status_id: n + 1,
          product_id: n + 1,
          product_config_saved_id: n + 1,
          item_description: Populator.sentences(2..10),
          comments: Populator.sentences(2..10),
          quantity: 2.0 * rand(),
          cancel_quantity: 2.0 * rand(),
          selected_amount: 2.0 * rand(),
          unit_price: 2.0 * rand(),
          unitList_price: 2.0 * rand(),
          is_modified_price: true,
          external_item_id: n + 1,
          from_asset_id: n + 1,
          budget_id: n + 1,
          budget_item_seq_id: n + 1,
          supplier_product_id: n + 1,
          product_category_id: n + 1,
          is_promo: true,
          shopping_list_id: n + 1,
          shopping_list_item_seq_id: n + 1,
          subscription_id: n + 1,
          override_gl_account_id: n + 1,
          sales_opportunity_id: n + 1
      )

    end

    99.times do |n|
      people = Person.create(
          party_id: n+1,
          first_name: Populator.sentences(2..3)[0,255],
          last_name: Populator.sentences(4..5)[0,255]
        )
    end

    99.times do |n|
      order_part = OrderPart.create(
          order_id: n+1,
          customer_party_id: n+1,
          order_part_seq_id: n+1,
          parent_part_seq_id: n+1,
          vendor_party_id: n+1,
          part_name: Populator.sentences(2..5)
        )
    end

    status = OrderHeaderStatus.create(:status_id => 1,:description=> 'Completed',:sequence_num => rand(1..10))
    status = OrderHeaderStatus.create(:status_id => 2,:description=> 'Finished',:sequence_num => rand(1..10))

  end
end