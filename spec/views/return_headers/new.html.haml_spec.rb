require 'spec_helper'

describe "return_headers/new" do
  before(:each) do
    assign(:return_header, stub_model(ReturnHeader).as_new_record)
  end

  it "renders new return_header form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => return_headers_path, :method => "post" do
    end
  end
end
