require 'spec_helper'

describe "return_headers/edit" do
  before(:each) do
    @return_header = assign(:return_header, stub_model(ReturnHeader))
  end

  it "renders the edit return_header form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => return_headers_path(@return_header), :method => "post" do
    end
  end
end
