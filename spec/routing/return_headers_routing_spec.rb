require "spec_helper"

describe ReturnHeadersController do
  describe "routing" do

    it "routes to #index" do
      get("/return_headers").should route_to("return_headers#index")
    end

    it "routes to #new" do
      get("/return_headers/new").should route_to("return_headers#new")
    end

    it "routes to #show" do
      get("/return_headers/1").should route_to("return_headers#show", :id => "1")
    end

    it "routes to #edit" do
      get("/return_headers/1/edit").should route_to("return_headers#edit", :id => "1")
    end

    it "routes to #create" do
      post("/return_headers").should route_to("return_headers#create")
    end

    it "routes to #update" do
      put("/return_headers/1").should route_to("return_headers#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/return_headers/1").should route_to("return_headers#destroy", :id => "1")
    end

  end
end
