require 'spec_helper'

describe Users::UsersController do

  # This should return the minimal set of attributes required to create a valid
  # User. As you add validations to User, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    {
        :first_name => "Mary",
        :last_name => "Luna",
        :username => "princess",
        :mobile_number => "12345",
        :password => "123321",
        :password_confirmation => "123321",
        :email => "abc123@gmail.com"

    }
  end
  def valid_attributes_update_all
    {
        :first_name => "Peter",
        :last_name => "Jack",
        :username => "peter",
        :mobile_number => "234522",
        :password => "nopass",
        :password_confirmation => "nopass",
        :current_password => "password",
        :email => "abc123@gmail.com",
        :address_line1 => "368 Newell Ave"

    }
  end
  def valid_attributes_update_pass
    {
        :password => "nopass",
        :password_confirmation => "nopass",
        :current_password => "password"
    }
  end
  def valid_attributes_update_info
    {
        :first_name => "Peter",
        :last_name => "Jack",
        :username => "peter",
        :mobile_number => "234522",
        :address_line1 => "368 Newell Ave",
        :city_name => "Akron",
        :country => "US"

    }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # UsersController. Be sure to keep this updated too.
  def valid_session
    {}
  end

    #  #raise merchant.to_yaml
    #  #raise user.to_yaml
  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    user = FactoryGirl.create(:user)
    user.confirm! # or set a confirmed_at inside the factory. Only necessary if you are using the confirmable module
    sign_in user
  end

  describe "CHECK current user" do
    it "should have a current_user" do
      # note the fact that I removed the "validate_session" parameter if this was a scaffold-generated controller
      subject.current_user.should_not be_nil
    end
  end

  describe "GET index" do
    it " all users as @users" do
      get 'index'
      response.should be_success
    end
  end
end
