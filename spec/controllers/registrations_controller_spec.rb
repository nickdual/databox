#require 'spec_helper'
#
#describe RegistrationsController do
#  describe "Sign up user" do
#    def valid_attributes
#      {
#          :first_name           => "test first_name",
#          :last_name            => "test last name",
#          :username             => "test username",
#          :mobile_emei          => "test mobile emei",
#          :mobile_authorized    => "mob auth",
#          :mobile_handset_brand => "mob handset brand",
#          :password             => "password",
#          :authorization_level  => "auth level",
#          :email                => "test@tester.com",
#          :mobile_number        => "123456789"
#      }
#    end
#    before(:each) do
#
#      #request.env["devise.mapping"] = Devise.mappings[:user]
#      @request.env["devise.mapping"] = Devise.mappings[:user]
#
#      FactoryGirl.create(:role,:name =>"Super User")
#      FactoryGirl.create(:role,:name => "Yacto employee")
#      FactoryGirl.create(:role,:name => "Merchant admin")
#      FactoryGirl.create(:role,:name => "Merchant employee")
#      FactoryGirl.create(:role,:name => "Consumer")
#      FactoryGirl.create(:role,:name => "Yacto system")
#      @request.host = 'localhost:3000'
#
#    end
#
#    it "Sign up automatic sign in" do
#      post :create,:user => FactoryGirl.attributes_for(:user),:format => 'json'
#      sign_in(assigns(:user))
#      assigns(:user).email.should  eq("abc748420@gmail.com")
#    end
#    it "Sign up fail,password = nil" do
#      post :create,:user => {:first_name => "test first_name",:last_name =>"test last_name",:email => "abc748420@gmail.com",:username =>"abc748420@gmail.com",:mobile_number => "123456789"} ,:format => 'json'
#      response.body.should have_content "{\"password\":[\"can't be blank\"]}"
#    end
#    it "User profile" do
#      @user = FactoryGirl.create(:user)
#      sign_in(@user)
#      get :edit
#      assigns(:user).email.should eq("abc748420@gmail.com")
#
#    end
#  end
#end