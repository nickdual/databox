require 'spec_helper'

describe PromotionCodesController do
  def valid_attributes_product_promotion
    {

      :billback_factor => 3 ,
      :override_org_party_id =>4 ,
      :promotion_name =>'MyString' ,
      :promotion_show_to_customer  => false ,
      :promotion_text => 'MyText',
      :require_code => false  ,
      :use_limit_per_customer => 3,
      :use_limit_per_order => 2,
      :use_limit_per_promotion => 2 ,
      :user_entered =>true
    }
  end

  def valid_attributes
    {
      :product_promotion_id => 1,
      :product_promotion_code_id => '12345'

    }
  end
  def invalid_attributes
    {
      :user_entered => false   ,
      :require_email_or_party => false  ,
      :use_limit_per_code => 'as' ,
      :use_limit_per_customer => 'as'   ,

    }
  end
  def valid_attributes_update_all
    {

      :user_entered => false   ,
      :require_email_or_party => false  ,
      :use_limit_per_code => 2 ,
      :use_limit_per_customer => 3   ,
      :from_date => '2012-12-18 11:02:59'  ,
      :thru_date => '2012-12-18 11:02:59'
    }
  end
  before :each do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @current_user = FactoryGirl.create(:user)
    sign_in @current_user
    controller.stub(:authenticate_user!).and_return true
  end

  describe "GET index " do
    it "list product promotion" do
      get :index
      response.should render_template :index
    end
  end


  describe "GET new" do
    it "a new promotion_code as @promotion_code" do
      get :new
      response.should render_template("new")
    end
  end


  describe "GET edit" do

    it "requested edit promotion_code" do
      @current_user = User.find(1)
      @product_promotion = FactoryGirl.create(:product_promotion)
      @promotion_code = FactoryGirl.create(:promotion_code)
      @promotion_code.created_by_user_login = @current_user
      @promotion_code.last_modified_by_user_login = @current_user
      @promotion_code.save
      get :edit, {:id => @promotion_code.product_promotion_code_id}
      response.code.should eq('200')
    end
    it "render edit template" do
      @product_promotion = FactoryGirl.create(:product_promotion)
      @promotion_code = FactoryGirl.create(:promotion_code)
      @promotion_code.created_by_user_login = @current_user
      @promotion_code.last_modified_by_user_login = @current_user
      @promotion_code.save
      get :edit ,{:id => @promotion_code.product_promotion_code_id}
      response.should render_template("edit")
    end
  end
  #
  describe "POST create" do
    describe "with valid params" do

      it "creates a new Promotioncode" do
        @product_promotion = FactoryGirl.create(:product_promotion)
        post :create, {:promotion_code => valid_attributes}
        assigns(:promotion_code).should be_a(PromotionCode)
        assigns(:promotion_code).should be_persisted
      end

      it "redirects after created promotion code" do
        @product_promotion = FactoryGirl.create(:product_promotion)
        post :create, {:promotion_code => valid_attributes  }
        response.should be_redirect
      end

    end

  end

  describe "PUT update" do
    describe "with valid params" do

      before(:each) do
        @current_user = User.find(1)
        @promotion_code = FactoryGirl.create(:promotion_code)
      end

      it "redirects to the promotion_code" do
        put :update, {:id => @promotion_code.to_param, :promotion_code => valid_attributes_update_all}
        @promotion_code.last_modified_by_user_login = @current_user
        @promotion_code.save
        response.should redirect_to '/promotion_codes'
      end

    end
  end
  #

  describe "DELETE destroy" do
    before(:each) do
      @product_promotion = FactoryGirl.create(:product_promotion)
      @promotion_code = FactoryGirl.create(:promotion_code)
      @promotion_code.created_by_user_login = @current_user
      @promotion_code.last_modified_by_user_login = @current_user
      @promotion_code.save
    end
    it "destroys the requested @promotion_code" do

      expect {
        delete :destroy, {:id => @promotion_code.to_param}
      }.to change(PromotionCode, :count).by(-1)
    end
  end

  describe "POST mass_insert" do
    context "with valid attributes" do
      it "mass insert without params" do
        post :mass_insert
      end
      it "mass insert" do
        post :mass_insert, {:quantity => 10}
      end
    end
    context "with invaild attributes" do
    end
  end

  describe "POST promotion_codes_per_page" do
    context "with vaild attributes" do
      it "params with per" do
        post :promotion_codes_per_page, {:per => 10}
      end
    end
    context "with invaild attributes" do
      it "params without per" do
        expect {
          post :promotion_codes_per_page
        }.to raise_error

      end
    end
  end

  describe "POST delete_promotion_codes" do
    context "with vaild attributes" do
      it "params with per and list" do
        post :delete_promotion_codes, {:per => 10, :list => [1, 2]}
      end
    end
    context "with invaild attributes" do
      it "params without per" do
        session[:promo_page] = 10
        post :delete_promotion_codes, {:list => [1, 2]}
      end
      it "params without list" do
        post :delete_promotion_codes, {:per => 10}
      end

    end
  end
end
