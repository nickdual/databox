# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :budget_item_type do
    description "MyString"
    name "MyString"
  end
end
