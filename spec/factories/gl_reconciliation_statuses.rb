# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :gl_reconciliation_status do
    status_id "MyString"
    sequence_num 1
    description "MyString"
  end
end
