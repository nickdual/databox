FactoryGirl.define do
  factory :country do
    id 1
    geonames_id 6252001
    phone "+1"
    iso_code_two_letter 'US'
    iso_code_three_letter 'USA'
    iso_number 840
    name "United States"
    capital "Washington"
    population 310232863
    continent "NA"
    currency_code "USD"
    currency_name "Dollar"
  end

end
