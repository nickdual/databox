FactoryGirl.define do
  factory :promotion_category do
    id 1
    group_id '1'
    product_promotion_id 1
    product_category_id 1
    sub_category_status ''
    created_at "2012-12-29 02:31:28"
    updated_at "2012-12-29 02:31:28"
  end
end
