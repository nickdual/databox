# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :gl_reconciliation_status_valid do
    status_id 1
    to_status_id 1
    transition_name "MyString"
  end
end
