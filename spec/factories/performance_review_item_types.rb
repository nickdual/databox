# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :performance_review_item_type do
    name "MyString"
    description "MyString"
  end
end
