# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :acctg_trans_entry_reconcile_status do
    description "MyString"
    sequence_num 1
    status_id "MyString"
  end
end
