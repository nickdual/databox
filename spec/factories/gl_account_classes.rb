# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :gl_account_class do
    name "MyString"
    description "MyString"
    parent_id 1
  end
end
