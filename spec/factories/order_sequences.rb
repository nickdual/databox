# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :order_sequence do
    description "MyString"
    name "MyString"
    sequence_num 1
  end
end
