# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :promotion_rule_action_category do
    product_category_id "MyString"
    sub_category_status ""
    group_id 1
    product_rule_action_id 1
  end
end
