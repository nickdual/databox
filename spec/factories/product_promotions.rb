FactoryGirl.define do
  factory :product_promotion do
    billback_factor 3
    override_org_party_id 4
    promotion_name 'MyString'
    promotion_show_to_customer   false
    promotion_text  'MyText'
    require_code  false
    use_limit_per_customer 3
    use_limit_per_order  2
    use_limit_per_promotion  2
    user_entered true
    user_id 1
    created_at "2012-12-29 02:31:28"
    updated_at "2012-12-29 02:31:28"
  end

  factory :invalid_product_promotion_1,:class => 'ProductPromotion' do
    promotion_name ''
    promotion_text 'MyText'
    user_entered false
  end
end
