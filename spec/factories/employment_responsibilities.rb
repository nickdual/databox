# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :employment_responsibility do
    description "MyString"
    name "MyString"
  end
end
