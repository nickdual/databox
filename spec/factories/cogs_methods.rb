# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :cogs_method do
    name "MyString"
    description "MyString"
    sequence_num 1
  end
end
