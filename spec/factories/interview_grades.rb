# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :interview_grade do
    description "MyString"
    name "MyString"
    sequence_num 1
  end
end
