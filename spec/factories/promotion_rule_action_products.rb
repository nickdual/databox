# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :promotion_rule_action_product do
    product_id 1
    sub_category_status ""
    promotion_rule_action_id 1
  end
end
