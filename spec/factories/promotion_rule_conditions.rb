# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :promotion_rule_condition do
    id 1
    promotion_rule_id 1
    operator_id 1
    value 1
    other 1
    shipment_method_id 1
    promotion_rule_condition_name 'PPIP_ORDER_TOTAL'
  end
end
