FactoryGirl.define do
  factory :product do
    id 1
    product_type_id 1
    product_name 'MyText'
    description 'MyText'
    comments 'MyText'
    sales_introduction_date '2012-11-23 11:20:42'
    sales_discontinuation_date '2012-11-23 11:20:42'
    sales_disc_when_not_avail false
    support_discontinuation_date '2012-11-23 11:20:42'
    require_inventory false
    requirement_method_id false
    charge_shipping false
    in_shipping_box false
    default_shipment_box_type_id 1
    returnable false
    require_amount false
    amount_uom_type_id 1
    fixed_amount 9.99
    origin_geo_id 1
    bill_of_material_level 1
  end
end
