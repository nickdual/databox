# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :tax_authority_rate_type do
    name "MyString"
    description "MyString"
  end
end
