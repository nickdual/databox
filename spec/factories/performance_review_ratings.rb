# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :performance_review_rating do
    name "MyString"
    description "MyString"
    sequence_num 1
  end
end
