# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :promotion_rule_action do
    id 1
    promotion_rule_id 1
    promotion_rule_action_name "PPRA_GIFT_WITH_PURCHASE"
    quantity 1
    amount 1
    item_id 1
    party_id "MyString"
    service_name "MyString"
    use_cart_quantity false
  end
end
