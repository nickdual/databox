# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :empl_position_responsibility do
    empl_position_id 1
    responsibility_enum_id 1
    from_date "2013-01-16 23:33:47"
    thru_date "2013-01-16 23:33:47"
    comments "MyText"
  end
end
